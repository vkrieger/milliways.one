<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<!-- 
dont forget enctype="multipart/form-data" to upload files!!!
Datei-Uploads funktionieren nur mit method="post".
Wichtig ist außerdem, dass Sie im einleitenden <form>-Tag die Angabe enctype="multipart/form-data" notieren.
Andernfalls erhalten Sie lediglich den Dateinamen der ausgewählten Datei übermittelt, nicht jedoch die Datei selbst.
ENCTYPE steht für die Art des Übertragunsformats, das bei der Kommunikation zwischen Browser und Server genutzt wird.
application/x-www-form-urlencoded der Standardwert - für die Übertragung großer Datenmengen, die Nicht-ASCII-Zeichen enthalten, weniger geeignet
multipart/form-data" wird z.B. beim File-Upload verwendet
enctype="text/plain" reiner Text / Ausgabe erfolgt an menschliche Wesen
-->

<body>
You may download <a href="referenceslist.xls">this Table</a> in Spreadsheet-Format (e.g. as CSV for MS-Excel) from Server.<br>
<br>

<?php

include 'include_setReferenceconstants.php';
include 'include_refs_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

$createdataset="";
$createpdf="";

// <form></form> must enclose all <input> to transfer to next app
echo '<form method="post" enctype="multipart/form-data" action="refs_pdf.php">';
echo '<input type="submit" name="createpdf" value="create pdf">';
/*
if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
	{
	echo '<input type="submit" name="createdataset" value="create dataset">';
	}	
*/
echo '<br><br>';	

/*
select datasets according to criteria. !!! table is named refs as references is not allowed !!!
*/

  	$dbquery = "SELECT * FROM refs WHERE

LOCATE('$partReferenceGUID', ReferenceGUID)>0 AND
LOCATE('$partReferenceCreateID', ReferenceCreateID)>0 AND
LOCATE('$partReferenceArchiveID', ReferenceArchiveID)>0 AND
LOCATE('$partReferenceProject', ReferenceProject)>0 AND
LOCATE('$partReferenceOwner', ReferenceOwner)>0 AND
LOCATE('$partReferenceType', ReferenceType)>0 AND
LOCATE('$partReferenceCategory', ReferenceCategory)>0 AND
LOCATE('$partReferenceName', ReferenceName)>0 AND
LOCATE('$partReferenceSummary', ReferenceSummary)>0 AND
LOCATE('$partReferenceClient', ReferenceClient)>0 AND
LOCATE('$partReferenceDivision', ReferenceDivision)>0 AND
LOCATE('$partReferenceContractor', ReferenceContractor)>0 AND
LOCATE('$partReferenceCode', ReferenceCode)>0 AND
LOCATE('$partReferenceContact', ReferenceContact)>0 AND
LOCATE('$partReferenceVolume', ReferenceVolume)>0 AND
LOCATE('$partReferenceFee', ReferenceFee)>0 AND
LOCATE('$partReferenceStartdate', ReferenceStartdate)>0 AND
LOCATE('$partReferenceEnddate', ReferenceEnddate)>0 AND
LOCATE('$partReferenceLocation', ReferenceLocation)>0 AND
LOCATE('$partReferenceCountry', ReferenceCountry)>0 AND
LOCATE('$partReferenceDescription', ReferenceDescription)>0 AND
LOCATE('$partReferenceScope', ReferenceScope)>0 AND
LOCATE('$partReferenceDetails', ReferenceDetails)>0 AND
LOCATE('$partReferenceStatus', ReferenceStatus)>0 AND
LOCATE('$partReferenceFilename', ReferenceFilename)>0 AND
LOCATE('$partReferenceRemarks', ReferenceRemarks)>0

ORDER BY $first_sorted_by, $then_sorted_by, $last_sorted_by " ;
	
  	$dbresult = mysqli_query($link,$dbquery);  echo mysqli_error($link);
  	
  	$header =""; //empty header for excel file (dataset structure)
  	$data	=""; //empty $data variable for excel file (all rows in one variable)

echo '<table border="1" cellspacing="0">';
 
echo '<tr>';

if ($list_ReferenceEN =='yes'){echo '<td>pdf EN DE</td>'; $header.="pdf". "\t";}
if ($list_ReferenceGUID =='yes'){echo '<td>GUID</td>'; $header.="GUID". "\t";}
if ($list_ReferenceCreateID =='yes'){echo '<td>CreateID</td>'; $header.="CreateID". "\t";}
if ($list_ReferenceArchiveID =='yes'){echo '<td>ArchiveID</td>'; $header.="ArchiveID". "\t";}
if ($list_ReferenceProject =='yes'){echo '<td>Project</td>'; $header.="Project". "\t";}
if ($list_ReferenceOwner =='yes'){echo '<td>Owner</td>'; $header.="Owner". "\t";}
if ($list_ReferenceType =='yes'){echo '<td>Type</td>'; $header.="Type". "\t";}
if ($list_ReferenceCategory =='yes'){echo '<td>Category</td>'; $header.="Category". "\t";}
if ($list_ReferenceName =='yes'){echo '<td>Name</td>'; $header.="Name". "\t";}
if ($list_ReferenceSummary =='yes'){echo '<td>Summary</td>'; $header.="Summary". "\t";}
if ($list_ReferenceClient =='yes'){echo '<td>Client</td>'; $header.="Client". "\t";}
if ($list_ReferenceContractor =='yes'){echo '<td>Contractor</td>'; $header.="Contractor". "\t";}
if ($list_ReferenceCode =='yes'){echo '<td>Code</td>'; $header.="Code". "\t";}
if ($list_ReferenceContact =='yes'){echo '<td>Contact</td>'; $header.="Contact". "\t";}
if ($list_ReferenceVolume =='yes'){echo '<td>Volume</td>'; $header.="Volume". "\t";}
if ($list_ReferenceFee =='yes'){echo '<td>Fee</td>'; $header.="Fee". "\t";}
if ($list_ReferenceStartdate =='yes'){echo '<td>Startdate</td>'; $header.="Startdate". "\t";}
if ($list_ReferenceEnddate =='yes'){echo '<td>Enddate</td>'; $header.="Enddate". "\t";}
if ($list_ReferenceLocation =='yes'){echo '<td>Location</td>'; $header.="Location". "\t";}
if ($list_ReferenceCountry =='yes'){echo '<td>Country</td>'; $header.="Country". "\t";}
if ($list_ReferenceDescription =='yes'){echo '<td>Description</td>'; $header.="Description". "\t";}
if ($list_ReferenceScope =='yes'){echo '<td>Scope</td>'; $header.="Scope". "\t";}
if ($list_ReferenceDetails =='yes'){echo '<td>Details</td>'; $header.="Details". "\t";}
if ($list_ReferenceStatus =='yes'){echo '<td>Status</td>'; $header.="Status". "\t";}
if ($list_ReferenceFilename =='yes'){echo '<td>Filename</td>'; $header.="Filename". "\t";}
if ($list_ReferenceRemarks =='yes'){echo '<td>Remarks</td>'; $header.="Remarks". "\t";}

	echo '</tr>';
	$header .= "\n"; //end of dataset in excel file (marks row end in data)


/*
outputs datasets and fills $data variable
*/

$DatasetCount = 0;

// control code echo $SystemProject.'<br>';

while($dbrow = mysqli_fetch_array($dbresult))
   	{
	$DatasetCount += 1;
	
	// output to main window and selected data to $data excel variable
   	echo '<tr>';
	
	if ($list_ReferenceEN =='yes') // checkbox array must not contain values with blanks
		{
		echo '<td align="right">';
		echo '<input type="checkbox" name="ReferenceEN[]" value="'.str_replace(" ","_",$dbrow['ReferenceCreateID']).'">';
		echo '<input type="checkbox" name="ReferenceDE[]" value="'.str_replace(" ","_",$dbrow['ReferenceCreateID']).'">';
		echo '</td>';
		}	
	if ($list_ReferenceGUID =='yes')
		{echo'<td>'.$dbrow['ReferenceGUID'].'</td>'; $data.='"'.$dbrow['ReferenceGUID'].'"'."\t";}
	if ($list_ReferenceCreateID=='yes')
		{
		echo '<td class="bluelink"><a style="font-size:xx-small" href="refs_modify.php?ReferenceCreateID='.$dbrow['ReferenceCreateID'].'">'.$dbrow['ReferenceCreateID'].'</a></td>';
		$data .= '"' . $dbrow['ReferenceCreateID'] . '"' . "\t";
		}
if ($list_ReferenceArchiveID =='yes'){echo'<td>'.$dbrow['ReferenceArchiveID'].'</td>'; $data.='"'.$dbrow['ReferenceArchiveID'].'"'."\t";}
if ($list_ReferenceProject =='yes'){echo'<td>'.$dbrow['ReferenceProject'].'</td>'; $data.='"'.$dbrow['ReferenceProject'].'"'."\t";}
if ($list_ReferenceOwner =='yes'){echo'<td>'.$dbrow['ReferenceOwner'].'</td>'; $data.='"'.$dbrow['ReferenceOwner'].'"'."\t";}
if ($list_ReferenceType =='yes'){echo'<td>'.$dbrow['ReferenceType'].'</td>'; $data.='"'.$dbrow['ReferenceType'].'"'."\t";}
if ($list_ReferenceCategory =='yes'){echo'<td>'.$dbrow['ReferenceCategory'].'</td>'; $data.='"'.$dbrow['ReferenceCategory'].'"'."\t";}
if ($list_ReferenceName =='yes'){echo'<td>'.$dbrow['ReferenceName'].'</td>'; $data.='"'.$dbrow['ReferenceName'].'"'."\t";}
if ($list_ReferenceSummary =='yes'){echo'<td>'.$dbrow['ReferenceSummary'].'</td>'; $data.='"'.$dbrow['ReferenceSummary'].'"'."\t";}
if ($list_ReferenceClient =='yes'){echo'<td>'.$dbrow['ReferenceClient'].'</td>'; $data.='"'.$dbrow['ReferenceClient'].'"'."\t";}
if ($list_ReferenceContractor =='yes'){echo'<td>'.$dbrow['ReferenceContractor'].'</td>'; $data.='"'.$dbrow['ReferenceContractor'].'"'."\t";}
if ($list_ReferenceCode =='yes'){echo'<td>'.$dbrow['ReferenceCode'].'</td>'; $data.='"'.$dbrow['ReferenceCode'].'"'."\t";}
if ($list_ReferenceContact =='yes'){echo'<td>'.$dbrow['ReferenceContact'].'</td>'; $data.='"'.$dbrow['ReferenceContact'].'"'."\t";}
if ($list_ReferenceVolume =='yes'){echo'<td>'.$dbrow['ReferenceVolume'].'</td>'; $data.='"'.$dbrow['ReferenceVolume'].'"'."\t";}
if ($list_ReferenceFee =='yes'){echo'<td>'.$dbrow['ReferenceFee'].'</td>'; $data.='"'.$dbrow['ReferenceFee'].'"'."\t";}
if ($list_ReferenceStartdate =='yes'){echo'<td>'.$dbrow['ReferenceStartdate'].'</td>'; $data.='"'.$dbrow['ReferenceStartdate'].'"'."\t";}
if ($list_ReferenceEnddate =='yes'){echo'<td>'.$dbrow['ReferenceEnddate'].'</td>'; $data.='"'.$dbrow['ReferenceEnddate'].'"'."\t";}
if ($list_ReferenceLocation =='yes'){echo'<td>'.$dbrow['ReferenceLocation'].'</td>'; $data.='"'.$dbrow['ReferenceLocation'].'"'."\t";}
if ($list_ReferenceCountry =='yes'){echo'<td>'.$dbrow['ReferenceCountry'].'</td>'; $data.='"'.$dbrow['ReferenceCountry'].'"'."\t";}
if ($list_ReferenceDescription =='yes'){echo'<td>'.$dbrow['ReferenceDescription'].'</td>'; $data.='"'.$dbrow['ReferenceDescription'].'"'."\t";}
if ($list_ReferenceScope =='yes'){echo'<td>'.$dbrow['ReferenceScope'].'</td>'; $data.='"'.$dbrow['ReferenceScope'].'"'."\t";}
if ($list_ReferenceDetails =='yes'){echo'<td>'.$dbrow['ReferenceDetails'].'</td>'; $data.='"'.$dbrow['ReferenceDetails'].'"'."\t";}
if ($list_ReferenceStatus =='yes'){echo'<td>'.$dbrow['ReferenceStatus'].'</td>'; $data.='"'.$dbrow['ReferenceStatus'].'"'."\t";}
	
if($list_ReferenceFilename=='yes')		
	{
	$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['ReferenceProject'].'/';
	
	echo	'<td class="bluelink">'
			.'<a style="font-size:xx-small" href="'
			.$ProjectFilePath
			.$dbrow['ReferenceFilename']
			.'">'
			.$dbrow['ReferenceFilename'].' ['.$dbrow['ReferenceFilesize'].'kB]'
			.'</a>';
	$data.='"'.$dbrow['ReferenceFilename'].' ['.$dbrow['ReferenceFilesize'].'kB]'.'"'."\t";
	}
	
if ($list_ReferenceRemarks =='yes'){echo'<td>'.$dbrow['ReferenceRemarks'].'</td>'; $data.='"'.$dbrow['ReferenceRemarks'].'"'."\t";}
	
	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)
	}
echo '<tr>';
echo '<td>'.$DatasetCount.' Datasets</td>';
echo '</tr>';	
echo '</table>';

echo '</form>';
	
	
/*
export selected table to .xls onto local server folder to download as offered from there
*/

$fp = fopen('referenceslist.xls','w');
fwrite($fp,$header);
fwrite($fp,"\n");
fwrite($fp,$data);
fclose($fp);

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2019-07-11 11:00</div>';

?>
</body>
</html>
