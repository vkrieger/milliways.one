<?php

/*
the following Doc constants are defined 
*/

// Arrays of Doc constants

// Category classifies Doc dataset in terms of status and mode type
$ProjectCategoryArray = [
'KSA',
'MENA',
'international',
'other',
];

$ProjectStatusArray = [
'potential',
'called',
'offered',
'declined',
'contracted',
'closed',
'lost',
'undefined',
'other',
];

// last change vkrieger 23.02.2016

?>