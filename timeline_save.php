<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

include 'include_setTimelineconstants.php';
include 'include_timeline_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

$CurrentTimeStamp = date("Y-m-d H:i:s");

// control code print_r ($_POST["blocked"]); echo '<br>';
// control code print_r ($_POST["work"]);
	
		if (!empty($_POST["blocked"])) 	{ $blocked	= $_POST["blocked"];} 	else {$blocked 	= "";}
		if (!empty($_POST["seminar"])) 	{ $seminar	= $_POST["seminar"];} 	else {$seminar 	= "";}

// control code print_r ($_POST["blocked"]); echo '<br>';
// control code print_r ($blocked);echo '<br>';
// control code print_r ($blocked[2014]);echo '<br>';
// control code print_r ($work);
		
	// initialize temporary $cal[$year] variable
	
	for ($y = 2021; $y <= 2023; $y++) 
		{
		$cal[$y]="";
		// control code echo date("L",mktime(0,0,0,1,1,$y));
		for ($d = 0 ; $d <= 364+date("L",mktime(0,0,0,1,1,$y)); $d++)
			{$cal[$y].="0";}
		// control code echo $cal[$y].'<br>';
		}
	// input checkbox array only stored checked checkboxes. value is used to identify their position
	// the resulting array looks like this: Array ( [2014] => Array ( [0] => 11 [1] => 12 ) [2015] => Array ( [0] => 11 [1] => 12 [2] => 13 ) ) 
	
	// only the position of checked checkboxes are stored in indexed string position starting with position 0
	// enter modified values. lower while command overides upper
	// substr_replace($string,$replacement,$start,$length) replaces a copy of string delimited by the start and
	// length parameters with the string given in replacement.
	// empty function prevents warning notice when string position not available any more
	
	for ($y = 2021; $y <= 2023; $y++)
		{
		// $i counts only over the total of checked checkboxes
		// empty() cannnot be used because first array position is zero
		// replace in string only if array exist
		if (!empty($seminar[$y])) {$i=0; while (array_key_exists($i,$seminar[$y]))	{ $cal[$y] = substr_replace($cal[$y], 'S' , $seminar[$y][$i], 1); 	$i++;}}
		if (!empty($blocked[$y])) {$i=0; while (array_key_exists($i,$blocked[$y])) { $cal[$y] = substr_replace($cal[$y], 'X' , $blocked[$y][$i], 1); 	$i++;}}	
		}
	
	// make TimelineYear from timeline[$year] - tbc !!!
	$Timeline2021 = $cal[2021]; // control code echo '<br>';
	$Timeline2022 = $cal[2022]; // control code echo '<br>';
	$Timeline2023 = $cal[2023]; // control code echo '<br>';
	
	
// deletedataset: puts current dataset in archive status by entering current datetime into ArchiveID
// updatedataset: puts current dataset in archive status by entering current datetime into ArchiveID and then createdataset


if (!empty($_POST["updatedataset"]) OR !empty($_POST["deletedataset"]))
	{	
	$dbchange = "UPDATE timeline SET TimelineArchiveID = '$CurrentTimeStamp' WHERE TimelineCreateID = '$TimelineCreateID'";
	$dbquery = mysqli_query($link,$dbchange) or die ("not updated!");
	}


// updatedataset: creates new dataset with same GUID and with current datetime in CreateID
// createdataset: creates new dataset with new GUID and with current datetime in CreateID, leaving the current untouched  


if (!empty($_POST["updatedataset"]) OR !empty($_POST["createdataset"]))
	
	{
	if (!empty($_POST["createdataset"])) {$TimelineGUID = $CurrentTimeStamp;}
	$TimelineCreateID = $CurrentTimeStamp;
	$TimelineArchiveID='0000-00-00 00:00:00';
	
	$dbchange = "INSERT INTO timeline SET

		TimelineGUID = '$TimelineGUID',
		TimelineCreateID = '$TimelineCreateID',
		TimelineArchiveID = '$TimelineArchiveID',
		TimelineProject = '$TimelineProject',
		TimelineOwner = '$TimelineOwner',
		TimelineName = '$TimelineName',
		TimelineType = '$TimelineType',
		TimelineCategory = '$TimelineCategory',
		Timeline2021 = '$Timeline2021',
		Timeline2022 = '$Timeline2022',
		Timeline2023 = '$Timeline2023',
		TimelineRemarks = '$TimelineRemarks'
		";
	$dbquery = mysqli_query($link,$dbchange) or die ("not created! ".mysqli_error($link));
	}

$_GET['TimelineCreateID']=$TimelineCreateID;	
include 'timeline_modify.php';


echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-08 18:00</div>';
?>
</body>
</html>

