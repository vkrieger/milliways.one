<?php

/*
the following Address constants are defined 
*/

// Arrays of Address constants

// Category classifies Address dataset in terms hierarchical position in contractual relation
$AddressCategoryArray = [
'client person',
'client company',
'subcontractor',
'supplier',
'cooperation',
'network',
'personnel',
'other',
];

// last change vkrieger 09.03.2016

?>