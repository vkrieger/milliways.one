<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; background-color:<?php echo $SystemColor; ?>;}
	input                               {font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

contacts listing criteria<br>

<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

include 'include_setContactconstants.php';
include 'include_contacts_postvariables.php';

if (!isset($_SESSION)) { session_start();}

	$listdatasets = "";
	
if ($listdatasets == "" AND $_SESSION['LoginType'])
	{		
    echo '<form method="post" enctype="multipart/form-data" action="contacts_list.php" target="main">';
	
	$partContactArchiveID ='0000-00-00 00:00:00'; // to firstly display only non-archived datasets when navigation is called
	
	echo '<table>';

	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ContactGUID" value="yes" >GUID</td>';
	echo '<td><input type="text" name="partContactGUID" size="20" maxlength="40" value="'.$partContactGUID.'"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ContactCreateID" value="yes" checked>CreateID(Link)</td>';
	echo '<td><input type="text" name="partContactCreateID" size="20" maxlength="40" value="'.$partContactCreateID.'"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ContactArchiveID" value="yes" >ArchiveID</td>';
	echo '<td><input type="text" name="partContactArchiveID" size="20" maxlength="40" value="'.$partContactArchiveID.'"></td>';
	
	echo '</tr>';
	
	echo '</table>';
	
	echo '<table>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ContactProject" value="yes">Project</td>';
	if (empty($SystemProject))
		{
		echo '<td><select type="text" name="partContactProject" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($SystemProjectArray as $Project) {echo '<option>'.$Project.'</option>';}
			echo '</select>';}
		else { echo '<input type="hidden" name="partContactProject" value="'.$SystemProject.'">';}
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ContactOwner" value="yes" >Owner</td>';
	echo '<td><input type="text" name="partContactOwner" size="8" maxlength="40" value="'.$partContactOwner.'"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ContactType" value="yes" >Type</td>';
	if ($SystemType == "public")
		{echo '<td><input style="background-color:#C0C0C0" type="text" name="partContactType" size="8" maxlength="40" value="public" readonly></td>';}
		else
		{echo '<td><input type="text" name="partContactType" size="8" maxlength="40" value="'.$partContactType.'"></td>';}
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ContactCategory" value="yes" checked>Category</td>';
	echo '<td><select type="text" name="partContactCategory" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($ContactCategoryArray as $Category) {echo '<option>'.$Category.'</option>';}
			echo '</select>';
    echo '</td>';
	echo '</tr>';    
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ContactFunction" value="yes" checked>Function</td>';
	echo '<td><select type="text" name="partContactFunction" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($ContactFunctionArray as $Function) {echo '<option>'.$Function.'</option>';}
			echo '</select>';
    echo '</td>';
	echo '</tr>';    
	
	echo '<tr><td><input type="checkbox" name="list_ContactPic" value="yes" checked>Pic</td><td><input type="text" name="partContactPic" size="8" maxlength="40" value="'.$partContactPic.'"></td></tr>';
	
	echo '<tr><td><input type="checkbox" name="list_ContactFirstname" value="yes" checked>Firstname</td><td><input type="text" name="partContactFirstname" size="8" maxlength="40" value="'.$partContactFirstname.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_ContactLastname" value="yes" checked>Lastname</td><td><input type="text" name="partContactLastname" size="8" maxlength="40" value="'.$partContactLastname.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_ContactInternet" value="yes" checked>Internet</td><td><input type="text" name="partContactInternet" size="8" maxlength="40" value="'.$partContactInternet.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_ContactTelecom" value="yes" checked>Telecom</td><td><input type="text" name="partContactTelecom" size="8" maxlength="40" value="'.$partContactTelecom.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_ContactPrivate" value="yes" >Private</td><td><input type="text" name="partContactPrivate" size="8" maxlength="40" value="'.$partContactPrivate.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_ContactBusiness" value="yes" >Business</td><td><input type="text" name="partContactBusiness" size="8" maxlength="40" value="'.$partContactBusiness.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_ContactSector" value="yes" >Sector</td><td><input type="text" name="partContactSector" size="8" maxlength="40" value="'.$partContactSector.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_ContactOrganization" value="yes" >Organization</td><td><input type="text" name="partContactOrganization" size="8" maxlength="40" value="'.$partContactOrganization.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_ContactDepartment" value="yes" >Department</td><td><input type="text" name="partContactDepartment" size="8" maxlength="40" value="'.$partContactDepartment.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_ContactPosition" value="yes" >Position</td><td><input type="text" name="partContactPosition" size="8" maxlength="40" value="'.$partContactPosition.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_ContactRemarks" value="yes" >Remarks</td><td><input type="text" name="partContactRemarks" size="8" maxlength="40" value="'.$partContactRemarks.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_ContactCV" value="yes" >CV</td><td><input type="text" name="partContactCV" size="8" maxlength="40" value="'.$partContactCV.'"></td></tr>';

	echo '</table>';

?>
		
		<!-- sorting part -->
        <table>
		<tr><td>first sorted by</td>
			<td><select name="first_sorted_by" size="1">
				<option value="ContactGUID">GUID</option>
				<option value="ContactCreateID">CreateID</option>
				<option value="ContactArchiveID">ArchiveID</option>
				<option value="ContactProject">Project</option>
				<option value="ContactOwner" >Owner</option>
				<option value="ContactType" >Type</option>
				<option value="ContactCategory" selected>Category</option>
				<option value="ContactFunction" >Function</option>
				<option value="ContactPic" >Pic</option>
				<option value="ContactFirstname" >First Name</option>
				<option value="ContactLastname" >Last Name</option>
				<option value="ContactPrivateTown" >Priv Town</option>
				<option value="ContactPrivateZIP" >Priv ZIP</option>
				<option value="ContactPrivateCountry" >Priv Country</option>
				<option value="ContactBusinessTown" >Biz Town</option>
				<option value="ContactBusinessZIP" >Biz ZIP</option>
				<option value="ContactBusinessCountry" >Biz Country</option>
				<option value="ContactSector" >Sector</option>
				<option value="ContactOrganization" >Orga</option>
				<option value="ContactDepartment" >Department</option>
				<option value="ContactPosition" >Position</option>
				<option value="ContactRemarks" >Remarks</option>
				<option value="ContactCV" >CV</option>
				</select>
      		</td>
      	</tr>
      	<tr><td>then sorted by</td>
			<td><select name="then_sorted_by" size="1">
				<option value="ContactGUID" selected>GUID</option>
				<option value="ContactCreateID">CreateID</option>
				<option value="ContactArchiveID">ArchiveID</option>
				<option value="ContactProject" >Project</option>
				<option value="ContactOwner" >Owner</option>
				<option value="ContactType" >Type</option>
				<option value="ContactCategory">Category</option>
				<option value="ContactFunction" >Function</option>
				<option value="ContactPic" >Pic</option>
				<option value="ContactFirstname" >First Name</option>
				<option value="ContactLastname" >Last Name</option>
				<option value="ContactPrivateTown" >Priv Town</option>
				<option value="ContactPrivateZIP" >Priv ZIP</option>
				<option value="ContactPrivateCountry" >Priv Country</option>
				<option value="ContactBusinessTown" >Biz Town</option>
				<option value="ContactBusinessZIP" >Biz ZIP</option>
				<option value="ContactBusinessCountry" >Biz Country</option>
				<option value="ContactSector" >Sector</option>
				<option value="ContactOrganization" >Orga</option>
				<option value="ContactDepartment" >Department</option>
				<option value="ContactPosition" >Position</option>
				<option value="ContactRemarks" >Remasrks</option>
				<option value="ContactCV" >CV</option>
				</select>
      		</td>
      	</tr>
    	<tr><td>last sorted by</td>
			<td><select name="last_sorted_by" size="1">
				<option value="ContactGUID" >GUID</option>
				<option value="ContactCreateID">CreateID</option>
				<option value="ContactArchiveID">ArchiveID</option>
				<option value="ContactProject" >Project</option>
				<option value="ContactOwner" selected>Owner</option>
				<option value="ContactType" >Type</option>
				<option value="ContactCategory" >Category</option>
				<option value="ContactFunction" >Function</option>
				<option value="ContactPic" >Pic</option>
				<option value="ContactFirstname" >First Name</option>
				<option value="ContactLastname" >Last Name</option>
				<option value="ContactPrivateTown" >Priv Town</option>
				<option value="ContactPrivateZIP" >Priv ZIP</option>
				<option value="ContactPrivateCountry" >Priv Country</option>
				<option value="ContactBusinessTown" >Biz Town</option>
				<option value="ContactBusinessZIP" >Biz ZIP</option>
				<option value="ContactBusinessCountry" >Biz Country</option>
				<option value="ContactSector" >Sector</option>
				<option value="ContactOrganization" >Orga</option>
				<option value="ContactDepartment" >Department</option>
				<option value="ContactPosition" >Position</option>
				<option value="ContactRemarks" >Remarks</option>
				<option value="ContactCV" >CV</option>
				</select>
      		</td>
      	</tr>
    	</table>

		<br>

		<table>
		<input type="submit" name="listdatasets" value="list datasets">
      	<input type="reset" value="reset values">
		</table>

    </form>
<?php
} elseif ( $listdatasets );

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-06 24:00</div>';

?>
</body>
</html>
