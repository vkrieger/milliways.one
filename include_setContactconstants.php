<?php

/*
the following Contact constants are defined 
*/

// Arrays of Contact constants

// Category classifies Contact dataset in terms of contractual relation
$ContactCategoryArray = [
    'management',
    'team',
    'client person',
    'client company',
    'cooperation',
    'subcontractor',
    'supplier',
    'network',
    'publisher',
    'hotel',
'other',
];
// Function classifies Contact dataset for function in seminars
$ContactFunctionArray = [
    'location',
    'accountable',
    'responsible',
    'backoffice',
    'referent',
    'host',
    'sponsor',
    'none',
    ];
// last change vkrieger 2021-02-06

?>