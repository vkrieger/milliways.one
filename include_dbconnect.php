<?php

/*
this code section addresses the database-server via mysqli commands by placing login values in variables
it is called at the beginning of each code where the database is accessed
this code section is the only place in milliways where database credentials are used

replace dbname by respective database 
replace username and password by username and password created in the respective database

create database via phpmyadmin or similar at will - make sure collation is UTF-8 / utf8_general_ci
create user with (all) privileges at will - but restrict user to this database only
*/

    $dbserver = "localhost";
	$dbname   = "brecht.com";
	$username = "brecht.com";
	$password = "none";

	$link = mysqli_connect($dbserver,$username,$password,$dbname) or die ("not connected to mysql server. login or password wrong?");
	$dbconnect = mysqli_select_db($link,$dbname) or die ("database not accessible");

// last change vk 2021-02-06 18:00
?>
