<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	input								{ font-size:12px ; font-family: Arial, Verdana, sans-serif;}
	select,option,textarea 				{ font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td                         { font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	*  									{ font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>
<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

include 'include_setProgramconstants.php';
include 'include_programs_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

if (!empty($_POST["historydataset"])) 
	{
	$_POST['partProgramGUID']=$ProgramGUID;	
	$_POST['partProgramArchiveID']='';
	$_POST['list_ProgramGUID']='yes';	
	$_POST['list_ProgramCreateID']='yes';	
	$_POST['list_ProgramArchiveID']='yes';	
	$_POST['list_ProgramOwner']='yes';	
	$_POST['list_ProgramName']='yes';	
	include 'programs_list.php';
	}

$CurrentTimeStamp = date("Y-m-d H:i:s");

// deletedataset: puts current dataset in archive status by entering current datetime into ArchiveID
// updatedataset: puts current dataset in archive status by entering current datetime into ArchiveID and then createdataset

if (!empty($_POST["updatedataset"]) OR !empty($_POST["deletedataset"]))
	{	
	$dbchange = "UPDATE programs SET ProgramArchiveID = '$CurrentTimeStamp' WHERE ProgramCreateID = '$ProgramCreateID'";
	$dbquery = mysqli_query($link,$dbchange) or die ("not updated!");
	}
// updatedataset: creates new dataset with same GUID and with current datetime in CreateID
// createdataset: creates new dataset with new GUID and with current datetime in CreateID, leaving the current untouched  

if (!empty($_POST["updatedataset"]) OR !empty($_POST["createdataset"]))
	
	{
	if (!empty($_POST["createdataset"])) {$ProgramGUID = $CurrentTimeStamp;}
	$ProgramCreateID = $CurrentTimeStamp;
	$ProgramArchiveID='0000-00-00 00:00:00';
	
	$ProjectFilePath='/data/ftp/'.$dataftpfolder.'/'.$ProgramProject.'/'; 
	$ProjectInitialFilePath='/data/ftp/'.$dataftpfolder.'/'.$ProgramInitialProject.'/';
	
	if (!empty($_FILES['ProgramFilename']['tmp_name']))
        {if ( move_uploaded_file($_FILES['ProgramFilename']['tmp_name'], $ProjectFilePath.basename ($_FILES['ProgramFilename']['name'])))
			{ echo $ProgramFilename = basename ($_FILES['ProgramFilename']['name']); echo ' uploaded!<br>'; $MediaPDFsize = round($_FILES['ProgramFilename']['size']/1000);}
			else { echo ' error uploading!<br>'; }
		} else {if (!$existor){$ProgramFilename=''; $ProgramFilesize='';}} 

	if ((!empty ($ProgramFilename)) AND empty($_FILES['ProgramFilename']['tmp_name'])) 
    	{ 	
		$ProgramFilenameExist = $ProgramFilename;
		copy ($ProjectInitialFilePath.$ProgramFilenameExist,$ProjectFilePath.$ProgramFilename);
		}	

	$dbchange = "INSERT INTO programs SET

ProgramGUID = '$ProgramGUID',
ProgramCreateID = '$ProgramCreateID',
ProgramArchiveID = '$ProgramArchiveID',
ProgramProject = '$ProgramProject',
ProgramOwner = '$ProgramOwner',
ProgramType = '$ProgramType',
ProgramCategory = '$ProgramCategory',
ProgramName = '$ProgramName',
ProgramTeaser = '$ProgramTeaser',
ProgramIntro = '$ProgramIntro',
ProgramTarget = '$ProgramTarget',
ProgramPromo = '$ProgramPromo',
ProgramText = '$ProgramText',
ProgramContent = '$ProgramContent',
ProgramLanguage = '$ProgramLanguage',
ProgramRegistration = '$ProgramRegistration',
ProgramFee = '$ProgramFee',
ProgramFilename = '$ProgramFilename',
ProgramFilesize = '$ProgramFilesize',
ProgramRemarks = '$ProgramRemarks'
		";
	$dbquery = mysqli_query($link,$dbchange) or die ("not created! ". mysqli_error($link));
	}

$_GET['ProgramCreateID']=$ProgramCreateID;	
include 'programs_modify.php';

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-07 14:00</div>';

?>
</body>
</html>

