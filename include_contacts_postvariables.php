<?php

/*
criteria are transported into this php by POST. empty POST variable causes empty part-variable thus no criteria
empty variables cause warnings.
*/

//
if (!empty($_POST["last_sorted_by"])) 	{ $last_sorted_by  	= $_POST["last_sorted_by"];} 	else {$last_sorted_by  	= "ContactGUID";}
if (!empty($_POST["first_sorted_by"])) 	{ $first_sorted_by  = $_POST["first_sorted_by"];} 	else {$first_sorted_by  = "ContactCategory";}
if (!empty($_POST["then_sorted_by"])) 	{ $then_sorted_by  	= $_POST["then_sorted_by"];} 	else {$then_sorted_by  	= "ContactOwner";}
if (!empty($_POST["existor"])) 			{ $existor  		= $_POST["existor"];} 			else {$existor  		= "";}

// generic variables
if (!empty($_POST['ContactGUID'])) {$ContactGUID =$_POST['ContactGUID'];} else {$ContactGUID ='';}
if (!empty($_POST['ContactCreateID'])) {$ContactCreateID =$_POST['ContactCreateID'];} else {$ContactCreateID ='';}
if (!empty($_POST['ContactArchiveID'])) {$ContactArchiveID =$_POST['ContactArchiveID'];} else {$ContactArchiveID ='';}
if (!empty($_POST['ContactProject'])) {$ContactProject =$_POST['ContactProject'];} else {$ContactProject ='';}
if (!empty($_POST['ContactInitialProject'])) {$ContactInitialProject =$_POST['ContactInitialProject'];} else {$ContactInitialProject ='';}
if (!empty($_POST['ContactOwner'])) {$ContactOwner =$_POST['ContactOwner'];} else {$ContactOwner ='';}
if (!empty($_POST['ContactType'])) {$ContactType =$_POST['ContactType'];} else {$ContactType ='';}
if (!empty($_POST['ContactCategory'])) {$ContactCategory =$_POST['ContactCategory'];} else {$ContactCategory ='';}
if (!empty($_POST['ContactFunction'])) {$ContactFunction =$_POST['ContactFunction'];} else {$ContactFunction ='';}
if (!empty($_POST['ContactPic'])) {$ContactPic =$_POST['ContactPic'];} else {$ContactPic ='';}
if (!empty($_POST['ContactFirstname'])) {$ContactFirstname =$_POST['ContactFirstname'];} else {$ContactFirstname ='';}
if (!empty($_POST['ContactLastname'])) {$ContactLastname =$_POST['ContactLastname'];} else {$ContactLastname ='';}
if (!empty($_POST['ContactEmail'])) {$ContactEmail =$_POST['ContactEmail'];} else {$ContactEmail ='';}
if (!empty($_POST['ContactEmailPrim'])) {$ContactEmailPrim =$_POST['ContactEmailPrim'];} else {$ContactEmailPrim ='';}
if (!empty($_POST['ContactEmailSec'])) {$ContactEmailSec =$_POST['ContactEmailSec'];} else {$ContactEmailSec ='';}
if (!empty($_POST['ContactWeb'])) {$ContactWeb =$_POST['ContactWeb'];} else {$ContactWeb ='';}
if (!empty($_POST['ContactTelecom'])) {$ContactTelecom =$_POST['ContactTelecom'];} else {$ContactTelecom ='';}
if (!empty($_POST['ContactTelBusiness'])) {$ContactTelBusiness =$_POST['ContactTelBusiness'];} else {$ContactTelBusiness ='';}
if (!empty($_POST['ContactTelPrivate'])) {$ContactTelPrivate =$_POST['ContactTelPrivate'];} else {$ContactTelPrivate ='';}
if (!empty($_POST['ContactTelMobile'])) {$ContactTelMobile =$_POST['ContactTelMobile'];} else {$ContactTelMobile ='';}
if (!empty($_POST['ContactFax'])) {$ContactFax =$_POST['ContactFax'];} else {$ContactFax ='';}
if (!empty($_POST['ContactPrivate'])) {$ContactPrivate =$_POST['ContactPrivate'];} else {$ContactPrivate ='';}
if (!empty($_POST['ContactPrivateTown'])) {$ContactPrivateTown =$_POST['ContactPrivateTown'];} else {$ContactPrivateTown ='';}
if (!empty($_POST['ContactPrivateZIP'])) {$ContactPrivateZIP =$_POST['ContactPrivateZIP'];} else {$ContactPrivateZIP ='';}
if (!empty($_POST['ContactPrivateCountry'])) {$ContactPrivateCountry =$_POST['ContactPrivateCountry'];} else {$ContactPrivateCountry ='';}
if (!empty($_POST['ContactBusiness'])) {$ContactBusiness =$_POST['ContactBusiness'];} else {$ContactBusiness ='';}
if (!empty($_POST['ContactBusinessTown'])) {$ContactBusinessTown =$_POST['ContactBusinessTown'];} else {$ContactBusinessTown ='';}
if (!empty($_POST['ContactBusinessZIP'])) {$ContactBusinessZIP =$_POST['ContactBusinessZIP'];} else {$ContactBusinessZIP ='';}
if (!empty($_POST['ContactBusinessCountry'])) {$ContactBusinessCountry =$_POST['ContactBusinessCountry'];} else {$ContactBusinessCountry ='';}
if (!empty($_POST['ContactSector'])) {$ContactSector =$_POST['ContactSector'];} else {$ContactSector ='';}
if (!empty($_POST['ContactOrganization'])) {$ContactOrganization =$_POST['ContactOrganization'];} else {$ContactOrganization ='';}
if (!empty($_POST['ContactDepartment'])) {$ContactDepartment =$_POST['ContactDepartment'];} else {$ContactDepartment ='';}
if (!empty($_POST['ContactPosition'])) {$ContactPosition =$_POST['ContactPosition'];} else {$ContactPosition ='';}
if (!empty($_POST['ContactRemarks'])) {$ContactRemarks =$_POST['ContactRemarks'];} else {$ContactRemarks ='';}
if (!empty($_POST['ContactCV'])) {$ContactCV =$_POST['ContactCV'];} else {$ContactCV ='';}

// list variables
if (!empty($_POST['list_ContactGUID'])) {$list_ContactGUID =$_POST['list_ContactGUID'];} else {$list_ContactGUID ='';}
if (!empty($_POST['list_ContactCreateID'])) {$list_ContactCreateID =$_POST['list_ContactCreateID'];} else {$list_ContactCreateID ='';}
if (!empty($_POST['list_ContactArchiveID'])) {$list_ContactArchiveID =$_POST['list_ContactArchiveID'];} else {$list_ContactArchiveID ='';}
if (!empty($_POST['list_ContactProject'])) {$list_ContactProject =$_POST['list_ContactProject'];} else {$list_ContactProject ='';}
if (!empty($_POST['list_ContactOwner'])) {$list_ContactOwner =$_POST['list_ContactOwner'];} else {$list_ContactOwner ='';}
if (!empty($_POST['list_ContactType'])) {$list_ContactType =$_POST['list_ContactType'];} else {$list_ContactType ='';}
if (!empty($_POST['list_ContactCategory'])) {$list_ContactCategory =$_POST['list_ContactCategory'];} else {$list_ContactCategory ='';}
if (!empty($_POST['list_ContactFunction'])) {$list_ContactFunction =$_POST['list_ContactFunction'];} else {$list_ContactFunction ='';}
if (!empty($_POST['list_ContactPic'])) {$list_ContactPic =$_POST['list_ContactPic'];} else {$list_ContactPic ='';}
if (!empty($_POST['list_ContactFirstname'])) {$list_ContactFirstname =$_POST['list_ContactFirstname'];} else {$list_ContactFirstname ='';}
if (!empty($_POST['list_ContactLastname'])) {$list_ContactLastname =$_POST['list_ContactLastname'];} else {$list_ContactLastname ='';}
if (!empty($_POST['list_ContactInternet'])) {$list_ContactInternet =$_POST['list_ContactInternet'];} else {$list_ContactInternet ='';}
if (!empty($_POST['list_ContactEmailPrim'])) {$list_ContactEmailPrim =$_POST['list_ContactEmailPrim'];} else {$list_ContactEmailPrim ='';}
if (!empty($_POST['list_ContactEmailSec'])) {$list_ContactEmailSec =$_POST['list_ContactEmailSec'];} else {$list_ContactEmailSec ='';}
if (!empty($_POST['list_ContactWeb'])) {$list_ContactWeb =$_POST['list_ContactWeb'];} else {$list_ContactWeb ='';}
if (!empty($_POST['list_ContactTelecom'])) {$list_ContactTelecom =$_POST['list_ContactTelecom'];} else {$list_ContactTelecom ='';}
if (!empty($_POST['list_ContactTelBusiness'])) {$list_ContactTelBusiness =$_POST['list_ContactTelBusiness'];} else {$list_ContactTelBusiness ='';}
if (!empty($_POST['list_ContactTelPrivate'])) {$list_ContactTelPrivate =$_POST['list_ContactTelPrivate'];} else {$list_ContactTelPrivate ='';}
if (!empty($_POST['list_ContactTelMobile'])) {$list_ContactTelMobile =$_POST['list_ContactTelMobile'];} else {$list_ContactTelMobile ='';}
if (!empty($_POST['list_ContactFax'])) {$list_ContactFax =$_POST['list_ContactFax'];} else {$list_ContactFax ='';}
if (!empty($_POST['list_ContactPrivate'])) {$list_ContactPrivate =$_POST['list_ContactPrivate'];} else {$list_ContactPrivate ='';}
if (!empty($_POST['list_ContactPrivateTown'])) {$list_ContactPrivateTown =$_POST['list_ContactPrivateTown'];} else {$list_ContactPrivateTown ='';}
if (!empty($_POST['list_ContactPrivateZIP'])) {$list_ContactPrivateZIP =$_POST['list_ContactPrivateZIP'];} else {$list_ContactPrivateZIP ='';}
if (!empty($_POST['list_ContactPrivateCountry'])) {$list_ContactPrivateCountry =$_POST['list_ContactPrivateCountry'];} else {$list_ContactPrivateCountry ='';}
if (!empty($_POST['list_ContactBusiness'])) {$list_ContactBusiness =$_POST['list_ContactBusiness'];} else {$list_ContactBusiness ='';}
if (!empty($_POST['list_ContactBusinessTown'])) {$list_ContactBusinessTown =$_POST['list_ContactBusinessTown'];} else {$list_ContactBusinessTown ='';}
if (!empty($_POST['list_ContactBusinessZIP'])) {$list_ContactBusinessZIP =$_POST['list_ContactBusinessZIP'];} else {$list_ContactBusinessZIP ='';}
if (!empty($_POST['list_ContactBusinessCountry'])) {$list_ContactBusinessCountry =$_POST['list_ContactBusinessCountry'];} else {$list_ContactBusinessCountry ='';}
if (!empty($_POST['list_ContactSector'])) {$list_ContactSector =$_POST['list_ContactSector'];} else {$list_ContactSector ='';}
if (!empty($_POST['list_ContactOrganization'])) {$list_ContactOrganization =$_POST['list_ContactOrganization'];} else {$list_ContactOrganization ='';}
if (!empty($_POST['list_ContactDepartment'])) {$list_ContactDepartment =$_POST['list_ContactDepartment'];} else {$list_ContactDepartment ='';}
if (!empty($_POST['list_ContactPosition'])) {$list_ContactPosition =$_POST['list_ContactPosition'];} else {$list_ContactPosition ='';}
if (!empty($_POST['list_ContactRemarks'])) {$list_ContactRemarks =$_POST['list_ContactRemarks'];} else {$list_ContactRemarks ='';}
if (!empty($_POST['list_ContactCV'])) {$list_ContactCV =$_POST['list_ContactCV'];} else {$list_ContactCV ='';}


 // part variables
 
if (!empty($_POST['partContactGUID'])) {$partContactGUID =$_POST['partContactGUID'];} else {$partContactGUID ='';}
if (!empty($_POST['partContactCreateID'])) {$partContactCreateID =$_POST['partContactCreateID'];} else {$partContactCreateID ='';}
if (!empty($_POST['partContactArchiveID'])) {$partContactArchiveID =$_POST['partContactArchiveID'];} else {$partContactArchiveID ='';}
if (!empty($_POST['partContactProject'])) {$partContactProject =$_POST['partContactProject'];} else {$partContactProject ='';}
if (!empty($_POST['partContactOwner'])) {$partContactOwner =$_POST['partContactOwner'];} else {$partContactOwner ='';}
if (!empty($_POST['partContactType'])) {$partContactType =$_POST['partContactType'];} else {$partContactType ='';}
if (!empty($_POST['partContactCategory'])) {$partContactCategory =$_POST['partContactCategory'];} else {$partContactCategory ='';}
if (!empty($_POST['partContactFunction'])) {$partContactFunction =$_POST['partContactFunction'];} else {$partContactFunction ='';}
if (!empty($_POST['partContactPic'])) {$partContactPic =$_POST['partContactPic'];} else {$partContactPic ='';}
if (!empty($_POST['partContactFirstname'])) {$partContactFirstname =$_POST['partContactFirstname'];} else {$partContactFirstname ='';}
if (!empty($_POST['partContactLastname'])) {$partContactLastname =$_POST['partContactLastname'];} else {$partContactLastname ='';}
if (!empty($_POST['partContactInternet'])) {$partContactInternet =$_POST['partContactInternet'];} else {$partContactInternet ='';}
if (!empty($_POST['partContactEmailPrim'])) {$partContactEmailPrim =$_POST['partContactEmailPrim'];} else {$partContactEmailPrim ='';}
if (!empty($_POST['partContactEmailSec'])) {$partContactEmailSec =$_POST['partContactEmailSec'];} else {$partContactEmailSec ='';}
if (!empty($_POST['partContactWeb'])) {$partContactWeb =$_POST['partContactWeb'];} else {$partContactWeb ='';}
if (!empty($_POST['partContactTelecom'])) {$partContactTelecom =$_POST['partContactTelecom'];} else {$partContactTelecom ='';}
if (!empty($_POST['partContactTelBusiness'])) {$partContactTelBusiness =$_POST['partContactTelBusiness'];} else {$partContactTelBusiness ='';}
if (!empty($_POST['partContactTelPrivate'])) {$partContactTelPrivate =$_POST['partContactTelPrivate'];} else {$partContactTelPrivate ='';}
if (!empty($_POST['partContactTelMobile'])) {$partContactTelMobile =$_POST['partContactTelMobile'];} else {$partContactTelMobile ='';}
if (!empty($_POST['partContactFax'])) {$partContactFax =$_POST['partContactFax'];} else {$partContactFax ='';}
if (!empty($_POST['partContactPrivate'])) {$partContactPrivate =$_POST['partContactPrivate'];} else {$partContactPrivate ='';}
if (!empty($_POST['partContactPrivateTown'])) {$partContactPrivateTown =$_POST['partContactPrivateTown'];} else {$partContactPrivateTown ='';}
if (!empty($_POST['partContactPrivateZIP'])) {$partContactPrivateZIP =$_POST['partContactPrivateZIP'];} else {$partContactPrivateZIP ='';}
if (!empty($_POST['partContactPrivateCountry'])) {$partContactPrivateCountry =$_POST['partContactPrivateCountry'];} else {$partContactPrivateCountry ='';}
if (!empty($_POST['partContactBusiness'])) {$partContactBusiness =$_POST['partContactBusiness'];} else {$partContactBusiness ='';}
if (!empty($_POST['partContactBusinessTown'])) {$partContactBusinessTown =$_POST['partContactBusinessTown'];} else {$partContactBusinessTown ='';}
if (!empty($_POST['partContactBusinessZIP'])) {$partContactBusinessZIP =$_POST['partContactBusinessZIP'];} else {$partContactBusinessZIP ='';}
if (!empty($_POST['partContactBusinessCountry'])) {$partContactBusinessCountry =$_POST['partContactBusinessCountry'];} else {$partContactBusinessCountry ='';}
if (!empty($_POST['partContactSector'])) {$partContactSector =$_POST['partContactSector'];} else {$partContactSector ='';}
if (!empty($_POST['partContactOrganization'])) {$partContactOrganization =$_POST['partContactOrganization'];} else {$partContactOrganization ='';}
if (!empty($_POST['partContactDepartment'])) {$partContactDepartment =$_POST['partContactDepartment'];} else {$partContactDepartment ='';}
if (!empty($_POST['partContactPosition'])) {$partContactPosition =$_POST['partContactPosition'];} else {$partContactPosition ='';}
if (!empty($_POST['partContactRemarks'])) {$partContactRemarks =$_POST['partContactRemarks'];} else {$partContactRemarks ='';}
if (!empty($_POST['partContactCV'])) {$partContactCV =$_POST['partContactCV'];} else {$partContactCV ='';}

 
?>
