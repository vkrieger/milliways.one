<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

You may download <a href="pendingslist.xls">this Table</a> in Spreadsheet-Format (e.g. as CSV for MS-Excel) from Server.
<br><br>

<?php

include 'include_setPendingconstants.php';
include 'include_pendings_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
	{
	echo '<form method="post" enctype="multipart/form-data" action="pendings_save.php">';
	echo '<input type="submit" name="createdataset" value="create dataset">';
	echo '</form>';
	}

  	$dbquery = "SELECT * FROM pendings WHERE

LOCATE('$partPendingGUID', PendingGUID)>0 AND
LOCATE('$partPendingCreateID', PendingCreateID)>0 AND
LOCATE('$partPendingArchiveID', PendingArchiveID)>0 AND
(LOCATE('$partPendingProject', PendingProject)>0  OR LOCATE('public', PendingType)>0) AND
LOCATE('$partPendingOwner', PendingOwner)>0 AND
LOCATE('$partPendingType', PendingType)>0 AND
LOCATE('$partPendingCategory', PendingCategory)>0 AND
LOCATE('$partPendingName', PendingName)>0 AND
LOCATE('$partPendingStatus', PendingStatus)>0 AND
LOCATE('$partPendingFilename', PendingFilename)>0 AND
LOCATE('$partPendingFilesize', PendingFilesize)>0 AND
LOCATE('$partPendingRemarks', PendingRemarks)>0

ORDER BY $first_sorted_by, $then_sorted_by, $last_sorted_by " ;
	
  	$dbresult = mysqli_query($link,$dbquery);  echo mysqli_error($link);
  	
  	$header =""; //empty header for excel file (dataset structure)
  	$data	=""; //empty $data variable for excel file (all rows in one variable)

echo '<table border="1" cellspacing="0">';
 
echo '<tr>';

if ($list_PendingGUID =='yes'){echo '<td>GUID</td>'; $header.="GUID". "\t";}
if ($list_PendingCreateID =='yes'){echo '<td>CreateID</td>'; $header.="CreateID". "\t";}
if ($list_PendingArchiveID =='yes'){echo '<td>ArchiveID</td>'; $header.="ArchiveID". "\t";}
if ($list_PendingProject =='yes'){echo '<td>Project</td>'; $header.="Project". "\t";}
if ($list_PendingRaiser =='yes'){echo '<td>Raiser</td>'; $header.="Raiser". "\t";}
if ($list_PendingOwner =='yes'){echo '<td>Owner</td>'; $header.="Owner". "\t";}
if ($list_PendingType =='yes'){echo '<td>Type</td>'; $header.="Type". "\t";}
if ($list_PendingCategory =='yes'){echo '<td>Category</td>'; $header.="Category". "\t";}
if ($list_PendingName =='yes'){echo '<td>Name</td>'; $header.="Name". "\t";}
if ($list_PendingStatus =='yes'){echo '<td>Status</td>'; $header.="Status". "\t";}
if ($list_PendingContent =='yes'){echo '<td>Content</td>'; $header.="Content". "\t";}
if ($list_PendingTask =='yes'){echo '<td>Task</td>'; $header.="Task". "\t";}
if ($list_PendingResponsible =='yes'){echo '<td>Responsible</td>'; $header.="Responsible". "\t";}
if ($list_PendingProcess =='yes'){echo '<td>Process</td>'; $header.="Process". "\t";}
if ($list_PendingDeliverable =='yes'){echo '<td>Deliverable</td>'; $header.="Deliverable". "\t";}
if ($list_PendingInterim =='yes'){echo '<td>Interim</td>'; $header.="Interim". "\t";}
if ($list_PendingResult =='yes'){echo '<td>Result</td>'; $header.="Result". "\t";}
if ($list_PendingContract =='yes'){echo '<td>Contract</td>'; $header.="Contract". "\t";}
if ($list_PendingFilename =='yes'){echo '<td>Filename</td>'; $header.="Filename". "\t";}
if ($list_PendingRemarks =='yes'){echo '<td>Remarks</td>'; $header.="Remarks". "\t";}

	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)


/*
outputs datasets and fills $data variable
*/

$DatasetCount = 0;

// control code echo $SystemProject.'<br>';

  	while($dbrow = mysqli_fetch_array($dbresult))
   	{
	$DatasetCount += 1;
	
	// output to main window and selected data to $data excel variable
   	echo '<tr>';
if ($list_PendingGUID =='yes'){echo'<td>'.$dbrow['PendingGUID'].'</td>'; $data.='"'.$dbrow['PendingGUID'].'"'."\t";}
if ($list_PendingCreateID=='yes')
	{
	echo '<td class="bluelink"><a style="font-size:xx-small" href="pendings_modify.php?PendingCreateID='.$dbrow['PendingCreateID'].'">'.$dbrow['PendingCreateID'].'</a></td>';
	$data .= '"' . $dbrow['PendingCreateID'] . '"' . "\t";
	}
if ($list_PendingArchiveID =='yes'){echo'<td>'.$dbrow['PendingArchiveID'].'</td>'; $data.='"'.$dbrow['PendingArchiveID'].'"'."\t";}
if ($list_PendingProject =='yes'){echo'<td>'.$dbrow['PendingProject'].'</td>'; $data.='"'.$dbrow['PendingProject'].'"'."\t";}
if ($list_PendingRaiser =='yes'){echo'<td>'.$dbrow['PendingRaiser'].'</td>'; $data.='"'.$dbrow['PendingRaiser'].'"'."\t";}
if ($list_PendingOwner =='yes'){echo'<td>'.$dbrow['PendingOwner'].'</td>'; $data.='"'.$dbrow['PendingOwner'].'"'."\t";}
if ($list_PendingType =='yes'){echo'<td>'.$dbrow['PendingType'].'</td>'; $data.='"'.$dbrow['PendingType'].'"'."\t";}
if ($list_PendingCategory =='yes'){echo'<td>'.$dbrow['PendingCategory'].'</td>'; $data.='"'.$dbrow['PendingCategory'].'"'."\t";}
if ($list_PendingName =='yes'){echo'<td>'.$dbrow['PendingName'].'</td>'; $data.='"'.$dbrow['PendingName'].'"'."\t";}
if ($list_PendingStatus =='yes'){echo'<td>'.$dbrow['PendingStatus'].'</td>'; $data.='"'.$dbrow['PendingStatus'].'"'."\t";}
if ($list_PendingContent =='yes'){echo'<td>'.$dbrow['PendingContent'].'</td>'; $data.='"'.$dbrow['PendingContent'].'"'."\t";}
if ($list_PendingTask =='yes'){echo'<td>'.$dbrow['PendingTask'].'</td>'; $data.='"'.$dbrow['PendingTask'].'"'."\t";}
if ($list_PendingResponsible =='yes'){echo'<td>'.$dbrow['PendingResponsible'].'</td>'; $data.='"'.$dbrow['PendingResponsible'].'"'."\t";}
if ($list_PendingProcess =='yes'){echo'<td>'.$dbrow['PendingProcess'].'</td>'; $data.='"'.$dbrow['PendingProcess'].'"'."\t";}
if ($list_PendingDeliverable =='yes'){echo'<td>'.$dbrow['PendingDeliverable'].'</td>'; $data.='"'.$dbrow['PendingDeliverable'].'"'."\t";}
if ($list_PendingInterim =='yes'){echo'<td>'.$dbrow['PendingInterim'].'</td>'; $data.='"'.$dbrow['PendingInterim'].'"'."\t";}
if ($list_PendingResult =='yes'){echo'<td>'.$dbrow['PendingResult'].'</td>'; $data.='"'.$dbrow['PendingResult'].'"'."\t";}
if ($list_PendingContract =='yes'){echo'<td>'.$dbrow['PendingContract'].'</td>'; $data.='"'.$dbrow['PendingContract'].'"'."\t";}
	
if ($list_PendingFilename=='yes')		
	{
	$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['PendingProject'].'/';
	
	echo	'<td class="bluelink">'
			.'<a style="font-size:xx-small" href="'
			.$ProjectFilePath
			.$dbrow['PendingFilename']
			.'">'
			.$dbrow['PendingFilename'].' ['.$dbrow['PendingFilesize'].'kB]'
			.'</a>';
	$data.='"'.$dbrow['PendingFilename'].' ['.$dbrow['PendingFilesize'].'kB]'.'"'."\t";
	}

if ($list_PendingRemarks =='yes'){echo'<td>'.$dbrow['PendingRemarks'].'</td>'; $data.='"'.$dbrow['PendingRemarks'].'"'."\t";}
	
	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)
	}
echo '<tr>';
echo '<td>'.$DatasetCount.' Datasets</td>';
echo '</tr>';	
echo '</table>';
/*
export selected table to .xls onto local server folder to download as offered from there
*/

$fp = fopen('pendingslist.xls','w');
fwrite($fp,$header);
fwrite($fp,"\n");
fwrite($fp,$data);
fclose($fp);

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2019-07-11 18:00</div>';

?>
</body>
</html>
