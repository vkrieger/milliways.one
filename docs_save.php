<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

<?php
// new error reporting
error_reporting(E_ALL);
ini_set("display_errors", 1);

include 'include_setDocconstants.php';
include 'include_docs_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

if (!empty($_POST["historydataset"])) 
	{
	$_POST['partDocGUID']=$DocGUID;	
	$_POST['partDocArchiveID']='';
	$_POST['list_DocGUID']='yes';	
	$_POST['list_DocCreateID']='yes';	
	$_POST['list_DocArchiveID']='yes';	
	$_POST['list_DocOwner']='yes';	
	$_POST['list_DocName']='yes';	
	$_POST['list_DocStatus']='yes';	
	include 'docs_list.php';
	}

$CurrentTimeStamp = date("Y-m-d H:i:s");

// deletedataset: puts current dataset in archive status by entering current datetime into ArchiveID
// updatedataset: puts current dataset in archive status by entering current datetime into ArchiveID and then createdataset

if (!empty($_POST["updatedataset"]) OR !empty($_POST["deletedataset"]))
	{	
	$dbchange = "UPDATE docs SET DocArchiveID = '$CurrentTimeStamp' WHERE DocCreateID = '$DocCreateID'";
	$dbquery = mysqli_query($link,$dbchange) or die ("not updated!");
	}
// updatedataset: creates new dataset with same GUID and with current datetime in CreateID
// createdataset: creates new dataset with new GUID and with current datetime in CreateID, leaving the current untouched  

if (!empty($_POST["updatedataset"]) OR !empty($_POST["createdataset"]))
	
	{
	if (!empty($_POST["createdataset"])) {$DocGUID = $CurrentTimeStamp;}
	$DocCreateID = $CurrentTimeStamp;
	$DocArchiveID='0000-00-00 00:00:00';
	
	$ProjectFilePath='/data/ftp/'.$dataftpfolder.'/'.$DocProject.'/'; 
	$ProjectInitialFilePath='/data/ftp/'.$dataftpfolder.'/'.$DocInitialProject.'/';
	
	if (!empty($_FILES['DocFilename']['tmp_name']))
        {if ( move_uploaded_file($_FILES['DocFilename']['tmp_name'], $ProjectFilePath.basename ($_FILES['DocFilename']['name'])))
			{ echo $DocFilename = basename ($_FILES['DocFilename']['name']); echo ' uploaded!<br />'; $DocFilesize = round($_FILES['DocFilename']['size']/1000);}
			else { echo ' error uploading!<br />'; }
		} else {if (!$existor){$DocFilename=''; $DocFilesize='';}} 

	if ((!empty ($DocFilename)) AND empty($_FILES['DocFilename']['tmp_name']))
    	{ 	
		$DocFilenameExist = $DocFilename;
		copy ($ProjectInitialFilePath.$DocFilenameExist,$ProjectFilePath.$DocFilename);
		}	
	
	$dbchange = "INSERT INTO docs SET

		DocGUID = '$DocGUID',
		DocCreateID = '$DocCreateID',
		DocArchiveID = '$DocArchiveID',
		DocProject = '$DocProject',
		DocOwner = '$DocOwner',
		DocType = '$DocType',
		DocCategory = '$DocCategory',
		DocName = '$DocName',
		DocFilename = '$DocFilename',
		DocFilesize = '$DocFilesize',
		DocStatus = '$DocStatus',
		DocRemarks = '$DocRemarks'
		";
	$dbquery = mysqli_query($link,$dbchange) or die (mysqli_error($link));
	}

$_GET['DocCreateID']=$DocCreateID;	
include 'docs_modify.php';

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-06 18:00</div>';

?>
</body>
</html>

