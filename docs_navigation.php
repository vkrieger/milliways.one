<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; background-color:<?php echo $SystemColor; ?>;}
	input                               {font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>
docs listing criteria<br>
<?php

include 'include_setDocconstants.php';
include 'include_docs_postvariables.php';

if (!isset($_SESSION)) { session_start();}

	$listdatasets = "";
	
if ($listdatasets == "" AND $_SESSION['LoginType'])
	{		
    echo '<form method="post" enctype="multipart/form-data" action="docs_list.php" target="main">';
	
	$partDocArchiveID ='0000-00-00 00:00:00'; // to firstly display only non-archived datasets when navigation is called
	
	echo '<table>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_DocGUID" value="yes" >GUID</td>';
	echo '<td><input type="text" name="partDocGUID" size="20" maxlength="40" value="'.$partDocGUID.'"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_DocCreateID" value="yes" checked>CreateID(Link)</td>';
	echo '<td><input type="text" name="partDocCreateID" size="20" maxlength="40" value="'.$partDocCreateID.'"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_DocArchiveID" value="yes" >ArchiveID</td>';
	echo '<td><input type="text" name="partDocArchiveID" size="20" maxlength="40" value="'.$partDocArchiveID.'"></td>';
	echo '</tr>';
	
	echo '</table>';
	
	echo '<table>';
	
	// echo 'This is control code for SystemProject '.$SystemProject.'<br />';
	// at this point all project CDEs with empty SystemProject (e.g. admin etc.) will show Project drop down
	// CDEs with non-empty System Project stay only in their regime list additionally all public type datasets
	// but they can choose the list_AppProject option for listing
	// the distinction towards SystemProject is necessary to avoid all projects in listing
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_DocProject" value="yes" checked>Project</td>';
	if (empty($SystemProject))
		{
		echo '<td><select type="text" name="partDocProject" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($SystemProjectArray as $Project) {echo '<option>'.$Project.'</option>';}
			echo '</select>';}
		else { echo '<input type="hidden" name="partDocProject" value="'.$SystemProject.'">';}
	echo '</tr>';
	
	// echo 'This is control code for partDocProject '.$partDocProject.'<br />';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_DocOwner" value="yes" >Owner</td>';
	echo '<td><input type="text" name="partDocOwner" size="8" maxlength="40" value="'.$partDocOwner.'"></td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td><input type="checkbox" name="list_DocType" value="yes" >Type</td>';
	if ($SystemType == "public")
		{echo '<td><input style="background-color:#C0C0C0" type="text" name="partDocType" size="8" maxlength="40" value="public" readonly></td>';}
		else
		{echo '<td><input type="text" name="partDocType" size="8" maxlength="40" value="'.$partDocType.'"></td>';}
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_DocCategory" value="yes" >Category</td>';
	echo '<td><select type="text" name="partDocCategory" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($DocCategoryArray as $Category) {echo '<option>'.$Category.'</option>';}
			echo '</select>';
    echo '</td>';
	echo '</tr>';    
	
	echo '<tr><td><input type="checkbox" name="list_DocName" value="yes" checked>Name</td><td><input type="text" name="partDocName" size="8" maxlength="40" value="'.$partDocName.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_DocStatus" value="yes" >Status</td><td><input type="text" name="partDocStatus" size="8" maxlength="40" value="'.$partDocStatus.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_DocFilename" value="yes" checked>Filename</td><td><input type="text" name="partDocFilename" size="8" maxlength="40" value="'.$partDocFilename.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_DocRemarks" value="yes" >Remarks</td><td><input type="text" name="partDocRemarks" size="8" maxlength="40" value="'.$partDocRemarks.'"></td></tr>';

	echo '</table>';

	?>
		
		<!-- sorting part -->
        <table>
		<tr><td>first sorted by</td>
			<td><select name="first_sorted_by" size="1">
				<option value="DocGUID" selected>GUID</option>
				<option value="DocCreateID">CreateID</option>
				<option value="DocArchiveID">ArchiveID</option>
				<option value="DocProject">Project</option>
				<option value="DocOwner" >Owner</option>
				<option value="DocType" >Type</option>
				<option value="DocCategory">Category</option>
				<option value="DocName" >Name</option>
				<option value="DocStatus" >Status</option>
				<option value="DocFilename" >Filename</option>
				<option value="DocRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
      	<tr><td>then sorted by</td>
			<td><select name="then_sorted_by" size="1">
				<option value="DocGUID">GUID</option>
				<option value="DocCreateID">CreateID</option>
				<option value="DocArchiveID">ArchiveID</option>
				<option value="DocProject" >Project</option>
				<option value="DocOwner" >Owner</option>
				<option value="DocType" >Type</option>
				<option value="DocCategory" selected>Category</option>
				<option value="DocName" >Name</option>
				<option value="DocStatus" >Status</option>
				<option value="DocFilename" >Filename</option>
				<option value="DocRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	<tr><td>last sorted by</td>
			<td><select name="last_sorted_by" size="1">
				<option value="DocGUID">GUID</option>
				<option value="DocCreateID">CreateID</option>
				<option value="DocArchiveID">ArchiveID</option>
				<option value="DocProject" >Project</option>
				<option value="DocOwner" selected>Owner</option>
				<option value="DocType" >Type</option>
				<option value="DocCategory" >Category</option>
				<option value="DocName" >Name</option>
				<option value="DocStatus" >Status</option>
				<option value="DocFilename" >Filename</option>
				<option value="DocRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	</table>

		<br>

		<table>
		<input type="submit" name="listdatasets" value="list datasets">
      	<input type="reset" value="reset values">
		</table>

    </form>
<?php
} elseif ( $listdatasets );

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-03 18:00</div>';

?>
</body>
</html>