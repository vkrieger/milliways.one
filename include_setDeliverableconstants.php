<?php

/*
the following Doc constants are defined 
*/

// Arrays of Deliverable constants
// some columns in database is of type VARCHAR(1,2,3)
// therefore values are shortened when stored in column

// Category classifies Deliverable dataset in terms of status and mode type
$DeliverableCategoryArray = [
'internal',
'confidential',
'external',
'public',
'other',
];

$DeliverableFileTypeArray = [
'XXX',
'DXF',
'DWG',
'DGN',
'RVT',
'IFC',
'PDF',
'DOC',
'XLS',
'CSV',
'ZIP',
'VECT',
'PIXL',
'TEXT',
];

$DeliverableFileClassArray = [
'internal',
'confidential',
'external',
'public',
'other',
];

$DeliverableFileContentArray = [
'other',
'Report',
'Survey',
'BOQ',
'Scheme',
'Schedule',
'2D-Drawing',
'SectionView',
'3D-Drawing',
'Animation',
'Model',
'MVD',
];

// column in database is of type VARCHAR(2)
$DeliverableFileContent1192Array = [
'XX undetermined',
'AF Animation File',
'CB Combined Model',
'CR Clash Rendition',
'DR 2D Drawing',
'M2 2D Model',
'M3 3D Model',
'MR Model Rendition',
'VS Visualization',
'BQ Bill of Quantities',
'CO Correspondence',
'CP Cost Plan',
'DB Data Base',
'FN File Note',
'HS Health and Safety',
'IE Information Exchange',
'MI Minutes / Notes',
'MS Method Statement',
'PP Presentation',
'PR Programme',
'RD Room Data Sheet',
'RI Request for Info',
'RP Report',
'SA Schedule of Accmdtn',
'CA Calculations',
'SH Schedule',
'SN Snagging List',
'SP Specification',
'SU Survey',
];

$DeliverableContentLoGArray = [
'XXX',
'100',
'200',
'300',
'400',
'500',
'600',
];

$DeliverableContentLoDArray = [
'XXX',
'100',
'200',
'300',
'400',
'500',
'600',
];

$DeliverableContentLoIArray = [
'XXX',
'100',
'200',
'300',
'400',
'500',
'600',
];

$DeliverableContentPapersizeArray = [
'XX',
'A0',
'A1',
'A2',
'A3',
'A4',
];

$DeliverableLocationFloor1192Array = [
'XX',
'ZZ',
'10',
'09',
'08',
'07',
'06',
'05',
'03',
'04',
'02',
'M2',
'01',
'M1',
'GF',
'B1',
'B2',
];

$DeliverableLocationFloorArray = [
'XX',
'10',
'09',
'08',
'07',
'06',
'05',
'03',
'04',
'02',
'01',
'00',
'B1',
'B2',
'B3',
];

// column in database is of type VARCHAR(3)
$DeliverableContractDisciplineArray = [
'XXX undetermined',
'ADM Administration',
'ARC Architecture',
'MEP MechElectrPlumb',
'STR Structural',
'CIV Civil Engineering',
'INT Interior Design',
'MED Medical Eng',
'LAB Lab Design',
'FLS Field Services',
'EXP ',
'LND Landscaping',
'FAM Facility Management',
];

// column in database is of type VARCHAR(2)
$DeliverableContractStageArray = [
'XX undefined',
'SD Strategic Draft',
'PB ',
'CD Concept Design',
'DD Detailed Design',
'TD ',
'CO ',
'HC ',
'IU ',
];

// column in database is of type VARCHAR(1)
$DeliverableContractRole1192Array = [
'X undetermined',
'A Architect',
'B Building Surveyor',
'C Civil Engineer',
'D Drainage Hiwy Engineer',
'E Electr Engineer',
'F Facility Manager',
'G Geograph Land Surveyor',
'H Heat Ventilation',
'I Interior Design',
'K Client',
'L Landscape Architect',
'M Mechanical Engineer',
'P Public Health Engineer',
'Q Quantity Surveyor',	
'S Structural Engineer',
'T Town and Country Planner',
'W Contractor',
'X Subontractor',
'Y Specialist Designer',
'Z General (non-disciplinary)',
];

// column in database is of type VARCHAR(1)
$DeliverableProcessStatusArray = [
'U undefined',
'I inquired',
'P pending',
'D delivered',
'R rejected',
'A accepted',
];

// column in database is of type VARCHAR(2)
$DeliverableProcessCode1192Array = [
'S0 initialized work in progress',
'S1 suitable for coordination',
'S2 suitable for information',
'S3 suitable for review',
'S4 suitable for approval',
'S6 suitable for PIM Authorization',
'S7 suitable for AIM Authorization',
'D1 suitable for costing',
'D2 suitable for tender',
'D3 suitable for contr design',
'D4 suitable for manufacture',
'AX authorized and accepted',
'BX partial sign off',
'CR as constructed',
];

$DeliverableProcessCodeArray = [
'X',
'A',
'B',
'C',
'D',
];

// last change vkrieger 08.06.2017

?>
