<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
	<title><?php echo $SystemProject; ?> Common Data Environment</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; background-color:<?php echo $SystemColor; ?>;}
	input                               {font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">

	<script>
		/* from http://javascript.about.com/library/blweekyear.htm */
		Date.prototype.getWeek = function()
			{
			var onejan = new Date(this.getFullYear(),0,1);
			return Math.ceil((((this - onejan) / 86400000) + onejan.getDay()+1)/7);
			}
		Date.prototype.getMDay = function() 	{return (this.getDay() + 6) %7;}
		Date.prototype.getISOYear = function()
			{
			var thu = new Date(this.getFullYear(), this.getMonth(), this.getDate()+3-this.getMDay());
			return thu.getFullYear();
			}
		Date.prototype.getISOWeek = function()
			{
			var onejan = new Date(this.getISOYear(),0,1);
			var wk = Math.ceil((((this - onejan) / 86400000) + onejan.getMDay()+1)/7);
			if (onejan.getMDay() > 3) wk--;return wk;
			}
		
		/* Live Date Script-visit http://www.dynamicdrive.com */
		var dayarray=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
		var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December")

		function getthedate()
			{
			var mydate=new Date()
			var year=mydate.getYear()
			if (year < 1000) year+=1900
			var day=mydate.getDay()
		
			var month=mydate.getMonth()
			var week=mydate.getISOWeek()
			var daym=mydate.getDate()
			if (daym<10) daym="0"+daym
			var hours=mydate.getHours()
			var hours_r=mydate.getHours()+1
			var minutes=mydate.getMinutes()
			var seconds=mydate.getSeconds()
			if (minutes<=9) minutes="0"+minutes
			if (seconds<=9) seconds="0"+seconds
			var cdate="UTC+1 Time: "+dayarray[day]+", "+daym+"."+montharray[month]+" "+year+", "+hours+":"+minutes+":"+seconds+" (week "+week+")"
			if (document.all) document.all.clock.innerHTML=cdate
				else if (document.getElementById) document.getElementById("clock").innerHTML=cdate
				else document.write(cdate)
			}

		if (!document.all&&!document.getElementById) getthedate()

		function goforit()
			{
			if (document.all||document.getElementById)
			setInterval("getthedate()",1000)
			}
	</script>


</head>

<body onLoad="goforit()">

<?php

include 'include_logins_postvariables.php';
include 'include_setSystemvariables.php'; // first $_POST variable then system specific variable

// control code
// echo 'This is SystemProject '.$SystemProject.'<br>';
// echo 'This is SystemType '.$SystemType.'<br>';
	
if ($SystemType == "public")
	{
	session_start();
	$_SESSION['LoginType']="viewer";
	$_SESSION['LoginLogin']="public";
	echo 'You are now logged in as '.$_SESSION['LoginLogin'].' '.$_SESSION['LoginType'].'<br>';
	echo 'Please continue with application <a href="header.php" target="header">here</a>!<br>';
	}
	else
	{
	echo '<table border="0" cellpadding=0">';
	echo '<tr><td class="whitelink" align="left" valign="center" style="font-size:16px">';
	echo '<a href="index.html" target="_blank">'.$SystemProject.' Common Data Environment</a>';
	echo '</td></tr>';
	echo '<tr><td><span style="font-size:32px">login or register</span></td></tr>';
	echo '</table>';

	$login='';
	$register='';

	if ($register == "" AND $login == "")
		{
		echo '<form method="post" action="logins_check.php" target="main">';
		echo '<table>';
		echo '<tr><td>Owner (Login)</td><td><input type="text" name="LoginLogin" size="8" maxlength="40" value="'.$LoginLogin.'"></td></tr>';
		echo '<tr><td>Password</td><td><input type="password" name="LoginPassword" size="8" maxlength="40" value="'.$LoginPassword.'"></td></tr>';
		echo '</table>';
		echo '<input type="submit" name="login" value="login into application">';
		echo '<input type="submit" name="register" value="modify registration of owner">';
      	echo '<input type="reset" value="reset values">';
		echo '</form>';
		} 
		elseif 
		($register OR $login);
	}
	
?>

<div align="right" style="font-size:16px" id="clock"></div>
<div align="right" style="font-size:8px">vk 2020-11-22 15:00</div>

</body>
</html>
