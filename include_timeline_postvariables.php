<?php

/*
criteria are transported into this php by POST. empty POST variable causes empty part-variable thus no criteria
empty variables cause warnings.
*/

//
if (!empty($_POST["last_sorted_by"])) 	{ $last_sorted_by  	= $_POST["last_sorted_by"];} 	else {$last_sorted_by  	= "TimelineGUID";}
if (!empty($_POST["first_sorted_by"])) 	{ $first_sorted_by  = $_POST["first_sorted_by"];} 	else {$first_sorted_by  = "TimelineCategory";}
if (!empty($_POST["then_sorted_by"])) 	{ $then_sorted_by  	= $_POST["then_sorted_by"];} 	else {$then_sorted_by  	= "TimelineOwner";}
if (!empty($_POST["existor"])) 			{ $existor  		= $_POST["existor"];} 			else {$existor  		= "";}


// generic variables
if (!empty($_POST['TimelineGUID'])) {$TimelineGUID =$_POST['TimelineGUID'];} else {$TimelineGUID ='';}
if (!empty($_POST['TimelineCreateID'])) {$TimelineCreateID =$_POST['TimelineCreateID'];} else {$TimelineCreateID ='';}
if (!empty($_POST['TimelineArchiveID'])) {$TimelineArchiveID =$_POST['TimelineArchiveID'];} else {$TimelineArchiveID ='';}
if (!empty($_POST['TimelineProject'])) {$TimelineProject =$_POST['TimelineProject'];} else {$TimelineProject ='';}
if (!empty($_POST['TimelineOwner'])) {$TimelineOwner =$_POST['TimelineOwner'];} else {$TimelineOwner ='';}
if (!empty($_POST['TimelineType'])) {$TimelineType =$_POST['TimelineType'];} else {$TimelineType ='';}
if (!empty($_POST['TimelineCategory'])) {$TimelineCategory =$_POST['TimelineCategory'];} else {$TimelineCategory ='';}
if (!empty($_POST['TimelineYear'])) {$TimelineYear =$_POST['TimelineYear'];} else {$TimelineYear ='';}
if (!empty($_POST['TimelineName'])) {$TimelineName =$_POST['TimelineName'];} else {$TimelineName ='';}
if (!empty($_POST['TimelineRemarks'])) {$TimelineRemarks =$_POST['TimelineRemarks'];} else {$TimelineRemarks ='';}

// list variables
if (!empty($_POST['list_TimelineGUID'])) {$list_TimelineGUID =$_POST['list_TimelineGUID'];} else {$list_TimelineGUID ='';}
if (!empty($_POST['list_TimelineCreateID'])) {$list_TimelineCreateID =$_POST['list_TimelineCreateID'];} else {$list_TimelineCreateID ='';}
if (!empty($_POST['list_TimelineArchiveID'])) {$list_TimelineArchiveID =$_POST['list_TimelineArchiveID'];} else {$list_TimelineArchiveID ='';}
if (!empty($_POST['list_TimelineProject'])) {$list_TimelineProject =$_POST['list_TimelineProject'];} else {$list_TimelineProject ='';}
if (!empty($_POST['list_TimelineOwner'])) {$list_TimelineOwner =$_POST['list_TimelineOwner'];} else {$list_TimelineOwner ='';}
if (!empty($_POST['list_TimelineType'])) {$list_TimelineType =$_POST['list_TimelineType'];} else {$list_TimelineType ='';}
if (!empty($_POST['list_TimelineCategory'])) {$list_TimelineCategory =$_POST['list_TimelineCategory'];} else {$list_TimelineCategory ='';}
if (!empty($_POST['list_TimelineYear'])) {$list_TimelineYear =$_POST['list_TimelineYear'];} else {$list_TimelineYear ='';}
if (!empty($_POST['list_TimelineName'])) {$list_TimelineName =$_POST['list_TimelineName'];} else {$list_TimelineName ='';}
if (!empty($_POST['list_TimelineRemarks'])) {$list_TimelineRemarks =$_POST['list_TimelineRemarks'];} else {$list_TimelineRemarks ='';}

 // part variables
 
if (!empty($_POST['partTimelineGUID'])) {$partTimelineGUID =$_POST['partTimelineGUID'];} else {$partTimelineGUID ='';}
if (!empty($_POST['partTimelineCreateID'])) {$partTimelineCreateID =$_POST['partTimelineCreateID'];} else {$partTimelineCreateID ='';}
if (!empty($_POST['partTimelineArchiveID'])) {$partTimelineArchiveID =$_POST['partTimelineArchiveID'];} else {$partTimelineArchiveID ='';}
if (!empty($_POST['partTimelineProject'])) {$partTimelineProject =$_POST['partTimelineProject'];} else {$partTimelineProject ='';}
if (!empty($_POST['partTimelineOwner'])) {$partTimelineOwner =$_POST['partTimelineOwner'];} else {$partTimelineOwner ='';}
if (!empty($_POST['partTimelineType'])) {$partTimelineType =$_POST['partTimelineType'];} else {$partTimelineType ='';}
if (!empty($_POST['partTimelineCategory'])) {$partTimelineCategory =$_POST['partTimelineCategory'];} else {$partTimelineCategory ='';}
if (!empty($_POST['partTimelineYear'])) {$partTimelineYear =$_POST['partTimelineYear'];} else {$partTimelineYear ='';}
if (!empty($_POST['partTimelineName'])) {$partTimelineName =$_POST['partTimelineName'];} else {$partTimelineName ='';}
if (!empty($_POST['partTimelineRemarks'])) {$partTimelineRemarks =$_POST['partTimelineRemarks'];} else {$partTimelineRemarks ='';}

 
?>
