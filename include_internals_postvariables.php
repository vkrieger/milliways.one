<?php

/*
criteria are transported into this php by POST. empty POST variable causes empty part-variable thus no criteria
empty variables cause warnings.
*/

//
if (!empty($_POST["last_sorted_by"])) 	{ $last_sorted_by  	= $_POST["last_sorted_by"];} 	else {$last_sorted_by  	= "InternalGUID";}
if (!empty($_POST["first_sorted_by"])) 	{ $first_sorted_by  = $_POST["first_sorted_by"];} 	else {$first_sorted_by  = "InternalCategory";}
if (!empty($_POST["then_sorted_by"])) 	{ $then_sorted_by  	= $_POST["then_sorted_by"];} 	else {$then_sorted_by  	= "InternalOwner";}
if (!empty($_POST["existor"])) 			{ $existor  		= $_POST["existor"];} 			else {$existor  		= "";}


// generic variables
if (!empty($_POST['InternalGUID'])) {$InternalGUID =$_POST['InternalGUID'];} else {$InternalGUID ='';}
if (!empty($_POST['InternalCreateID'])) {$InternalCreateID =$_POST['InternalCreateID'];} else {$InternalCreateID ='';}
if (!empty($_POST['InternalArchiveID'])) {$InternalArchiveID =$_POST['InternalArchiveID'];} else {$InternalArchiveID ='';}
if (!empty($_POST['InternalProject'])) {$InternalProject =$_POST['InternalProject'];} else {$InternalProject ='';}
if (!empty($_POST['InternalInitialProject'])) {$InternalInitialProject =$_POST['InternalInitialProject'];} else {$InternalInitialProject ='';}
if (!empty($_POST['InternalOwner'])) {$InternalOwner =$_POST['InternalOwner'];} else {$InternalOwner ='';}
if (!empty($_POST['InternalInterners'])) {$InternalInterners =$_POST['InternalInterners'];} else {$InternalInterners ='';}
if (!empty($_POST['InternalType'])) {$InternalType =$_POST['InternalType'];} else {$InternalType ='';}
if (!empty($_POST['InternalCategory'])) {$InternalCategory =$_POST['InternalCategory'];} else {$InternalCategory ='';}
if (!empty($_POST['InternalName'])) {$InternalName =$_POST['InternalName'];} else {$InternalName ='';}
if (!empty($_POST['InternalStatus'])) {$InternalStatus =$_POST['InternalStatus'];} else {$InternalStatus ='';}
if (!empty($_POST['InternalFilenames'])) {$InternalFilenames =$_POST['InternalFilenames'];} else {$InternalFilenames ='';}
if (!empty($_POST['InternalFilesizes'])) {$InternalFilesizes =$_POST['InternalFilesizes'];} else {$InternalFilesizes ='';}
if (!empty($_POST['InternalRemarks'])) {$InternalRemarks =$_POST['InternalRemarks'];} else {$InternalRemarks ='';}

// list variables
if (!empty($_POST['list_InternalGUID'])) {$list_InternalGUID =$_POST['list_InternalGUID'];} else {$list_InternalGUID ='';}
if (!empty($_POST['list_InternalCreateID'])) {$list_InternalCreateID =$_POST['list_InternalCreateID'];} else {$list_InternalCreateID ='';}
if (!empty($_POST['list_InternalArchiveID'])) {$list_InternalArchiveID =$_POST['list_InternalArchiveID'];} else {$list_InternalArchiveID ='';}
if (!empty($_POST['list_InternalProject'])) {$list_InternalProject =$_POST['list_InternalProject'];} else {$list_InternalProject ='';}
if (!empty($_POST['list_InternalOwner'])) {$list_InternalOwner =$_POST['list_InternalOwner'];} else {$list_InternalOwner ='';}
if (!empty($_POST['list_InternalInterners'])) {$list_InternalInterners =$_POST['list_InternalInterners'];} else {$list_InternalInterners ='';}
if (!empty($_POST['list_InternalType'])) {$list_InternalType =$_POST['list_InternalType'];} else {$list_InternalType ='';}
if (!empty($_POST['list_InternalCategory'])) {$list_InternalCategory =$_POST['list_InternalCategory'];} else {$list_InternalCategory ='';}
if (!empty($_POST['list_InternalName'])) {$list_InternalName =$_POST['list_InternalName'];} else {$list_InternalName ='';}
if (!empty($_POST['list_InternalStatus'])) {$list_InternalStatus =$_POST['list_InternalStatus'];} else {$list_InternalStatus ='';}
if (!empty($_POST['list_InternalFilenames'])) {$list_InternalFilenames =$_POST['list_InternalFilenames'];} else {$list_InternalFilenames ='';}
if (!empty($_POST['list_InternalFilesizes'])) {$list_InternalFilesizes =$_POST['list_InternalFilesizes'];} else {$list_InternalFilesizes ='';}
if (!empty($_POST['list_InternalRemarks'])) {$list_InternalRemarks =$_POST['list_InternalRemarks'];} else {$list_InternalRemarks ='';}

 // part variables
 
if (!empty($_POST['partInternalGUID'])) {$partInternalGUID =$_POST['partInternalGUID'];} else {$partInternalGUID ='';}
if (!empty($_POST['partInternalCreateID'])) {$partInternalCreateID =$_POST['partInternalCreateID'];} else {$partInternalCreateID ='';}
if (!empty($_POST['partInternalArchiveID'])) {$partInternalArchiveID =$_POST['partInternalArchiveID'];} else {$partInternalArchiveID ='';}
if (!empty($_POST['partInternalProject'])) {$partInternalProject =$_POST['partInternalProject'];} else {$partInternalProject ='';}
if (!empty($_POST['partInternalOwner'])) {$partInternalOwner =$_POST['partInternalOwner'];} else {$partInternalOwner ='';}
if (!empty($_POST['partInternalInterners'])) {$partInternalInterners =$_POST['partInternalInterners'];} else {$partInternalInterners ='';}
if (!empty($_POST['partInternalType'])) {$partInternalType =$_POST['partInternalType'];} else {$partInternalType ='';}
if (!empty($_POST['partInternalCategory'])) {$partInternalCategory =$_POST['partInternalCategory'];} else {$partInternalCategory ='';}
if (!empty($_POST['partInternalName'])) {$partInternalName =$_POST['partInternalName'];} else {$partInternalName ='';}
if (!empty($_POST['partInternalStatus'])) {$partInternalStatus =$_POST['partInternalStatus'];} else {$partInternalStatus ='';}
if (!empty($_POST['partInternalFilenames'])) {$partInternalFilenames =$_POST['partInternalFilenames'];} else {$partInternalFilenames ='';}
if (!empty($_POST['partInternalFilesizes'])) {$partInternalFilesizes =$_POST['partInternalFilesizes'];} else {$partInternalFilesizes ='';}
if (!empty($_POST['partInternalRemarks'])) {$partInternalRemarks =$_POST['partInternalRemarks'];} else {$partInternalRemarks ='';}

 // last change vk 21.05.2017
?>
