<?php

/*
Systemvariables and Systemconstants apply to all application routines in the System
Applicationconstants apply only to specific application routine
the following Systemconstants are defined 
*/

$selection 	='';

// Arrays of SystemProject constants

// Project is owner of dataset
$SystemProjectArray = [
'admin',
'BrechtNet',
];

// Type and Regime classifies accessibilty of dataset
$SystemRegimeArray = [
'internal',
'project',
'public',
'other',
];

$SystemTypeArray = [
'internal',
'project',
'public',
'other',
];

// last change vkrieger 2021-01-28 23:00

?>