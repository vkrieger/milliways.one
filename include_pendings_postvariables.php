<?php

/*
criteria are transported into this php by POST. empty POST variable causes empty part-variable thus no criteria
empty variables cause warnings.
*/

//
if (!empty($_POST["last_sorted_by"])) 	{ $last_sorted_by  	= $_POST["last_sorted_by"];} 	else {$last_sorted_by  	= "PendingGUID";}
if (!empty($_POST["first_sorted_by"])) 	{ $first_sorted_by  = $_POST["first_sorted_by"];} 	else {$first_sorted_by  = "PendingCategory";}
if (!empty($_POST["then_sorted_by"])) 	{ $then_sorted_by  	= $_POST["then_sorted_by"];} 	else {$then_sorted_by  	= "PendingOwner";}
if (!empty($_POST["existor"])) 			{ $existor  		= $_POST["existor"];} 			else {$existor  		= "";}


// generic variables
if (!empty($_POST['PendingGUID'])) {$PendingGUID =$_POST['PendingGUID'];} else {$PendingGUID ='';}
if (!empty($_POST['PendingCreateID'])) {$PendingCreateID =$_POST['PendingCreateID'];} else {$PendingCreateID ='';}
if (!empty($_POST['PendingArchiveID'])) {$PendingArchiveID =$_POST['PendingArchiveID'];} else {$PendingArchiveID ='';}
if (!empty($_POST['PendingProject'])) {$PendingProject =$_POST['PendingProject'];} else {$PendingProject ='';}
if (!empty($_POST['PendingInitialProject'])) {$PendingInitialProject =$_POST['PendingInitialProject'];} else {$PendingInitialProject ='';}
if (!empty($_POST['PendingOwner'])) {$PendingOwner =$_POST['PendingOwner'];} else {$PendingOwner ='';}
if (!empty($_POST['PendingType'])) {$PendingType =$_POST['PendingType'];} else {$PendingType ='';}
if (!empty($_POST['PendingCategory'])) {$PendingCategory =$_POST['PendingCategory'];} else {$PendingCategory ='';}
if (!empty($_POST['PendingName'])) {$PendingName =$_POST['PendingName'];} else {$PendingName ='';}
if (!empty($_POST['PendingStatus'])) {$PendingStatus =$_POST['PendingStatus'];} else {$PendingStatus ='';}
if (!empty($_POST['PendingRaiser'])) {$PendingRaiser =$_POST['PendingRaiser'];} else {$PendingRaiser ='';}
if (!empty($_POST['PendingContent'])) {$PendingContent =$_POST['PendingContent'];} else {$PendingContent ='';}
if (!empty($_POST['PendingTask'])) {$PendingTask =$_POST['PendingTask'];} else {$PendingTask ='';}
if (!empty($_POST['PendingResponsible'])) {$PendingResponsible =$_POST['PendingResponsible'];} else {$PendingResponsible ='';}
if (!empty($_POST['PendingProcess'])) {$PendingProcess =$_POST['PendingProcess'];} else {$PendingProcess ='';}
if (!empty($_POST['PendingDeliverable'])) {$PendingDeliverable =$_POST['PendingDeliverable'];} else {$PendingDeliverable ='';}
if (!empty($_POST['PendingInterim'])) {$PendingInterim =$_POST['PendingInterim'];} else {$PendingInterim ='';}
if (!empty($_POST['PendingResult'])) {$PendingResult =$_POST['PendingResult'];} else {$PendingResult ='';}
if (!empty($_POST['PendingContract'])) {$PendingContract =$_POST['PendingContract'];} else {$PendingContract ='';}
if (!empty($_POST['PendingFilename'])) {$PendingFilename =$_POST['PendingFilename'];} else {$PendingFilename ='';}
if (!empty($_POST['PendingFilesize'])) {$PendingFilesize =$_POST['PendingFilesize'];} else {$PendingFilesize ='';}
if (!empty($_POST['PendingRemarks'])) {$PendingRemarks =$_POST['PendingRemarks'];} else {$PendingRemarks ='';}

// list variables
if (!empty($_POST['list_PendingGUID'])) {$list_PendingGUID =$_POST['list_PendingGUID'];} else {$list_PendingGUID ='';}
if (!empty($_POST['list_PendingCreateID'])) {$list_PendingCreateID =$_POST['list_PendingCreateID'];} else {$list_PendingCreateID ='';}
if (!empty($_POST['list_PendingArchiveID'])) {$list_PendingArchiveID =$_POST['list_PendingArchiveID'];} else {$list_PendingArchiveID ='';}
if (!empty($_POST['list_PendingProject'])) {$list_PendingProject =$_POST['list_PendingProject'];} else {$list_PendingProject ='';}
if (!empty($_POST['list_PendingOwner'])) {$list_PendingOwner =$_POST['list_PendingOwner'];} else {$list_PendingOwner ='';}
if (!empty($_POST['list_PendingType'])) {$list_PendingType =$_POST['list_PendingType'];} else {$list_PendingType ='';}
if (!empty($_POST['list_PendingCategory'])) {$list_PendingCategory =$_POST['list_PendingCategory'];} else {$list_PendingCategory ='';}
if (!empty($_POST['list_PendingName'])) {$list_PendingName =$_POST['list_PendingName'];} else {$list_PendingName ='';}
if (!empty($_POST['list_PendingStatus'])) {$list_PendingStatus =$_POST['list_PendingStatus'];} else {$list_PendingStatus ='';}
if (!empty($_POST['list_PendingRaiser'])) {$list_PendingRaiser =$_POST['list_PendingRaiser'];} else {$list_PendingRaiser ='';}
if (!empty($_POST['list_PendingContent'])) {$list_PendingContent =$_POST['list_PendingContent'];} else {$list_PendingContent ='';}
if (!empty($_POST['list_PendingTask'])) {$list_PendingTask =$_POST['list_PendingTask'];} else {$list_PendingTask ='';}
if (!empty($_POST['list_PendingResponsible'])) {$list_PendingResponsible =$_POST['list_PendingResponsible'];} else {$list_PendingResponsible ='';}
if (!empty($_POST['list_PendingProcess'])) {$list_PendingProcess =$_POST['list_PendingProcess'];} else {$list_PendingProcess ='';}
if (!empty($_POST['list_PendingDeliverable'])) {$list_PendingDeliverable =$_POST['list_PendingDeliverable'];} else {$list_PendingDeliverable ='';}
if (!empty($_POST['list_PendingInterim'])) {$list_PendingInterim =$_POST['list_PendingInterim'];} else {$list_PendingInterim ='';}
if (!empty($_POST['list_PendingResult'])) {$list_PendingResult =$_POST['list_PendingResult'];} else {$list_PendingResult ='';}
if (!empty($_POST['list_PendingContract'])) {$list_PendingContract =$_POST['list_PendingContract'];} else {$list_PendingContract ='';}
if (!empty($_POST['list_PendingFilename'])) {$list_PendingFilename =$_POST['list_PendingFilename'];} else {$list_PendingFilename ='';}
if (!empty($_POST['list_PendingFilesize'])) {$list_PendingFilesize =$_POST['list_PendingFilesize'];} else {$list_PendingFilesize ='';}
if (!empty($_POST['list_PendingRemarks'])) {$list_PendingRemarks =$_POST['list_PendingRemarks'];} else {$list_PendingRemarks ='';}

 // part variables
 
if (!empty($_POST['partPendingGUID'])) {$partPendingGUID =$_POST['partPendingGUID'];} else {$partPendingGUID ='';}
if (!empty($_POST['partPendingCreateID'])) {$partPendingCreateID =$_POST['partPendingCreateID'];} else {$partPendingCreateID ='';}
if (!empty($_POST['partPendingArchiveID'])) {$partPendingArchiveID =$_POST['partPendingArchiveID'];} else {$partPendingArchiveID ='';}
if (!empty($_POST['partPendingProject'])) {$partPendingProject =$_POST['partPendingProject'];} else {$partPendingProject ='';}
if (!empty($_POST['partPendingOwner'])) {$partPendingOwner =$_POST['partPendingOwner'];} else {$partPendingOwner ='';}
if (!empty($_POST['partPendingType'])) {$partPendingType =$_POST['partPendingType'];} else {$partPendingType ='';}
if (!empty($_POST['partPendingCategory'])) {$partPendingCategory =$_POST['partPendingCategory'];} else {$partPendingCategory ='';}
if (!empty($_POST['partPendingName'])) {$partPendingName =$_POST['partPendingName'];} else {$partPendingName ='';}
if (!empty($_POST['partPendingStatus'])) {$partPendingStatus =$_POST['partPendingStatus'];} else {$partPendingStatus ='';}
if (!empty($_POST['partPendingRaiser'])) {$partPendingRaiser =$_POST['partPendingRaiser'];} else {$partPendingRaiser ='';}
if (!empty($_POST['partPendingContent'])) {$partPendingContent =$_POST['partPendingContent'];} else {$partPendingContent ='';}
if (!empty($_POST['partPendingTask'])) {$partPendingTask =$_POST['partPendingTask'];} else {$partPendingTask ='';}
if (!empty($_POST['partPendingResponsible'])) {$partPendingResponsible =$_POST['partPendingResponsible'];} else {$partPendingResponsible ='';}
if (!empty($_POST['partPendingProcess'])) {$partPendingProcess =$_POST['partPendingProcess'];} else {$partPendingProcess ='';}
if (!empty($_POST['partPendingDeliverable'])) {$partPendingDeliverable =$_POST['partPendingDeliverable'];} else {$partPendingDeliverable ='';}
if (!empty($_POST['partPendingInterim'])) {$partPendingInterim =$_POST['partPendingInterim'];} else {$partPendingInterim ='';}
if (!empty($_POST['partPendingResult'])) {$partPendingResult =$_POST['partPendingResult'];} else {$partPendingResult ='';}
if (!empty($_POST['partPendingContract'])) {$partPendingContract =$_POST['partPendingContract'];} else {$partPendingContract ='';}
if (!empty($_POST['partPendingFilename'])) {$partPendingFilename =$_POST['partPendingFilename'];} else {$partPendingFilename ='';}
if (!empty($_POST['partPendingFilesize'])) {$partPendingFilesize =$_POST['partPendingFilesize'];} else {$partPendingFilesize ='';}
if (!empty($_POST['partPendingRemarks'])) {$partPendingRemarks =$_POST['partPendingRemarks'];} else {$partPendingRemarks ='';}

 //last change vk 2017-ß2-27
?>
