<?php

/*
the following Internal constants are defined 
*/

// Arrays of Internal constants

// Category classifies Internal dataset in terms of status and mode type
$InternalCategoryArray = [
'undefined',
'other',
'brochure',
'contract',
'CV',
'drawing',
'graphics',
'invoice',
'letter',
'minutes',
'norm',
'presentation',
'regulation',
'request',
'report',
'schedule',
'spreadsheet',
'template',
];

// last change vkrieger 05.05.2017

?>