<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; background-color:<?php echo $SystemColor; ?>;}
	input                               {font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

projects listing criteria<br>

<?php

include 'include_setProjectconstants.php';
include 'include_projects_postvariables.php';

if (!isset($_SESSION)) { session_start();}

	$listdatasets = "";
	
if ($listdatasets == "" AND $_SESSION['LoginType'])
	{		
    echo '<form method="post" enctype="multipart/form-data" action="projects_list.php" target="main">';
	
	$partProjectArchiveID ='0000-00-00 00:00:00'; // to firstly display only non-archived datasets when navigation is called
	
	echo '<table>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ProjectGUID" value="yes" >GUID</td>';
	echo '<td><input type="text" name="partProjectGUID" size="20" maxlength="40" value="'.$partProjectGUID.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ProjectCreateID" value="yes" checked>CreateID(Link)</td>';
	echo '<td><input type="text" name="partProjectCreateID" size="20" maxlength="40" value="'.$partProjectCreateID.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ProjectArchiveID" value="yes" >ArchiveID</td>';
	echo '<td><input type="text" name="partProjectArchiveID" size="20" maxlength="40" value="'.$partProjectArchiveID.'"></td>';
	echo '</tr>';
	echo '</table>';
	
	echo '<table>';
	// echo 'This is control code for SystemProject '.$SystemProject.'<br>';
	if (empty($SystemProject))
		{
		echo '<tr>';
		echo '<td><input type="checkbox" name="list_ProjectProject" value="yes" checked>Project</td>';
		echo '<td><select type="text" name="partProjectProject" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($SystemProjectArray as $Project) {echo '<option>'.$Project.'</option>';}
			echo '</select>';
		echo '</tr>';
		}
		else
		{
		echo '<input type="hidden" name="partProjectProject" value="'.$SystemProject.'">';
		}
	
	// echo 'This is control code for partDocProject '.$partDocProject.'<br>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ProjectOwner" value="yes" >Owner</td>';
	echo '<td><input type="text" name="partProjectOwner" size="8" maxlength="40" value="'.$partProjectOwner.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ProjectType" value="yes" >Type</td>';
	if ($SystemType == "public")
		{echo '<td><input style="background-color:#C0C0C0" type="text" name="partProjectType" size="8" maxlength="40" value="public" readonly></td>';}
		else
		{echo '<td><input type="text" name="partProjectType" size="8" maxlength="40" value="'.$partProjectType.'"></td>';}
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ProjectCategory" value="yes" >Category</td>';
	echo '<td><select type="text" name="partProjectCategory" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($ProjectCategoryArray as $Category) {echo '<option>'.$Category.'</option>';}
			echo '</select>';
    echo '</td>';
	echo '</tr>';    
	
echo '<tr><td><input type="checkbox" name="list_ProjectName" value="yes" checked>Name </td><td><input type="text" name="partProjectName" size="8" maxlength="40" value="'.$partProjectName.'"></td></tr>';

echo '<tr>';
	echo '<td><input type="checkbox" name="list_ProjectStatus" value="yes" >Status</td>';
	echo '<td><select type="text" name="partProjectStatus" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($ProjectStatusArray as $Status) {echo '<option>'.$Status.'</option>';}
			echo '</select>';
    echo '</td>';
	echo '</tr>';    

echo '<tr><td><input type="checkbox" name="list_ProjectAquisitionNo" value="yes" >AquisitionNo</td><td><input type="text" name="partProjectAquisitionNo" size="8" maxlength="40" value="'.$partProjectAquisitionNo.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectNumber" value="yes" >Number</td><td><input type="text" name="partProjectNumber" size="8" maxlength="40" value="'.$partProjectNumber.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectCode" value="yes" >Code</td><td><input type="text" name="partProjectCode" size="8" maxlength="40" value="'.$partProjectCode.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectDescription" value="yes" >Description</td><td><input type="text" name="partProjectDescription" size="8" maxlength="40" value="'.$partProjectDescription.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectCountry" value="yes" >Country</td><td><input type="text" name="partProjectCountry" size="8" maxlength="40" value="'.$partProjectCountry.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectLocation" value="yes" >Location</td><td><input type="text" name="partProjectLocation" size="8" maxlength="40" value="'.$partProjectLocation.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectClient" value="yes" >Client</td><td><input type="text" name="partProjectClient" size="8" maxlength="40" value="'.$partProjectClient.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectOrigin" value="yes" >Origin</td><td><input type="text" name="partProjectOrigin" size="8" maxlength="40" value="'.$partProjectOrigin.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectBudget" value="yes" >Budget</td><td><input type="text" name="partProjectBudget" size="8" maxlength="40" value="'.$partProjectBudget.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectFigures" value="yes" >Figures</td><td><input type="text" name="partProjectFigures" size="8" maxlength="40" value="'.$partProjectFigures.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectPeriod" value="yes" >Period</td><td><input type="text" name="partProjectPeriod" size="8" maxlength="40" value="'.$partProjectPeriod.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectFee" value="yes" >Fee</td><td><input type="text" name="partProjectFee" size="8" maxlength="40" value="'.$partProjectFee.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectCharges" value="yes" >Charges</td><td><input type="text" name="partProjectCharges" size="8" maxlength="40" value="'.$partProjectCharges.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectIntServices" value="yes" >IntServices</td><td><input type="text" name="partProjectIntServices" size="8" maxlength="40" value="'.$partProjectIntServices.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectExtServices" value="yes" >ExtServices</td><td><input type="text" name="partProjectExtServices" size="8" maxlength="40" value="'.$partProjectExtServices.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectDirector" value="yes" >Director</td><td><input type="text" name="partProjectDirector" size="8" maxlength="40" value="'.$partProjectDirector.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectManager" value="yes" >Manager</td><td><input type="text" name="partProjectManager" size="8" maxlength="40" value="'.$partProjectManager.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectParticipants" value="yes" >Participants</td><td><input type="text" name="partProjectParticipants" size="8" maxlength="40" value="'.$partProjectParticipants.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectPartners" value="yes" >Partners</td><td><input type="text" name="partProjectPartners" size="8" maxlength="40" value="'.$partProjectPartners.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectCompetitors" value="yes" >Competitors</td><td><input type="text" name="partProjectCompetitors" size="8" maxlength="40" value="'.$partProjectCompetitors.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectSector" value="yes" >Sector</td><td><input type="text" name="partProjectSector" size="8" maxlength="40" value="'.$partProjectSector.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectDepartment" value="yes" >Department</td><td><input type="text" name="partProjectDepartment" size="8" maxlength="40" value="'.$partProjectDepartment.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectChances" value="yes" >Chances</td><td><input type="text" name="partProjectChances" size="8" maxlength="40" value="'.$partProjectChances.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectNextDeadline" value="yes" >NextDeadline</td><td><input type="text" name="partProjectNextDeadline" size="8" maxlength="40" value="'.$partProjectNextDeadline.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectStatusMemo" value="yes" >StatusMemo</td><td><input type="text" name="partProjectStatusMemo" size="8" maxlength="40" value="'.$partProjectStatusMemo.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectCallDate" value="yes" >CallDate</td><td><input type="text" name="partProjectCallDate" size="8" maxlength="40" value="'.$partProjectCallDate.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectCallMemo" value="yes" >CallMemo</td><td><input type="text" name="partProjectCallMemo" size="8" maxlength="40" value="'.$partProjectCallMemo.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectOfferDate" value="yes" >OfferDate</td><td><input type="text" name="partProjectOfferDate" size="8" maxlength="40" value="'.$partProjectOfferDate.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectOfferMemo" value="yes" >OfferMemo</td><td><input type="text" name="partProjectOfferMemo" size="8" maxlength="40" value="'.$partProjectOfferMemo.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectContractDate" value="yes" >ContractDate</td><td><input type="text" name="partProjectContractDate" size="8" maxlength="40" value="'.$partProjectContractDate.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectContractMemo" value="yes" >ContractMemo</td><td><input type="text" name="partProjectContractMemo" size="8" maxlength="40" value="'.$partProjectContractMemo.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectLostDate" value="yes" >LostDate</td><td><input type="text" name="partProjectLostDate" size="8" maxlength="40" value="'.$partProjectLostDate.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectLostMemo" value="yes" >LostMemo</td><td><input type="text" name="partProjectLostMemo" size="8" maxlength="40" value="'.$partProjectLostMemo.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectFilename" value="yes" >Filename</td><td><input type="text" name="partProjectFilename" size="8" maxlength="40" value="'.$partProjectFilename.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ProjectRemarks" value="yes" >Remarks</td><td><input type="text" name="partProjectRemarks" size="8" maxlength="40" value="'.$partProjectRemarks.'"></td></tr>';

	echo '</table>';

	?>
		
		<!-- sorting part -->
        <table>
		<tr><td>first sorted by</td>
			<td><select name="first_sorted_by" size="1">
				<option value="ProjectGUID" selected>GUID</option>
				<option value="ProjectCreateID">CreateID</option>
				<option value="ProjectArchiveID">ArchiveID</option>
				<option value="ProjectProject">Project</option>
				<option value="ProjectOwner" >Owner</option>
				<option value="ProjectType" >Type</option>
				<option value="ProjectCategory">Category</option>
				<option value="ProjectName" >Name</option>
				<option value="ProjectStatus" >Status</option>
				<option value="ProjectFilename" >Filename</option>
				<option value="ProjectFilesize" >Filesize</option>
				<option value="ProjectRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
      	<tr><td>then sorted by</td>
			<td><select name="then_sorted_by" size="1">
				<option value="ProjectGUID">GUID</option>
				<option value="ProjectCreateID">CreateID</option>
				<option value="ProjectArchiveID">ArchiveID</option>
				<option value="ProjectProject" >Project</option>
				<option value="ProjectOwner" >Owner</option>
				<option value="ProjectType" >Type</option>
				<option value="ProjectCategory" selected>Category</option>
				<option value="ProjectName" >Name</option>
				<option value="ProjectStatus" >Status</option>
				<option value="ProjectFilename" >Filename</option>
				<option value="ProjectFilesize" >Filesize</option>
				<option value="ProjectRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	<tr><td>last sorted by</td>
			<td><select name="last_sorted_by" size="1">
				<option value="ProjectGUID">GUID</option>
				<option value="ProjectCreateID">CreateID</option>
				<option value="ProjectArchiveID">ArchiveID</option>
				<option value="ProjectProject" >Project</option>
				<option value="ProjectOwner" selected>Owner</option>
				<option value="ProjectType" >Type</option>
				<option value="ProjectCategory" >Category</option>
				<option value="ProjectName" >Name</option>
				<option value="ProjectStatus" >Status</option>
				<option value="ProjectFilename" >Filename</option>
				<option value="ProjectFilesize" >Filesize</option>
				<option value="ProjectRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	</table>

		<br>

		<table>
		<input type="submit" name="listdatasets" value="list datasets">
      	<input type="reset" value="reset values">
		</table>

    </form>
<?php
} elseif ( $listdatasets );

echo '<div align="right" style="font-size: 8px;">last source change vk 13.03.17 18:00</div>';

?>
</body>
</html>