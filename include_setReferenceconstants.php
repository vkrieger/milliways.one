<?php

/*
the following Reference constants are defined 
*/

// Arrays of Reference constants

// Category classifies Reference dataset in terms of status and mode type
$ReferenceTypeArray = [
'undefined',
'other',
'public',
'private',
'PPP',
];

$ReferenceCategoryArray = [
'undefined',
'other',
'automotive',
'administration',
'education',
'exhibition',
'office',
'powersupply',
'healthcare',
'hospital',
'housing',
'industry',
'leisure',
'laboratory',
'landscape',
'datacenter',
'infrastructure',
'special constr',
'various',
];

$ReferenceDivisionArray = [
'other',
'ARC',
'Architecture',
'STR',
'Structural Engineering',
'MEP',
'Building System Enginnering',
'BIM',
'Building Information Modelling',
];

$ReferenceDisziplinArray = [
'andere',
'ARC',
'Architektur',
'STR',
'Statik',
'TGA',
'Technische Gebaeudeausruestung',
'BIM',
'Building Information Modelling',
];


// last change vkrieger 24.04.2016

?>