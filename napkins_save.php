<!DOCTYPE html public "-//W3C//DTD HTML 4.0 //EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	* 				{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                         {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea		{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 			{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>
<?php

include 'include_setNapkinconstants.php';
include 'include_napkins_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

if (!empty($_POST["historydataset"])) 
	{
	$_POST['partNapkinGUID']=$NapkinGUID;	
	$_POST['partNapkinArchiveID']='';
	$_POST['list_NapkinGUID']='yes';	
	$_POST['list_NapkinCreateID']='yes';	
	$_POST['list_NapkinArchiveID']='yes';	
	$_POST['list_NapkinOwner']='yes';	
	$_POST['list_NapkinName']='yes';	
	$_POST['list_NapkinStatus']='yes';	
	include 'napkins_list.php';
	}

$CurrentTimeStamp = date("Y-m-d H:i:s");

if (!empty($_POST["updatedataset"]) OR !empty($_POST["deletedataset"]))
	{	
	$dbchange = "UPDATE napkins SET NapkinArchiveID = '$CurrentTimeStamp' WHERE NapkinCreateID = '$NapkinCreateID'";
	$dbquery = mysqli_query($link,$dbchange) or die ("not updated!");
	}

if (!empty($_POST["updatedataset"]) OR !empty($_POST["createdataset"]))
	{
	if (!empty($_POST["createdataset"])) {$NapkinGUID = $CurrentTimeStamp;}
	$NapkinCreateID = $CurrentTimeStamp;
	$NapkinArchiveID='0000-00-00 00:00:00';
		
	if (empty($_POST["ExistorArray"])) 
		{
		$NapkinFilenames = '';
		$NapkinFilesizes = '';	
		}
	
	$ProjectFilePath='/data/ftp/'.$dataftpfolder.'/'.$NapkinProject.'/'; 
	$ProjectInitialFilePath='/data/ftp/'.$dataftpfolder.'/'.$NapkinInitialProject.'/';
	
	if ((!empty($NapkinFilenames)) AND (!empty($_POST["ExistorArray"])))
		{
		foreach ($_POST["ExistorArray"] as $key => $Existor)
			{
			$FilenameArray[$key] = strtok($Existor,' [') ;
			$rest = strtok(' [');
			$FilesizeArray[$key] = strtok($rest,'kB]');
			}
		
		$NapkinFilenames = implode ("\n",$FilenameArray);
		$NapkinFilesizes = implode ("\n",$FilesizeArray);
		}
	
	if (!empty($_FILES['Filename']['tmp_name'])) // when $_FILES['Filename']['tmp_name'] is not empty new Filename is given
 		{ 	if ( move_uploaded_file($_FILES['Filename']['tmp_name'], $ProjectFilePath.basename ($_FILES['Filename']['name'])))
			{ 
			echo basename ($_FILES['Filename']['name']); 
			echo ' uploaded!<br>'; 
			$NapkinFilenames .= "\n".basename ($_FILES['Filename']['name']);
			$NapkinFilesizes .= "\n".round ($_FILES['Filename']['size']/1000);
			}
			else { echo ' error uploading!<br>'; }
		}
		
	$dbchange = "INSERT INTO napkins SET

		NapkinGUID = '$NapkinGUID',
		NapkinCreateID = '$NapkinCreateID',
		NapkinArchiveID = '$NapkinArchiveID',
		NapkinProject = '$NapkinProject',
		NapkinOwner = '$NapkinOwner',
		NapkinType = '$NapkinType',
		NapkinCategory = '$NapkinCategory',
		NapkinName = '$NapkinName',
		NapkinFilenames = '$NapkinFilenames',
		NapkinFilesizes = '$NapkinFilesizes',
		NapkinStatus = '$NapkinStatus',
		NapkinRemarks = '$NapkinRemarks'
		";
	$dbquery = mysqli_query($link,$dbchange) or die ("not created!");
	}

$_GET['NapkinCreateID']=$NapkinCreateID;	
include 'napkins_modify.php';

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2019-07-11 18:00</div>';

?>
</body>
</html>

