<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

You may download <a href="contactslist.xls">this Table</a> in Spreadsheet-Format (e.g. as CSV for MS-Excel) from Server.
<br><br>

<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

include 'include_setContactconstants.php';
include 'include_contacts_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

// <form></form> must enclose all <input> to transfer to next app
echo '<form method="post" enctype="multipart/form-data" action="contacts_save.php">';

if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
	{
	echo '<input type="submit" name="createdataset" value="create dataset">';
	}

echo '<br /><br />';
// control code
// echo 'This is control code for ContactProject '.$ContactProject.'<br>';
// echo 'This is control code for partContactProject '.$partContactProject.'<br>';
// echo 'This is control code for $_POST '.$_POST['partContactProject'].'<br>';
// echo 'This is control code for SystemProject '.$SystemProject.'<br>';


/*
select datasets according to criteria. fone and mail criteria are bundled
2021-02-06 ContactRegime deleted, ContactFunction added in database brecht.com
*/

  	$dbquery = "SELECT * FROM contacts WHERE

LOCATE('$partContactGUID', ContactGUID)>0 AND
LOCATE('$partContactCreateID', ContactCreateID)>0 AND
LOCATE('$partContactArchiveID', ContactArchiveID)>0 AND
(LOCATE('$partContactProject', ContactProject)>0  OR LOCATE('public', ContactType)>0) AND 
LOCATE('$partContactOwner', ContactOwner)>0 AND
LOCATE('$partContactType', ContactType)>0 AND
LOCATE('$partContactCategory', ContactCategory)>0 AND
LOCATE('$partContactFunction', ContactFunction)>0 AND
LOCATE('$partContactPic', ContactPic)>0 AND
LOCATE('$partContactFirstname', ContactFirstname)>0 AND
LOCATE('$partContactLastname', ContactLastname)>0 AND
LOCATE('$partContactInternet', ContactEmailPrim)>0 AND
LOCATE('$partContactInternet', ContactEmailSec)>0 AND
LOCATE('$partContactInternet', ContactWeb)>0 AND
LOCATE('$partContactTelecom', ContactTelBusiness)>0 AND
LOCATE('$partContactTelecom', ContactTelPrivate)>0 AND
LOCATE('$partContactTelecom', ContactTelMobile)>0 AND
LOCATE('$partContactTelecom', ContactFax)>0 AND
LOCATE('$partContactPrivate', ContactPrivate)>0 AND
LOCATE('$partContactPrivate', ContactPrivateTown)>0 AND
LOCATE('$partContactPrivate', ContactPrivateZIP)>0 AND
LOCATE('$partContactPrivate', ContactPrivateCountry)>0 AND
LOCATE('$partContactBusiness', ContactBusiness)>0 AND
LOCATE('$partContactBusiness', ContactBusinessTown)>0 AND
LOCATE('$partContactBusiness', ContactBusinessZIP)>0 AND
LOCATE('$partContactBusiness', ContactBusinessCountry)>0 AND
LOCATE('$partContactSector', ContactSector)>0 AND
LOCATE('$partContactOrganization', ContactOrganization)>0 AND
LOCATE('$partContactDepartment', ContactDepartment)>0 AND
LOCATE('$partContactPosition', ContactPosition)>0 AND
LOCATE('$partContactRemarks', ContactRemarks)>0 AND
LOCATE('$partContactCV', ContactCV)>0

ORDER BY $first_sorted_by, $then_sorted_by, $last_sorted_by " ;
	
  	$dbresult = mysqli_query($link,$dbquery);  echo mysqli_error($link);
  	
  	$header =""; //empty header for excel file (dataset structure)
  	$data	=""; //empty $data variable for excel file (all rows in one variable)

echo '<table border="1" cellspacing="0">';
 
echo '<tr>';

if ($list_ContactGUID =='yes'){echo '<td>GUID</td>'; $header.="GUID". "\t";}
if ($list_ContactCreateID =='yes'){echo '<td>CreateID</td>'; $header.="CreateID". "\t";}
if ($list_ContactArchiveID =='yes'){echo '<td>ArchiveID</td>'; $header.="ArchiveID". "\t";}
if ($list_ContactProject =='yes'){echo '<td>Project</td>'; $header.="Project". "\t";}
if ($list_ContactOwner =='yes'){echo '<td>Owner</td>'; $header.="Owner". "\t";}
if ($list_ContactType =='yes'){echo '<td>Type</td>'; $header.="Type". "\t";}
if ($list_ContactCategory =='yes'){echo '<td>Category</td>'; $header.="Category". "\t";}
if ($list_ContactFunction =='yes'){echo '<td>Function</td>'; $header.="Function". "\t";}
if ($list_ContactPic =='yes'){echo '<td>Pic</td>'; $header.="Pic". "\t";}
if ($list_ContactFirstname =='yes'){echo '<td>Firstname</td>'; $header.="Firstname". "\t";}
if ($list_ContactLastname =='yes'){echo '<td>Lastname</td>'; $header.="Lastname". "\t";}
if ($list_ContactInternet =='yes'){echo '<td>EmailPrim</td>'; $header.="EmailPrim". "\t";}
if ($list_ContactInternet =='yes'){echo '<td>EmailSec</td>'; $header.="EmailSec". "\t";}
if ($list_ContactInternet =='yes'){echo '<td>Web https://</td>'; $header.="Web". "\t";}
if ($list_ContactTelecom =='yes'){echo '<td>TelBusiness</td>'; $header.="TelBusiness". "\t";}
if ($list_ContactTelecom =='yes'){echo '<td>TelPrivate</td>'; $header.="TelPrivate". "\t";}
if ($list_ContactTelecom =='yes'){echo '<td>TelMobile</td>'; $header.="TelMobile". "\t";}
if ($list_ContactTelecom =='yes'){echo '<td>Fax</td>'; $header.="Fax". "\t";}
if ($list_ContactPrivate =='yes'){echo '<td>Private</td>'; $header.="Private". "\t";}
if ($list_ContactPrivate =='yes'){echo '<td>PrivateTown</td>'; $header.="PrivateTown". "\t";}
if ($list_ContactPrivate =='yes'){echo '<td>PrivateZIP</td>'; $header.="PrivateZIP". "\t";}
if ($list_ContactPrivate =='yes'){echo '<td>PrivateCountry</td>'; $header.="PrivateCountry". "\t";}
if ($list_ContactBusiness =='yes'){echo '<td>Business</td>'; $header.="Business". "\t";}
if ($list_ContactBusiness =='yes'){echo '<td>BusinessTown</td>'; $header.="BusinessTown". "\t";}
if ($list_ContactBusiness =='yes'){echo '<td>BusinessZIP</td>'; $header.="BusinessZIP". "\t";}
if ($list_ContactBusiness =='yes'){echo '<td>BusinessCountry</td>'; $header.="BusinessCountry". "\t";}
if ($list_ContactSector =='yes'){echo '<td>Sector</td>'; $header.="Sector". "\t";}
if ($list_ContactOrganization =='yes'){echo '<td>Organization</td>'; $header.="Organization". "\t";}
if ($list_ContactDepartment =='yes'){echo '<td>Department</td>'; $header.="Department". "\t";}
if ($list_ContactPosition =='yes'){echo '<td>Position</td>'; $header.="Position". "\t";}
if ($list_ContactRemarks =='yes'){echo '<td>Remarks</td>'; $header.="Remarks". "\t";}
if ($list_ContactCV =='yes'){echo '<td>CV</td>'; $header.="CV". "\t";}

echo '</tr>';
$data .= "\n"; //end of dataset in excel file (marks row end in data)


/*
outputs datasets and fills $data variable
*/

$DatasetCount = 0;

  	while($dbrow = mysqli_fetch_array($dbresult))
   	{
	$DatasetCount += 1;
	
// output to main window and selected data to $data excel variable
echo '<tr>';
if ($list_ContactGUID =='yes'){echo'<td>'.$dbrow['ContactGUID'].'</td>'; $data.='"'.$dbrow['ContactGUID'].'"'."\t";}
if ($list_ContactCreateID=='yes')
	{
	echo '<td class="bluelink"><a style="font-size:xx-small" href="contacts_modify.php?ContactCreateID='.$dbrow['ContactCreateID'].'">'.$dbrow['ContactCreateID'].'</a></td>';
	$data .= '"' . $dbrow['ContactCreateID'] . '"' . "\t";
	}
if ($list_ContactArchiveID =='yes'){echo'<td>'.$dbrow['ContactArchiveID'].'</td>'; $data.='"'.$dbrow['ContactArchiveID'].'"'."\t";}
if ($list_ContactProject =='yes'){echo'<td>'.$dbrow['ContactProject'].'</td>'; $data.='"'.$dbrow['ContactProject'].'"'."\t";}
if ($list_ContactOwner =='yes'){echo'<td>'.$dbrow['ContactOwner'].'</td>'; $data.='"'.$dbrow['ContactOwner'].'"'."\t";}
if ($list_ContactType =='yes'){echo'<td>'.$dbrow['ContactType'].'</td>'; $data.='"'.$dbrow['ContactType'].'"'."\t";}
if ($list_ContactCategory =='yes'){echo'<td>'.$dbrow['ContactCategory'].'</td>'; $data.='"'.$dbrow['ContactCategory'].'"'."\t";}
if ($list_ContactFunction =='yes'){echo'<td>'.$dbrow['ContactFunction'].'</td>'; $data.='"'.$dbrow['ContactFunction'].'"'."\t";}

$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['ContactProject'].'/';
if ($list_ContactPic =='yes'){echo'<td><img src="'.$ProjectFilePath.$dbrow['ContactPic'].'" height="50"></td>'; $data.='"'.$dbrow['ContactPic'].'"'."\t";}

if ($list_ContactFirstname =='yes'){echo'<td>'.$dbrow['ContactFirstname'].'</td>'; $data.='"'.$dbrow['ContactFirstname'].'"'."\t";}
if ($list_ContactLastname =='yes'){echo'<td>'.$dbrow['ContactLastname'].'</td>'; $data.='"'.$dbrow['ContactLastname'].'"'."\t";}
if ($list_ContactInternet =='yes'){echo'<td><a href="mailto:'.$dbrow['ContactEmailPrim'].'" >'.$dbrow['ContactEmailPrim'].'</a></td>'; $data.='"'.$dbrow['ContactEmailPrim'].'"'."\t";}
if ($list_ContactInternet =='yes'){echo'<td><a href="mailto:'.$dbrow['ContactEmailSec'].'" >'.$dbrow['ContactEmailSec'].'</a></td>'; $data.='"'.$dbrow['ContactEmailSec'].'"'."\t";}
if ($list_ContactInternet =='yes'){echo'<td><a href="https://'.$dbrow['ContactWeb'].'" target="_blank" >'.$dbrow['ContactWeb'].'</a></td>'; $data.='"'.$dbrow['ContactWeb'].'"'."\t";}
if ($list_ContactTelecom =='yes'){echo'<td>'.$dbrow['ContactTelBusiness'].'</td>'; $data.='"'.$dbrow['ContactTelBusiness'].'"'."\t";}
if ($list_ContactTelecom =='yes'){echo'<td>'.$dbrow['ContactTelPrivate'].'</td>'; $data.='"'.$dbrow['ContactTelPrivate'].'"'."\t";}
if ($list_ContactTelecom =='yes'){echo'<td>'.$dbrow['ContactTelMobile'].'</td>'; $data.='"'.$dbrow['ContactTelMobile'].'"'."\t";}
if ($list_ContactTelecom =='yes'){echo'<td>'.$dbrow['ContactFax'].'</td>'; $data.='"'.$dbrow['ContactFax'].'"'."\t";}
if ($list_ContactPrivate =='yes'){echo'<td>'.$dbrow['ContactPrivate'].'</td>'; $data.='"'.$dbrow['ContactPrivate'].'"'."\t";}
if ($list_ContactPrivate =='yes'){echo'<td>'.$dbrow['ContactPrivateTown'].'</td>'; $data.='"'.$dbrow['ContactPrivateTown'].'"'."\t";}
if ($list_ContactPrivate =='yes'){echo'<td>'.$dbrow['ContactPrivateZIP'].'</td>'; $data.='"'.$dbrow['ContactPrivateZIP'].'"'."\t";}
if ($list_ContactPrivate =='yes'){echo'<td>'.$dbrow['ContactPrivateCountry'].'</td>'; $data.='"'.$dbrow['ContactPrivateCountry'].'"'."\t";}
if ($list_ContactBusiness =='yes'){echo'<td>'.$dbrow['ContactBusiness'].'</td>'; $data.='"'.$dbrow['ContactBusiness'].'"'."\t";}
if ($list_ContactBusiness =='yes'){echo'<td>'.$dbrow['ContactBusinessTown'].'</td>'; $data.='"'.$dbrow['ContactBusinessTown'].'"'."\t";}
if ($list_ContactBusiness =='yes'){echo'<td>'.$dbrow['ContactBusinessZIP'].'</td>'; $data.='"'.$dbrow['ContactBusinessZIP'].'"'."\t";}
if ($list_ContactBusiness =='yes'){echo'<td>'.$dbrow['ContactBusinessCountry'].'</td>'; $data.='"'.$dbrow['ContactBusinessCountry'].'"'."\t";}
if ($list_ContactSector =='yes'){echo'<td>'.$dbrow['ContactSector'].'</td>'; $data.='"'.$dbrow['ContactSector'].'"'."\t";}
if ($list_ContactOrganization =='yes'){echo'<td>'.$dbrow['ContactOrganization'].'</td>'; $data.='"'.$dbrow['ContactOrganization'].'"'."\t";}
if ($list_ContactDepartment =='yes'){echo'<td>'.$dbrow['ContactDepartment'].'</td>'; $data.='"'.$dbrow['ContactDepartment'].'"'."\t";}
if ($list_ContactPosition =='yes'){echo'<td>'.$dbrow['ContactPosition'].'</td>'; $data.='"'.$dbrow['ContactPosition'].'"'."\t";}
if ($list_ContactRemarks =='yes'){echo'<td>'.$dbrow['ContactRemarks'].'</td>'; $data.='"'.$dbrow['ContactRemarks'].'"'."\t";}
if ($list_ContactCV =='yes'){echo'<td>'.$dbrow['ContactCV'].'</td>'; $data.='"'.$dbrow['ContactCV'].'"'."\t";}
	
	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)
	}
	
	echo '<tr>';
	echo '<td>'.$DatasetCount.' Datasets</td>';
	echo '</tr>';	

	echo '</table>';

echo '</form>';
/*
export selected table to .xls onto local server folder to download as offered from there
*/

$fp = fopen('contactslist.xls','w');
fwrite($fp,$header);
fwrite($fp,"\n");
fwrite($fp,$data);
fclose($fp);

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-06 02:00</div>';

?>
</body>
</html>
