<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
	<title><?php echo $SystemProject; ?> Common Data Environment</title>
	<style> 	
	* 		{font-family:Arial,Verdana,sans-serif; font-size:large; background-color:<?php echo $SystemColor; ?>; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">

	<script>
		/* from http://javascript.about.com/library/blweekyear.htm */
		Date.prototype.getWeek = function()
			{
			var onejan = new Date(this.getFullYear(),0,1);
			return Math.ceil((((this - onejan) / 86400000) + onejan.getDay()+1)/7);
			}
		Date.prototype.getMDay = function() 	{return (this.getDay() + 6) %7;}
		Date.prototype.getISOYear = function()
			{
			var thu = new Date(this.getFullYear(), this.getMonth(), this.getDate()+3-this.getMDay());
			return thu.getFullYear();
			}
		Date.prototype.getISOWeek = function()
			{
			var onejan = new Date(this.getISOYear(),0,1);
			var wk = Math.ceil((((this - onejan) / 86400000) + onejan.getMDay()+1)/7);
			if (onejan.getMDay() > 3) wk--;return wk;
			}
		
		/* Live Date Script-visit http://www.dynamicdrive.com */
		var dayarray=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
		var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December")

		function getthedate()
			{
			var mydate=new Date()
			var year=mydate.getYear()
			if (year < 1000) year+=1900
			var day=mydate.getDay()
		
			var month=mydate.getMonth()
			var week=mydate.getISOWeek()
			var daym=mydate.getDate()
			if (daym<10) daym="0"+daym
			var hours=mydate.getHours()
			var hours_r=mydate.getHours()+1
			var minutes=mydate.getMinutes()
			var seconds=mydate.getSeconds()
			if (minutes<=9) minutes="0"+minutes
			if (seconds<=9) seconds="0"+seconds
			var cdate="UTC+1 Time: "+dayarray[day]+", "+daym+"."+montharray[month]+" "+year+", "+hours+":"+minutes+":"+seconds+" CET (week "+week+")"
			if (document.all) document.all.clock.innerHTML=cdate
				else if (document.getElementById) document.getElementById("clock").innerHTML=cdate
				else document.write(cdate)
			}

		if (!document.all&&!document.getElementById) getthedate()

		function goforit()
			{
			if (document.all||document.getElementById)
			setInterval("getthedate()",1000)
			}
	</script>


</head>

<body onLoad="goforit()">

<?php 

	echo '<table border="0" cellpadding=0">';
	echo '<tr>';
	echo '<td class="whitelink" align="left" valign="center" style="font-size:16px">';
	echo '<a style="font-size:16px" href="index.html" target="_blank">'.$SystemProject.' Common Data Environment</a>';
	echo ' - ';
	echo '<a style="font-size:16px" href="https://milliways.online" target="_blank">find manuals @ milliways.online</a>';
	echo '</td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td class="whitelink" >';
	if (strstr($SystemApps,'media')) 		{echo '<a href="media_navigation.php" target="navigation"><span style="font-size:32px">media > </span></a>';}
	if (strstr($SystemApps,'furniture')) 	{echo '<a href="furniture_navigation.php" target="navigation"><span style="font-size:32px">furniture + </span></a>';}
	if (strstr($SystemApps,'equipment')) 	{echo '<a href="equipment_navigation.php" target="navigation"><span style="font-size:32px">equipment > </span></a>';}
	if (strstr($SystemApps,'workplaces')) 	{echo '<a href="workplaces_navigation.php" target="navigation"><span style="font-size:32px">workplaces + </span></a>';}
	if (strstr($SystemApps,'infralink')) 	{echo '<a href="infralink_navigation.php" target="navigation"><span style="font-size:32px">infralink > </span></a>';}
	if (strstr($SystemApps,'rooms')) 		{echo '<a href="rooms_navigation.php" target="navigation"><span style="font-size:32px">rooms > </span></a>';}
	echo '</td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td class="whitelink" >';
	if (strstr($SystemApps,'contacts')) 	{echo '<a href="contacts_navigation.php" target="navigation"><span style="font-size:32px">contacts + </span></a>';}
	if (strstr($SystemApps,'programs')) 	{echo '<a href="programs_navigation.php" target="navigation"><span style="font-size:32px">contents </span></a>';}
	if (strstr($SystemApps,'seminars')) 	{echo '<a href="seminars_navigation.php" target="navigation"><span style="font-size:32px">> seminars </span></a>';}
	if (strstr($SystemApps,'timeline')) 	{echo '<a href="timeline_navigation.php" target="navigation"><span style="font-size:32px">> timeline </span></a>';}
	echo '</td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td class="whitelink" >';
	if (strstr($SystemApps,'addresses')) 	{echo '<a href="addresses_navigation.php" target="navigation"><span style="font-size:32px">addresses </span></a>';}
	if (strstr($SystemApps,'events')) 		{echo '<a href="events_navigation.php" target="navigation"><span style="font-size:32px">events </span></a>';}
	if (strstr($SystemApps,'calendar')) 	{echo '<a href="calendar_navigation.php" target="navigation"><span style="font-size:32px">calendar </span></a>';}
	if (strstr($SystemApps,'claims')) 		{echo '<a href="claims_navigation.php" target="navigation"><span style="font-size:32px">claims </span></a>';}
	if (strstr($SystemApps,'deliverables')) {echo '<a href="deliverables_navigation.php" target="navigation"><span style="font-size:32px">deliverables </span></a>';}
	if (strstr($SystemApps,'docs')) 		{echo '<a href="docs_navigation.php" target="navigation"><span style="font-size:32px">docs </span></a>';}
	if (strstr($SystemApps,'internals')) 	{echo '<a href="internals_navigation.php" target="navigation"><span style="font-size:32px">internals </span></a>';}
	if (strstr($SystemApps,'napkins')) 		{echo '<a href="napkins_navigation.php" target="navigation"><span style="font-size:32px">napkins </span></a>';}
	if (strstr($SystemApps,'pendings')) 	{echo '<a href="pendings_navigation.php" target="navigation"><span style="font-size:32px">pendings </span></a>';}
	if (strstr($SystemApps,'projects')) 	{echo '<a href="projects_navigation.php" target="navigation"><span style="font-size:32px">projects </span></a>';}
	if (strstr($SystemApps,'references')) 	{echo '<a href="refs_navigation.php" target="navigation"><span style="font-size:32px">references </span></a>';}
	if (strstr($SystemApps,'terms')) 		{echo '<a href="terms_navigation.php" target="navigation"><span style="font-size:32px">terms </span></a>';}
	echo '</td>';
	echo '</tr>';
	echo '</table>';
	
?>
	
<div align="right" style="font-size:16px" id="clock"></div>
<div align="right" style="font-size:8px">vk 2021-02-07 14:00</div>

</body>
</html>
