<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>


<body>

You may download <a href="programslist.xls">this Table</a> in Spreadsheet-Format (e.g. as CSV for MS-Excel) from Server.
<br /><br />

<?php

// new error handling 

error_reporting(E_ALL);
ini_set("display_errors", 1);


include 'include_setProgramconstants.php';
include 'include_programs_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

// <form></form> must enclose all <input> to transfer to next app
echo '<form method="post" enctype="multipart/form-data" action="programs_save.php">';
	
if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
	{
	echo '<input type="submit" name="createdataset" value="create dataset">';
	}

echo '<br /><br />';

/*
select datasets according to criteria. 
*/

  	$dbquery = "SELECT * FROM programs WHERE

LOCATE('$partProgramGUID', ProgramGUID)>0 AND
LOCATE('$partProgramCreateID', ProgramCreateID)>0 AND
LOCATE('$partProgramArchiveID', ProgramArchiveID)>0 AND
(LOCATE('$partProgramProject', ProgramProject)>0  OR LOCATE('public', ProgramType)>0) AND
LOCATE('$partProgramOwner', ProgramOwner)>0 AND
LOCATE('$partProgramType', ProgramType)>0 AND
LOCATE('$partProgramCategory', ProgramCategory)>0 AND
LOCATE('$partProgramName', ProgramName)>0 AND
LOCATE('$partProgramTeaser', ProgramTeaser)>0 AND
LOCATE('$partProgramIntro', ProgramIntro)>0 AND
LOCATE('$partProgramTarget', ProgramTarget)>0 AND
LOCATE('$partProgramPromo', ProgramPromo)>0 AND
LOCATE('$partProgramText', ProgramText)>0 AND
LOCATE('$partProgramContent', ProgramContent)>0 AND
LOCATE('$partProgramLanguage', ProgramLanguage)>0 AND
LOCATE('$partProgramRegistration', ProgramRegistration)>0 AND
LOCATE('$partProgramFee', ProgramFee)>0 AND
LOCATE('$partProgramFilename', ProgramFilename)>0 AND
LOCATE('$partProgramRemarks', ProgramRemarks)>0

ORDER BY $first_sorted_by, $then_sorted_by, $last_sorted_by " ;
	
  	$dbresult = mysqli_query($link,$dbquery);  echo mysqli_error($link);
  	
  	$header =""; //empty header for excel file (dataset structure)
  	$data	=""; //empty $data variable for excel file (all rows in one variable)

echo '<table border="1" cellspacing="0">';
 
echo '<tr>';

if ($list_ProgramGUID =='yes'){echo '<td>GUID</td>'; $header.="GUID". "\t";}
if ($list_ProgramCreateID =='yes'){echo '<td>CreateID</td>'; $header.="CreateID". "\t";}
if ($list_ProgramArchiveID =='yes'){echo '<td>ArchiveID</td>'; $header.="ArchiveID". "\t";}
if ($list_ProgramProject =='yes'){echo '<td>Project</td>'; $header.="Project". "\t";}
if ($list_ProgramOwner =='yes'){echo '<td>Owner</td>'; $header.="Owner". "\t";}
if ($list_ProgramType =='yes'){echo '<td>Type</td>'; $header.="Type". "\t";}
if ($list_ProgramCategory =='yes'){echo '<td>Category</td>'; $header.="Category". "\t";}

if ($list_ProgramName =='yes'){echo '<td>Name</td>'; $header.="Name". "\t";}
if ($list_ProgramTeaser =='yes'){echo '<td>Teaser</td>'; $header.="Teaser". "\t";}
if ($list_ProgramIntro =='yes'){echo '<td>Intro</td>'; $header.="Intro". "\t";}
if ($list_ProgramTarget =='yes'){echo '<td>Target</td>'; $header.="Target". "\t";}
if ($list_ProgramPromo =='yes'){echo '<td>Promo</td>'; $header.="Promo". "\t";}
if ($list_ProgramText =='yes'){echo '<td>Text</td>'; $header.="Text". "\t";}
if ($list_ProgramContent =='yes'){echo '<td>Content</td>'; $header.="Content". "\t";}
if ($list_ProgramLanguage =='yes'){echo '<td>Language</td>'; $header.="Language". "\t";}

if ($list_ProgramRegistration =='yes'){echo '<td>Registration</td>'; $header.="Registration". "\t";}
if ($list_ProgramFee =='yes'){echo '<td>Fee</td>'; $header.="Fee". "\t";}

if ($list_ProgramFilename =='yes'){echo '<td>Filename</td>'; $header.="Filename". "\t";}
if ($list_ProgramRemarks =='yes'){echo '<td>Remarks</td>'; $header.="Remarks". "\t";}

	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)


/*
outputs datasets and fills $data variable
*/

$DatasetCount = 0;

  	while($dbrow = mysqli_fetch_array($dbresult))
   	{
	$DatasetCount += 1;
	
	// output to main window and selected data to $data excel variable

   	echo '<tr>';
if ($list_ProgramGUID =='yes'){echo'<td>'.$dbrow['ProgramGUID'].'</td>'; $data.='"'.$dbrow['ProgramGUID'].'"'."\t";}
if ($list_ProgramCreateID=='yes')
		{
		echo '<td class="bluelink"><a style="font-size:xx-small" href="programs_modify.php?ProgramCreateID='.$dbrow['ProgramCreateID'].'">'.$dbrow['ProgramCreateID'].'</a></td>';
		$data .= '"' . $dbrow['ProgramCreateID'] . '"' . "\t";
		}
if ($list_ProgramArchiveID =='yes'){echo'<td>'.$dbrow['ProgramArchiveID'].'</td>'; $data.='"'.$dbrow['ProgramArchiveID'].'"'."\t";}
if ($list_ProgramProject =='yes'){echo'<td>'.$dbrow['ProgramProject'].'</td>'; $data.='"'.$dbrow['ProgramProject'].'"'."\t";}
if ($list_ProgramOwner =='yes'){echo'<td>'.$dbrow['ProgramOwner'].'</td>'; $data.='"'.$dbrow['ProgramOwner'].'"'."\t";}
if ($list_ProgramType =='yes'){echo'<td>'.$dbrow['ProgramType'].'</td>'; $data.='"'.$dbrow['ProgramType'].'"'."\t";}
if ($list_ProgramCategory =='yes'){echo'<td>'.$dbrow['ProgramCategory'].'</td>'; $data.='"'.$dbrow['ProgramCategory'].'"'."\t";}

if ($list_ProgramName =='yes'){echo'<td>'.$dbrow['ProgramName'].'</td>'; $data.='"'.$dbrow['ProgramName'].'"'."\t";}
if ($list_ProgramTeaser =='yes'){echo'<td>'.$dbrow['ProgramTeaser'].'</td>'; $data.='"'.$dbrow['ProgramTeaser'].'"'."\t";}
if ($list_ProgramIntro =='yes'){echo'<td>'.$dbrow['ProgramIntro'].'</td>'; $data.='"'.$dbrow['ProgramIntro'].'"'."\t";}
if ($list_ProgramTarget =='yes'){echo'<td>'.$dbrow['ProgramTarget'].'</td>'; $data.='"'.$dbrow['ProgramTarget'].'"'."\t";}
if ($list_ProgramPromo =='yes'){echo'<td>'.$dbrow['ProgramPromo'].'</td>'; $data.='"'.$dbrow['ProgramPromo'].'"'."\t";}
if ($list_ProgramText =='yes'){echo'<td>'.$dbrow['ProgramText'].'</td>'; $data.='"'.$dbrow['ProgramText'].'"'."\t";}
if ($list_ProgramContent =='yes'){echo'<td>'.$dbrow['ProgramContent'].'</td>'; $data.='"'.$dbrow['ProgramContent'].'"'."\t";}
if ($list_ProgramLanguage =='yes'){echo'<td>'.$dbrow['ProgramLanguage'].'</td>'; $data.='"'.$dbrow['ProgramLanguage'].'"'."\t";}

if ($list_ProgramRegistration =='yes'){echo'<td>'.$dbrow['ProgramRegistration'].'</td>'; $data.='"'.$dbrow['ProgramRegistration'].'"'."\t";}
if ($list_ProgramFee =='yes'){echo'<td>'.$dbrow['ProgramFee'].'</td>'; $data.='"'.$dbrow['ProgramFee'].'"'."\t";}

if($list_ProgramFilename=='yes')		
	{
	$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['ProgramProject'].'/';
	
	echo	'<td class="bluelink">'
			.'<a style="font-size:xx-small" href="'
			.$ProjectFilePath
			.$dbrow['ProgramFilename']
			.'">'
			.$dbrow['ProgramFilename'].'['.$dbrow['ProgramFilesize'].'kB]'
			.'</a>';
	$data.='"'.$dbrow['ProgramFilename'].'['.$dbrow['ProgramFilesize'].'kB]'.'"'."\t";
	}

if ($list_ProgramRemarks =='yes'){echo'<td>'.$dbrow['ProgramRemarks'].'</td>'; $data.='"'.$dbrow['ProgramRemarks'].'"'."\t";}
	
	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)
	}
echo '<tr>';
echo '<td>'.$DatasetCount.' Datasets</td>';
echo '</tr>';	
echo '</table>';

echo '</form>';

/*
export selected table to .xls onto local server folder to download as offered from there
*/

$fp = fopen('programslist.xls','w');
fwrite($fp,$header);
fwrite($fp,"\n");
fwrite($fp,$data);
fclose($fp);

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-07 08:00</div>';

?>
</body>
</html>
