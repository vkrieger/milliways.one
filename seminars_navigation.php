<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; background-color:<?php echo $SystemColor; ?>;}
	input                               {font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>
seminars listing criteria<br>

<?php
// new error reporting
error_reporting(E_ALL);
ini_set("display_errors", 1);

include 'include_setSeminarconstants.php';
include 'include_seminars_postvariables.php';

if (!isset($_SESSION)) { session_start();}

	$listdatasets = "";
	
if ($listdatasets == "" AND $_SESSION['LoginType'])
	{		
    echo '<form method="post" enctype="multipart/form-data" action="seminars_list.php" target="main">';
	
	$partSeminarArchiveID ='0000-00-00 00:00:00'; // to firstly display only non-archived datasets when navigation is called
	
	echo '<table>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_SeminarGUID" value="yes" >GUID</td>';
	echo '<td><input type="text" name="partSeminarGUID" size="20" maxlength="40" value="'.$partSeminarGUID.'"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_SeminarCreateID" value="yes" checked>CreateID(Link)</td>';
	echo '<td><input type="text" name="partSeminarCreateID" size="20" maxlength="40" value="'.$partSeminarCreateID.'"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_SeminarArchiveID" value="yes" >ArchiveID</td>';
	echo '<td><input type="text" name="partSeminarArchiveID" size="20" maxlength="40" value="'.$partSeminarArchiveID.'"></td>';
	echo '</tr>';
	
	echo '</table>';
	
	echo '<table>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_SeminarOwner" value="yes" >Owner</td>';
	echo '<td><input type="text" name="partSeminarOwner" size="8" maxlength="40" value="'.$partSeminarOwner.'"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_SeminarProject" value="yes" >Project</td>';
	if (empty($SystemProject))
		{
		echo '<td><select type="text" name="partSeminarProject" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($SystemProjectArray as $Project) {echo '<option>'.$Project.'</option>';}
			echo '</select>';}
		else { echo '<input type="hidden" name="partSeminarProject" value="'.$SystemProject.'">';}
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_SeminarType" value="yes" checked>Type</td>';
	if ($SystemType == "public")
		{echo '<td><input style="background-color:#C0C0C0" type="text" name="partSeminarType" size="8" maxlength="40" value="public" readonly></td>';}
		else
		{echo '<td><input type="text" name="partSeminarType" size="8" maxlength="40" value="'.$partSeminarType.'"></td>';}
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_SeminarCategory" value="yes" checked>Category</td>';
	echo '<td><select type="text" name="partSeminarCategory" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($SeminarCategoryArray as $Category) {echo '<option>'.$Category.'</option>';}
			echo '</select>';
    echo '</td>';
	echo '</tr>';    
	
	echo '</table>';

	echo '<table>';

	// time attributes
	
	echo '<tr><td><input type="checkbox" name="list_SeminarBegin" value="yes" checked>Begin</td><td><input type="text" name="partSeminarBegin" size="8" maxlength="40" value="'.$partSeminarBegin.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_SeminarYear" value="yes" >Year</td><td><input type="text" name="partSeminarYear" size="8" maxlength="40" value="'.$partSeminarYear.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_SeminarDuration" value="yes" checked >Duration</td><td><input type="text" name="partSeminarDuration" size="8" maxlength="40" value="'.$partSeminarDuration.'"></td></tr>';

	echo '<tr>';
	echo '<td><input type="checkbox" name="list_SeminarStatus" value="yes" checked>Status</td>';
	echo '<td><select type="text" name="partSeminarStatus" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($SeminarStatusArray as $Status) {echo '<option>'.$Status.'</option>';}
			echo '</select>';
    echo '</td>';
	echo '</tr>';    
	

	// content attributes

	echo '<tr><td><input type="checkbox" name="list_SeminarName" value="yes" checked >Name</td><td><input type="text" name="partSeminarName" size="8" maxlength="40" value="'.$partSeminarName.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_SeminarContent" value="yes" >Content</td><td><input type="text" name="partSeminarContent" size="8" maxlength="40" value="'.$partSeminarContent.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_SeminarLanguage" value="yes" >Language</td><td><input type="text" name="partSeminarLanguage" size="8" maxlength="40" value="'.$partSeminarLanguage.'"></td></tr>';


	// geo attributes
	
	echo '<tr><td><input type="checkbox" name="list_SeminarLocation" value="yes" checked >Location</td><td><input type="text" name="partSeminarLocation" size="8" maxlength="40" value="'.$partSeminarLocation.'"></td></tr>';
	// echo '<tr><td><input type="checkbox" name="list_SeminarAddress" value="yes" >Address</td><td><input type="text" name="partSeminarAddress" size="8" maxlength="40" value="'.$partSeminarAddress.'"></td></tr>';
	// echo '<tr><td><input type="checkbox" name="list_SeminarCountry" value="yes" >Country</td><td><input type="text" name="partSeminarCountry" size="8" maxlength="40" value="'.$partSeminarCountry.'"></td></tr>';


	// orga attributes 

	echo '<tr><td><input type="checkbox" name="list_SeminarHost" value="yes" checked >Host</td><td><input type="text" name="partSeminarHost" size="8" maxlength="40" value="'.$partSeminarHost.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_SeminarSponsor" value="yes" >Sponsor</td><td><input type="text" name="partSeminarSponsor" size="8" maxlength="40" value="'.$partSeminarSponsor.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_SeminarContact" value="yes" >Contact</td><td><input type="text" name="partSeminarContact" size="8" maxlength="40" value="'.$partSeminarContact.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_SeminarWeb" value="yes" >Web</td><td><input type="text" name="partSeminarWeb" size="8" maxlength="40" value="'.$partSeminarWeb.'"></td></tr>';


	// budget attributes

	echo '<tr><td><input type="checkbox" name="list_SeminarRegistration" value="yes" >Registration</td><td><input type="text" name="partSeminarRegistration" size="8" maxlength="40" value="'.$partSeminarRegistration.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_SeminarFee" value="yes" checked >Fee</td><td><input type="text" name="partSeminarFee" size="8" maxlength="40" value="'.$partSeminarFee.'"></td></tr>';


	// other attributes
	
	echo '<tr><td><input type="checkbox" name="list_SeminarFilename" value="yes" checked>Filename</td><td><input type="text" name="partSeminarFilename" size="8" maxlength="40" value="'.$partSeminarFilename.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_SeminarRemarks" value="yes" checked >Remarks</td><td><input type="text" name="partSeminarRemarks" size="8" maxlength="40" value="'.$partSeminarRemarks.'"></td></tr>';

	echo '</table>';

	?>
		
		<!-- sorting part -->
        <table>
		<tr><td>first sorted by</td>
			<td><select name="first_sorted_by" size="1">
				<option value="SeminarGUID" >GUID</option>
				<option value="SeminarCreateID" >CreateID</option>
				<option value="SeminarArchiveID" >ArchiveID</option>
				<option value="SeminarProject">Project</option>
				<option value="SeminarOwner" >Owner</option>
				<option value="SeminarType" >Type</option>
				<option value="SeminarCategory">Category</option>
				<option value="SeminarBegin" selected>Begin</option>
				<option value="SeminarYear" >Year</option>
				<option value="SeminarName" >Name</option>
				<option value="SeminarLocation" >Location</option>
				<option value="SeminarFee" >Fee</option>
				<option value="SeminarStatus" >Status</option>
				<option value="SeminarFilename" >Filename</option>
				<option value="SeminarRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
      	<tr><td>then sorted by</td>
			<td><select name="then_sorted_by" size="1">
				<option value="SeminarGUID" >GUID</option>
				<option value="SeminarCreateID" >CreateID</option>
				<option value="SeminarArchiveID" >ArchiveID</option>
				<option value="SeminarProject" >Project</option>
				<option value="SeminarOwner" >Owner</option>
				<option value="SeminarType" >Type</option>
				<option value="SeminarCategory" selected>Category</option>
				<option value="SeminarBegin" >Begin</option>
				<option value="SeminarYear" >Year</option>
				<option value="SeminarName" >Name</option>
				<option value="SeminarLocation" >Location</option>
				<option value="SeminarFee" >Fee</option>
				<option value="SeminarStatus" >Status</option>
				<option value="SeminarFilename" >Filename</option>
				<option value="SeminarRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	<tr><td>last sorted by</td>
			<td><select name="last_sorted_by" size="1">
				<option value="SeminarGUID" >GUID</option>
				<option value="SeminarCreateID" >CreateID</option>
				<option value="SeminarArchiveID" >ArchiveID</option>
				<option value="SeminarProject" >Project</option>
				<option value="SeminarOwner" selected>Owner</option>
				<option value="SeminarType" >Type</option>
				<option value="SeminarCategory" >Category</option>
				<option value="SeminarBegin" >Begin</option>
				<option value="SeminarYear" >Year</option>
				<option value="SeminarName" >Name</option>
				<option value="SeminarLocation" >Location</option>
				<option value="SeminarFee" >Fee</option>
				<option value="SeminarStatus" >Status</option>
				<option value="SeminarFilename" >Filename</option>
				<option value="SeminarRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	</table>

		<br>

		<table>
		<input type="submit" name="listdatasets" value="list datasets">
      	<input type="reset" value="reset values">
		</table>

    </form>
<?php
} elseif ( $listdatasets );

echo '<div align="right" style="font-size: 8px;">last source change vk 2021-02-04 08:00</div>';

?>
</body>
</html>