<?php

/*
criteria are transported into this php by POST. empty POST variable causes empty part-variable thus no criteria
empty variables cause warnings.
*/

//
if (!empty($_POST["last_sorted_by"])) 	{ $last_sorted_by  	= $_POST["last_sorted_by"];} 	else {$last_sorted_by  	= "AddressGUID";}
if (!empty($_POST["first_sorted_by"])) 	{ $first_sorted_by  = $_POST["first_sorted_by"];} 	else {$first_sorted_by  = "AddressCategory";}
if (!empty($_POST["then_sorted_by"])) 	{ $then_sorted_by  	= $_POST["then_sorted_by"];} 	else {$then_sorted_by  	= "AddressOwner";}
if (!empty($_POST["existor"])) 			{ $existor  		= $_POST["existor"];} 			else {$existor  		= "";}

// generic variables
if (!empty($_POST['AddressGUID'])) {$AddressGUID =$_POST['AddressGUID'];} else {$AddressGUID ='';}
if (!empty($_POST['AddressCreateID'])) {$AddressCreateID =$_POST['AddressCreateID'];} else {$AddressCreateID ='';}
if (!empty($_POST['AddressArchiveID'])) {$AddressArchiveID =$_POST['AddressArchiveID'];} else {$AddressArchiveID ='';}
if (!empty($_POST['AddressProject'])) {$AddressProject =$_POST['AddressProject'];} else {$AddressProject ='';}
if (!empty($_POST['AddressOwner'])) {$AddressOwner =$_POST['AddressOwner'];} else {$AddressOwner ='';}
if (!empty($_POST['AddressType'])) {$AddressType =$_POST['AddressType'];} else {$AddressType ='';}
if (!empty($_POST['AddressCategory'])) {$AddressCategory =$_POST['AddressCategory'];} else {$AddressCategory ='';}
if (!empty($_POST['AddressFirstname'])) {$AddressFirstname =$_POST['AddressFirstname'];} else {$AddressFirstname ='';}
if (!empty($_POST['AddressLastname'])) {$AddressLastname =$_POST['AddressLastname'];} else {$AddressLastname ='';}
if (!empty($_POST['AddressEmail'])) {$AddressEmail =$_POST['AddressEmail'];} else {$AddressEmail ='';}
if (!empty($_POST['AddressEmailPrim'])) {$AddressEmailPrim =$_POST['AddressEmailPrim'];} else {$AddressEmailPrim ='';}
if (!empty($_POST['AddressEmailSec'])) {$AddressEmailSec =$_POST['AddressEmailSec'];} else {$AddressEmailSec ='';}
if (!empty($_POST['AddressTelecom'])) {$AddressTelecom =$_POST['AddressTelecom'];} else {$AddressTelecom ='';}
if (!empty($_POST['AddressTelBusiness'])) {$AddressTelBusiness =$_POST['AddressTelBusiness'];} else {$AddressTelBusiness ='';}
if (!empty($_POST['AddressTelPrivate'])) {$AddressTelPrivate =$_POST['AddressTelPrivate'];} else {$AddressTelPrivate ='';}
if (!empty($_POST['AddressTelMobile'])) {$AddressTelMobile =$_POST['AddressTelMobile'];} else {$AddressTelMobile ='';}
if (!empty($_POST['AddressFax'])) {$AddressFax =$_POST['AddressFax'];} else {$AddressFax ='';}
if (!empty($_POST['AddressPrivate'])) {$AddressPrivate =$_POST['AddressPrivate'];} else {$AddressPrivate ='';}
if (!empty($_POST['AddressPrivateTown'])) {$AddressPrivateTown =$_POST['AddressPrivateTown'];} else {$AddressPrivateTown ='';}
if (!empty($_POST['AddressPrivateZIP'])) {$AddressPrivateZIP =$_POST['AddressPrivateZIP'];} else {$AddressPrivateZIP ='';}
if (!empty($_POST['AddressPrivateCountry'])) {$AddressPrivateCountry =$_POST['AddressPrivateCountry'];} else {$AddressPrivateCountry ='';}
if (!empty($_POST['AddressBusiness'])) {$AddressBusiness =$_POST['AddressBusiness'];} else {$AddressBusiness ='';}
if (!empty($_POST['AddressBusinessTown'])) {$AddressBusinessTown =$_POST['AddressBusinessTown'];} else {$AddressBusinessTown ='';}
if (!empty($_POST['AddressBusinessZIP'])) {$AddressBusinessZIP =$_POST['AddressBusinessZIP'];} else {$AddressBusinessZIP ='';}
if (!empty($_POST['AddressBusinessCountry'])) {$AddressBusinessCountry =$_POST['AddressBusinessCountry'];} else {$AddressBusinessCountry ='';}
if (!empty($_POST['AddressSector'])) {$AddressSector =$_POST['AddressSector'];} else {$AddressSector ='';}
if (!empty($_POST['AddressOrganization'])) {$AddressOrganization =$_POST['AddressOrganization'];} else {$AddressOrganization ='';}
if (!empty($_POST['AddressDepartment'])) {$AddressDepartment =$_POST['AddressDepartment'];} else {$AddressDepartment ='';}
if (!empty($_POST['AddressPosition'])) {$AddressPosition =$_POST['AddressPosition'];} else {$AddressPosition ='';}
if (!empty($_POST['AddressRemarks'])) {$AddressRemarks =$_POST['AddressRemarks'];} else {$AddressRemarks ='';}

// list variables
if (!empty($_POST['list_AddressGUID'])) {$list_AddressGUID =$_POST['list_AddressGUID'];} else {$list_AddressGUID ='';}
if (!empty($_POST['list_AddressCreateID'])) {$list_AddressCreateID =$_POST['list_AddressCreateID'];} else {$list_AddressCreateID ='';}
if (!empty($_POST['list_AddressArchiveID'])) {$list_AddressArchiveID =$_POST['list_AddressArchiveID'];} else {$list_AddressArchiveID ='';}
if (!empty($_POST['list_AddressProject'])) {$list_AddressProject =$_POST['list_AddressProject'];} else {$list_AddressProject ='';}
if (!empty($_POST['list_AddressOwner'])) {$list_AddressOwner =$_POST['list_AddressOwner'];} else {$list_AddressOwner ='';}
if (!empty($_POST['list_AddressType'])) {$list_AddressType =$_POST['list_AddressType'];} else {$list_AddressType ='';}
if (!empty($_POST['list_AddressCategory'])) {$list_AddressCategory =$_POST['list_AddressCategory'];} else {$list_AddressCategory ='';}
if (!empty($_POST['list_AddressFirstname'])) {$list_AddressFirstname =$_POST['list_AddressFirstname'];} else {$list_AddressFirstname ='';}
if (!empty($_POST['list_AddressLastname'])) {$list_AddressLastname =$_POST['list_AddressLastname'];} else {$list_AddressLastname ='';}
if (!empty($_POST['list_AddressEmail'])) {$list_AddressEmail =$_POST['list_AddressEmail'];} else {$list_AddressEmail ='';}
if (!empty($_POST['list_AddressEmailPrim'])) {$list_AddressEmailPrim =$_POST['list_AddressEmailPrim'];} else {$list_AddressEmailPrim ='';}
if (!empty($_POST['list_AddressEmailSec'])) {$list_AddressEmailSec =$_POST['list_AddressEmailSec'];} else {$list_AddressEmailSec ='';}
if (!empty($_POST['list_AddressTelecom'])) {$list_AddressTelecom =$_POST['list_AddressTelecom'];} else {$list_AddressTelecom ='';}
if (!empty($_POST['list_AddressTelBusiness'])) {$list_AddressTelBusiness =$_POST['list_AddressTelBusiness'];} else {$list_AddressTelBusiness ='';}
if (!empty($_POST['list_AddressTelPrivate'])) {$list_AddressTelPrivate =$_POST['list_AddressTelPrivate'];} else {$list_AddressTelPrivate ='';}
if (!empty($_POST['list_AddressTelMobile'])) {$list_AddressTelMobile =$_POST['list_AddressTelMobile'];} else {$list_AddressTelMobile ='';}
if (!empty($_POST['list_AddressFax'])) {$list_AddressFax =$_POST['list_AddressFax'];} else {$list_AddressFax ='';}
if (!empty($_POST['list_AddressPrivate'])) {$list_AddressPrivate =$_POST['list_AddressPrivate'];} else {$list_AddressPrivate ='';}
if (!empty($_POST['list_AddressPrivateTown'])) {$list_AddressPrivateTown =$_POST['list_AddressPrivateTown'];} else {$list_AddressPrivateTown ='';}
if (!empty($_POST['list_AddressPrivateZIP'])) {$list_AddressPrivateZIP =$_POST['list_AddressPrivateZIP'];} else {$list_AddressPrivateZIP ='';}
if (!empty($_POST['list_AddressPrivateCountry'])) {$list_AddressPrivateCountry =$_POST['list_AddressPrivateCountry'];} else {$list_AddressPrivateCountry ='';}
if (!empty($_POST['list_AddressBusiness'])) {$list_AddressBusiness =$_POST['list_AddressBusiness'];} else {$list_AddressBusiness ='';}
if (!empty($_POST['list_AddressBusinessTown'])) {$list_AddressBusinessTown =$_POST['list_AddressBusinessTown'];} else {$list_AddressBusinessTown ='';}
if (!empty($_POST['list_AddressBusinessZIP'])) {$list_AddressBusinessZIP =$_POST['list_AddressBusinessZIP'];} else {$list_AddressBusinessZIP ='';}
if (!empty($_POST['list_AddressBusinessCountry'])) {$list_AddressBusinessCountry =$_POST['list_AddressBusinessCountry'];} else {$list_AddressBusinessCountry ='';}
if (!empty($_POST['list_AddressSector'])) {$list_AddressSector =$_POST['list_AddressSector'];} else {$list_AddressSector ='';}
if (!empty($_POST['list_AddressOrganization'])) {$list_AddressOrganization =$_POST['list_AddressOrganization'];} else {$list_AddressOrganization ='';}
if (!empty($_POST['list_AddressDepartment'])) {$list_AddressDepartment =$_POST['list_AddressDepartment'];} else {$list_AddressDepartment ='';}
if (!empty($_POST['list_AddressPosition'])) {$list_AddressPosition =$_POST['list_AddressPosition'];} else {$list_AddressPosition ='';}
if (!empty($_POST['list_AddressRemarks'])) {$list_AddressRemarks =$_POST['list_AddressRemarks'];} else {$list_AddressRemarks ='';}


 // part variables
 
if (!empty($_POST['partAddressGUID'])) {$partAddressGUID =$_POST['partAddressGUID'];} else {$partAddressGUID ='';}
if (!empty($_POST['partAddressCreateID'])) {$partAddressCreateID =$_POST['partAddressCreateID'];} else {$partAddressCreateID ='';}
if (!empty($_POST['partAddressArchiveID'])) {$partAddressArchiveID =$_POST['partAddressArchiveID'];} else {$partAddressArchiveID ='';}
if (!empty($_POST['partAddressProject'])) {$partAddressProject =$_POST['partAddressProject'];} else {$partAddressProject ='';}
if (!empty($_POST['partAddressOwner'])) {$partAddressOwner =$_POST['partAddressOwner'];} else {$partAddressOwner ='';}
if (!empty($_POST['partAddressType'])) {$partAddressType =$_POST['partAddressType'];} else {$partAddressType ='';}
if (!empty($_POST['partAddressCategory'])) {$partAddressCategory =$_POST['partAddressCategory'];} else {$partAddressCategory ='';}
if (!empty($_POST['partAddressFirstname'])) {$partAddressFirstname =$_POST['partAddressFirstname'];} else {$partAddressFirstname ='';}
if (!empty($_POST['partAddressLastname'])) {$partAddressLastname =$_POST['partAddressLastname'];} else {$partAddressLastname ='';}
if (!empty($_POST['partAddressEmail'])) {$partAddressEmail =$_POST['partAddressEmail'];} else {$partAddressEmail ='';}
if (!empty($_POST['partAddressEmailPrim'])) {$partAddressEmailPrim =$_POST['partAddressEmailPrim'];} else {$partAddressEmailPrim ='';}
if (!empty($_POST['partAddressEmailSec'])) {$partAddressEmailSec =$_POST['partAddressEmailSec'];} else {$partAddressEmailSec ='';}
if (!empty($_POST['partAddressTelecom'])) {$partAddressTelecom =$_POST['partAddressTelecom'];} else {$partAddressTelecom ='';}
if (!empty($_POST['partAddressTelBusiness'])) {$partAddressTelBusiness =$_POST['partAddressTelBusiness'];} else {$partAddressTelBusiness ='';}
if (!empty($_POST['partAddressTelPrivate'])) {$partAddressTelPrivate =$_POST['partAddressTelPrivate'];} else {$partAddressTelPrivate ='';}
if (!empty($_POST['partAddressTelMobile'])) {$partAddressTelMobile =$_POST['partAddressTelMobile'];} else {$partAddressTelMobile ='';}
if (!empty($_POST['partAddressFax'])) {$partAddressFax =$_POST['partAddressFax'];} else {$partAddressFax ='';}
if (!empty($_POST['partAddressPrivate'])) {$partAddressPrivate =$_POST['partAddressPrivate'];} else {$partAddressPrivate ='';}
if (!empty($_POST['partAddressPrivateTown'])) {$partAddressPrivateTown =$_POST['partAddressPrivateTown'];} else {$partAddressPrivateTown ='';}
if (!empty($_POST['partAddressPrivateZIP'])) {$partAddressPrivateZIP =$_POST['partAddressPrivateZIP'];} else {$partAddressPrivateZIP ='';}
if (!empty($_POST['partAddressPrivateCountry'])) {$partAddressPrivateCountry =$_POST['partAddressPrivateCountry'];} else {$partAddressPrivateCountry ='';}
if (!empty($_POST['partAddressBusiness'])) {$partAddressBusiness =$_POST['partAddressBusiness'];} else {$partAddressBusiness ='';}
if (!empty($_POST['partAddressBusinessTown'])) {$partAddressBusinessTown =$_POST['partAddressBusinessTown'];} else {$partAddressBusinessTown ='';}
if (!empty($_POST['partAddressBusinessZIP'])) {$partAddressBusinessZIP =$_POST['partAddressBusinessZIP'];} else {$partAddressBusinessZIP ='';}
if (!empty($_POST['partAddressBusinessCountry'])) {$partAddressBusinessCountry =$_POST['partAddressBusinessCountry'];} else {$partAddressBusinessCountry ='';}
if (!empty($_POST['partAddressSector'])) {$partAddressSector =$_POST['partAddressSector'];} else {$partAddressSector ='';}
if (!empty($_POST['partAddressOrganization'])) {$partAddressOrganization =$_POST['partAddressOrganization'];} else {$partAddressOrganization ='';}
if (!empty($_POST['partAddressDepartment'])) {$partAddressDepartment =$_POST['partAddressDepartment'];} else {$partAddressDepartment ='';}
if (!empty($_POST['partAddressPosition'])) {$partAddressPosition =$_POST['partAddressPosition'];} else {$partAddressPosition ='';}
if (!empty($_POST['partAddressRemarks'])) {$partAddressRemarks =$_POST['partAddressRemarks'];} else {$partAddressRemarks ='';}

 
?>
