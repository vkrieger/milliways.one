<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; background-color:<?php echo $SystemColor; ?>;}
	input                               {font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

events listing criteria<br>

<?php

include 'include_setEventconstants.php';
include 'include_events_postvariables.php';

if (!isset($_SESSION)) { session_start();}

	$listdatasets = "";
	
if ($listdatasets == "" AND $_SESSION['LoginType'])
	{		
    echo '<form method="post" enctype="multipart/form-data" action="events_list.php" target="main">';
	
	$partEventArchiveID ='0000-00-00 00:00:00'; // to firstly display only non-archived datasets when navigation is called
	
	echo '<table>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_EventGUID" value="yes" >GUID</td>';
	echo '<td><input type="text" name="partEventGUID" size="20" maxlength="40" value="'.$partEventGUID.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_EventCreateID" value="yes" checked>CreateID(Link)</td>';
	echo '<td><input type="text" name="partEventCreateID" size="20" maxlength="40" value="'.$partEventCreateID.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_EventArchiveID" value="yes" >ArchiveID</td>';
	echo '<td><input type="text" name="partEventArchiveID" size="20" maxlength="40" value="'.$partEventArchiveID.'"></td>';
	echo '</tr>';
	echo '</table>';
	
	echo '<table>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_EventOwner" value="yes" >Owner</td>';
	echo '<td><input type="text" name="partEventOwner" size="8" maxlength="40" value="'.$partEventOwner.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_EventProject" value="yes" checked>Project</td>';
	if (empty($SystemProject))
		{
		echo '<td><select type="text" name="partEventProject" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($SystemProjectArray as $Project) {echo '<option>'.$Project.'</option>';}
			echo '</select>';}
		else { echo '<input type="hidden" name="partEventProject" value="'.$SystemProject.'">';}
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_EventType" value="yes" checked>Type</td>';
	if ($SystemType == "public")
		{echo '<td><input style="background-color:#C0C0C0" type="text" name="partEventType" size="8" maxlength="40" value="public" readonly></td>';}
		else
		{echo '<td><input type="text" name="partEventType" size="8" maxlength="40" value="'.$partEventType.'"></td>';}
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_EventCategory" value="yes" checked>Category</td>';
	echo '<td><select type="text" name="partEventCategory" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($EventCategoryArray as $Category) {echo '<option>'.$Category.'</option>';}
			echo '</select>';
    echo '</td>';
	echo '</tr>';    
	
echo '</table>';
echo '<table>';
	
echo '<tr><td><input type="checkbox" name="list_EventBegin" value="yes" checked>Begin</td><td><input type="text" name="partEventBegin" size="8" maxlength="40" value="'.$partEventBegin.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_EventYear" value="yes" >Year</td><td><input type="text" name="partEventYear" size="8" maxlength="40" value="'.$partEventYear.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_EventDuration" value="yes" checked >Duration</td><td><input type="text" name="partEventDuration" size="8" maxlength="40" value="'.$partEventDuration.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_EventName" value="yes" checked>Name</td><td><input type="text" name="partEventName" size="8" maxlength="40" value="'.$partEventName.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_EventContent" value="yes" >Content</td><td><input type="text" name="partEventContent" size="8" maxlength="40" value="'.$partEventContent.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_EventLocation" value="yes" checked>Location</td><td><input type="text" name="partEventLocation" size="8" maxlength="40" value="'.$partEventLocation.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_EventAddress" value="yes" >Address</td><td><input type="text" name="partEventAddress" size="8" maxlength="40" value="'.$partEventAddress.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_EventCountry" value="yes" >Country</td><td><input type="text" name="partEventCountry" size="8" maxlength="40" value="'.$partEventCountry.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_EventLanguage" value="yes" >Language</td><td><input type="text" name="partEventLanguage" size="8" maxlength="40" value="'.$partEventLanguage.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_EventHost" value="yes" >Host</td><td><input type="text" name="partEventHost" size="8" maxlength="40" value="'.$partEventHost.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_EventSponsor" value="yes" >Sponsor</td><td><input type="text" name="partEventSponsor" size="8" maxlength="40" value="'.$partEventSponsor.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_EventContact" value="yes" >Contact</td><td><input type="text" name="partEventContact" size="8" maxlength="40" value="'.$partEventContact.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_EventRegistration" value="yes" >Registration</td><td><input type="text" name="partEventRegistration" size="8" maxlength="40" value="'.$partEventRegistration.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_EventFee" value="yes" >Fee</td><td><input type="text" name="partEventFee" size="8" maxlength="40" value="'.$partEventFee.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_EventWeb" value="yes" >Web</td><td><input type="text" name="partEventWeb" size="8" maxlength="40" value="'.$partEventWeb.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_EventStatus" value="yes" >Status</td><td><input type="text" name="partEventStatus" size="8" maxlength="40" value="'.$partEventStatus.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_EventFilename" value="yes" checked>Filename</td><td><input type="text" name="partEventFilename" size="8" maxlength="40" value="'.$partEventFilename.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_EventRemarks" value="yes" >Remarks</td><td><input type="text" name="partEventRemarks" size="8" maxlength="40" value="'.$partEventRemarks.'"></td></tr>';

	echo '</table>';

	?>
		
		<!-- sorting part -->
        <table>
		<tr><td>first sorted by</td>
			<td><select name="first_sorted_by" size="1">
				<option value="EventGUID" >GUID</option>
				<option value="EventCreateID" >CreateID</option>
				<option value="EventArchiveID" >ArchiveID</option>
				<option value="EventProject">Project</option>
				<option value="EventOwner" >Owner</option>
				<option value="EventType" >Type</option>
				<option value="EventCategory">Category</option>
				<option value="EventBegin" selected>Begin</option>
				<option value="EventYear" >Year</option>
				<option value="EventName" >Name</option>
				<option value="EventLocation" >Location</option>
				<option value="EventFee" >Fee</option>
				<option value="EventStatus" >Status</option>
				<option value="EventFilename" >Filename</option>
				<option value="EventRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
      	<tr><td>then sorted by</td>
			<td><select name="then_sorted_by" size="1">
				<option value="EventGUID" >GUID</option>
				<option value="EventCreateID" >CreateID</option>
				<option value="EventArchiveID" >ArchiveID</option>
				<option value="EventProject" >Project</option>
				<option value="EventOwner" >Owner</option>
				<option value="EventType" >Type</option>
				<option value="EventCategory" selected>Category</option>
				<option value="EventBegin" >Begin</option>
				<option value="EventYear" >Year</option>
				<option value="EventName" >Name</option>
				<option value="EventLocation" >Location</option>
				<option value="EventFee" >Fee</option>
				<option value="EventStatus" >Status</option>
				<option value="EventFilename" >Filename</option>
				<option value="EventRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	<tr><td>last sorted by</td>
			<td><select name="last_sorted_by" size="1">
				<option value="EventGUID" >GUID</option>
				<option value="EventCreateID" >CreateID</option>
				<option value="EventArchiveID" >ArchiveID</option>
				<option value="EventProject" >Project</option>
				<option value="EventOwner" selected>Owner</option>
				<option value="EventType" >Type</option>
				<option value="EventCategory" >Category</option>
				<option value="EventBegin" >Begin</option>
				<option value="EventYear" >Year</option>
				<option value="EventName" >Name</option>
				<option value="EventLocation" >Location</option>
				<option value="EventFee" >Fee</option>
				<option value="EventStatus" >Status</option>
				<option value="EventFilename" >Filename</option>
				<option value="EventRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	</table>

		<br>

		<table>
		<input type="submit" name="listdatasets" value="list datasets">
      	<input type="reset" value="reset values">
		</table>

    </form>
<?php
} elseif ( $listdatasets );

echo '<div align="right" style="font-size: 8px;">last source change vk 2020-10-30 18:00</div>';

?>
</body>
</html>