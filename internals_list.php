<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

You may download <a href="internalslist.xls">this Table</a> in Spreadsheet-Format (e.g. as CSV for MS-Excel) from Server.
<br><br>

<?php

include 'include_setInternalconstants.php';
include 'include_internals_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

// <form></form> must enclose all <input> to transfer $variables to next action app
echo '<form method="post" enctype="multipart/form-data" action="internals_save.php">';
	
// this is in case of admin login (i.e. all projects are selectable)
// if project is chosen in navigation is empty (i.e. all projects are selected) create dataset will have no project or public project set
// if project is chosen in navigation by $partAppProject this value must be transferred to app_save.php
if (!empty($partInternalProject)) {echo '<input type="hidden" name="InternalProject" value="'.$partInternalProject.'">';}

if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
	{
	echo '<input type="submit" name="createdataset" value="create dataset">';
	}

echo '<br><br>';

/*
select datasets according to criteria. 
*/
$LoginLogin = $_SESSION['LoginLogin'];

  	$dbquery = "SELECT * FROM internals WHERE

LOCATE('$partInternalGUID', InternalGUID)>0 AND
LOCATE('$partInternalCreateID', InternalCreateID)>0 AND
LOCATE('$partInternalArchiveID', InternalArchiveID)>0 AND
(LOCATE('$partInternalProject', InternalProject)>0  OR LOCATE('public', InternalType)>0) AND
LOCATE('$partInternalOwner', InternalOwner)>0 AND
LOCATE('$partInternalType', InternalType)>0 AND
LOCATE('$partInternalCategory', InternalCategory)>0 AND
LOCATE('$partInternalName', InternalName)>0 AND
LOCATE('$partInternalStatus', InternalStatus)>0 AND
LOCATE('$partInternalFilenames', InternalFilenames)>0 AND
LOCATE('$partInternalRemarks', InternalRemarks)>0 AND
LOCATE('$LoginLogin', InternalInterners) >0 

ORDER BY $first_sorted_by, $then_sorted_by, $last_sorted_by " ;
	
  	$dbresult = mysqli_query($link,$dbquery);  echo mysqli_error($link);
  	
  	$header =""; //empty header for excel file (dataset structure)
  	$data	=""; //empty $data variable for excel file (all rows in one variable)

echo '<table border="1" cellspacing="0">';
 
echo '<tr>';

if ($list_InternalGUID =='yes'){echo '<td>GUID</td>'; $header.="GUID". "\t";}
if ($list_InternalCreateID =='yes'){echo '<td>CreateID</td>'; $header.="CreateID". "\t";}
if ($list_InternalArchiveID =='yes'){echo '<td>ArchiveID</td>'; $header.="ArchiveID". "\t";}
if ($list_InternalProject =='yes'){echo '<td>Project</td>'; $header.="Project". "\t";}
if ($list_InternalOwner =='yes'){echo '<td>Owner</td>'; $header.="Owner". "\t";}
if ($list_InternalInterners =='yes'){echo '<td>Interners</td>'; $header.="Interners". "\t";}
if ($list_InternalType =='yes'){echo '<td>Type</td>'; $header.="Type". "\t";}
if ($list_InternalCategory =='yes'){echo '<td>Category</td>'; $header.="Category". "\t";}
if ($list_InternalName =='yes'){echo '<td>Name</td>'; $header.="Name". "\t";}
if ($list_InternalStatus =='yes'){echo '<td>Status</td>'; $header.="Status". "\t";}
if ($list_InternalFilenames =='yes'){echo '<td>Filenames[kB]</td>'; $header.="Filenames[kB]". "\t";}
if ($list_InternalRemarks =='yes'){echo '<td>Remarks</td>'; $header.="Remarks". "\t";}

	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)


/*
outputs datasets and fills $data variable
*/

$DatasetCount = 0;

// control code echo $SystemProject.'<br>';

while($dbrow = mysqli_fetch_array($dbresult))
   	{
	$DatasetCount += 1;
	
	// output to main window and selected data to $data excel variable
   	echo '<tr>';
	if ($list_InternalGUID =='yes'){echo'<td>'.$dbrow['InternalGUID'].'</td>'; $data.='"'.$dbrow['InternalGUID'].'"'."\t";}
	if ($list_InternalCreateID=='yes')
		{
		echo '<td class="bluelink"><a style="font-size:xx-small" href="internals_modify.php?InternalCreateID='.$dbrow['InternalCreateID'].'">'.$dbrow['InternalCreateID'].'</a></td>';
		$data .= '"' . $dbrow['InternalCreateID'] . '"' . "\t";
		}
	if ($list_InternalArchiveID =='yes'){echo'<td>'.$dbrow['InternalArchiveID'].'</td>'; $data.='"'.$dbrow['InternalArchiveID'].'"'."\t";}
	if ($list_InternalProject =='yes'){echo'<td>'.$dbrow['InternalProject'].'</td>'; $data.='"'.$dbrow['InternalProject'].'"'."\t";}
	if ($list_InternalOwner =='yes'){echo'<td>'.$dbrow['InternalOwner'].'</td>'; $data.='"'.$dbrow['InternalOwner'].'"'."\t";}
	if ($list_InternalInterners =='yes'){echo'<td>'.$dbrow['InternalInterners'].'</td>'; $data.='"'.$dbrow['InternalInterners'].'"'."\t";}
	if ($list_InternalType =='yes'){echo'<td>'.$dbrow['InternalType'].'</td>'; $data.='"'.$dbrow['InternalType'].'"'."\t";}
	if ($list_InternalCategory =='yes'){echo'<td>'.$dbrow['InternalCategory'].'</td>'; $data.='"'.$dbrow['InternalCategory'].'"'."\t";}
	if ($list_InternalName =='yes'){echo'<td>'.$dbrow['InternalName'].'</td>'; $data.='"'.$dbrow['InternalName'].'"'."\t";}
	if ($list_InternalStatus =='yes'){echo'<td>'.$dbrow['InternalStatus'].'</td>'; $data.='"'.$dbrow['InternalStatus'].'"'."\t";}
	
	if($list_InternalFilenames=='yes')		
		{
		// only if Filenames exist
		if (!empty($dbrow['InternalFilenames']))
			{
			// makes FilenameArray from String from $dbrow['InternalFilenames']
			$FilenameArray = explode("\n",$dbrow['InternalFilenames']);
			$FilesizeArray = explode("\n",$dbrow['InternalFilesizes']);
			// must be dataftp instead of data/ftp and make use of Alias in http.conf !!!
			$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['InternalProject'].'/';
			echo '<td class="bluelink">';
			foreach ($FilenameArray as $key => $Filename) 
				{
				// allows external URL to be included - URL must be changed directly and manually in dataset
				if (strpos($Filename,'http'))
					// external URL - modify correctly in database directly
					{echo '<a href="'.$Filename.'" target="_blank">'.$Filename.'</a>';}
					else
					// normal Link  - no external URL
					{echo '<a href="'.$ProjectFilePath.$Filename.'">'.$Filename.'</a> ['.$FilesizeArray[$key].'kB] ';}
				}
			echo '</td>';	
			}
		$data.='"'.$dbrow['InternalFilenames'].'['.$dbrow['InternalFilesizes'].'kB]'.'"'."\t"; // not adapted !!!
		}
	
	if ($list_InternalRemarks =='yes'){echo'<td>'.$dbrow['InternalRemarks'].'</td>'; $data.='"'.$dbrow['InternalRemarks'].'"'."\t";}
	
	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)
	}
echo '<tr>';
echo '<td>'.$DatasetCount.' Datasets</td>';
echo '</tr>';	
echo '</table>';

echo '</form>';
	
/*
export selected table to .xls onto local server folder to download as offered from there
*/

$fp = fopen('internalslist.xls','w');
fwrite($fp,$header);
fwrite($fp,"\n");
fwrite($fp,$data);
fclose($fp);

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2020-09-03 00:00</div>';4

?>
</body>
</html>
