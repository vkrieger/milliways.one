<!DOCtype html public "-//W3C//DTD HTML 4.0 //EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>
<?php

include 'include_setPendingconstants.php';
include 'include_pendings_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

if (!empty($_POST["historydataset"])) 
	{
	$_POST['partPendingGUID']=$PendingGUID;	
	$_POST['partPendingArchiveID']='';
	$_POST['list_PendingGUID']='yes';	
	$_POST['list_PendingCreateID']='yes';	
	$_POST['list_PendingArchiveID']='yes';	
	$_POST['list_PendingOwner']='yes';	
	$_POST['list_PendingName']='yes';	
	$_POST['list_PendingStatus']='yes';	
	include 'pendings_list.php';
	}

$CurrentTimeStamp = date("Y-m-d H:i:s");

if (!empty($_POST["updatedataset"]) OR !empty($_POST["deletedataset"]))
	{	
	$dbchange = "UPDATE pendings SET PendingArchiveID = '$CurrentTimeStamp' WHERE PendingCreateID = '$PendingCreateID'";
	$dbquery = mysqli_query($link,$dbchange) or die ("not updated!");
	}

if (!empty($_POST["updatedataset"]) OR !empty($_POST["createdataset"]))	
	{
	if (!empty($_POST["createdataset"])) {$PendingGUID = $CurrentTimeStamp;}
	$PendingCreateID = $CurrentTimeStamp;
	$PendingArchiveID='0000-00-00 00:00:00';
	
	$ProjectFilePath='/data/ftp/'.$dataftpfolder.'/'.$PendingProject.'/'; 
	$ProjectInitialFilePath='/data/ftp/'.$dataftpfolder.'/'.$PendingInitialProject.'/';
		
	if (!empty($_FILES['PendingFilename']['tmp_name']))
        {if ( move_uploaded_file($_FILES['PendingFilename']['tmp_name'], $ProjectFilePath.basename ($_FILES['PendingFilename']['name'])))
			{ echo $PendingFilename = basename ($_FILES['PendingFilename']['name']); echo ' uploaded!<br>'; $MediaPDFsize = round($_FILES['PendingFilename']['size']/1000);}
			else { echo ' error uploading!<br>'; }
		} else {if (!$existor){$PendingFilename=''; $PendingFilesize='';}} 

	if ((!empty ($PendingFilename)) AND empty($_FILES['PendingFilename']['tmp_name'])) 
    	{ 	
		$PendingFilenameExist = $PendingFilename;
		copy ($ProjectInitialFilePath.$PendingFilenameExist,$ProjectFilePath.$PendingFilename);
		}	
		
	$dbchange = "INSERT INTO pendings SET

		PendingGUID = '$PendingGUID',
		PendingCreateID = '$PendingCreateID',
		PendingArchiveID = '$PendingArchiveID',
		PendingProject = '$PendingProject',
		PendingOwner = '$PendingOwner',
		PendingType = '$PendingType',
		PendingCategory = '$PendingCategory',
		PendingName = '$PendingName',
		PendingStatus = '$PendingStatus',
		PendingRaiser = '$PendingRaiser',
		PendingContent = '$PendingContent',
		PendingTask = '$PendingTask',
		PendingResponsible = '$PendingResponsible',
		PendingProcess = '$PendingProcess',
		PendingDeliverable = '$PendingDeliverable',
		PendingInterim = '$PendingInterim',
		PendingResult = '$PendingResult',
		PendingContract = '$PendingContract',
		PendingFilename = '$PendingFilename',
		PendingFilesize = '$PendingFilesize',
		PendingRemarks = '$PendingRemarks'
		";
	$dbquery = mysqli_query($link,$dbchange) or die ("not created!");
	}
	
$_GET['PendingCreateID']=$PendingCreateID;	
include 'pendings_modify.php';

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2019-07-11 18:00</div>';

?>
</body>
</html>

