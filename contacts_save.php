<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

<?php
// new error handling 
error_reporting(E_ALL);
ini_set("display_errors", 1);


include 'include_setContactconstants.php';
include 'include_contacts_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

if (!empty($_POST["historydataset"])) 
	{
	$_POST['partContactGUID']=$ContactGUID;	
	$_POST['partContactArchiveID']='';
	$_POST['list_ContactGUID']='yes';	
	$_POST['list_ContactCreateID']='yes';	
	$_POST['list_ContactArchiveID']='yes';	
	$_POST['list_ContactOwner']='yes';	
	$_POST['list_ContactName']='yes';	
	$_POST['list_ContactStatus']='yes';	
	include 'contacts_list.php';
	}


$CurrentTimeStamp = date("Y-m-d H:i:s");

// deletedataset: puts current dataset in archive status by entering current datetime into ArchiveID
// updatedataset: puts current dataset in archive status by entering current datetime into ArchiveID and then createdataset

if (!empty($_POST["updatedataset"]) OR !empty($_POST["deletedataset"]))
	{
	// control codeecho
	//echo $ContactGUID.'<br>';
	//echo $ContactCreateID.'<br>';
	//echo $ContactArchiveID.'<br>';
	//echo $CurrentTimeStamp.'<br>';
	
	$dbchange = "UPDATE contacts SET ContactArchiveID = '$CurrentTimeStamp' WHERE ContactCreateID = '$ContactCreateID'";
	$dbquery = mysqli_query($link,$dbchange) or die ("not updated!");
	}

// updatedataset: creates new dataset with same GUID and with current datetime in CreateID
// createdataset: creates new dataset with new GUID and with current datetime in CreateID, leaving the current untouched  

if (!empty($_POST["updatedataset"]) OR !empty($_POST["createdataset"]))
	{
	if (!empty($_POST["createdataset"])) {$ContactGUID = $CurrentTimeStamp;}
		$ContactCreateID = $CurrentTimeStamp;
		$ContactArchiveID='0000-00-00 00:00:00';
	
	// control code
	// echo $ContactGUID.'<br/>';
	// echo $ContactCreateID.'<br />';
	// echo $ContactArchiveID.'<br />';
	// echo $CurrentTimeStamp.'<br />';

	// use /data/ftp/ instead of /dataftp/ with move_uploaded_file
	// InitialProject origins in modify app
	$ProjectFilePath = '/data/ftp/'.$dataftpfolder.'/'.$ContactProject.'/'; 
	$ProjectInitialFilePath = '/data/ftp/'.$dataftpfolder.'/'.$ContactInitialProject.'/';
	
	// control code
	// echo $_FILES['ContactPic']['tmp_name'].'<br />';
	// echo $_FILES['ContactPic']['name'].'<br />';
	if (!empty($_FILES['ContactPic']['tmp_name']))
		// file with filename exists to be uploaded
        {if (move_uploaded_file($_FILES['ContactPic']['tmp_name'], $ProjectFilePath.basename($_FILES['ContactPic']['name'])))
			{ echo $ContactPic = basename($_FILES['ContactPic']['name']); echo ' uploaded!<br />';} // upload was successful
			else 
			{ echo ' error uploading!<br />';} // no upload by move_uploaded_file
		} 
	
	// in case of dataset update $ContactPic from input from modify and therefore $_FILES['ContactPic']['tmp_name'] may be empty
	// in case of project change this ensures the new location of $ContactPic
	// $ProjectInitialFilepath comes from InitialProject which comes from modify app at project attribute  
	if ((!empty ($ContactPic)) AND empty($_FILES['ContactPic']['tmp_name']))
    	{ 	
		$ContactPicExist = $ContactPic;
		copy ($ProjectInitialFilePath.$ContactPicExist,$ProjectFilePath.$ContactPic);
		}	
	
	$dbchange = "INSERT INTO contacts SET

		ContactGUID = '$ContactGUID',
		ContactCreateID = '$ContactCreateID',
		ContactArchiveID = '$ContactArchiveID',
		ContactProject = '$ContactProject',
		ContactOwner = '$ContactOwner',
		ContactCategory = '$ContactCategory',
		ContactFunction = '$ContactFunction',
		ContactType = '$ContactType',
		ContactFirstname = '$ContactFirstname',
		ContactLastname = '$ContactLastname',
		ContactEmailPrim = '$ContactEmailPrim',
		ContactEmailSec = '$ContactEmailSec',
		ContactWeb = '$ContactWeb',
		ContactTelBusiness = '$ContactTelBusiness',
		ContactTelPrivate = '$ContactTelPrivate',
		ContactTelMobile = '$ContactTelMobile',
		ContactFax = '$ContactFax',
		ContactPrivate = '$ContactPrivate',
		ContactPrivateTown = '$ContactPrivateTown',
		ContactPrivateZIP = '$ContactPrivateZIP',
		ContactPrivateCountry = '$ContactPrivateCountry',
		ContactBusiness = '$ContactBusiness',
		ContactBusinessTown = '$ContactBusinessTown',
		ContactBusinessZIP = '$ContactBusinessZIP',
		ContactBusinessCountry = '$ContactBusinessCountry',
		ContactSector = '$ContactSector',
		ContactOrganization = '$ContactOrganization',
		ContactDepartment = '$ContactDepartment',
		ContactPosition = '$ContactPosition',
		ContactRemarks = '$ContactRemarks',
		ContactCV = '$ContactCV',
		ContactPic = '$ContactPic'
		";
	$dbquery = mysqli_query($link,$dbchange) or die ("not created! ".mysqli_error($link));
	}
	// mysqli_error shows errors in combination with error reporting at beginning of this code
	// if (mysqli_connect_errno()){echo "Failed to connect to MySQL: " . mysqli_connect_error();}

$_GET['ContactCreateID']=$ContactCreateID;	
include 'contacts_modify.php';

?>
</body>
</html>

