<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	* 				{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                         {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea		{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 			{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

!!! Do <b>NOT</b> use semicolon (;) in input fields !!! <b>NO</b> special characters and blanks in filenames - use underscore ( _ ) instead!!! 
<br><br>

<?php
		
include 'include_setNapkinconstants.php';
include 'include_napkins_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

$NapkinCreateID = $_GET["NapkinCreateID"];

$dbquery = " SELECT * FROM napkins WHERE LOCATE ('$NapkinCreateID', NapkinCreateID) >0 ";
$dbresult = mysqli_query($link,$dbquery);
$dbrow = mysqli_fetch_array($dbresult);

	$updatedataset="";
	$createdataset="";
	$deletedataset="";
	$historydataset="";
	
if ($updatedataset == "" AND $createdataset == "" AND $deletedataset == "" AND $historydataset =="")
	{
	echo '<form method="post" action="napkins_save.php" enctype="multipart/form-data" >';
	if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
		{
		echo '<input type="submit" name="createdataset" value="create dataset">';
		echo '<input type="submit" name="updatedataset" value="update dataset">';
    	echo '<input type="submit" name="deletedataset" value="delete dataset">';
      	echo '<input type="reset" value="reset values">';
      	echo '<br><br>';
	    }

	echo '<table>';
	echo '<tr>';
	echo '<td align="right">GUID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="NapkinGUID" size="60" maxlength="100" value="'.$dbrow['NapkinGUID'].'" readonly></td>';
	echo '<td><input type="submit" name="historydataset" value="history of datasets"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">CreateID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="NapkinCreateID" size="60" maxlength="100" value="'.$dbrow['NapkinCreateID'].'" readonly></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">ArchiveID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="NapkinArchiveID" size="60" maxlength="100" value="'.$dbrow['NapkinArchiveID'].'" readonly></td>';
	echo '</tr>';
	echo '<input type="hidden" name="NapkinInitialProject" value="'.$dbrow['NapkinProject'].'">';
	echo '<input type="hidden" name="NapkinProject" value="'.$dbrow['NapkinProject'].'">';
	echo '<tr>';
	echo '<td align="right">Project</td>';
	echo '<td><select name="NapkinProject" size="1">';
		if (!empty($SystemProject))
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($SystemProject==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		else
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($dbrow['NapkinProject']==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		echo '</select></td>';		
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Owner (readonly)</td>';
	echo '<td>'.'['.$dbrow['NapkinOwner'].'] - create/update transfers to '.'<input style="background-color:#C0C0C0" type="text" name="NapkinOwner" size="15" maxlength="100" value="'.$_SESSION['LoginLogin'].'" readonly></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Type</td>';
	echo '<td><select name="NapkinType" size="1">';
			foreach ($SystemTypeArray as $Type) {echo '<option'; if ($dbrow['NapkinType']==$Type) {echo ' selected';} echo '>'.$Type.'</option>';} 
			echo '</select></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Category</td>';
	echo '<td><select name="NapkinCategory" size="1">';
			foreach ($NapkinCategoryArray as $Category) {echo '<option'; if ($dbrow['NapkinCategory']==$Category) {echo ' selected';} echo '>'.$Category.'</option>';} 
			echo '</select></td>';
	echo '</tr>';

echo '<tr><td align="right">Name </td><td><input type="text" name="NapkinName" size="60" maxlength="255" value="'.$dbrow['NapkinName'].'"></td></tr>';
echo '<tr><td align="right">Status </td><td><input type="text" name="NapkinStatus" size="60" maxlength="255" value="'.$dbrow['NapkinStatus'].'"></td></tr>';

// must be dataftp instead of data/ftp and make use of Alias in http.conf !!!
$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['NapkinProject'].'/';

// take apart NapkinFilenames into single Links
// only if NapkinFilenames exists
if (!empty($dbrow['NapkinFilenames']))
	{
	// makes FilenameArray from String from $dbrow['NapkinFilenames']
	$FilenameArray = explode("\n",$dbrow['NapkinFilenames']);
	$FilesizeArray = explode("\n",$dbrow['NapkinFilesizes']);
	
	// control code
	// print_r($FilenameArray); 
	// echo '<br>';
	
	// clean FilenameArray from empty fields
	foreach ($FilenameArray as $key => $Filename)	
		{
		// control code
		// echo 'key:'.$key.'<br>';
		// echo 'Filename:'.$Filename.'<br>';
			
		// checks if filename exists by finding the delimiter '.'
		if (!strpos($Filename,'.'))
			{
			// control code
			// echo 'unsetted key:'.$key.'<br>';
			// echo 'unsetted Filename:'.$Filename.'<br>';
			
			unset ($FilenameArray[$key]);
			$FilenameArray = array_values ($FilenameArray);
			unset ($FilesizeArray[$key]);
			$FilesizeArray = array_values ($FilesizeArray);
			}
		}

	// control code
	// print_r($FilenameArray); 
	// echo '<br>';
	
/*
- ExistorArray[key] has value "Filename" when Filename when checkbox is not unchecked
- $_FILES['NapkinFilename']['tmp_name'] is given when new Filename is uploaded 
- when ExistorArray[key] is unchecked NapkinFilenames should be emptied from Filename in app_save.php
*/
	
	// lists Filenames from FilenameArray as Links
	foreach ($FilenameArray as $key => $Filename)
		{
		echo '<tr>';
		echo '<td align="right" >';
		echo '<input type="checkbox" name="ExistorArray[]" value="'.$Filename.' ['.$FilesizeArray[$key].'kB]" checked>';
		echo '</td>';
		echo '<td class="bluelink">';
		echo '<a href="'.$ProjectFilePath.$Filename.'">'.$Filename.' ['.$FilesizeArray[$key].'kB]</a>';
		echo '</td>';
		echo '</tr>';
		}
		
	// implode FilenameArray to NapkinFilenames 
	$NapkinFilenames = implode ("\n",$FilenameArray);
	$NapkinFilesizes = implode ("\n",$FilesizeArray);
		
	// makes sure that NapkinFilenames is available in app_save.php
	echo '<input type="hidden" name="NapkinFilenames" value="'.$NapkinFilenames.'">';
	echo '<input type="hidden" name="NapkinFilesizes" value="'.$NapkinFilesizes.'">';

	}

echo '<tr>';
echo '<td>';
echo '</td>';
echo '<td>';
echo ' add File (NO blanks in Filename!!!) ';
echo '<input type="file" name="Filename" size="15">';
echo ' ';
echo '<input type="submit" name="updatedataset" value="update">';
echo '</td>';
echo '</tr>';


echo '<tr><td align="right">Remarks </td><td><input type="text" name="NapkinRemarks" size="60" maxlength="255" value="'.$dbrow['NapkinRemarks'].'"></td></tr>';
	
	echo '</table>';

echo '</form>';

} elseif ( $updatedataset OR $createdataset OR $deletedataset OR $historydataset)

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2019-07-11 18:00</div>';

?>
</font>
</body>
</html>
