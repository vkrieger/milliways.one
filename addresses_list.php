<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

You may download <a href="addresseslist.xls">this Table</a> in Spreadsheet-Format (e.g. as CSV for MS-Excel) from Server.
<br><br>

<?php

include 'include_setAddressconstants.php';
include 'include_addresses_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

// <form></form> must enclose all <input> to transfer to next app
echo '<form method="post" enctype="multipart/form-data" action="addresses_save.php">';

if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
	{
	echo '<input type="submit" name="createdataset" value="create dataset">';
	}

echo '<br /><br />';

// control code
// echo 'This is control code for AddressProject '.$AddressProject.'<br>';
// echo 'This is control code for partAddressProject '.$partAddressProject.'<br>';
// echo 'This is control code for $_POST '.$_POST['partAddressProject'].'<br>';
// echo 'This is control code for SystemProject '.$SystemProject.'<br>';


/*
select datasets according to criteria. fone and mail criteria are bundled
*/

  	$dbquery = "SELECT * FROM addresses WHERE

LOCATE('$partAddressGUID', AddressGUID)>0 AND
LOCATE('$partAddressCreateID', AddressCreateID)>0 AND
LOCATE('$partAddressArchiveID', AddressArchiveID)>0 AND
(LOCATE('$partAddressProject', AddressProject)>0  OR LOCATE('public', AddressType)>0) AND 
LOCATE('$partAddressOwner', AddressOwner)>0 AND
LOCATE('$partAddressType', AddressType)>0 AND
LOCATE('$partAddressCategory', AddressCategory)>0 AND
LOCATE('$partAddressFirstname', AddressFirstname)>0 AND
LOCATE('$partAddressLastname', AddressLastname)>0 AND
LOCATE('$partAddressEmail', AddressEmailPrim)>0 AND
LOCATE('$partAddressEmail', AddressEmailSec)>0 AND
LOCATE('$partAddressTelecom', AddressTelBusiness)>0 AND
LOCATE('$partAddressTelecom', AddressTelPrivate)>0 AND
LOCATE('$partAddressTelecom', AddressTelMobile)>0 AND
LOCATE('$partAddressTelecom', AddressFax)>0 AND
LOCATE('$partAddressPrivate', AddressPrivate)>0 AND
LOCATE('$partAddressPrivate', AddressPrivateTown)>0 AND
LOCATE('$partAddressPrivate', AddressPrivateZIP)>0 AND
LOCATE('$partAddressPrivate', AddressPrivateCountry)>0 AND
LOCATE('$partAddressBusiness', AddressBusiness)>0 AND
LOCATE('$partAddressBusiness', AddressBusinessTown)>0 AND
LOCATE('$partAddressBusiness', AddressBusinessZIP)>0 AND
LOCATE('$partAddressBusiness', AddressBusinessCountry)>0 AND
LOCATE('$partAddressSector', AddressSector)>0 AND
LOCATE('$partAddressOrganization', AddressOrganization)>0 AND
LOCATE('$partAddressDepartment', AddressDepartment)>0 AND
LOCATE('$partAddressPosition', AddressPosition)>0 AND
LOCATE('$partAddressRemarks', AddressRemarks)>0

ORDER BY $first_sorted_by, $then_sorted_by, $last_sorted_by " ;
	
  	$dbresult = mysqli_query($link,$dbquery);  echo mysqli_error($link);
  	
  	$header =""; //empty header for excel file (dataset structure)
  	$data	=""; //empty $data variable for excel file (all rows in one variable)

echo '<table border="1" cellspacing="0">';
 
echo '<tr>';

if ($list_AddressGUID =='yes'){echo '<td>GUID</td>'; $header.="GUID". "\t";}
if ($list_AddressCreateID =='yes'){echo '<td>CreateID</td>'; $header.="CreateID". "\t";}
if ($list_AddressArchiveID =='yes'){echo '<td>ArchiveID</td>'; $header.="ArchiveID". "\t";}
if ($list_AddressProject =='yes'){echo '<td>Project</td>'; $header.="Project". "\t";}
if ($list_AddressOwner =='yes'){echo '<td>Owner</td>'; $header.="Owner". "\t";}
if ($list_AddressType =='yes'){echo '<td>Type</td>'; $header.="Type". "\t";}
if ($list_AddressCategory =='yes'){echo '<td>Category</td>'; $header.="Category". "\t";}
if ($list_AddressFirstname =='yes'){echo '<td>Firstname</td>'; $header.="Firstname". "\t";}
if ($list_AddressLastname =='yes'){echo '<td>Lastname</td>'; $header.="Lastname". "\t";}
if ($list_AddressEmail =='yes'){echo '<td>EmailPrim</td>'; $header.="EmailPrim". "\t";}
if ($list_AddressEmail =='yes'){echo '<td>EmailSec</td>'; $header.="EmailSec". "\t";}
if ($list_AddressTelecom =='yes'){echo '<td>TelBusiness</td>'; $header.="TelBusiness". "\t";}
if ($list_AddressTelecom =='yes'){echo '<td>TelPrivate</td>'; $header.="TelPrivate". "\t";}
if ($list_AddressTelecom =='yes'){echo '<td>TelMobile</td>'; $header.="TelMobile". "\t";}
if ($list_AddressTelecom =='yes'){echo '<td>Fax</td>'; $header.="Fax". "\t";}
if ($list_AddressPrivate =='yes'){echo '<td>Private</td>'; $header.="Private". "\t";}
if ($list_AddressPrivate =='yes'){echo '<td>PrivateTown</td>'; $header.="PrivateTown". "\t";}
if ($list_AddressPrivate =='yes'){echo '<td>PrivateZIP</td>'; $header.="PrivateZIP". "\t";}
if ($list_AddressPrivate =='yes'){echo '<td>PrivateCountry</td>'; $header.="PrivateCountry". "\t";}
if ($list_AddressBusiness =='yes'){echo '<td>Business</td>'; $header.="Business". "\t";}
if ($list_AddressBusiness =='yes'){echo '<td>BusinessTown</td>'; $header.="BusinessTown". "\t";}
if ($list_AddressBusiness =='yes'){echo '<td>BusinessZIP</td>'; $header.="BusinessZIP". "\t";}
if ($list_AddressBusiness =='yes'){echo '<td>BusinessCountry</td>'; $header.="BusinessCountry". "\t";}
if ($list_AddressSector =='yes'){echo '<td>Sector</td>'; $header.="Sector". "\t";}
if ($list_AddressOrganization =='yes'){echo '<td>Organization</td>'; $header.="Organization". "\t";}
if ($list_AddressDepartment =='yes'){echo '<td>Department</td>'; $header.="Department". "\t";}
if ($list_AddressPosition =='yes'){echo '<td>Position</td>'; $header.="Position". "\t";}
if ($list_AddressRemarks =='yes'){echo '<td>Remarks</td>'; $header.="Remarks". "\t";}

	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)


/*
outputs datasets and fills $data variable
*/

$DatasetCount = 0;

  	while($dbrow = mysqli_fetch_array($dbresult))
   	{
	$DatasetCount += 1;
	
	// output to main window and selected data to $data excel variable
   	echo '<tr>';
if ($list_AddressGUID =='yes'){echo'<td>'.$dbrow['AddressGUID'].'</td>'; $data.='"'.$dbrow['AddressGUID'].'"'."\t";}
if ($list_AddressCreateID=='yes')
	{
	echo '<td class="bluelink"><a style="font-size:xx-small" href="addresses_modify.php?AddressCreateID='.$dbrow['AddressCreateID'].'">'.$dbrow['AddressCreateID'].'</a></td>';
	$data .= '"' . $dbrow['AddressCreateID'] . '"' . "\t";
	}
if ($list_AddressArchiveID =='yes'){echo'<td>'.$dbrow['AddressArchiveID'].'</td>'; $data.='"'.$dbrow['AddressArchiveID'].'"'."\t";}
if ($list_AddressProject =='yes'){echo'<td>'.$dbrow['AddressProject'].'</td>'; $data.='"'.$dbrow['AddressProject'].'"'."\t";}
if ($list_AddressOwner =='yes'){echo'<td>'.$dbrow['AddressOwner'].'</td>'; $data.='"'.$dbrow['AddressOwner'].'"'."\t";}
if ($list_AddressType =='yes'){echo'<td>'.$dbrow['AddressType'].'</td>'; $data.='"'.$dbrow['AddressType'].'"'."\t";}
if ($list_AddressCategory =='yes'){echo'<td>'.$dbrow['AddressCategory'].'</td>'; $data.='"'.$dbrow['AddressCategory'].'"'."\t";}
if ($list_AddressFirstname =='yes'){echo'<td>'.$dbrow['AddressFirstname'].'</td>'; $data.='"'.$dbrow['AddressFirstname'].'"'."\t";}
if ($list_AddressLastname =='yes'){echo'<td>'.$dbrow['AddressLastname'].'</td>'; $data.='"'.$dbrow['AddressLastname'].'"'."\t";}
if ($list_AddressEmail =='yes'){echo'<td>'.$dbrow['AddressEmailPrim'].'</td>'; $data.='"'.$dbrow['AddressEmailPrim'].'"'."\t";}
if ($list_AddressEmail =='yes'){echo'<td>'.$dbrow['AddressEmailSec'].'</td>'; $data.='"'.$dbrow['AddressEmailSec'].'"'."\t";}
if ($list_AddressTelecom =='yes'){echo'<td>'.$dbrow['AddressTelBusiness'].'</td>'; $data.='"'.$dbrow['AddressTelBusiness'].'"'."\t";}
if ($list_AddressTelecom =='yes'){echo'<td>'.$dbrow['AddressTelPrivate'].'</td>'; $data.='"'.$dbrow['AddressTelPrivate'].'"'."\t";}
if ($list_AddressTelecom =='yes'){echo'<td>'.$dbrow['AddressTelMobile'].'</td>'; $data.='"'.$dbrow['AddressTelMobile'].'"'."\t";}
if ($list_AddressTelecom =='yes'){echo'<td>'.$dbrow['AddressFax'].'</td>'; $data.='"'.$dbrow['AddressFax'].'"'."\t";}
if ($list_AddressPrivate =='yes'){echo'<td>'.$dbrow['AddressPrivate'].'</td>'; $data.='"'.$dbrow['AddressPrivate'].'"'."\t";}
if ($list_AddressPrivate =='yes'){echo'<td>'.$dbrow['AddressPrivateTown'].'</td>'; $data.='"'.$dbrow['AddressPrivateTown'].'"'."\t";}
if ($list_AddressPrivate =='yes'){echo'<td>'.$dbrow['AddressPrivateZIP'].'</td>'; $data.='"'.$dbrow['AddressPrivateZIP'].'"'."\t";}
if ($list_AddressPrivate =='yes'){echo'<td>'.$dbrow['AddressPrivateCountry'].'</td>'; $data.='"'.$dbrow['AddressPrivateCountry'].'"'."\t";}
if ($list_AddressBusiness =='yes'){echo'<td>'.$dbrow['AddressBusiness'].'</td>'; $data.='"'.$dbrow['AddressBusiness'].'"'."\t";}
if ($list_AddressBusiness =='yes'){echo'<td>'.$dbrow['AddressBusinessTown'].'</td>'; $data.='"'.$dbrow['AddressBusinessTown'].'"'."\t";}
if ($list_AddressBusiness =='yes'){echo'<td>'.$dbrow['AddressBusinessZIP'].'</td>'; $data.='"'.$dbrow['AddressBusinessZIP'].'"'."\t";}
if ($list_AddressBusiness =='yes'){echo'<td>'.$dbrow['AddressBusinessCountry'].'</td>'; $data.='"'.$dbrow['AddressBusinessCountry'].'"'."\t";}
if ($list_AddressSector =='yes'){echo'<td>'.$dbrow['AddressSector'].'</td>'; $data.='"'.$dbrow['AddressSector'].'"'."\t";}
if ($list_AddressOrganization =='yes'){echo'<td>'.$dbrow['AddressOrganization'].'</td>'; $data.='"'.$dbrow['AddressOrganization'].'"'."\t";}
if ($list_AddressDepartment =='yes'){echo'<td>'.$dbrow['AddressDepartment'].'</td>'; $data.='"'.$dbrow['AddressDepartment'].'"'."\t";}
if ($list_AddressPosition =='yes'){echo'<td>'.$dbrow['AddressPosition'].'</td>'; $data.='"'.$dbrow['AddressPosition'].'"'."\t";}
if ($list_AddressRemarks =='yes'){echo'<td>'.$dbrow['AddressRemarks'].'</td>'; $data.='"'.$dbrow['AddressRemarks'].'"'."\t";}
	
	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)
	}
echo '<tr>';
echo '<td>'.$DatasetCount.' Datasets</td>';
echo '</tr>';	
echo '</table>';

echo '</form>';
/*
export selected table to .xls onto local server folder to download as offered from there
*/

$fp = fopen('addresseslist.xls','w');
fwrite($fp,$header);
fwrite($fp,"\n");
fwrite($fp,$data);
fclose($fp);

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-03 18:00</div>';

?>
</body>
</html>
