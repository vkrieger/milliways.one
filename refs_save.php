<!DOCTYPE html public "-//W3C//DTD HTML 4.0 //EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>
<?php


include 'include_setReferenceconstants.php';
include 'include_refs_postvariables.php';
include 'include_dbconnect.php';

// control code
// echo "ReferencePDF:";
// print_r($ReferencePDF);
// echo '<br>';
// echo "ReferenceCategory:";
// echo $ReferenceCategory.' - '.$_POST['list_ReferenceCategory'].' - '.$_POST['partReferenceCategory'];
// echo '<br>';

if (!isset($_SESSION)) { session_start();}

if (!empty($_POST["historydataset"])) 
	{
	$_POST['partReferenceGUID']=$ReferenceGUID;	
	$_POST['partReferenceArchiveID']='';
	$_POST['list_ReferenceGUID']='yes';	
	$_POST['list_ReferenceCreateID']='yes';	
	$_POST['list_ReferenceArchiveID']='yes';	
	$_POST['list_ReferenceOwner']='yes';	
	$_POST['list_ReferenceName']='yes';	
	$_POST['list_ReferenceStatus']='yes';	
	include 'refs_list.php';
	}

$CurrentTimeStamp = date("Y-m-d H:i:s");

// deletedataset: puts current dataset in archive status by entering current datetime into ArchiveID
// updatedataset: puts current dataset in archive status by entering current datetime into ArchiveID and then createdataset

if (!empty($_POST["updatedataset"]) OR !empty($_POST["deletedataset"]))
	{	
	$dbchange = "UPDATE refs SET ReferenceArchiveID = '$CurrentTimeStamp' WHERE ReferenceCreateID = '$ReferenceCreateID'";
	$dbquery = mysqli_query($link,$dbchange) or die ("not updated!");
	}
// updatedataset: creates new dataset with same GUID and with current datetime in CreateID
// createdataset: creates new dataset with new GUID and with current datetime in CreateID, leaving the current untouched  

if (!empty($_POST["updatedataset"]) OR !empty($_POST["createdataset"]))
	
	{
	if (!empty($_POST["createdataset"])) {$ReferenceGUID = $CurrentTimeStamp;}
	$ReferenceCreateID = $CurrentTimeStamp;
	$ReferenceArchiveID='0000-00-00 00:00:00';
	
	$ProjectFilePath='/data/ftp/'.$dataftpfolder.'/'.$ReferenceProject.'/'; 
	$ProjectInitialFilePath='/data/ftp/'.$dataftpfolder.'/'.$ReferenceInitialProject.'/';
		
	if (!empty($_FILES['ReferenceFilename']['tmp_name']))
        {if ( move_uploaded_file($_FILES['ReferenceFilename']['tmp_name'], $ProjectFilePath.basename ($_FILES['ReferenceFilename']['name'])))
			{ echo $ReferenceFilename = basename ($_FILES['ReferenceFilename']['name']); echo ' uploaded!<br>'; $MediaPDFsize = round($_FILES['ReferenceFilename']['size']/1000);}
			else { echo ' error uploading!<br>'; }
		} else {if (!$existor){$ReferenceFilename=''; $ReferenceFilesize='';}} 

	if ((!empty ($ReferenceFilename)) AND empty($_FILES['ReferenceFilename']['tmp_name'])) 
    	{ 	
		$ReferenceFilenameExist = $ReferenceFilename;
		copy ($ProjectInitialFilePath.$ReferenceFilenameExist,$ProjectFilePath.$ReferenceFilename);
		}	

	$dbchange = "INSERT INTO refs SET

		ReferenceGUID = '$ReferenceGUID',
		ReferenceCreateID = '$ReferenceCreateID',
		ReferenceArchiveID = '$ReferenceArchiveID',
		ReferenceProject = '$ReferenceProject',
		ReferenceOwner = '$ReferenceOwner',
		ReferenceType = '$ReferenceType',
		ReferenceCategory = '$ReferenceCategory',
		ReferenceName = '$ReferenceName',
		ReferenceNameDE = '$ReferenceNameDE',
		ReferenceSummary = '$ReferenceSummary',
		ReferenceSummaryDE = '$ReferenceSummaryDE',
ReferenceClient = '$ReferenceClient',
ReferenceDivision = '$ReferenceDivision',
ReferenceDisziplin = '$ReferenceDisziplin',
ReferenceContractor = '$ReferenceContractor',
ReferenceCode = '$ReferenceCode',
ReferenceContact = '$ReferenceContact',
ReferenceVolume = '$ReferenceVolume',
ReferenceFee = '$ReferenceFee',
ReferenceStartdate = '$ReferenceStartdate',
ReferenceEnddate = '$ReferenceEnddate',
ReferenceLocation = '$ReferenceLocation',
ReferenceCountry = '$ReferenceCountry',
ReferenceDescription = '$ReferenceDescription',
ReferenceDescriptionDE = '$ReferenceDescriptionDE',
ReferenceScope = '$ReferenceScope',
ReferenceScopeDE = '$ReferenceScopeDE',
ReferenceDetails = '$ReferenceDetails',
ReferenceDetailsDE = '$ReferenceDetailsDE',

		ReferenceStatus = '$ReferenceStatus',
		ReferenceFilename = '$ReferenceFilename',
		ReferenceFilesize = '$ReferenceFilesize',
		ReferenceRemarks = '$ReferenceRemarks'
		";
	$dbquery = mysqli_query($link,$dbchange) or die ("not created!");
	}

$_GET['ReferenceCreateID']=$ReferenceCreateID;	
include 'refs_modify.php';

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2019-07-11 18:00</div>';

?>
</body>
</html>

