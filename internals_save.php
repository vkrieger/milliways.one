<!DOCTYPE html public "-//W3C//DTD HTML 4.0 //EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>
<?php

include 'include_setInternalconstants.php';
include 'include_internals_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

if (!empty($_POST["historydataset"])) 
	{
	$_POST['partInternalGUID']=$InternalGUID;	
	$_POST['partInternalArchiveID']='';
	$_POST['list_InternalGUID']='yes';	
	$_POST['list_InternalCreateID']='yes';	
	$_POST['list_InternalArchiveID']='yes';	
	$_POST['list_InternalOwner']='yes';	
	$_POST['list_InternalName']='yes';	
	$_POST['list_InternalStatus']='yes';	
	include 'internals_list.php';
	}

$CurrentTimeStamp = date("Y-m-d H:i:s");
// control code echo 'Project:'.$_POST["InternalProject"].'<br>';
// control code echo 'Filenames:'.$_POST["InternalFilenames"].'<br>'; // does not work when create called directly from app_list.php

	/*
	case op	exist newsingle  action
	1    C	-     -          Create ID nofileaction 
	2    U	-     -          Update ID nofileaction
	3    C	-     y          Create ID upload file create filenames  
	4	 U  -     y          Update ID upload file create filenames
	5    C  y     y          Create ID upload file update filenames
	6    U  y     y          Update ID upload file update filenames
	7    C  y     -          Create ID copy file maintain filenames
	8    U  y     -          Update ID copy file maintain filenames
	*/

// deletedataset: puts current dataset in archive status by entering current datetime into ArchiveID
// updatedataset: puts current dataset in archive status by entering current datetime into ArchiveID and then createdataset

if (!empty($_POST["updatedataset"]) OR !empty($_POST["deletedataset"]))
	{	
	$dbchange = "UPDATE internals SET InternalArchiveID = '$CurrentTimeStamp' WHERE InternalCreateID = '$InternalCreateID'";
	$dbquery = mysqli_query($link,$dbchange) or die ("not updated!");
	}
// updatedataset: creates new dataset with same GUID and with current datetime in CreateID
// createdataset: creates new dataset with new GUID and with current datetime in CreateID, leaving the current untouched  

if (!empty($_POST["updatedataset"]) OR !empty($_POST["createdataset"]))
	{
	if (!empty($_POST["createdataset"])) {$InternalGUID = $CurrentTimeStamp;}
	$InternalCreateID = $CurrentTimeStamp;
	$InternalArchiveID='0000-00-00 00:00:00';

	// in case this code is directly called from create button from app_list.php 
	// the project is only noted in $partAppProject in app_list.php from app_navigation.php
	// and $AppProject is empty
	// $partAppProject must therefore transferred to $AppProject
	if (empty($_POST["InternalProject"]))
		{$InternalProject=$_POST["InternalProject"];}
	
	if (!empty($_POST["Interner"]))
		{
		// adding-Interner-to-InternalInterners code for InternalInterners Attribute in internals_modify.php
		if ($_POST["Interner"]<>"none") {$InternalInterners .= "\n".$_POST["Interner"];}	
		}
		
	/* control code
	echo $InternalFilenames;
	if (!empty($_POST["ExistorArray"])) {print_r ($_POST["ExistorArray"]);}
	*/
	
	// if all files are unchecked clear all variables
	if (empty($_POST["ExistorArray"])) 
		{
		$InternalFilenames = '';
		$InternalFilesizes = '';	
		}
	
	// must be data/ftp instead of alias dataftp for move_uploaded_file!!!
	$ProjectFilePath='/data/ftp/'.$dataftpfolder.'/'.$InternalProject.'/'; 
	$ProjectInitialFilePath='/data/ftp/'.$dataftpfolder.'/'.$InternalInitialProject.'/';
	
	// edit Filenames according to ExistorArray[]
	// only if $Filenames is existing - that means already existing files are there and the implode must be updates
	if ((!empty($InternalFilenames)) AND (!empty($_POST["ExistorArray"])))
		{
		// evaluate ExistorArray to delete Filename from FilenameArray
		// ExistorArray contains only checked Filenames and Filesizes
		foreach ($_POST["ExistorArray"] as $key => $Existor)
			{
			$FilenameArray[$key] = strtok($Existor,' [') ;
			$rest = strtok(' [');
			$FilesizeArray[$key] = strtok($rest,'kB]');
			}
		
		// implode FilenameArray to InternalFilenames 
		$InternalFilenames = implode ("\n",$FilenameArray);
		$InternalFilesizes = implode ("\n",$FilesizeArray);
		}
	
	// upload file Filename and add to Filename to Filenames
	if (!empty($_FILES['Filename']['tmp_name'])) // when $_FILES['Filename']['tmp_name'] is not empty new Filename is given
        // upload and test if successful
		{ 	if ( move_uploaded_file($_FILES['Filename']['tmp_name'], $ProjectFilePath.basename ($_FILES['Filename']['name'])))
			// when upload sucessful  recieves user filename
			{ 
			echo basename ($_FILES['Filename']['name']); 
			echo ' uploaded!<br>'; 
			$InternalFilenames .= "\n".basename ($_FILES['Filename']['name']);
			$InternalFilesizes .= "\n".round ($_FILES['Filename']['size']/1000);
			}
			else { echo ' error uploading!<br>'; }
		}
		
	$dbchange = "INSERT INTO internals SET

		InternalGUID = '$InternalGUID',
		InternalCreateID = '$InternalCreateID',
		InternalArchiveID = '$InternalArchiveID',
		InternalProject = '$InternalProject',
		InternalOwner = '$InternalOwner',
		InternalInterners = '$InternalInterners',
		InternalType = '$InternalType',
		InternalCategory = '$InternalCategory',
		InternalName = '$InternalName',
		InternalFilenames = '$InternalFilenames',
		InternalFilesizes = '$InternalFilesizes',
		InternalStatus = '$InternalStatus',
		InternalRemarks = '$InternalRemarks'
		";
	$dbquery = mysqli_query($link,$dbchange) or die ("not created!");
	}

$_GET['InternalCreateID']=$InternalCreateID;	
include 'internals_modify.php';

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2019-07-11 18:00</div>';

?>
</body>
</html>

