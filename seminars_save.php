<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	input								{ font-size:12px ; font-family: Arial, Verdana, sans-serif;}
	select,option,textarea 				{ font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td                         { font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	*  									{ font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>
<?php

// new error reporting
error_reporting(E_ALL);
ini_set("display_errors", 1);

include 'include_setSeminarconstants.php';
include 'include_seminars_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

if (!empty($_POST["historydataset"])) 
	{
	$_POST['partSeminarGUID']=$SeminarGUID;	
	$_POST['partSeminarArchiveID']='';
	$_POST['list_SeminarGUID']='yes';	
	$_POST['list_SeminarCreateID']='yes';	
	$_POST['list_SeminarArchiveID']='yes';	
	$_POST['list_SeminarOwner']='yes';	
	$_POST['list_SeminarName']='yes';	
	$_POST['list_SeminarStatus']='yes';	
	include 'seminars_list.php';
	}

$CurrentTimeStamp = date("Y-m-d H:i:s");

// deletedataset: puts current dataset in archive status by entering current datetime into ArchiveID
// updatedataset: puts current dataset in archive status by entering current datetime into ArchiveID and then createdataset

if (!empty($_POST["updatedataset"]) OR !empty($_POST["deletedataset"]))
	{	
	$dbchange = "UPDATE seminars SET SeminarArchiveID = '$CurrentTimeStamp' WHERE SeminarCreateID = '$SeminarCreateID'";
	$dbquery = mysqli_query($link,$dbchange) or die ("not updated!".mysqli_error($link));
	}
// updatedataset: creates new dataset with same GUID and with current datetime in CreateID
// createdataset: creates new dataset with new GUID and with current datetime in CreateID, leaving the current untouched  

if (!empty($_POST["updatedataset"]) OR !empty($_POST["createdataset"]))
	
	{
	if (!empty($_POST["createdataset"])) {$SeminarGUID = $CurrentTimeStamp;}
	$SeminarCreateID = $CurrentTimeStamp;
	$SeminarArchiveID='0000-00-00 00:00:00';

	// Begin from type timestamp must not be empty
	if (empty($SeminarBegin)) {$SeminarBegin = $CurrentTimeStamp;}
	// Year from type integer must not be empty - should be derived from $CurrentTimeStamp
	if (empty($SeminarYear)) {$SeminarYear = '2021';}
	
	$ProjectFilePath='/data/ftp/'.$dataftpfolder.'/'.$SeminarProject.'/'; 
	$ProjectInitialFilePath='/data/ftp/'.$dataftpfolder.'/'.$SeminarInitialProject.'/';
	
	if (!empty($_FILES['SeminarFilename']['tmp_name']))
        {if ( move_uploaded_file($_FILES['SeminarFilename']['tmp_name'], $ProjectFilePath.basename ($_FILES['SeminarFilename']['name'])))
			{ echo $SeminarFilename = basename ($_FILES['SeminarFilename']['name']); echo ' uploaded!<br>'; $MediaPDFsize = round($_FILES['SeminarFilename']['size']/1000);}
			else { echo ' error uploading!<br>'; }
		} else {if (!$existor){$SeminarFilename=''; $SeminarFilesize='';}} 

	if ((!empty ($SeminarFilename)) AND empty($_FILES['SeminarFilename']['tmp_name'])) 
    	{ 	
		$SeminarFilenameExist = $SeminarFilename;
		copy ($ProjectInitialFilePath.$SeminarFilenameExist,$ProjectFilePath.$SeminarFilename);
		}	

	$dbchange = "INSERT INTO seminars SET

SeminarGUID = '$SeminarGUID',
SeminarCreateID = '$SeminarCreateID',
SeminarArchiveID = '$SeminarArchiveID',
SeminarProject = '$SeminarProject',
SeminarOwner = '$SeminarOwner',
SeminarType = '$SeminarType',
SeminarCategory = '$SeminarCategory',
SeminarBegin = '$SeminarBegin',
SeminarYear = '$SeminarYear',
SeminarDuration = '$SeminarDuration',
SeminarStatus = '$SeminarStatus',
SeminarName = '$SeminarName',
SeminarContent = '$SeminarContent',
SeminarReferent1 = '$SeminarReferent1',
SeminarReferent2 = '$SeminarReferent2',
SeminarReferent3 = '$SeminarReferent3',
SeminarLanguage = '$SeminarLanguage',
SeminarLocation = '$SeminarLocation',
SeminarHost = '$SeminarHost',
SeminarSponsor = '$SeminarSponsor',
SeminarContact = '$SeminarContact',
SeminarWeb = '$SeminarWeb',
SeminarRegistration = '$SeminarRegistration',
SeminarFee = '$SeminarFee',
SeminarFilename = '$SeminarFilename',
SeminarFilesize = '$SeminarFilesize',
SeminarRemarks = '$SeminarRemarks'
		";
	$dbquery = mysqli_query($link,$dbchange) or die ("not created!".mysqli_error($link));
	}

/* taken out from above
SeminarAddress = '$SeminarAddress',
SeminarCountry = '$SeminarCountry',
*/

$_GET['SeminarCreateID']=$SeminarCreateID;	
include 'seminars_modify.php';

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-06 14:00</div>';

?>
</body>
</html>

