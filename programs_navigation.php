<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; background-color:<?php echo $SystemColor; ?>;}
	input                               {font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>
contents listing criteria<br>
<?php

include 'include_setProgramconstants.php';
include 'include_programs_postvariables.php';

if (!isset($_SESSION)) { session_start();}

	$listdatasets = "";
	
if ($listdatasets == "" AND $_SESSION['LoginType'])
	{		
    echo '<form method="post" enctype="multipart/form-data" action="programs_list.php" target="main">';
	
	$partProgramArchiveID ='0000-00-00 00:00:00'; // to firstly display only non-archived datasets when navigation is called
	
	echo '<table>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ProgramGUID" value="yes" >GUID</td>';
	echo '<td><input type="text" name="partProgramGUID" size="20" maxlength="40" value="'.$partProgramGUID.'"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ProgramCreateID" value="yes" checked>CreateID(Link)</td>';
	echo '<td><input type="text" name="partProgramCreateID" size="20" maxlength="40" value="'.$partProgramCreateID.'"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ProgramArchiveID" value="yes" >ArchiveID</td>';
	echo '<td><input type="text" name="partProgramArchiveID" size="20" maxlength="40" value="'.$partProgramArchiveID.'"></td>';
	echo '</tr>';
	
	echo '</table>';
	
	echo '<table>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ProgramOwner" value="yes" >Owner</td>';
	echo '<td><input type="text" name="partProgramOwner" size="8" maxlength="40" value="'.$partProgramOwner.'"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ProgramProject" value="yes" >Project</td>';
	if (empty($SystemProject))
		{
		echo '<td><select type="text" name="partProgramProject" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($SystemProjectArray as $Project) {echo '<option>'.$Project.'</option>';}
			echo '</select>';}
		else { echo '<input type="hidden" name="partProgramProject" value="'.$SystemProject.'">';}
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ProgramType" value="yes" checked>Type</td>';
	if ($SystemType == "public")
		{echo '<td><input style="background-color:#C0C0C0" type="text" name="partProgramType" size="8" maxlength="40" value="public" readonly></td>';}
		else
		{echo '<td><input type="text" name="partProgramType" size="8" maxlength="40" value="'.$partProgramType.'"></td>';}
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ProgramCategory" value="yes" checked>Category</td>';
	echo '<td><select type="text" name="partProgramCategory" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($ProgramCategoryArray as $Category) {echo '<option>'.$Category.'</option>';}
			echo '</select>';
    echo '</td>';
	echo '</tr>';    
	
	echo '</table>';

	echo '<table>';

	// content attributes

	echo '<tr><td><input type="checkbox" name="list_ProgramName" value="yes" checked >Name</td><td><input type="text" name="partProgramName" size="8" maxlength="40" value="'.$partProgramName.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_ProgramTeaser" value="yes" checked >Teaser</td><td><input type="text" name="partProgramTeaser" size="8" maxlength="40" value="'.$partProgramTeaser.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_ProgramIntro" value="yes" checked >Intro</td><td><input type="text" name="partProgramIntro" size="8" maxlength="40" value="'.$partProgramIntro.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_ProgramTarget" value="yes" checked >Target</td><td><input type="text" name="partProgramTarget" size="8" maxlength="40" value="'.$partProgramTarget.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_ProgramPromo" value="yes" checked >Promo</td><td><input type="text" name="partProgramPromo" size="8" maxlength="40" value="'.$partProgramPromo.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_ProgramText" value="yes" checked >Text</td><td><input type="text" name="partProgramText" size="8" maxlength="40" value="'.$partProgramText.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_ProgramContent" value="yes" checked>Content</td><td><input type="text" name="partProgramContent" size="8" maxlength="40" value="'.$partProgramContent.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_ProgramLanguage" value="yes" >Language</td><td><input type="text" name="partProgramLanguage" size="8" maxlength="40" value="'.$partProgramLanguage.'"></td></tr>';


	// budget attributes

	echo '<tr><td><input type="checkbox" name="list_ProgramRegistration" value="yes" >Registration</td><td><input type="text" name="partProgramRegistration" size="8" maxlength="40" value="'.$partProgramRegistration.'"></td></tr>';
	
	echo '<tr><td><input type="checkbox" name="list_ProgramFee" value="yes" checked >Fee</td><td><input type="text" name="partProgramFee" size="8" maxlength="40" value="'.$partProgramFee.'"></td></tr>';


	// other attributes
	
	echo '<tr><td><input type="checkbox" name="list_ProgramFilename" value="yes" checked>Filename</td><td><input type="text" name="partProgramFilename" size="8" maxlength="40" value="'.$partProgramFilename.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_ProgramRemarks" value="yes" checked >Remarks</td><td><input type="text" name="partProgramRemarks" size="8" maxlength="40" value="'.$partProgramRemarks.'"></td></tr>';

	echo '</table>';

	?>
		
		<!-- sorting part -->
        <table>
		<tr><td>first sorted by</td>
			<td><select name="first_sorted_by" size="1">
				<option value="ProgramGUID" >GUID</option>
				<option value="ProgramCreateID" >CreateID</option>
				<option value="ProgramArchiveID" >ArchiveID</option>
				<option value="ProgramProject">Project</option>
				<option value="ProgramOwner" >Owner</option>
				<option value="ProgramType" >Type</option>
				<option value="ProgramCategory">Category</option>
				<option value="ProgramName" selected>Name</option>
				<option value="ProgramFee" >Fee</option>
				<option value="ProgramStatus" >Status</option>
				<option value="ProgramFilename" >Filename</option>
				<option value="ProgramRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
      	<tr><td>then sorted by</td>
			<td><select name="then_sorted_by" size="1">
            <option value="ProgramGUID" >GUID</option>
				<option value="ProgramCreateID" >CreateID</option>
				<option value="ProgramArchiveID" >ArchiveID</option>
				<option value="ProgramProject">Project</option>
				<option value="ProgramOwner" >Owner</option>
				<option value="ProgramType" >Type</option>
				<option value="ProgramCategory">Category</option>
				<option value="ProgramName" selected>Name</option>
				<option value="ProgramFee" >Fee</option>
				<option value="ProgramStatus" >Status</option>
				<option value="ProgramFilename" >Filename</option>
				<option value="ProgramRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	<tr><td>last sorted by</td>
			<td><select name="last_sorted_by" size="1">
            <option value="ProgramGUID" >GUID</option>
				<option value="ProgramCreateID" >CreateID</option>
				<option value="ProgramArchiveID" >ArchiveID</option>
				<option value="ProgramProject">Project</option>
				<option value="ProgramOwner" >Owner</option>
				<option value="ProgramType" >Type</option>
				<option value="ProgramCategory">Category</option>
				<option value="ProgramName" selected>Name</option>
				<option value="ProgramFee" >Fee</option>
				<option value="ProgramStatus" >Status</option>
				<option value="ProgramFilename" >Filename</option>
				<option value="ProgramRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	</table>

		<br>

		<table>
		<input type="submit" name="listdatasets" value="list datasets">
      	<input type="reset" value="reset values">
		</table>

    </form>
<?php
} elseif ( $listdatasets );

echo '<div align="right" style="font-size: 8px;">last source change vk 2021-02-07 08:00</div>';

?>
</body>
</html>