<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 				{font-size:16px ; font-family: Arial, Verdana, sans-serif; background-color:<?php echo $SystemColor; ?>;}
	input                           {font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	select,option,textarea		{font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	table,tr,td 			{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

napkins listing criteria<br>

<?php

include 'include_setNapkinconstants.php';
include 'include_napkins_postvariables.php';

if (!isset($_SESSION)) { session_start();}

	$listdatasets = "";
	
if ($listdatasets == "" AND $_SESSION['LoginType'])
	{		
    echo '<form method="post" enctype="multipart/form-data" action="napkins_list.php" target="main">';
	
	$partNapkinArchiveID ='0000-00-00 00:00:00'; // to firstly display only non-archived datasets when navigation is called
	
	echo '<table>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_NapkinGUID" value="yes" >GUID</td>';
	echo '<td><input type="text" name="partNapkinGUID" size="20" maxlength="40" value="'.$partNapkinGUID.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_NapkinCreateID" value="yes" checked>CreateID(Link)</td>';
	echo '<td><input type="text" name="partNapkinCreateID" size="20" maxlength="40" value="'.$partNapkinCreateID.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_NapkinArchiveID" value="yes" >ArchiveID</td>';
	echo '<td><input type="text" name="partNapkinArchiveID" size="20" maxlength="40" value="'.$partNapkinArchiveID.'"></td>';
	echo '</tr>';
	echo '</table>';
	
	echo '<table>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_NapkinOwner" value="yes" >Owner</td>';
	echo '<td><input type="text" name="partNapkinOwner" size="8" maxlength="40" value="'.$partNapkinOwner.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_NapkinProject" value="yes" checked>Project</td>';
	if (empty($SystemProject))
		{
		echo '<td><select type="text" name="partNapkinProject" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($SystemProjectArray as $Project) {echo '<option>'.$Project.'</option>';}
			echo '</select>';}
		else { echo '<input type="hidden" name="partNapkinProject" value="'.$SystemProject.'">';}
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_NapkinOwner" value="yes" >Owner</td>';
	echo '<td><input type="text" name="partNapkinOwner" size="8" maxlength="40" value="'.$partNapkinOwner.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_NapkinType" value="yes" >Type</td>';
	if ($SystemType == "public")
		{echo '<td><input style="background-color:#C0C0C0" type="text" name="partNapkinType" size="8" maxlength="40" value="public" readonly></td>';}
		else
		{echo '<td><input type="text" name="partNapkinType" size="8" maxlength="40" value="'.$partNapkinType.'"></td>';}
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_NapkinCategory" value="yes" >Category</td>';
	echo '<td><select type="text" name="partNapkinCategory" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($NapkinCategoryArray as $Category) {echo '<option>'.$Category.'</option>';}
			echo '</select>';
    echo '</td>';
	echo '</tr>';    
	
echo '<tr><td><input type="checkbox" name="list_NapkinName" value="yes" checked>Name</td><td><input type="text" name="partNapkinName" size="8" maxlength="40" value="'.$partNapkinName.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_NapkinStatus" value="yes" >Status</td><td><input type="text" name="partNapkinStatus" size="8" maxlength="40" value="'.$partNapkinStatus.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_NapkinFilenames" value="yes" checked>Filenames</td><td><input type="text" name="partNapkinFilenames" size="8" maxlength="40" value="'.$partNapkinFilenames.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_NapkinRemarks" value="yes" >Remarks</td><td><input type="text" name="partNapkinRemarks" size="8" maxlength="40" value="'.$partNapkinRemarks.'"></td></tr>';

	echo '</table>';

	?>
		
		<!-- sorting part -->
        <table>
		<tr><td>first sorted by</td>
			<td><select name="first_sorted_by" size="1">
				<option value="NapkinGUID" selected>GUID</option>
				<option value="NapkinCreateID">CreateID</option>
				<option value="NapkinArchiveID">ArchiveID</option>
				<option value="NapkinProject">Project</option>
				<option value="NapkinOwner" >Owner</option>
				<option value="NapkinType" >Type</option>
				<option value="NapkinCategory">Category</option>
				<option value="NapkinName" >Name</option>
				<option value="NapkinStatus" >Status</option>
				<option value="NapkinFilenames" >Filename</option>
				<option value="NapkinRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
      	<tr><td>then sorted by</td>
			<td><select name="then_sorted_by" size="1">
				<option value="NapkinGUID">GUID</option>
				<option value="NapkinCreateID">CreateID</option>
				<option value="NapkinArchiveID">ArchiveID</option>
				<option value="NapkinProject" >Project</option>
				<option value="NapkinOwner" >Owner</option>
				<option value="NapkinType" >Type</option>
				<option value="NapkinCategory" selected>Category</option>
				<option value="NapkinName" >Name</option>
				<option value="NapkinStatus" >Status</option>
				<option value="NapkinFilenames" >Filename</option>
				<option value="NapkinRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	<tr><td>last sorted by</td>
			<td><select name="last_sorted_by" size="1">
				<option value="NapkinGUID">GUID</option>
				<option value="NapkinCreateID">CreateID</option>
				<option value="NapkinArchiveID">ArchiveID</option>
				<option value="NapkinProject" >Project</option>
				<option value="NapkinOwner" selected>Owner</option>
				<option value="NapkinType" >Type</option>
				<option value="NapkinCategory" >Category</option>
				<option value="NapkinName" >Name</option>
				<option value="NapkinStatus" >Status</option>
				<option value="NapkinFilenames" >Filename</option>
				<option value="NapkinRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	</table>

		<br>

		<table>
		<input type="submit" name="listdatasets" value="list datasets">
      	<input type="reset" value="reset values">
		</table>

    </form>
<?php
} elseif ( $listdatasets );

echo '<div align="right" style="font-size: 8px;">last source change vk 08.08.17 18:00</div>';

?>
</body>
</html>
