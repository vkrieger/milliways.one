<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

!!! Do <b>NOT</b> use semicolon (;) in input fields !!! <b>NO</b> special characters and blanks in filenames - use underscore ( _ ) instead!!! 
<br /><br />

<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
		
include 'include_setDocconstants.php';
include 'include_docs_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

$DocCreateID = $_GET["DocCreateID"];

$dbquery = " SELECT * FROM docs WHERE LOCATE ('$DocCreateID', DocCreateID) >0 ";
$dbresult = mysqli_query($link,$dbquery);
$dbrow = mysqli_fetch_array($dbresult);

	$updatedataset="";
	$createdataset="";
	$deletedataset="";
	$historydataset="";
	
if ($updatedataset == "" AND $createdataset == "" AND $deletedataset == "" AND $historydataset =="")
	{
	echo '<form method="post" action="docs_save.php" enctype="multipart/form-data" >';
	if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
		{
		echo '<input type="submit" name="createdataset" value="create dataset">';
		echo '<input type="submit" name="updatedataset" value="update dataset">';
    	echo '<input type="submit" name="deletedataset" value="delete dataset">';
      	echo '<input type="reset" value="reset values">';
      	echo '<br /><br />';
	    }

	echo '<table>';
	
	echo '<tr>';
	echo '<td align="right">GUID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="DocGUID" size="60" maxlength="100" value="'.$dbrow['DocGUID'].'" readonly></td>';
	echo '<td><input type="submit" name="historydataset" value="history of datasets"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">CreateID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="DocCreateID" size="60" maxlength="100" value="'.$dbrow['DocCreateID'].'" readonly></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">ArchiveID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="DocArchiveID" size="60" maxlength="100" value="'.$dbrow['DocArchiveID'].'" readonly></td>';
	echo '</tr>';
	
	// This is necessary to save the Project in $AppInitialProject and AppProject which might be changed in dataset
	// Make sure the postvariables are defined
	echo '<input type="hidden" name="DocInitialProject" value="'.$dbrow['DocProject'].'">';
	echo '<input type="hidden" name="DocProject" value="'.$dbrow['DocProject'].'">';
	
	echo '<tr>';
	echo '<td align="right">Project</td>';
	// if SystemProject is not empty this should preselect the selected project 
	// but if this is called directly from listing no project is selected
	// then the SystemProject should be preselected
	// if SystemProject is empty this should preselect the selected project by the Project value of the selected dataset
	// but if this is called directly from listing public project is selected
	// SystemTypeArray and SystemRegimeArray are stored in SystemConstants
	/*
	from    list           modify
	empty   undef(public)  dbrow
	project system/dbrow   dbrow	
	*/
	echo '<td><select name="DocProject" size="1">';
	if (!empty($SystemProject))
		{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($SystemProject==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		else
		{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($dbrow['DocProject']==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
	echo '</select></td>';		
	echo '</tr>';	
	
	echo '<tr>';
	echo '<td align="right">Owner (readonly)</td>';
	echo '<td>'.'['.$dbrow['DocOwner'].'] - create/update transfers to '.'<input style="background-color:#C0C0C0" type="text" name="DocOwner" size="15" maxlength="100" value="'.$_SESSION['LoginLogin'].'" readonly></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">Type</td>';
	echo '<td><select name="DocType" size="1">';
		foreach ($SystemTypeArray as $Type) {echo '<option'; if ($dbrow['DocType']==$Type) {echo ' selected';} echo '>'.$Type.'</option>';} 
		echo '</select></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">Category</td>';
	echo '<td><select name="DocCategory" size="1">';
		foreach ($DocCategoryArray as $Category) {echo '<option'; if ($dbrow['DocCategory']==$Category) {echo ' selected';} echo '>'.$Category.'</option>';} 
		echo '</select></td>';
	echo '</tr>';

	echo '<tr><td align="right">Name </td><td><input type="text" name="DocName" size="60" maxlength="255" value="'.$dbrow['DocName'].'"></td></tr>';

	echo '<tr><td align="right">Status </td><td><input type="text" name="DocStatus" size="60" maxlength="255" value="'.$dbrow['DocStatus'].'"></td></tr>';

/*
- existor has value "File" when $dbrow['DocFilename'] is given 
- $_FILES['DocFilename']['tmp_name'] is given
- when new DocFilename is uploaded existor should be checked
- when existor is unchecked DocFilename should be emptied
*/
// hidden input transfers dbrow[] to FileName and FileSize
// SystemProject would be empty in case of public and admin login

// must be dataftp instead of data/ftp and make use of Alias in http.conf !!!
	$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['DocProject'].'/';	

	echo '<tr>';
	echo '<td align="right">';
	echo '<input type="checkbox" name="existor" value="File"'; if ($dbrow['DocFilename']) {echo ' checked';} echo '> Filename (max.10MB)</td>';
	echo '<input type="hidden" name="DocFilename" value="'.$dbrow['DocFilename'].'">';
	echo '<input type="hidden" name="DocFilesize" value="'.$dbrow['DocFilesize'].'">';
	echo '<td align="left" class="bluelink"><a href="'.$ProjectFilePath.$dbrow['DocFilename'].'">'.$dbrow['DocFilename'].'</a> ['.$dbrow['DocFilesize'].'kB]</td>';
	echo '<td><input type="file" name="DocFilename" size="15"></td>';
	echo '</tr>';

	echo '<tr><td align="right">Remarks </td><td><input type="text" name="DocRemarks" size="60" maxlength="255" value="'.$dbrow['DocRemarks'].'"></td></tr>';
	
	echo '</table>';

echo '</form>';

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-03 18:00</div>';

} elseif ( $updatedataset OR $createdataset OR $deletedataset OR $historydataset)

?>
</font>
</body>
</html>
