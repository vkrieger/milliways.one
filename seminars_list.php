<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>


<body>

You may download <a href="seminarslist.xls">this Table</a> in Spreadsheet-Format (e.g. as CSV for MS-Excel) from Server.
<br /><br />

<?php

// new error handling 
error_reporting(E_ALL);
ini_set("display_errors", 1);


include 'include_setSeminarconstants.php';
include 'include_seminars_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

// <form></form> must enclose all <input> to transfer to next app
echo '<form method="post" enctype="multipart/form-data" action="seminars_save.php">';
	
if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
	{
	echo '<input type="submit" name="createdataset" value="create dataset">';
	}

echo '<br /><br />';

/*
select datasets according to criteria. 
*/

  	$dbquery = "SELECT * FROM seminars WHERE

LOCATE('$partSeminarGUID', SeminarGUID)>0 AND
LOCATE('$partSeminarCreateID', SeminarCreateID)>0 AND
LOCATE('$partSeminarArchiveID', SeminarArchiveID)>0 AND
(LOCATE('$partSeminarProject', SeminarProject)>0  OR LOCATE('public', SeminarType)>0) AND
LOCATE('$partSeminarOwner', SeminarOwner)>0 AND
LOCATE('$partSeminarType', SeminarType)>0 AND
LOCATE('$partSeminarCategory', SeminarCategory)>0 AND
LOCATE('$partSeminarBegin', SeminarBegin)>0 AND
LOCATE('$partSeminarYear', SeminarYear)>0 AND
LOCATE('$partSeminarDuration', SeminarDuration)>0 AND
LOCATE('$partSeminarStatus', SeminarStatus)>0 AND
LOCATE('$partSeminarName', SeminarName)>0 AND
LOCATE('$partSeminarContent', SeminarContent)>0 AND
LOCATE('$partSeminarLanguage', SeminarLanguage)>0 AND
LOCATE('$partSeminarLocation', SeminarLocation)>0 AND
LOCATE('$partSeminarHost', SeminarHost)>0 AND
LOCATE('$partSeminarSponsor', SeminarSponsor)>0 AND
LOCATE('$partSeminarContact', SeminarContact)>0 AND
LOCATE('$partSeminarWeb', SeminarWeb)>0 AND
LOCATE('$partSeminarRegistration', SeminarRegistration)>0 AND
LOCATE('$partSeminarFee', SeminarFee)>0 AND
LOCATE('$partSeminarFilename', SeminarFilename)>0 AND
LOCATE('$partSeminarRemarks', SeminarRemarks)>0

ORDER BY $first_sorted_by, $then_sorted_by, $last_sorted_by " ;

/*taken out
LOCATE('$partSeminarAddress', SeminarAddress)>0 AND
LOCATE('$partSeminarCountry', SeminarCountry)>0 AND
*/
	
  	$dbresult = mysqli_query($link,$dbquery);  echo mysqli_error($link);
  	
  	$header =""; //empty header for excel file (dataset structure)
  	$data	=""; //empty $data variable for excel file (all rows in one variable)

echo '<table border="1" cellspacing="0">';
 
echo '<tr>';

if ($list_SeminarGUID =='yes'){echo '<td>GUID</td>'; $header.="GUID". "\t";}
if ($list_SeminarCreateID =='yes'){echo '<td>CreateID</td>'; $header.="CreateID". "\t";}
if ($list_SeminarArchiveID =='yes'){echo '<td>ArchiveID</td>'; $header.="ArchiveID". "\t";}
if ($list_SeminarProject =='yes'){echo '<td>Project</td>'; $header.="Project". "\t";}
if ($list_SeminarOwner =='yes'){echo '<td>Owner</td>'; $header.="Owner". "\t";}
if ($list_SeminarType =='yes'){echo '<td>Type</td>'; $header.="Type". "\t";}
if ($list_SeminarCategory =='yes'){echo '<td>Category</td>'; $header.="Category". "\t";}

if ($list_SeminarBegin =='yes'){echo '<td>Begin</td>'; $header.="Begin". "\t";}
if ($list_SeminarYear =='yes'){echo '<td>Year</td>'; $header.="Year". "\t";}
if ($list_SeminarDuration =='yes'){echo '<td>Duration</td>'; $header.="Duration". "\t";}
if ($list_SeminarStatus =='yes'){echo '<td>Status</td>'; $header.="Status". "\t";}

if ($list_SeminarName =='yes'){echo '<td>Name</td>'; $header.="Name". "\t";}
if ($list_SeminarContent =='yes'){echo '<td>Content</td>'; $header.="Content". "\t";}
if ($list_SeminarLanguage =='yes'){echo '<td>Language</td>'; $header.="Language". "\t";}

if ($list_SeminarLocation =='yes'){echo '<td>Location</td>'; $header.="Location". "\t";}
// if ($list_SeminarAddress =='yes'){echo '<td>Address</td>'; $header.="Address". "\t";}
// if ($list_SeminarCountry =='yes'){echo '<td>Country</td>'; $header.="Country". "\t";}

if ($list_SeminarHost =='yes'){echo '<td>Host</td>'; $header.="Host". "\t";}
if ($list_SeminarSponsor =='yes'){echo '<td>Sponsor</td>'; $header.="Sponsor". "\t";}
if ($list_SeminarContact =='yes'){echo '<td>Contact</td>'; $header.="Contact". "\t";}
if ($list_SeminarWeb =='yes'){echo '<td>Web</td>'; $header.="Web". "\t";}

if ($list_SeminarRegistration =='yes'){echo '<td>Registration</td>'; $header.="Registration". "\t";}
if ($list_SeminarFee =='yes'){echo '<td>Fee</td>'; $header.="Fee". "\t";}

if ($list_SeminarFilename =='yes'){echo '<td>Filename</td>'; $header.="Filename". "\t";}
if ($list_SeminarRemarks =='yes'){echo '<td>Remarks</td>'; $header.="Remarks". "\t";}

	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)


/*
outputs datasets and fills $data variable
*/

$DatasetCount = 0;

  	while($dbrow = mysqli_fetch_array($dbresult))
   	{
	$DatasetCount += 1;
	
	// output to main window and selected data to $data excel variable

   	echo '<tr>';
if ($list_SeminarGUID =='yes'){echo'<td>'.$dbrow['SeminarGUID'].'</td>'; $data.='"'.$dbrow['SeminarGUID'].'"'."\t";}
if ($list_SeminarCreateID=='yes')
		{
		echo '<td class="bluelink"><a style="font-size:xx-small" href="seminars_modify.php?SeminarCreateID='.$dbrow['SeminarCreateID'].'">'.$dbrow['SeminarCreateID'].'</a></td>';
		$data .= '"' . $dbrow['SeminarCreateID'] . '"' . "\t";
		}
if ($list_SeminarArchiveID =='yes'){echo'<td>'.$dbrow['SeminarArchiveID'].'</td>'; $data.='"'.$dbrow['SeminarArchiveID'].'"'."\t";}
if ($list_SeminarProject =='yes'){echo'<td>'.$dbrow['SeminarProject'].'</td>'; $data.='"'.$dbrow['SeminarProject'].'"'."\t";}
if ($list_SeminarOwner =='yes'){echo'<td>'.$dbrow['SeminarOwner'].'</td>'; $data.='"'.$dbrow['SeminarOwner'].'"'."\t";}
if ($list_SeminarType =='yes'){echo'<td>'.$dbrow['SeminarType'].'</td>'; $data.='"'.$dbrow['SeminarType'].'"'."\t";}
if ($list_SeminarCategory =='yes'){echo'<td>'.$dbrow['SeminarCategory'].'</td>'; $data.='"'.$dbrow['SeminarCategory'].'"'."\t";}

if ($list_SeminarBegin =='yes'){echo'<td>'.$dbrow['SeminarBegin'].'</td>'; $data.='"'.$dbrow['SeminarBegin'].'"'."\t";}
if ($list_SeminarYear =='yes'){echo'<td>'.$dbrow['SeminarYear'].'</td>'; $data.='"'.$dbrow['SeminarYear'].'"'."\t";}
if ($list_SeminarDuration =='yes'){echo'<td>'.$dbrow['SeminarDuration'].'</td>'; $data.='"'.$dbrow['SeminarDuration'].'"'."\t";}
if ($list_SeminarStatus =='yes'){echo'<td>'.$dbrow['SeminarStatus'].'</td>'; $data.='"'.$dbrow['SeminarStatus'].'"'."\t";}

if ($list_SeminarName =='yes'){echo'<td>'.$dbrow['SeminarName'].'</td>'; $data.='"'.$dbrow['SeminarName'].'"'."\t";}
if ($list_SeminarContent =='yes'){echo'<td>'.$dbrow['SeminarContent'].'</td>'; $data.='"'.$dbrow['SeminarContent'].'"'."\t";}
if ($list_SeminarLanguage =='yes'){echo'<td>'.$dbrow['SeminarLanguage'].'</td>'; $data.='"'.$dbrow['SeminarLanguage'].'"'."\t";}

if ($list_SeminarLocation =='yes'){echo'<td>'.$dbrow['SeminarLocation'].'</td>'; $data.='"'.$dbrow['SeminarLocation'].'"'."\t";}
// if ($list_SeminarAddress =='yes'){echo'<td>'.$dbrow['SeminarAddress'].'</td>'; $data.='"'.$dbrow['SeminarAddress'].'"'."\t";}
// if ($list_SeminarCountry =='yes'){echo'<td>'.$dbrow['SeminarCountry'].'</td>'; $data.='"'.$dbrow['SeminarCountry'].'"'."\t";}

if ($list_SeminarHost =='yes'){echo'<td>'.$dbrow['SeminarHost'].'</td>'; $data.='"'.$dbrow['SeminarHost'].'"'."\t";}
if ($list_SeminarSponsor =='yes'){echo'<td>'.$dbrow['SeminarSponsor'].'</td>'; $data.='"'.$dbrow['SeminarSponsor'].'"'."\t";}
if ($list_SeminarContact =='yes'){echo'<td>'.$dbrow['SeminarContact'].'</td>'; $data.='"'.$dbrow['SeminarContact'].'"'."\t";}
if ($list_SeminarWeb =='yes'){echo'<td>'.$dbrow['SeminarWeb'].'</td>'; $data.='"'.$dbrow['SeminarWeb'].'"'."\t";}

if ($list_SeminarRegistration =='yes'){echo'<td>'.$dbrow['SeminarRegistration'].'</td>'; $data.='"'.$dbrow['SeminarRegistration'].'"'."\t";}
if ($list_SeminarFee =='yes'){echo'<td>'.$dbrow['SeminarFee'].'</td>'; $data.='"'.$dbrow['SeminarFee'].'"'."\t";}

if($list_SeminarFilename=='yes')		
	{
	$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['SeminarProject'].'/';
	
	echo	'<td class="bluelink">'
			.'<a style="font-size:xx-small" href="'
			.$ProjectFilePath
			.$dbrow['SeminarFilename']
			.'">'
			.$dbrow['SeminarFilename'].'['.$dbrow['SeminarFilesize'].'kB]'
			.'</a>';
	$data.='"'.$dbrow['SeminarFilename'].'['.$dbrow['SeminarFilesize'].'kB]'.'"'."\t";
	}

if ($list_SeminarRemarks =='yes'){echo'<td>'.$dbrow['SeminarRemarks'].'</td>'; $data.='"'.$dbrow['SeminarRemarks'].'"'."\t";}
	
	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)
	}
echo '<tr>';
echo '<td>'.$DatasetCount.' Datasets</td>';
echo '</tr>';	
echo '</table>';

echo '</form>';

/*
export selected table to .xls onto local server folder to download as offered from there
*/

$fp = fopen('seminarslist.xls','w');
fwrite($fp,$header);
fwrite($fp,"\n");
fwrite($fp,$data);
fclose($fp);

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-06 08:00</div>';

?>
</body>
</html>
