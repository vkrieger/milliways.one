<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 				{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                         {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea		{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 			{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

You may download <a href="napkinslist.xls">this Table</a> in Spreadsheet-Format (e.g. as CSV for MS-Excel) from Server.
<br><br>

<?php

include 'include_setNapkinconstants.php';
include 'include_napkins_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

echo '<form method="post" enctype="multipart/form-data" action="napkins_save.php">';	
if (!empty($partNapkinProject)) {echo '<input type="hidden" name="NapkinProject" value="'.$partNapkinProject.'">';}
if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
	{echo '<input type="submit" name="createdataset" value="create dataset">';}

echo '<br><br>';

$LoginLogin = $_SESSION['LoginLogin'];

  	$dbquery = "SELECT * FROM napkins WHERE

LOCATE('$partNapkinGUID', NapkinGUID)>0 AND
LOCATE('$partNapkinCreateID', NapkinCreateID)>0 AND
LOCATE('$partNapkinArchiveID', NapkinArchiveID)>0 AND
(LOCATE('$partNapkinProject', NapkinProject)>0  OR LOCATE('public', NapkinType)>0) AND
LOCATE('$partNapkinOwner', NapkinOwner)>0 AND
LOCATE('$partNapkinType', NapkinType)>0 AND
LOCATE('$partNapkinCategory', NapkinCategory)>0 AND
LOCATE('$partNapkinName', NapkinName)>0 AND
LOCATE('$partNapkinStatus', NapkinStatus)>0 AND
LOCATE('$partNapkinFilenames', NapkinFilenames)>0 AND
LOCATE('$partNapkinRemarks', NapkinRemarks)>0


ORDER BY $first_sorted_by, $then_sorted_by, $last_sorted_by " ;
	
  	$dbresult = mysqli_query($link,$dbquery);  echo mysqli_error($link);
  	
  	$header =""; //empty header for excel file (dataset structure)
  	$data	=""; //empty $data variable for excel file (all rows in one variable)

echo '<table border="1" cellspacing="0">';
 
echo '<tr>';

if ($list_NapkinGUID =='yes'){echo '<td>GUID</td>'; $header.="GUID". "\t";}
if ($list_NapkinCreateID =='yes'){echo '<td>CreateID</td>'; $header.="CreateID". "\t";}
if ($list_NapkinArchiveID =='yes'){echo '<td>ArchiveID</td>'; $header.="ArchiveID". "\t";}
if ($list_NapkinProject =='yes'){echo '<td>Project</td>'; $header.="Project". "\t";}
if ($list_NapkinOwner =='yes'){echo '<td>Owner</td>'; $header.="Owner". "\t";}
if ($list_NapkinType =='yes'){echo '<td>Type</td>'; $header.="Type". "\t";}
if ($list_NapkinCategory =='yes'){echo '<td>Category</td>'; $header.="Category". "\t";}
if ($list_NapkinName =='yes'){echo '<td>Name</td>'; $header.="Name". "\t";}
if ($list_NapkinStatus =='yes'){echo '<td>Status</td>'; $header.="Status". "\t";}
if ($list_NapkinFilenames =='yes'){echo '<td>Filenames[kB]</td>'; $header.="Filenames[kB]". "\t";}
if ($list_NapkinRemarks =='yes'){echo '<td>Remarks</td>'; $header.="Remarks". "\t";}

	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)


$DatasetCount = 0;

while($dbrow = mysqli_fetch_array($dbresult))
   	{
	$DatasetCount += 1;
  	echo '<tr>';
	if ($list_NapkinGUID =='yes'){echo'<td>'.$dbrow['NapkinGUID'].'</td>'; $data.='"'.$dbrow['NapkinGUID'].'"'."\t";}
	if ($list_NapkinCreateID=='yes')
		{
		echo '<td class="bluelink"><a style="font-size:xx-small" href="napkins_modify.php?NapkinCreateID='.$dbrow['NapkinCreateID'].'">'.$dbrow['NapkinCreateID'].'</a></td>';
		$data .= '"' . $dbrow['NapkinCreateID'] . '"' . "\t";
		}
	if ($list_NapkinArchiveID =='yes'){echo'<td>'.$dbrow['NapkinArchiveID'].'</td>'; $data.='"'.$dbrow['NapkinArchiveID'].'"'."\t";}
	if ($list_NapkinProject =='yes'){echo'<td>'.$dbrow['NapkinProject'].'</td>'; $data.='"'.$dbrow['NapkinProject'].'"'."\t";}
	if ($list_NapkinOwner =='yes'){echo'<td>'.$dbrow['NapkinOwner'].'</td>'; $data.='"'.$dbrow['NapkinOwner'].'"'."\t";}
	if ($list_NapkinType =='yes'){echo'<td>'.$dbrow['NapkinType'].'</td>'; $data.='"'.$dbrow['NapkinType'].'"'."\t";}
	if ($list_NapkinCategory =='yes'){echo'<td>'.$dbrow['NapkinCategory'].'</td>'; $data.='"'.$dbrow['NapkinCategory'].'"'."\t";}
	if ($list_NapkinName =='yes'){echo'<td>'.$dbrow['NapkinName'].'</td>'; $data.='"'.$dbrow['NapkinName'].'"'."\t";}
	if ($list_NapkinStatus =='yes'){echo'<td>'.$dbrow['NapkinStatus'].'</td>'; $data.='"'.$dbrow['NapkinStatus'].'"'."\t";}
	
	if($list_NapkinFilenames=='yes')		
		{
		if (!empty($dbrow['NapkinFilenames']))
			{
			$FilenameArray = explode("\n",$dbrow['NapkinFilenames']);
			$FilesizeArray = explode("\n",$dbrow['NapkinFilesizes']);
			$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['NapkinProject'].'/';
			echo '<td class="bluelink">';
			foreach ($FilenameArray as $key => $Filename) 
				{echo '<a href="'.$ProjectFilePath.$Filename.'">'.$Filename.'</a> ['.$FilesizeArray[$key].'kB] ';}
			echo '</td>';	
			}
		$data.='"'.$dbrow['NapkinFilenames'].'['.$dbrow['NapkinFilesizes'].'kB]'.'"'."\t"; // not adapted !!!
		}
	
	if ($list_NapkinRemarks =='yes'){echo'<td>'.$dbrow['NapkinRemarks'].'</td>'; $data.='"'.$dbrow['NapkinRemarks'].'"'."\t";}
	
	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)
	}
echo '<tr>';
echo '<td>'.$DatasetCount.' Datasets</td>';
echo '</tr>';	
echo '</table>';

echo '</form>';

$fp = fopen('napkinslist.xls','w');
fwrite($fp,$header);
fwrite($fp,"\n");
fwrite($fp,$data);
fclose($fp);

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2019-07-11 00:00</div>';4

?>
</body>
</html>
