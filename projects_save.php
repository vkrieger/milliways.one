<!doctype html public "-//W3C//DTD HTML 4.0 //EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>
<?php

include 'include_setProjectconstants.php';
include 'include_projects_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

$CurrentTimeStamp = date("Y-m-d H:i:s");

// deletedataset: puts current dataset in archive status by entering current datetime into ArchiveID
// updatedataset: puts current dataset in archive status by entering current datetime into ArchiveID and then createdataset

if (!empty($_POST["updatedataset"]) OR !empty($_POST["deletedataset"]))
	{	
	$dbchange = "UPDATE projects SET ProjectArchiveID = '$CurrentTimeStamp' WHERE ProjectCreateID = '$ProjectCreateID'";
	$dbquery = mysqli_query($link,$dbchange) or die ("not updated!");
	}
// updatedataset: creates new dataset with same GUID and with current datetime in CreateID
// createdataset: creates new dataset with new GUID and with current datetime in CreateID, leaving the current untouched  

if (!empty($_POST["updatedataset"]) OR !empty($_POST["createdataset"]))
	
	{
	if (!empty($_POST["createdataset"])) {$ProjectGUID = $CurrentTimeStamp;}
	$ProjectCreateID = $CurrentTimeStamp;
	$ProjectArchiveID='0000-00-00 00:00:00';
	
	$ProjectFilePath='/data/ftp/'.$dataftpfolder.'/'.$ProjectProject.'/'; 
	$ProjectInitialFilePath='/data/ftp/'.$dataftpfolder.'/'.$ProjectInitialProject.'/';
		
	if (!empty($_FILES['ProjectFilename']['tmp_name']))
        {if ( move_uploaded_file($_FILES['ProjectFilename']['tmp_name'], $ProjectFilePath.basename ($_FILES['ProjectFilename']['name'])))
			{ echo $ProjectFilename = basename ($_FILES['ProjectFilename']['name']); echo ' uploaded!<br>'; $MediaPDFsize = round($_FILES['ProjectFilename']['size']/1000);}
			else { echo ' error uploading!<br>'; }
		} else {if (!$existor){$ProjectFilename=''; $ProjectFilesize='';}} 

	if ((!empty ($ProjectFilename)) AND empty($_FILES['ProjectFilename']['tmp_name'])) 
    	{ 	
		$ProjectFilenameExist = $ProjectFilename;
		copy ($ProjectInitialFilePath.$ProjectFilenameExist,$ProjectFilePath.$ProjectFilename);
		}	

	$dbchange = "INSERT INTO projects SET
	
ProjectGUID = '$ProjectGUID',
ProjectCreateID = '$ProjectCreateID',
ProjectArchiveID = '$ProjectArchiveID',
ProjectProject = '$ProjectProject',
ProjectOwner = '$ProjectOwner',
ProjectType = '$ProjectType',
ProjectCategory = '$ProjectCategory',
ProjectName = '$ProjectName',
ProjectStatus = '$ProjectStatus',
ProjectAquisitionNo = '$ProjectAquisitionNo',
ProjectNumber = '$ProjectNumber',
ProjectCode = '$ProjectCode',
ProjectDescription = '$ProjectDescription',
ProjectCountry = '$ProjectCountry',
ProjectLocation = '$ProjectLocation',
ProjectClient = '$ProjectClient',
ProjectOrigin = '$ProjectOrigin',
ProjectBudget = '$ProjectBudget',
ProjectFigures = '$ProjectFigures',
ProjectPeriod = '$ProjectPeriod',
ProjectFee = '$ProjectFee',
ProjectCharges = '$ProjectCharges',
ProjectIntServices = '$ProjectIntServices',
ProjectExtServices = '$ProjectExtServices',
ProjectDirector = '$ProjectDirector',
ProjectManager = '$ProjectManager',
ProjectParticipants = '$ProjectParticipants',
ProjectPartners = '$ProjectPartners',
ProjectCompetitors = '$ProjectCompetitors',
ProjectSector = '$ProjectSector',
ProjectDepartment = '$ProjectDepartment',
ProjectChances = '$ProjectChances',
ProjectNextDeadline = '$ProjectNextDeadline',
ProjectStatusMemo = '$ProjectStatusMemo',
ProjectCallDate = '$ProjectCallDate',
ProjectCallMemo = '$ProjectCallMemo',
ProjectOfferDate = '$ProjectOfferDate',
ProjectOfferMemo = '$ProjectOfferMemo',
ProjectContractDate = '$ProjectContractDate',
ProjectContractMemo = '$ProjectContractMemo',
ProjectLostDate = '$ProjectLostDate',
ProjectLostMemo = '$ProjectLostMemo',
ProjectRemarks = '$ProjectRemarks',
ProjectFilename = '$ProjectFilename',
ProjectFilesize = '$ProjectFilesize'
		";
	$dbquery = mysqli_query($link,$dbchange) or die ("not created!");
	}

$_GET['ProjectCreateID']=$ProjectCreateID;	
include 'projects_modify.php';

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2019-07-11 18:00</div>';

?>
</body>
</html>

