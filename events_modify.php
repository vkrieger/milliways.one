<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	input, a							{ font-size:12px ; font-family: Arial, Verdana, sans-serif;}
	select,option,textarea 				{ font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td                         { font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	*  									{ font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

!!! Do <b>NOT</b> use semicolon (;) in input fields !!! <b>NO</b> blanks in filenames - use underscore ( _ ) instead!!! 
<br><br>

<?php
		
include 'include_setEventconstants.php';
include 'include_events_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

$EventCreateID = $_GET["EventCreateID"];

$dbquery = " SELECT * FROM events WHERE LOCATE ('$EventCreateID', EventCreateID) >0 ";
$dbresult = mysqli_query($link,$dbquery);
$dbrow = mysqli_fetch_array($dbresult);

	$updatedataset="";
	$createdataset="";
	$deletedataset="";
	$historydataset="";
	
if ($updatedataset == "" AND $createdataset == "" AND $deletedataset == "" AND $historydataset =="")
	{
	echo '<form method="post" action="events_save.php" enctype="multipart/form-data" >';
	if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
		{
		echo '<input type="submit" name="createdataset" value="create dataset">';
		echo '<input type="submit" name="updatedataset" value="update dataset">';
    	echo '<input type="submit" name="deletedataset" value="delete dataset">';
      	echo '<input type="reset" value="reset values">';
      	echo '<br><br>';
	    }

	echo '<table>';
	echo '<tr>';
	echo '<td align="right">GUID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="EventGUID" size="60" maxlength="100" value="'.$dbrow['EventGUID'].'" readonly></td>';
	echo '<td><input type="submit" name="historydataset" value="history of datasets"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">CreateID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="EventCreateID" size="60" maxlength="100" value="'.$dbrow['EventCreateID'].'" readonly></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">ArchiveID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="EventArchiveID" size="60" maxlength="100" value="'.$dbrow['EventArchiveID'].'" readonly></td>';
	echo '</tr>';
	echo '<input type="hidden" name="EventInitialProject" value="'.$dbrow['EventProject'].'">';
	echo '<input type="hidden" name="EventProject" value="'.$dbrow['EventProject'].'">';
	echo '<td align="right">Project</td>';
	echo '<td><select name="EventProject" size="1">';
		if (!empty($SystemProject))
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($SystemProject==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		else
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($dbrow['EventProject']==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		echo '</select></td>';		
		echo '<tr>';
	echo '<td align="right">Owner (readonly)</td>';
	echo '<td>'.'['.$dbrow['EventOwner'].'] - create/update transfers to '.'<input style="background-color:#C0C0C0" type="text" name="EventOwner" size="15" maxlength="100" value="'.$_SESSION['LoginLogin'].'" readonly></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Type</td>';
	echo '<td><select name="EventType" size="1">';
			foreach ($SystemTypeArray as $Type) {echo '<option'; if ($dbrow['EventType']==$Type) {echo ' selected';} echo '>'.$Type.'</option>';} 
			echo '</select></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Category</td>';
	echo '<td><select name="EventCategory" size="1">';
			foreach ($EventCategoryArray as $Category) {echo '<option'; if ($dbrow['EventCategory']==$Category) {echo ' selected';} echo '>'.$Category.'</option>';} 
			echo '</select></td>';
	echo '</tr>';

echo '<tr><td align="right">Begin</td><td><input type="text" name="EventBegin" size="60" maxlength="255" value="'.$dbrow['EventBegin'].'"></td></tr>';
echo '<tr><td align="right">Year</td><td><input type="text" name="EventYear" size="60" maxlength="255" value="'.$dbrow['EventYear'].'"></td></tr>';
echo '<tr><td align="right">Duration</td><td><input type="text" name="EventDuration" size="60" maxlength="255" value="'.$dbrow['EventDuration'].'"></td></tr>';
echo '<tr><td align="right">Name</td><td><input type="text" name="EventName" size="60" maxlength="255" value="'.$dbrow['EventName'].'"></td></tr>';
echo '<tr><td align="right">Content</td><td><input type="text" name="EventContent" size="60" maxlength="255" value="'.$dbrow['EventContent'].'"></td></tr>';
echo '<tr><td align="right">Location</td><td><input type="text" name="EventLocation" size="60" maxlength="255" value="'.$dbrow['EventLocation'].'"></td></tr>';
echo '<tr><td align="right">Address</td><td><input type="text" name="EventAddress" size="60" maxlength="255" value="'.$dbrow['EventAddress'].'"></td></tr>';
echo '<tr><td align="right">Country</td><td><input type="text" name="EventCountry" size="60" maxlength="255" value="'.$dbrow['EventCountry'].'"></td></tr>';
echo '<tr><td align="right">Language</td><td><input type="text" name="EventLanguage" size="60" maxlength="255" value="'.$dbrow['EventLanguage'].'"></td></tr>';

echo '<tr><td align="right">Host</td><td><textarea name="EventHost" cols="62" rows="4" value="'.$dbrow['EventHost'].'">'.$dbrow['EventHost'].'</textarea></td></tr>';
echo '<tr><td align="right">Sponsor</td><td><textarea name="EventSponsor" cols="62" rows="4" value="'.$dbrow['EventSponsor'].'">'.$dbrow['EventSponsor'].'</textarea></td></tr>';
echo '<tr><td align="right">Contact</td><td><textarea name="EventContact" cols="62" rows="4" value="'.$dbrow['EventContact'].'">'.$dbrow['EventContact'].'</textarea></td></tr>';
echo '<tr><td align="right">Registration</td><td><textarea name="EventRegistration" cols="62" rows="4" value="'.$dbrow['EventRegistration'].'">'.$dbrow['EventRegistration'].'</textarea></td></tr>';

echo '<tr><td align="right">Fee</td><td><input type="text" name="EventFee" size="60" maxlength="255" value="'.$dbrow['EventFee'].'"></td></tr>';
echo '<tr><td align="right">Web</td><td><input type="text" name="EventWeb" size="60" maxlength="255" value="'.$dbrow['EventWeb'].'"></td></tr>';
echo '<tr><td align="right">Status</td><td><input type="text" name="EventStatus" size="60" maxlength="255" value="'.$dbrow['EventStatus'].'"></td></tr>';

$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['EventProject'].'/';	

echo '<tr>';
echo '<td align="right">';
echo '<input type="checkbox" name="existor" value="File"'; if ($dbrow['EventFilename']) {echo ' checked';} echo '> Filename (max.10MB)</td>';
echo '<input type="hidden" name="EventFilename" value="'.$dbrow['EventFilename'].'">';
echo '<input type="hidden" name="EventFilesize" value="'.$dbrow['EventFilesize'].'">';
echo '<td align="left" class="bluelink"><a href="'.$ProjectFilePath.$dbrow['EventFilename'].'">'.$dbrow['EventFilename'].'</a> ['.$dbrow['EventFilesize'].'kB]</td>';
echo '<td><input type="file" name="EventFilename" size="15"></td>';
echo '</tr>';


echo '<tr><td align="right">Remarks </td><td><input type="text" name="EventRemarks" size="60" maxlength="255" value="'.$dbrow['EventRemarks'].'"></td></tr>';
	
	echo '</table>';

echo '</form>';

} elseif ( $updatedataset OR $createdataset OR $deletedataset OR $historydataset)

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2020-10-30 18:00</div>';

?>
</font>
</body>
</html>
