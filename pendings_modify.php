<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

!!! Do <b>NOT</b> use semicolon (;) in input fields !!! <b>NO</b> special characters and blanks in filenames - use underscore ( _ ) instead!!! 
<br><br>

<?php
		
include 'include_setPendingconstants.php';
include 'include_pendings_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

$PendingCreateID = $_GET["PendingCreateID"];

$dbquery = " SELECT * FROM pendings WHERE LOCATE ('$PendingCreateID', PendingCreateID) >0 ";
$dbresult = mysqli_query($link,$dbquery);
$dbrow = mysqli_fetch_array($dbresult);

	$updatedataset="";
	$createdataset="";
	$deletedataset="";
	$historydataset="";
	
if ($updatedataset == "" AND $createdataset == "" AND $deletedataset == "" AND $historydataset == "" )
	{
	echo '<form method="post" action="pendings_save.php" enctype="multipart/form-data" >';
	if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
		{
		echo '<input type="submit" name="createdataset" value="create dataset">';
		echo '<input type="submit" name="updatedataset" value="update dataset">';
    	echo '<input type="submit" name="deletedataset" value="delete dataset">';
      	echo '<input type="reset" value="reset values">';
      	echo '<br><br>';
	    }

	echo '<table>';
	echo '<tr>';
	echo '<td align="right">GUID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="PendingGUID" size="60" maxlength="100" value="'.$dbrow['PendingGUID'].'" readonly></td>';
	echo '<td><input type="submit" name="historydataset" value="history of datasets"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">CreateID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="PendingCreateID" size="60" maxlength="100" value="'.$dbrow['PendingCreateID'].'" readonly></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">ArchiveID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="PendingArchiveID" size="60" maxlength="100" value="'.$dbrow['PendingArchiveID'].'" readonly></td>';
	echo '</tr>';
	echo '<input type="hidden" name="PendingInitialProject" value="'.$dbrow['PendingProject'].'">';
	echo '<input type="hidden" name="PendingProject" value="'.$dbrow['PendingProject'].'">';
	echo '<tr>';
	echo '<td align="right">Project</td>';
	echo '<td><select name="PendingProject" size="1">';
		if (!empty($SystemProject))
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($SystemProject==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		else
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($dbrow['PendingProject']==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		echo '</select></td>';		
		echo '<tr>';
	echo '<td align="right">Raiser (readonly)</td>';
	echo '<td>'.'['.$dbrow['PendingRaiser'].'] - create transfers to '.'<input style="background-color:#C0C0C0" type="text" name="PendingRaiser" size="15" maxlength="100" value="'.$_SESSION['LoginLogin'].'" readonly></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Owner (readonly)</td>';
	echo '<td>'.'['.$dbrow['PendingOwner'].'] - create/update transfers to '.'<input style="background-color:#C0C0C0" type="text" name="PendingOwner" size="15" maxlength="100" value="'.$_SESSION['LoginLogin'].'" readonly></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Type</td>';
	echo '<td><select name="PendingType" size="1">';
			foreach ($SystemTypeArray as $Type) {echo '<option'; if ($dbrow['PendingType']==$Type) {echo ' selected';} echo '>'.$Type.'</option>';} 
			echo '</select></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Category</td>';
	echo '<td><select name="PendingCategory" size="1">';
			foreach ($PendingCategoryArray as $Category) {echo '<option'; if ($dbrow['PendingCategory']==$Category) {echo ' selected';} echo '>'.$Category.'</option>';} 
			echo '</select></td>';
	echo '</tr>';

	echo '<tr><td align="right">Name </td><td><input type="text" name="PendingName" size="60" maxlength="255" value="'.$dbrow['PendingName'].'"></td></tr>';
	
	echo '<tr>';
	echo '<td align="right">Status</td>';
	echo '<td><select name="PendingStatus" size="1">';
			foreach ($PendingStatusArray as $Status) {echo '<option'; if ($dbrow['PendingStatus']==$Status) {echo ' selected';} echo '>'.$Status.'</option>';} 
			echo '</select></td>';
	echo '</tr>';

	//echo '<tr><td align="right">Status </td><td><input type="text" name="PendingStatus" size="60" maxlength="255" value="'.$dbrow['PendingStatus'].'"></td></tr>';

echo '<tr><td align="right">Content</td><td><textarea name="PendingContent" cols="62" rows="4" value="'.$dbrow['PendingContent'].'">'.$dbrow['PendingContent'].'</textarea></td></tr>';
echo '<tr><td align="right">Task </td><td><input type="text" name="PendingTask" size="60" maxlength="255" value="'.$dbrow['PendingTask'].'"></td></tr>';
echo '<tr><td align="right">Responsible </td><td><input type="text" name="PendingResponsible" size="60" maxlength="255" value="'.$dbrow['PendingResponsible'].'"></td></tr>';
echo '<tr><td align="right">Process </td><td><input type="text" name="PendingProcess" size="60" maxlength="255" value="'.$dbrow['PendingProcess'].'"></td></tr>';
echo '<tr><td align="right">Deliverable </td><td><input type="text" name="PendingDeliverable" size="60" maxlength="255" value="'.$dbrow['PendingDeliverable'].'"></td></tr>';
echo '<tr><td align="right">Interim</td><td><textarea name="PendingInterim" cols="62" rows="4" value="'.$dbrow['PendingInterim'].'">'.$dbrow['PendingInterim'].'</textarea></td></tr>';
echo '<tr><td align="right">Result</td><td><textarea name="PendingResult" cols="62" rows="4" value="'.$dbrow['PendingResult'].'">'.$dbrow['PendingResult'].'</textarea></td></tr>';
echo '<tr><td align="right">Contract</td><td><textarea name="PendingContract" cols="62" rows="4" value="'.$dbrow['PendingContract'].'">'.$dbrow['PendingContract'].'</textarea></td></tr>';

$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['PendingProject'].'/';	

echo '<tr>';
echo '<td align="right">';
echo '<input type="checkbox" name="existor" value="File"'; if ($dbrow['PendingFilename']) {echo ' checked';} echo '> Filename (max.10MB)</td>';
echo '<input type="hidden" name="PendingFilename" value="'.$dbrow['PendingFilename'].'">';
echo '<input type="hidden" name="PendingFilesize" value="'.$dbrow['PendingFilesize'].'">';
echo '<td align="left" class="bluelink"><a href="'.$ProjectFilePath.$dbrow['PendingFilename'].'">'.$dbrow['PendingFilename'].'</a> ['.$dbrow['PendingFilesize'].'kB]</td>';
echo '<td><input type="file" name="PendingFilename" size="15"></td>';
echo '</tr>';

echo '<tr><td align="right">Remarks</td><td><textarea name="PendingRemarks" cols="62" rows="4" value="'.$dbrow['PendingRemarks'].'">'.$dbrow['PendingRemarks'].'</textarea></td></tr>';
	
	echo '</table>';

echo '</form>';

} elseif ( $updatedataset OR $createdataset OR $deletedataset OR $historydataset)

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2019-07-11 18:00</div>';

?>
</font>
</body>
</html>
