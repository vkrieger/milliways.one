<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

!!! Do <b>NOT</b> use semicolon (;) in input fields !!! <b>NO</b> special characters and blanks in filenames - use underscore ( _ ) instead!!! 
<br /><br />

<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
	
include 'include_setContactconstants.php';
include 'include_contacts_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

$ContactCreateID = $_GET["ContactCreateID"];

$dbquery = " SELECT * FROM contacts WHERE LOCATE ('$ContactCreateID', ContactCreateID) >0 ";
$dbresult = mysqli_query($link,$dbquery);
$dbrow = mysqli_fetch_array($dbresult);

	$updatedataset="";
	$createdataset="";
	$deletedataset="";
	$historydataset="";
	
if ($updatedataset == "" AND $createdataset == "" AND $deletedataset == ""  AND $historydataset == "" )
	{
	echo '<form method="post" action="contacts_save.php" enctype="multipart/form-data" >';
	if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
		{
		echo '<input type="submit" name="createdataset" value="create dataset">';
		echo '<input type="submit" name="updatedataset" value="update dataset">';
    	echo '<input type="submit" name="deletedataset" value="delete dataset">';
      	echo '<input type="reset" value="reset values">';
      	echo '<br /><br />';
	    }

	echo '<table>';
	
	echo '<tr>';
	echo '<td align="right">GUID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="ContactGUID" size="60" maxlength="100" value="'.$dbrow['ContactGUID'].'" readonly></td>';
	echo '<td><input type="submit" name="historydataset" value="history of datasets"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">CreateID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="ContactCreateID" size="60" maxlength="100" value="'.$dbrow['ContactCreateID'].'" readonly></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">ArchiveID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="ContactArchiveID" size="60" maxlength="100" value="'.$dbrow['ContactArchiveID'].'" readonly></td>';
	echo '</tr>';
	
	// This is necessary to save the Project in $AppInitialProject and AppProject which might be changed in dataset
	// Make sure the postvariables are defined
	echo '<input type="hidden" name="ContactInitialProject" value="'.$dbrow['ContactProject'].'">';
	echo '<input type="hidden" name="ContactProject" value="'.$dbrow['ContactProject'].'">';
		
	echo '<tr>';
	echo '<td align="right">Project</td>';
	// if SystemProject is not empty this should preselect the selected project 
	// but if this is called directly from listing no project is selected
	// then the SystemProject should be preselected
	// if SystemProject is empty this should preselect the selected project by the Project value of the selected dataset
	// but if this is called directly from listing public project is selected
	// SystemTypeArray and SystemRegimeArray are stored in SystemConstants
	/*
	from    list           modify
	empty   undef(public)  dbrow
	project system/dbrow   dbrow	
	*/
	echo '<td><select name="ContactProject" size="1">';
	if (!empty($SystemProject))
		{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($SystemProject==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		else
		{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($dbrow['ContactProject']==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		echo '</select></td>';			
	echo '</tr>';	
	
	echo '<tr>';
	echo '<td align="right">Owner (readonly)</td>';
	echo '<td>'.'['.$dbrow['ContactOwner'].'] - create/update transfers to '.'<input style="background-color:#C0C0C0" type="text" name="ContactOwner" size="15" maxlength="100" value="'.$_SESSION['LoginLogin'].'" readonly></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">Type</td>';
	echo '<td><select name="ContactType" size="1">';
		foreach ($SystemTypeArray as $Type) {echo '<option'; if ($dbrow['ContactType']==$Type) {echo ' selected';} echo '>'.$Type.'</option>';} 
	echo '</select></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">Category</td>';
	echo '<td><select name="ContactCategory" size="1">';
			foreach ($ContactCategoryArray as $Category) {echo '<option'; if ($dbrow['ContactCategory']==$Category) {echo ' selected';} echo '>'.$Category.'</option>';} 
			echo '</select></td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td align="right">Function</td>';
	echo '<td><select name="ContactFunction" size="1">';
			foreach ($ContactFunctionArray as $Function) {echo '<option'; if ($dbrow['ContactFunction']==$Function) {echo ' selected';} echo '>'.$Function.'</option>';} 
			echo '</select></td>';
	echo '</tr>';


/*
- existor has value "File" when $dbrow['AppFilename'] is given 
- $_FILES['AppFilename']['tmp_name'] is given
- when new AppFilename is uploaded existor should be checked
- when existor is unchecked AppFilename should be emptied
*/
// hidden input transfers dbrow[] to FileName and FileSize
// SystemProject would be empty in case of public and admin login

// must be dataftp instead of data/ftp and make use of Alias in http.conf !!!

	$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['ContactProject'].'/';	

	echo '<tr>';
	echo '<td align="right">Picture </td>';
	// content of $dbrow['ContactPic'] is saved in $ContactPic
	echo '<input type="hidden" name="ContactPic" value="'.$dbrow['ContactPic'].'">';
	// displays picture from file in dataftp
	echo '<td><img src="'.$ProjectFilePath.$dbrow['ContactPic'].'" height="80">  ';
	// allows upload of new picture
	// with $ContactPic prefilled
	echo '<input type="file" name="ContactPic" size="15"></td>';
	echo '</tr>';

	echo '<tr><td align="right">Firstname </td><td><input type="text" name="ContactFirstname" size="60" maxlength="255" value="'.$dbrow['ContactFirstname'].'"></td></tr>';

	echo '<tr><td align="right">Lastname </td><td><input type="text" name="ContactLastname" size="60" maxlength="255" value="'.$dbrow['ContactLastname'].'"></td></tr>';

	echo '<tr><td align="right">Email Prim </td><td><input type="text" name="ContactEmailPrim" size="60" maxlength="255" value="'.$dbrow['ContactEmailPrim'].'"></td></tr>';

	echo '<tr><td align="right">Email Sec </td><td><input type="text" name="ContactEmailSec" size="60" maxlength="255" value="'.$dbrow['ContactEmailSec'].'"></td></tr>';

	echo '<tr><td align="right">Web https://</td><td><input type="text" name="ContactWeb" size="60" maxlength="255" value="'.$dbrow['ContactWeb'].'"></td></tr>';

	echo '<tr><td align="right">Tel Business </td><td><input type="text" name="ContactTelBusiness" size="60" maxlength="255" value="'.$dbrow['ContactTelBusiness'].'"></td></tr>';
	
	echo '<tr><td align="right">Tel Private </td><td><input type="text" name="ContactTelPrivate" size="60" maxlength="255" value="'.$dbrow['ContactTelPrivate'].'"></td></tr>';

	echo '<tr><td align="right">Tel Mobile </td><td><input type="text" name="ContactTelMobile" size="60" maxlength="255" value="'.$dbrow['ContactTelMobile'].'"></td></tr>';

	echo '<tr><td align="right">Fax </td><td><input type="text" name="ContactFax" size="60" maxlength="255" value="'.$dbrow['ContactFax'].'"></td></tr>';

	echo '<tr><td align="right">Priv Contact</td><td><input type="text" name="ContactPrivate" size="60" maxlength="255" value="'.$dbrow['ContactPrivate'].'"></td></tr>';	
	echo '<tr><td align="right">Priv ZIP </td><td><input type="text" name="ContactPrivateZIP" size="60" maxlength="255" value="'.$dbrow['ContactPrivateZIP'].'"></td></tr>';
	echo '<tr><td align="right">Priv Town </td><td><input type="text" name="ContactPrivateTown" size="60" maxlength="255" value="'.$dbrow['ContactPrivateTown'].'"></td></tr>';
	echo '<tr><td align="right">Priv Country </td><td><input type="text" name="ContactPrivateCountry" size="60" maxlength="255" value="'.$dbrow['ContactPrivateCountry'].'"></td></tr>';

	echo '<tr><td align="right">Biz Contact </td><td><input type="text" name="ContactBusiness" size="60" maxlength="255" value="'.$dbrow['ContactBusiness'].'"></td></tr>';
	echo '<tr><td align="right">Biz ZIP </td><td><input type="text" name="ContactBusinessZIP" size="60" maxlength="255" value="'.$dbrow['ContactBusinessZIP'].'"></td></tr>';
	echo '<tr><td align="right">Biz Town </td><td><input type="text" name="ContactBusinessTown" size="60" maxlength="255" value="'.$dbrow['ContactBusinessTown'].'"></td></tr>';
	echo '<tr><td align="right">Biz Country </td><td><input type="text" name="ContactBusinessCountry" size="60" maxlength="255" value="'.$dbrow['ContactBusinessCountry'].'"></td></tr>';

	echo '<tr><td align="right">Sector </td><td><input type="text" name="ContactSector" size="60" maxlength="255" value="'.$dbrow['ContactSector'].'"></td></tr>';

	echo '<tr><td align="right">Organization </td><td><input type="text" name="ContactOrganization" size="60" maxlength="255" value="'.$dbrow['ContactOrganization'].'"></td></tr>';

	echo '<tr><td align="right">Department </td><td><input type="text" name="ContactDepartment" size="60" maxlength="255" value="'.$dbrow['ContactDepartment'].'"></td></tr>';

	echo '<tr><td align="right">Position </td><td><input type="text" name="ContactPosition" size="60" maxlength="255" value="'.$dbrow['ContactPosition'].'"></td></tr>';

	echo '<tr><td align="right">Remarks </td><td><textarea name="ContactRemarks" cols="120" rows="6" value="'.$dbrow['ContactRemarks'].'">'.$dbrow['ContactRemarks'].'</textarea></td></tr>';	

	echo '<tr><td align="right">CV </td><td><textarea name="ContactCV" cols="120" rows="6" value="'.$dbrow['ContactCV'].'">'.$dbrow['ContactCV'].'</textarea></td></tr>';
	
	echo '</table>';

echo '</form>';

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-06 24:00</div>';

} elseif ( $updatedataset OR $createdataset OR $deletedataset OR historydataset)

?>
</body>
</html>
