<?php

/*
criteria are transported into this php by POST. empty POST variable causes empty part-variable thus no criteria
empty variables cause warnings.
*/

//
if (!empty($_POST["last_sorted_by"])) 	{ $last_sorted_by  	= $_POST["last_sorted_by"];} 	else {$last_sorted_by  	= "ProgramGUID";}
if (!empty($_POST["first_sorted_by"])) 	{ $first_sorted_by  = $_POST["first_sorted_by"];} 	else {$first_sorted_by  = "ProgramCategory";}
if (!empty($_POST["then_sorted_by"])) 	{ $then_sorted_by  	= $_POST["then_sorted_by"];} 	else {$then_sorted_by  	= "ProgramOwner";}
if (!empty($_POST["existor"])) 			{ $existor  		= $_POST["existor"];} 			else {$existor  		= "";}


// generic variables
if (!empty($_POST['ProgramGUID'])) {$ProgramGUID =$_POST['ProgramGUID'];} else {$ProgramGUID ='';}
if (!empty($_POST['ProgramCreateID'])) {$ProgramCreateID =$_POST['ProgramCreateID'];} else {$ProgramCreateID ='';}
if (!empty($_POST['ProgramArchiveID'])) {$ProgramArchiveID =$_POST['ProgramArchiveID'];} else {$ProgramArchiveID ='';}
if (!empty($_POST['ProgramProject'])) {$ProgramProject =$_POST['ProgramProject'];} else {$ProgramProject ='';}
if (!empty($_POST['ProgramInitialProject'])) {$ProgramInitialProject =$_POST['ProgramInitialProject'];} else {$ProgramInitialProject ='';}
if (!empty($_POST['ProgramOwner'])) {$ProgramOwner =$_POST['ProgramOwner'];} else {$ProgramOwner ='';}
if (!empty($_POST['ProgramType'])) {$ProgramType =$_POST['ProgramType'];} else {$ProgramType ='';}
if (!empty($_POST['ProgramCategory'])) {$ProgramCategory =$_POST['ProgramCategory'];} else {$ProgramCategory ='';}
//if (!empty($_POST['ProgramStatus'])) {$ProgramStatus =$_POST['ProgramStatus'];} else {$ProgramStatus ='';}
if (!empty($_POST['ProgramBegin'])) {$ProgramBegin =$_POST['ProgramBegin'];} else {$ProgramBegin ='';}
if (!empty($_POST['ProgramYear'])) {$ProgramYear =$_POST['ProgramYear'];} else {$ProgramYear ='';}
if (!empty($_POST['ProgramDuration'])) {$ProgramDuration =$_POST['ProgramDuration'];} else {$ProgramDuration ='';}
if (!empty($_POST['ProgramName'])) {$ProgramName =$_POST['ProgramName'];} else {$ProgramName ='';}
if (!empty($_POST['ProgramTeaser'])) {$ProgramTeaser =$_POST['ProgramTeaser'];} else {$ProgramTeaser ='';}
if (!empty($_POST['ProgramIntro'])) {$ProgramIntro =$_POST['ProgramIntro'];} else {$ProgramIntro ='';}
if (!empty($_POST['ProgramTarget'])) {$ProgramTarget =$_POST['ProgramTarget'];} else {$ProgramTarget ='';}
if (!empty($_POST['ProgramPromo'])) {$ProgramPromo =$_POST['ProgramPromo'];} else {$ProgramPromo ='';}
if (!empty($_POST['ProgramText'])) {$ProgramText =$_POST['ProgramText'];} else {$ProgramText ='';}
if (!empty($_POST['ProgramContent'])) {$ProgramContent =$_POST['ProgramContent'];} else {$ProgramContent ='';}
if (!empty($_POST['ProgramLanguage'])) {$ProgramLanguage =$_POST['ProgramLanguage'];} else {$ProgramLanguage ='';}
//if (!empty($_POST['ProgramLocation'])) {$ProgramLocation =$_POST['ProgramLocation'];} else {$ProgramLocation ='';}
//if (!empty($_POST['ProgramAddress'])) {$ProgramAddress =$_POST['ProgramAddress'];} else {$ProgramAddress ='';}
//if (!empty($_POST['ProgramCountry'])) {$ProgramCountry =$_POST['ProgramCountry'];} else {$ProgramCountry ='';}
//if (!empty($_POST['ProgramHost'])) {$ProgramHost =$_POST['ProgramHost'];} else {$ProgramHost ='';}
//if (!empty($_POST['ProgramSponsor'])) {$ProgramSponsor =$_POST['ProgramSponsor'];} else {$ProgramSponsor ='';}
//if (!empty($_POST['ProgramContact'])) {$ProgramContact =$_POST['ProgramContact'];} else {$ProgramContact ='';}
//if (!empty($_POST['ProgramWeb'])) {$ProgramWeb =$_POST['ProgramWeb'];} else {$ProgramWeb ='';}
if (!empty($_POST['ProgramRegistration'])) {$ProgramRegistration =$_POST['ProgramRegistration'];} else {$ProgramRegistration ='';}
if (!empty($_POST['ProgramFee'])) {$ProgramFee =$_POST['ProgramFee'];} else {$ProgramFee ='';}
if (!empty($_POST['ProgramFilename'])) {$ProgramFilename =$_POST['ProgramFilename'];} else {$ProgramFilename ='';}
if (!empty($_POST['ProgramFilesize'])) {$ProgramFilesize =$_POST['ProgramFilesize'];} else {$ProgramFilesize ='';}
if (!empty($_POST['ProgramRemarks'])) {$ProgramRemarks =$_POST['ProgramRemarks'];} else {$ProgramRemarks ='';}


// list variables
if (!empty($_POST['list_ProgramGUID'])) {$list_ProgramGUID =$_POST['list_ProgramGUID'];} else {$list_ProgramGUID ='';}
if (!empty($_POST['list_ProgramCreateID'])) {$list_ProgramCreateID =$_POST['list_ProgramCreateID'];} else {$list_ProgramCreateID ='';}
if (!empty($_POST['list_ProgramArchiveID'])) {$list_ProgramArchiveID =$_POST['list_ProgramArchiveID'];} else {$list_ProgramArchiveID ='';}
if (!empty($_POST['list_ProgramProject'])) {$list_ProgramProject =$_POST['list_ProgramProject'];} else {$list_ProgramProject ='';}
if (!empty($_POST['list_ProgramOwner'])) {$list_ProgramOwner =$_POST['list_ProgramOwner'];} else {$list_ProgramOwner ='';}
if (!empty($_POST['list_ProgramType'])) {$list_ProgramType =$_POST['list_ProgramType'];} else {$list_ProgramType ='';}
if (!empty($_POST['list_ProgramCategory'])) {$list_ProgramCategory =$_POST['list_ProgramCategory'];} else {$list_ProgramCategory ='';}
// if (!empty($_POST['list_ProgramStatus'])) {$list_ProgramStatus =$_POST['list_ProgramStatus'];} else {$list_ProgramStatus ='';}
if (!empty($_POST['list_ProgramBegin'])) {$list_ProgramBegin =$_POST['list_ProgramBegin'];} else {$list_ProgramBegin ='';}
if (!empty($_POST['list_ProgramYear'])) {$list_ProgramYear =$_POST['list_ProgramYear'];} else {$list_ProgramYear ='';}
if (!empty($_POST['list_ProgramDuration'])) {$list_ProgramDuration =$_POST['list_ProgramDuration'];} else {$list_ProgramDuration ='';}
if (!empty($_POST['list_ProgramName'])) {$list_ProgramName =$_POST['list_ProgramName'];} else {$list_ProgramName ='';}
if (!empty($_POST['list_ProgramTeaser'])) {$list_ProgramTeaser =$_POST['list_ProgramTeaser'];} else {$list_ProgramTeaser ='';}
if (!empty($_POST['list_ProgramIntro'])) {$list_ProgramIntro =$_POST['list_ProgramIntro'];} else {$list_ProgramIntro ='';}
if (!empty($_POST['list_ProgramTarget'])) {$list_ProgramTarget =$_POST['list_ProgramTarget'];} else {$list_ProgramTarget ='';}
if (!empty($_POST['list_ProgramPromo'])) {$list_ProgramPromo =$_POST['list_ProgramPromo'];} else {$list_ProgramPromo ='';}
if (!empty($_POST['list_ProgramText'])) {$list_ProgramText =$_POST['list_ProgramText'];} else {$list_ProgramText ='';}
if (!empty($_POST['list_ProgramContent'])) {$list_ProgramContent =$_POST['list_ProgramContent'];} else {$list_ProgramContent ='';}
if (!empty($_POST['list_ProgramLanguage'])) {$list_ProgramLanguage =$_POST['list_ProgramLanguage'];} else {$list_ProgramLanguage ='';}
/*
if (!empty($_POST['list_ProgramLocation'])) {$list_ProgramLocation =$_POST['list_ProgramLocation'];} else {$list_ProgramLocation ='';}
if (!empty($_POST['list_ProgramAddress'])) {$list_ProgramAddress =$_POST['list_ProgramAddress'];} else {$list_ProgramAddress ='';}
if (!empty($_POST['list_ProgramCountry'])) {$list_ProgramCountry =$_POST['list_ProgramCountry'];} else {$list_ProgramCountry ='';}
if (!empty($_POST['list_ProgramHost'])) {$list_ProgramHost =$_POST['list_ProgramHost'];} else {$list_ProgramHost ='';}
if (!empty($_POST['list_ProgramSponsor'])) {$list_ProgramSponsor =$_POST['list_ProgramSponsor'];} else {$list_ProgramSponsor ='';}
if (!empty($_POST['list_ProgramContact'])) {$list_ProgramContact =$_POST['list_ProgramContact'];} else {$list_ProgramContact ='';}
if (!empty($_POST['list_ProgramWeb'])) {$list_ProgramWeb =$_POST['list_ProgramWeb'];} else {$list_ProgramWeb ='';}
*/
if (!empty($_POST['list_ProgramRegistration'])) {$list_ProgramRegistration =$_POST['list_ProgramRegistration'];} else {$list_ProgramRegistration ='';}
if (!empty($_POST['list_ProgramFee'])) {$list_ProgramFee =$_POST['list_ProgramFee'];} else {$list_ProgramFee ='';}
if (!empty($_POST['list_ProgramFilename'])) {$list_ProgramFilename =$_POST['list_ProgramFilename'];} else {$list_ProgramFilename ='';}
if (!empty($_POST['list_ProgramFilesize'])) {$list_ProgramFilesize =$_POST['list_ProgramFilesize'];} else {$list_ProgramFilesize ='';}
if (!empty($_POST['list_ProgramRemarks'])) {$list_ProgramRemarks =$_POST['list_ProgramRemarks'];} else {$list_ProgramRemarks ='';}

 // part variables
 
if (!empty($_POST['partProgramGUID'])) {$partProgramGUID =$_POST['partProgramGUID'];} else {$partProgramGUID ='';}
if (!empty($_POST['partProgramCreateID'])) {$partProgramCreateID =$_POST['partProgramCreateID'];} else {$partProgramCreateID ='';}
if (!empty($_POST['partProgramArchiveID'])) {$partProgramArchiveID =$_POST['partProgramArchiveID'];} else {$partProgramArchiveID ='';}
if (!empty($_POST['partProgramProject'])) {$partProgramProject =$_POST['partProgramProject'];} else {$partProgramProject ='';}
if (!empty($_POST['partProgramOwner'])) {$partProgramOwner =$_POST['partProgramOwner'];} else {$partProgramOwner ='';}
if (!empty($_POST['partProgramType'])) {$partProgramType =$_POST['partProgramType'];} else {$partProgramType ='';}
if (!empty($_POST['partProgramCategory'])) {$partProgramCategory =$_POST['partProgramCategory'];} else {$partProgramCategory ='';}
// if (!empty($_POST['partProgramStatus'])) {$partProgramStatus =$_POST['partProgramStatus'];} else {$partProgramStatus ='';}
if (!empty($_POST['partProgramBegin'])) {$partProgramBegin =$_POST['partProgramBegin'];} else {$partProgramBegin ='';}
if (!empty($_POST['partProgramYear'])) {$partProgramYear =$_POST['partProgramYear'];} else {$partProgramYear ='';}
if (!empty($_POST['partProgramDuration'])) {$partProgramDuration =$_POST['partProgramDuration'];} else {$partProgramDuration ='';}
if (!empty($_POST['partProgramName'])) {$partProgramName =$_POST['partProgramName'];} else {$partProgramName ='';}
if (!empty($_POST['partProgramTeaser'])) {$partProgramTeaser =$_POST['partProgramTeaser'];} else {$partProgramTeaser ='';}
if (!empty($_POST['partProgramIntro'])) {$partProgramIntro =$_POST['partProgramIntro'];} else {$partProgramIntro ='';}
if (!empty($_POST['partProgramTarget'])) {$partProgramTarget =$_POST['partProgramTarget'];} else {$partProgramTarget ='';}
if (!empty($_POST['partProgramPromo'])) {$partProgramPromo =$_POST['partProgramPromo'];} else {$partProgramPromo ='';}
if (!empty($_POST['partProgramText'])) {$partProgramText =$_POST['partProgramText'];} else {$partProgramText ='';}
if (!empty($_POST['partProgramContent'])) {$partProgramContent =$_POST['partProgramContent'];} else {$partProgramContent ='';}
if (!empty($_POST['partProgramLanguage'])) {$partProgramLanguage =$_POST['partProgramLanguage'];} else {$partProgramLanguage ='';}
/*
if (!empty($_POST['partProgramLocation'])) {$partProgramLocation =$_POST['partProgramLocation'];} else {$partProgramLocation ='';}
if (!empty($_POST['partProgramAddress'])) {$partProgramAddress =$_POST['partProgramAddress'];} else {$partProgramAddress ='';}
if (!empty($_POST['partProgramCountry'])) {$partProgramCountry =$_POST['partProgramCountry'];} else {$partProgramCountry ='';}
if (!empty($_POST['partProgramHost'])) {$partProgramHost =$_POST['partProgramHost'];} else {$partProgramHost ='';}
if (!empty($_POST['partProgramSponsor'])) {$partProgramSponsor =$_POST['partProgramSponsor'];} else {$partProgramSponsor ='';}
if (!empty($_POST['partProgramContact'])) {$partProgramContact =$_POST['partProgramContact'];} else {$partProgramContact ='';}
if (!empty($_POST['partProgramWeb'])) {$partProgramWeb =$_POST['partProgramWeb'];} else {$partProgramWeb ='';}
*/
if (!empty($_POST['partProgramRegistration'])) {$partProgramRegistration =$_POST['partProgramRegistration'];} else {$partProgramRegistration ='';}
if (!empty($_POST['partProgramFee'])) {$partProgramFee =$_POST['partProgramFee'];} else {$partProgramFee ='';}
if (!empty($_POST['partProgramFilename'])) {$partProgramFilename =$_POST['partProgramFilename'];} else {$partProgramFilename ='';}
if (!empty($_POST['partProgramFilesize'])) {$partProgramFilesize =$_POST['partProgramFilesize'];} else {$partProgramFilesize ='';}
if (!empty($_POST['partProgramRemarks'])) {$partProgramRemarks =$_POST['partProgramRemarks'];} else {$partProgramRemarks ='';}

 // last change vk 2021-02-07
?>
