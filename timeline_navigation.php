<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; background-color:<?php echo $SystemColor; ?>;}
	input                               {font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

timeline listing criteria<br>

<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

include 'include_setTimelineconstants.php';
include 'include_timeline_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

	$listdatasets = "";
	
if ($listdatasets == "" AND $_SESSION['LoginType'])
	{		
    echo '<form method="post" enctype="multipart/form-data" action="timeline_list.php" target="main">';
	
	$partTimelineArchiveID ='0000-00-00 00:00:00'; // to firstly display only non-archived datasets when navigation is called
	$startdate 	= time();
	$enddate 	= mktime(0, 0, 0, 12, 31, date("Y")); 

	echo '<table>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_TimelineGUID" value="yes" >GUID</td>';
	echo '<td><input type="text" name="partTimelineGUID" size="20" maxlength="40" value="'.$partTimelineGUID.'"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_TimelineCreateID" value="yes" checked>CreateID(Link)</td>';
	echo '<td><input type="text" name="partTimelineCreateID" size="20" maxlength="40" value="'.$partTimelineCreateID.'"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_TimelineArchiveID" value="yes" >ArchiveID</td>';
	echo '<td><input type="text" name="partTimelineArchiveID" size="20" maxlength="40" value="'.$partTimelineArchiveID.'"></td>';
	echo '</tr>';
	
	echo '</table>';
	
	echo '<table>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_TimelineProject" value="yes" checked>Project</td>';
	if (empty($SystemProject))
		{
		echo '<td><select type="text" name="partTimelineProject" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($SystemProjectArray as $Project) {echo '<option>'.$Project.'</option>';}
			echo '</select>';}
		else { echo '<input type="hidden" name="partTimelineProject" value="'.$SystemProject.'">';}
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_TimelineOwner" value="yes" >Owner</td>';
	echo '<td><input type="text" name="partTimelineOwner" size="8" maxlength="40" value="'.$partTimelineOwner.'"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td style="vertical-align:top"><input type="checkbox" name="list_TimelineName" value="yes" checked>Name</td>';
	echo '<td><select type="text" name="TimelineNameArray[]" size="5" multiple>';
		// showing all TimelineName entries 
		// in all timelines of this Project
		// remove double entries by DISTINCT
	if (empty($SystemProject)) {$whereproject="";} else {$whereproject = "WHERE TimelineProject = '".$SystemProject."'";}
	$dbquery = "SELECT DISTINCT TimelineName FROM timeline ".$whereproject." ORDER BY TimelineName";
	$dbresult = mysqli_query($link,$dbquery);  echo mysqli_error($link);
	while($dbrow = mysqli_fetch_array($dbresult)) 
		{echo '<option selected>'.$dbrow['TimelineName'].'</option>';}
		echo '</select>';
	echo '</td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_TimelineType" value="yes" >Type</td>';
	if ($SystemType == "public")
		{echo '<td><input style="background-color:#C0C0C0" type="text" name="partTimelineType" size="8" maxlength="40" value="public" readonly></td>';}
		else
		{echo '<td><input type="text" name="partTimelineType" size="8" maxlength="40" value="'.$partTimelineType.'"></td>';}
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_TimelineCategory" value="yes" >Category</td>';
	echo '<td><select type="text" name="partTimelineCategory" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($TimelineCategoryArray as $Category) {echo '<option>'.$Category.'</option>';}
			echo '</select>';
    echo '</td>';
	echo '</tr>';    
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_TimelineRemarks" value="yes" >Remarks</td>';
	echo '<td><input type="text" name="partTimelineRemarks" size="8" maxlength="40" value="'.$partTimelineRemarks.'"></td>';
	echo '</tr>';
	
	// here start and end date is asked : standard start is today
	echo '<tr>';
	echo '<td>start date</td>';
	echo '<td><input type="text" name="startdate" size="10" maxlength="40" value="'.date("Y-m-d",$startdate).'"</td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td>end date</td>';
	echo '<td><input type="text" name="enddate" size="10" maxlength="40" value="'.date("Y-m-d",$enddate).'"</td>';
	echo '</tr>';
	
	echo '</table>';

echo '<br />';

echo 'legend for listing';
echo '<table border="1" cellspacing="0">';
echo '<tr><td align="right" style="background-color:#FFFFFF">Open days</td>					<td style="background-color:#FFFFFF">0</td></tr>';
echo '<tr><td align="right" style="background-color:#FFFFFF">blocked / holidays</td>	<td style="background-color:#FE0000">X</td></tr>';
echo '<tr><td align="right" style="background-color:#FFFFFF">Seminar </td>		            <td style="background-color:#00FF00">S</td></tr>';
echo '</table>';

echo '<br />';

echo '<table>';
echo '<input type="submit" name="selection" value="list datasets">';
echo '<input type="reset" value="reset values">';
echo '</table>';

 echo '</form>';

} elseif ( $selection == "list datasets");

echo '<div align="right" style="font-size: 8px;">last source change vk 2021-02-08 18:00</div>';

?>

</body>
</html>
