<?php

/*
criteria are transported into this php by POST. empty POST variable causes empty part-variable thus no criteria
empty variables cause warnings.
*/

//
if (!empty($_POST["last_sorted_by"])) 	{ $last_sorted_by  	= $_POST["last_sorted_by"];} 	else {$last_sorted_by  	= "EventGUID";}
if (!empty($_POST["first_sorted_by"])) 	{ $first_sorted_by  = $_POST["first_sorted_by"];} 	else {$first_sorted_by  = "EventCategory";}
if (!empty($_POST["then_sorted_by"])) 	{ $then_sorted_by  	= $_POST["then_sorted_by"];} 	else {$then_sorted_by  	= "EventOwner";}
if (!empty($_POST["existor"])) 			{ $existor  		= $_POST["existor"];} 			else {$existor  		= "";}


// generic variables
if (!empty($_POST['EventGUID'])) {$EventGUID =$_POST['EventGUID'];} else {$EventGUID ='';}
if (!empty($_POST['EventCreateID'])) {$EventCreateID =$_POST['EventCreateID'];} else {$EventCreateID ='';}
if (!empty($_POST['EventArchiveID'])) {$EventArchiveID =$_POST['EventArchiveID'];} else {$EventArchiveID ='';}
if (!empty($_POST['EventProject'])) {$EventProject =$_POST['EventProject'];} else {$EventProject ='';}
if (!empty($_POST['EventInitialProject'])) {$EventInitialProject =$_POST['EventInitialProject'];} else {$EventInitialProject ='';}
if (!empty($_POST['EventOwner'])) {$EventOwner =$_POST['EventOwner'];} else {$EventOwner ='';}
if (!empty($_POST['EventType'])) {$EventType =$_POST['EventType'];} else {$EventType ='';}
if (!empty($_POST['EventCategory'])) {$EventCategory =$_POST['EventCategory'];} else {$EventCategory ='';}
if (!empty($_POST['EventBegin'])) {$EventBegin =$_POST['EventBegin'];} else {$EventBegin ='';}
if (!empty($_POST['EventYear'])) {$EventYear =$_POST['EventYear'];} else {$EventYear ='';}
if (!empty($_POST['EventDuration'])) {$EventDuration =$_POST['EventDuration'];} else {$EventDuration ='';}
if (!empty($_POST['EventName'])) {$EventName =$_POST['EventName'];} else {$EventName ='';}
if (!empty($_POST['EventContent'])) {$EventContent =$_POST['EventContent'];} else {$EventContent ='';}
if (!empty($_POST['EventLocation'])) {$EventLocation =$_POST['EventLocation'];} else {$EventLocation ='';}
if (!empty($_POST['EventAddress'])) {$EventAddress =$_POST['EventAddress'];} else {$EventAddress ='';}
if (!empty($_POST['EventCountry'])) {$EventCountry =$_POST['EventCountry'];} else {$EventCountry ='';}
if (!empty($_POST['EventLanguage'])) {$EventLanguage =$_POST['EventLanguage'];} else {$EventLanguage ='';}
if (!empty($_POST['EventHost'])) {$EventHost =$_POST['EventHost'];} else {$EventHost ='';}
if (!empty($_POST['EventSponsor'])) {$EventSponsor =$_POST['EventSponsor'];} else {$EventSponsor ='';}
if (!empty($_POST['EventContact'])) {$EventContact =$_POST['EventContact'];} else {$EventContact ='';}
if (!empty($_POST['EventRegistration'])) {$EventRegistration =$_POST['EventRegistration'];} else {$EventRegistration ='';}
if (!empty($_POST['EventFee'])) {$EventFee =$_POST['EventFee'];} else {$EventFee ='';}
if (!empty($_POST['EventWeb'])) {$EventWeb =$_POST['EventWeb'];} else {$EventWeb ='';}
if (!empty($_POST['EventStatus'])) {$EventStatus =$_POST['EventStatus'];} else {$EventStatus ='';}
if (!empty($_POST['EventFilename'])) {$EventFilename =$_POST['EventFilename'];} else {$EventFilename ='';}
if (!empty($_POST['EventFilesize'])) {$EventFilesize =$_POST['EventFilesize'];} else {$EventFilesize ='';}
if (!empty($_POST['EventRemarks'])) {$EventRemarks =$_POST['EventRemarks'];} else {$EventRemarks ='';}


// list variables
if (!empty($_POST['list_EventGUID'])) {$list_EventGUID =$_POST['list_EventGUID'];} else {$list_EventGUID ='';}
if (!empty($_POST['list_EventCreateID'])) {$list_EventCreateID =$_POST['list_EventCreateID'];} else {$list_EventCreateID ='';}
if (!empty($_POST['list_EventArchiveID'])) {$list_EventArchiveID =$_POST['list_EventArchiveID'];} else {$list_EventArchiveID ='';}
if (!empty($_POST['list_EventProject'])) {$list_EventProject =$_POST['list_EventProject'];} else {$list_EventProject ='';}
if (!empty($_POST['list_EventOwner'])) {$list_EventOwner =$_POST['list_EventOwner'];} else {$list_EventOwner ='';}
if (!empty($_POST['list_EventType'])) {$list_EventType =$_POST['list_EventType'];} else {$list_EventType ='';}
if (!empty($_POST['list_EventCategory'])) {$list_EventCategory =$_POST['list_EventCategory'];} else {$list_EventCategory ='';}
if (!empty($_POST['list_EventBegin'])) {$list_EventBegin =$_POST['list_EventBegin'];} else {$list_EventBegin ='';}
if (!empty($_POST['list_EventYear'])) {$list_EventYear =$_POST['list_EventYear'];} else {$list_EventYear ='';}
if (!empty($_POST['list_EventDuration'])) {$list_EventDuration =$_POST['list_EventDuration'];} else {$list_EventDuration ='';}
if (!empty($_POST['list_EventName'])) {$list_EventName =$_POST['list_EventName'];} else {$list_EventName ='';}
if (!empty($_POST['list_EventContent'])) {$list_EventContent =$_POST['list_EventContent'];} else {$list_EventContent ='';}
if (!empty($_POST['list_EventLocation'])) {$list_EventLocation =$_POST['list_EventLocation'];} else {$list_EventLocation ='';}
if (!empty($_POST['list_EventAddress'])) {$list_EventAddress =$_POST['list_EventAddress'];} else {$list_EventAddress ='';}
if (!empty($_POST['list_EventCountry'])) {$list_EventCountry =$_POST['list_EventCountry'];} else {$list_EventCountry ='';}
if (!empty($_POST['list_EventLanguage'])) {$list_EventLanguage =$_POST['list_EventLanguage'];} else {$list_EventLanguage ='';}
if (!empty($_POST['list_EventHost'])) {$list_EventHost =$_POST['list_EventHost'];} else {$list_EventHost ='';}
if (!empty($_POST['list_EventSponsor'])) {$list_EventSponsor =$_POST['list_EventSponsor'];} else {$list_EventSponsor ='';}
if (!empty($_POST['list_EventContact'])) {$list_EventContact =$_POST['list_EventContact'];} else {$list_EventContact ='';}
if (!empty($_POST['list_EventRegistration'])) {$list_EventRegistration =$_POST['list_EventRegistration'];} else {$list_EventRegistration ='';}
if (!empty($_POST['list_EventFee'])) {$list_EventFee =$_POST['list_EventFee'];} else {$list_EventFee ='';}
if (!empty($_POST['list_EventWeb'])) {$list_EventWeb =$_POST['list_EventWeb'];} else {$list_EventWeb ='';}
if (!empty($_POST['list_EventStatus'])) {$list_EventStatus =$_POST['list_EventStatus'];} else {$list_EventStatus ='';}
if (!empty($_POST['list_EventFilename'])) {$list_EventFilename =$_POST['list_EventFilename'];} else {$list_EventFilename ='';}
if (!empty($_POST['list_EventFilesize'])) {$list_EventFilesize =$_POST['list_EventFilesize'];} else {$list_EventFilesize ='';}
if (!empty($_POST['list_EventRemarks'])) {$list_EventRemarks =$_POST['list_EventRemarks'];} else {$list_EventRemarks ='';}

 // part variables
 
if (!empty($_POST['partEventGUID'])) {$partEventGUID =$_POST['partEventGUID'];} else {$partEventGUID ='';}
if (!empty($_POST['partEventCreateID'])) {$partEventCreateID =$_POST['partEventCreateID'];} else {$partEventCreateID ='';}
if (!empty($_POST['partEventArchiveID'])) {$partEventArchiveID =$_POST['partEventArchiveID'];} else {$partEventArchiveID ='';}
if (!empty($_POST['partEventProject'])) {$partEventProject =$_POST['partEventProject'];} else {$partEventProject ='';}
if (!empty($_POST['partEventOwner'])) {$partEventOwner =$_POST['partEventOwner'];} else {$partEventOwner ='';}
if (!empty($_POST['partEventType'])) {$partEventType =$_POST['partEventType'];} else {$partEventType ='';}
if (!empty($_POST['partEventCategory'])) {$partEventCategory =$_POST['partEventCategory'];} else {$partEventCategory ='';}
if (!empty($_POST['partEventBegin'])) {$partEventBegin =$_POST['partEventBegin'];} else {$partEventBegin ='';}
if (!empty($_POST['partEventYear'])) {$partEventYear =$_POST['partEventYear'];} else {$partEventYear ='';}
if (!empty($_POST['partEventDuration'])) {$partEventDuration =$_POST['partEventDuration'];} else {$partEventDuration ='';}
if (!empty($_POST['partEventName'])) {$partEventName =$_POST['partEventName'];} else {$partEventName ='';}
if (!empty($_POST['partEventContent'])) {$partEventContent =$_POST['partEventContent'];} else {$partEventContent ='';}
if (!empty($_POST['partEventLocation'])) {$partEventLocation =$_POST['partEventLocation'];} else {$partEventLocation ='';}
if (!empty($_POST['partEventAddress'])) {$partEventAddress =$_POST['partEventAddress'];} else {$partEventAddress ='';}
if (!empty($_POST['partEventCountry'])) {$partEventCountry =$_POST['partEventCountry'];} else {$partEventCountry ='';}
if (!empty($_POST['partEventLanguage'])) {$partEventLanguage =$_POST['partEventLanguage'];} else {$partEventLanguage ='';}
if (!empty($_POST['partEventHost'])) {$partEventHost =$_POST['partEventHost'];} else {$partEventHost ='';}
if (!empty($_POST['partEventSponsor'])) {$partEventSponsor =$_POST['partEventSponsor'];} else {$partEventSponsor ='';}
if (!empty($_POST['partEventContact'])) {$partEventContact =$_POST['partEventContact'];} else {$partEventContact ='';}
if (!empty($_POST['partEventRegistration'])) {$partEventRegistration =$_POST['partEventRegistration'];} else {$partEventRegistration ='';}
if (!empty($_POST['partEventFee'])) {$partEventFee =$_POST['partEventFee'];} else {$partEventFee ='';}
if (!empty($_POST['partEventWeb'])) {$partEventWeb =$_POST['partEventWeb'];} else {$partEventWeb ='';}
if (!empty($_POST['partEventStatus'])) {$partEventStatus =$_POST['partEventStatus'];} else {$partEventStatus ='';}
if (!empty($_POST['partEventFilename'])) {$partEventFilename =$_POST['partEventFilename'];} else {$partEventFilename ='';}
if (!empty($_POST['partEventFilesize'])) {$partEventFilesize =$_POST['partEventFilesize'];} else {$partEventFilesize ='';}
if (!empty($_POST['partEventRemarks'])) {$partEventRemarks =$_POST['partEventRemarks'];} else {$partEventRemarks ='';}

 // last change vk 2017-05-13
?>
