<?php

/*
criteria are transported into this php by POST. empty POST variable causes empty part-variable thus no criteria
empty variables cause warnings.
*/

//
if (!empty($_POST["last_sorted_by"])) 	{ $last_sorted_by  	= $_POST["last_sorted_by"];} 	else {$last_sorted_by  	= "NapkinGUID";}
if (!empty($_POST["first_sorted_by"])) 	{ $first_sorted_by  	= $_POST["first_sorted_by"];} 	else {$first_sorted_by  = "NapkinCategory";}
if (!empty($_POST["then_sorted_by"])) 	{ $then_sorted_by  	= $_POST["then_sorted_by"];} 	else {$then_sorted_by  	= "NapkinOwner";}
if (!empty($_POST["existor"])) 		{ $existor  		= $_POST["existor"];} 		else {$existor  	= "";}


// generic variables
if (!empty($_POST['NapkinGUID'])) {$NapkinGUID =$_POST['NapkinGUID'];} else {$NapkinGUID ='';}
if (!empty($_POST['NapkinCreateID'])) {$NapkinCreateID =$_POST['NapkinCreateID'];} else {$NapkinCreateID ='';}
if (!empty($_POST['NapkinArchiveID'])) {$NapkinArchiveID =$_POST['NapkinArchiveID'];} else {$NapkinArchiveID ='';}
if (!empty($_POST['NapkinProject'])) {$NapkinProject =$_POST['NapkinProject'];} else {$NapkinProject ='';}
if (!empty($_POST['NapkinInitialProject'])) {$NapkinInitialProject =$_POST['NapkinInitialProject'];} else {$NapkinInitialProject ='';}
if (!empty($_POST['NapkinOwner'])) {$NapkinOwner =$_POST['NapkinOwner'];} else {$NapkinOwner ='';}
if (!empty($_POST['NapkinType'])) {$NapkinType =$_POST['NapkinType'];} else {$NapkinType ='';}
if (!empty($_POST['NapkinCategory'])) {$NapkinCategory =$_POST['NapkinCategory'];} else {$NapkinCategory ='';}
if (!empty($_POST['NapkinName'])) {$NapkinName =$_POST['NapkinName'];} else {$NapkinName ='';}
if (!empty($_POST['NapkinStatus'])) {$NapkinStatus =$_POST['NapkinStatus'];} else {$NapkinStatus ='';}
if (!empty($_POST['NapkinFilenames'])) {$NapkinFilenames =$_POST['NapkinFilenames'];} else {$NapkinFilenames ='';}
if (!empty($_POST['NapkinFilesizes'])) {$NapkinFilesizes =$_POST['NapkinFilesizes'];} else {$NapkinFilesizes ='';}
if (!empty($_POST['NapkinRemarks'])) {$NapkinRemarks =$_POST['NapkinRemarks'];} else {$NapkinRemarks ='';}

// list variables
if (!empty($_POST['list_NapkinGUID'])) {$list_NapkinGUID =$_POST['list_NapkinGUID'];} else {$list_NapkinGUID ='';}
if (!empty($_POST['list_NapkinCreateID'])) {$list_NapkinCreateID =$_POST['list_NapkinCreateID'];} else {$list_NapkinCreateID ='';}
if (!empty($_POST['list_NapkinArchiveID'])) {$list_NapkinArchiveID =$_POST['list_NapkinArchiveID'];} else {$list_NapkinArchiveID ='';}
if (!empty($_POST['list_NapkinProject'])) {$list_NapkinProject =$_POST['list_NapkinProject'];} else {$list_NapkinProject ='';}
if (!empty($_POST['list_NapkinOwner'])) {$list_NapkinOwner =$_POST['list_NapkinOwner'];} else {$list_NapkinOwner ='';}
if (!empty($_POST['list_NapkinType'])) {$list_NapkinType =$_POST['list_NapkinType'];} else {$list_NapkinType ='';}
if (!empty($_POST['list_NapkinCategory'])) {$list_NapkinCategory =$_POST['list_NapkinCategory'];} else {$list_NapkinCategory ='';}
if (!empty($_POST['list_NapkinName'])) {$list_NapkinName =$_POST['list_NapkinName'];} else {$list_NapkinName ='';}
if (!empty($_POST['list_NapkinStatus'])) {$list_NapkinStatus =$_POST['list_NapkinStatus'];} else {$list_NapkinStatus ='';}
if (!empty($_POST['list_NapkinFilenames'])) {$list_NapkinFilenames =$_POST['list_NapkinFilenames'];} else {$list_NapkinFilenames ='';}
if (!empty($_POST['list_NapkinFilesizes'])) {$list_NapkinFilesizes =$_POST['list_NapkinFilesizes'];} else {$list_NapkinFilesizes ='';}
if (!empty($_POST['list_NapkinRemarks'])) {$list_NapkinRemarks =$_POST['list_NapkinRemarks'];} else {$list_NapkinRemarks ='';}

 // part variables
 
if (!empty($_POST['partNapkinGUID'])) {$partNapkinGUID =$_POST['partNapkinGUID'];} else {$partNapkinGUID ='';}
if (!empty($_POST['partNapkinCreateID'])) {$partNapkinCreateID =$_POST['partNapkinCreateID'];} else {$partNapkinCreateID ='';}
if (!empty($_POST['partNapkinArchiveID'])) {$partNapkinArchiveID =$_POST['partNapkinArchiveID'];} else {$partNapkinArchiveID ='';}
if (!empty($_POST['partNapkinProject'])) {$partNapkinProject =$_POST['partNapkinProject'];} else {$partNapkinProject ='';}
if (!empty($_POST['partNapkinOwner'])) {$partNapkinOwner =$_POST['partNapkinOwner'];} else {$partNapkinOwner ='';}
if (!empty($_POST['partNapkinType'])) {$partNapkinType =$_POST['partNapkinType'];} else {$partNapkinType ='';}
if (!empty($_POST['partNapkinCategory'])) {$partNapkinCategory =$_POST['partNapkinCategory'];} else {$partNapkinCategory ='';}
if (!empty($_POST['partNapkinName'])) {$partNapkinName =$_POST['partNapkinName'];} else {$partNapkinName ='';}
if (!empty($_POST['partNapkinStatus'])) {$partNapkinStatus =$_POST['partNapkinStatus'];} else {$partNapkinStatus ='';}
if (!empty($_POST['partNapkinFilenames'])) {$partNapkinFilenames =$_POST['partNapkinFilenames'];} else {$partNapkinFilenames ='';}
if (!empty($_POST['partNapkinFilesizes'])) {$partNapkinFilesizes =$_POST['partNapkinFilesizes'];} else {$partNapkinFilesizes ='';}
if (!empty($_POST['partNapkinRemarks'])) {$partNapkinRemarks =$_POST['partNapkinRemarks'];} else {$partNapkinRemarks ='';}

 // last change vk 21.05.2017
?>
