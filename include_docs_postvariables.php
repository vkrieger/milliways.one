<?php

/*
criteria are transported into this php by POST. empty POST variable causes empty part-variable thus no criteria
empty variables cause warnings.
*/

//
if (!empty($_POST["last_sorted_by"])) 	{ $last_sorted_by  	= $_POST["last_sorted_by"];} 	else {$last_sorted_by  	= "DocGUID";}
if (!empty($_POST["first_sorted_by"])) 	{ $first_sorted_by  = $_POST["first_sorted_by"];} 	else {$first_sorted_by  = "DocCategory";}
if (!empty($_POST["then_sorted_by"])) 	{ $then_sorted_by  	= $_POST["then_sorted_by"];} 	else {$then_sorted_by  	= "DocOwner";}
if (!empty($_POST["existor"])) 			{ $existor  		= $_POST["existor"];} 			else {$existor  		= "";}


// generic variables
if (!empty($_POST['DocGUID'])) {$DocGUID =$_POST['DocGUID'];} else {$DocGUID ='';}
if (!empty($_POST['DocCreateID'])) {$DocCreateID =$_POST['DocCreateID'];} else {$DocCreateID ='';}
if (!empty($_POST['DocArchiveID'])) {$DocArchiveID =$_POST['DocArchiveID'];} else {$DocArchiveID ='';}
if (!empty($_POST['DocProject'])) {$DocProject =$_POST['DocProject'];} else {$DocProject ='';}
if (!empty($_POST['DocInitialProject'])) {$DocInitialProject =$_POST['DocInitialProject'];} else {$DocInitialProject ='';}
if (!empty($_POST['DocOwner'])) {$DocOwner =$_POST['DocOwner'];} else {$DocOwner ='';}
if (!empty($_POST['DocType'])) {$DocType =$_POST['DocType'];} else {$DocType ='';}
if (!empty($_POST['DocCategory'])) {$DocCategory =$_POST['DocCategory'];} else {$DocCategory ='';}
if (!empty($_POST['DocName'])) {$DocName =$_POST['DocName'];} else {$DocName ='';}
if (!empty($_POST['DocStatus'])) {$DocStatus =$_POST['DocStatus'];} else {$DocStatus ='';}
if (!empty($_POST['DocFilename'])) {$DocFilename =$_POST['DocFilename'];} else {$DocFilename ='';}
if (!empty($_POST['DocFilesize'])) {$DocFilesize =$_POST['DocFilesize'];} else {$DocFilesize ='';}
if (!empty($_POST['DocRemarks'])) {$DocRemarks =$_POST['DocRemarks'];} else {$DocRemarks ='';}

// list variables
if (!empty($_POST['list_DocGUID'])) {$list_DocGUID =$_POST['list_DocGUID'];} else {$list_DocGUID ='';}
if (!empty($_POST['list_DocCreateID'])) {$list_DocCreateID =$_POST['list_DocCreateID'];} else {$list_DocCreateID ='';}
if (!empty($_POST['list_DocArchiveID'])) {$list_DocArchiveID =$_POST['list_DocArchiveID'];} else {$list_DocArchiveID ='';}
if (!empty($_POST['list_DocProject'])) {$list_DocProject =$_POST['list_DocProject'];} else {$list_DocProject ='';}
if (!empty($_POST['list_DocOwner'])) {$list_DocOwner =$_POST['list_DocOwner'];} else {$list_DocOwner ='';}
if (!empty($_POST['list_DocType'])) {$list_DocType =$_POST['list_DocType'];} else {$list_DocType ='';}
if (!empty($_POST['list_DocCategory'])) {$list_DocCategory =$_POST['list_DocCategory'];} else {$list_DocCategory ='';}
if (!empty($_POST['list_DocName'])) {$list_DocName =$_POST['list_DocName'];} else {$list_DocName ='';}
if (!empty($_POST['list_DocStatus'])) {$list_DocStatus =$_POST['list_DocStatus'];} else {$list_DocStatus ='';}
if (!empty($_POST['list_DocFilename'])) {$list_DocFilename =$_POST['list_DocFilename'];} else {$list_DocFilename ='';}
if (!empty($_POST['list_DocFilesize'])) {$list_DocFilesize =$_POST['list_DocFilesize'];} else {$list_DocFilesize ='';}
if (!empty($_POST['list_DocRemarks'])) {$list_DocRemarks =$_POST['list_DocRemarks'];} else {$list_DocRemarks ='';}

 // part variables
 
if (!empty($_POST['partDocGUID'])) {$partDocGUID =$_POST['partDocGUID'];} else {$partDocGUID ='';}
if (!empty($_POST['partDocCreateID'])) {$partDocCreateID =$_POST['partDocCreateID'];} else {$partDocCreateID ='';}
if (!empty($_POST['partDocArchiveID'])) {$partDocArchiveID =$_POST['partDocArchiveID'];} else {$partDocArchiveID ='';}
if (!empty($_POST['partDocProject'])) {$partDocProject =$_POST['partDocProject'];} else {$partDocProject ='';}
if (!empty($_POST['partDocOwner'])) {$partDocOwner =$_POST['partDocOwner'];} else {$partDocOwner ='';}
if (!empty($_POST['partDocType'])) {$partDocType =$_POST['partDocType'];} else {$partDocType ='';}
if (!empty($_POST['partDocCategory'])) {$partDocCategory =$_POST['partDocCategory'];} else {$partDocCategory ='';}
if (!empty($_POST['partDocName'])) {$partDocName =$_POST['partDocName'];} else {$partDocName ='';}
if (!empty($_POST['partDocStatus'])) {$partDocStatus =$_POST['partDocStatus'];} else {$partDocStatus ='';}
if (!empty($_POST['partDocFilename'])) {$partDocFilename =$_POST['partDocFilename'];} else {$partDocFilename ='';}
if (!empty($_POST['partDocFilesize'])) {$partDocFilesize =$_POST['partDocFilesize'];} else {$partDocFilesize ='';}
if (!empty($_POST['partDocRemarks'])) {$partDocRemarks =$_POST['partDocRemarks'];} else {$partDocRemarks ='';}

 
?>
