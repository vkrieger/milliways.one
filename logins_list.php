<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input                               {font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

	You are Owner of the listed datasets. Select to modify! <br>
	You may download <a href="loginslist.xls">this Table</a> in Spreadsheet-Format (e.g. as CSV for MS-Excel) from Server.<br>
	<br>

<?php
	
include 'include_logins_postvariables.php';
include 'include_dbconnect.php';
/*
select datasets according to criteria
*/

// select all logins of owner
$LoginOwner=$LoginLogin; // Owner is unknown yet - only Login is given by query
$dbquery = "SELECT * FROM logins WHERE '$LoginOwner'=LoginOwner " ;
$dbresult = mysqli_query($link,$dbquery);  echo mysqli_error($link);

$header =''; //empty header for csv file (dataset structure)
$data	=''; //empty $data variable for csv file (all rows in one variable)

echo '<table border="1" cellspacing="0">';
echo '<tr>';

// listing header without LoginPassword
echo '<td>GUID</td>'; $header .="GUID". "\t";
echo '<td>CreateID</td>'; $header .="CreateID". "\t";
echo '<td>ArchiveID</td>'; $header .="ArchiveID". "\t";
echo '<td>Owner</td>'; $header .="Owner". "\t";
echo '<td>Login</td>'; $header .="Login". "\t";
echo '<td>Type</td>'; $header .="Type". "\t";
echo '<td>Project</td>'; $header .="Project". "\t";
echo '<td>Remarks</td>'; $header .="Remarks". "\t";
echo '<td>Lastname</td>'; $header .="Lastname". "\t";
echo '<td>Firstname</td>'; $header .="Firstname". "\t";
echo '<td>Email</td>'; $header .="Email". "\t";
echo '<td>Tel</td>'; $header .="Tel". "\t";
echo '<td>Mobile</td>'; $header .="Mobile". "\t";
echo '<td>Organization</td>'; $header .="Organization". "\t";

echo '</tr>';

// outputs datasets and fill $data variable without list criteria without LoginPassword
while($dbrow = mysqli_fetch_array($dbresult))
   	{
	echo'<tr>';
	echo'<td>'.$dbrow['LoginGUID'].'</td>'; $data.='"'.$dbrow['LoginGUID'].'"'."\t";
	echo'<td class="bluelink"><a style="font-size:xx-small" href="logins_modify.php?LoginCreateID='.$dbrow['LoginCreateID'].'">'.$dbrow['LoginCreateID'].'</a></td>'; $data.='"'.$dbrow['LoginCreateID'].'"'."\t";
	echo'<td>'.$dbrow['LoginArchiveID'].'</td>'; $data.='"'.$dbrow['LoginArchiveID'].'"'."\t";
	echo'<td>'.$dbrow['LoginOwner'].'</td>'; $data.='"'.$dbrow['LoginOwner'].'"'."\t";
	echo'<td>'.$dbrow['LoginLogin'].'</td>'; $data.='"'.$dbrow['LoginLogin'].'"'."\t";
	echo'<td>'.$dbrow['LoginType'].'</td>'; $data.='"'.$dbrow['LoginType'].'"'."\t";
	echo'<td>'.$dbrow['LoginProject'].'</td>'; $data.='"'.$dbrow['LoginProject'].'"'."\t";
	echo'<td>'.$dbrow['LoginRemarks'].'</td>'; $data.='"'.$dbrow['LoginRemarks'].'"'."\t";
	echo'<td>'.$dbrow['LoginLastname'].'</td>'; $data.='"'.$dbrow['LoginLastname'].'"'."\t";
	echo'<td>'.$dbrow['LoginFirstname'].'</td>'; $data.='"'.$dbrow['LoginFirstname'].'"'."\t";
	echo'<td>'.$dbrow['LoginPrimaryEmail'].'</td>'; $data.='"'.$dbrow['LoginPrimaryEmail'].'"'."\t";
	echo'<td>'.$dbrow['LoginTelBusiness'].'</td>'; $data.='"'.$dbrow['LoginTelBusiness'].'"'."\t";
	echo'<td>'.$dbrow['LoginTelMobile'].'</td>'; $data.='"'.$dbrow['LoginTelMobile'].'"'."\t";
	echo'<td>'.$dbrow['LoginOrganization'].'</td>'; $data.='"'.$dbrow['LoginOrganization'].'"'."\t";
	echo'</tr>';
   	}

$data .= "\n"; //end of dataset in excel file (marks row end in data) 
echo '</table>';

// export selected table to .xls onto local server folder to download as offered from there

$fp = fopen('loginslist.xls','w');
fwrite($fp,$header);
fwrite($fp,"\n");
fwrite($fp,$data);
fclose($fp);



echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2019-07-11 18:00</div>';

?>
</body>
</html>
