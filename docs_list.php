<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

You may download <a href="docslist.xls">this Table</a> in Spreadsheet-Format (e.g. as CSV for MS-Excel) from Server.
<br /><br />

<?php

include 'include_setDocconstants.php';
include 'include_docs_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

// <form></form> must enclose all <input> to transfer to next app
echo '<form method="post" enctype="multipart/form-data" action="docs_save.php">';
	
if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
	{
	echo '<input type="submit" name="createdataset" value="create dataset">';
	}

echo '<br /><br />';

/*
select datasets according to criteria
add datasets that are from all other Projects but of Type public
*/


  	$dbquery = "SELECT * FROM docs WHERE

LOCATE('$partDocGUID', DocGUID)>0 AND
LOCATE('$partDocCreateID', DocCreateID)>0 AND
LOCATE('$partDocArchiveID', DocArchiveID)>0 AND
(LOCATE('$partDocProject', DocProject)>0 OR LOCATE('public', DocType)>0) AND
LOCATE('$partDocOwner', DocOwner)>0 AND
LOCATE('$partDocType', DocType)>0  AND 
LOCATE('$partDocCategory', DocCategory)>0 AND
LOCATE('$partDocName', DocName)>0 AND
LOCATE('$partDocStatus', DocStatus)>0 AND
LOCATE('$partDocFilename', DocFilename)>0 AND
LOCATE('$partDocRemarks', DocRemarks)>0

ORDER BY $first_sorted_by, $then_sorted_by, $last_sorted_by " ;
	
  	$dbresult = mysqli_query($link,$dbquery);  echo mysqli_error($link);
  	
  	$header =""; //empty header for excel file (dataset structure)
  	$data	=""; //empty $data variable for excel file (all rows in one variable)

echo '<table border="1" cellspacing="0">';
 
echo '<tr>';

if ($list_DocGUID =='yes'){echo '<td>GUID</td>'; $header.="GUID". "\t";}
if ($list_DocCreateID =='yes'){echo '<td>CreateID</td>'; $header.="CreateID". "\t";}
if ($list_DocArchiveID =='yes'){echo '<td>ArchiveID</td>'; $header.="ArchiveID". "\t";}
if ($list_DocProject =='yes'){echo '<td>Project</td>'; $header.="Project". "\t";}
if ($list_DocOwner =='yes'){echo '<td>Owner</td>'; $header.="Owner". "\t";}
if ($list_DocType =='yes'){echo '<td>Type</td>'; $header.="Type". "\t";}
if ($list_DocCategory =='yes'){echo '<td>Category</td>'; $header.="Category". "\t";}
if ($list_DocName =='yes'){echo '<td>Name</td>'; $header.="Name". "\t";}
if ($list_DocStatus =='yes'){echo '<td>Status</td>'; $header.="Status". "\t";}
if ($list_DocFilename =='yes'){echo '<td>Filename[kB]</td>'; $header.="Filename[kB]". "\t";}
if ($list_DocRemarks =='yes'){echo '<td>Remarks</td>'; $header.="Remarks". "\t";}

	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)


/*
outputs datasets and fills $data variable
*/

$DatasetCount = 0;

// control code echo $SystemProject.'<br>';

  	while($dbrow = mysqli_fetch_array($dbresult))
   	{
	$DatasetCount += 1;
	
	// output to main window and selected data to $data excel variable
   	echo '<tr>';
if ($list_DocGUID =='yes'){echo'<td>'.$dbrow['DocGUID'].'</td>'; $data.='"'.$dbrow['DocGUID'].'"'."\t";}
if ($list_DocCreateID=='yes')
	{
	echo '<td class="bluelink"><a style="font-size:xx-small" href="docs_modify.php?DocCreateID='.$dbrow['DocCreateID'].'">'.$dbrow['DocCreateID'].'</a></td>';
	$data .= '"' . $dbrow['DocCreateID'] . '"' . "\t";
	}
if ($list_DocArchiveID =='yes'){echo'<td>'.$dbrow['DocArchiveID'].'</td>'; $data.='"'.$dbrow['DocArchiveID'].'"'."\t";}
if ($list_DocProject =='yes'){echo'<td>'.$dbrow['DocProject'].'</td>'; $data.='"'.$dbrow['DocProject'].'"'."\t";}
if ($list_DocOwner =='yes'){echo'<td>'.$dbrow['DocOwner'].'</td>'; $data.='"'.$dbrow['DocOwner'].'"'."\t";}
if ($list_DocType =='yes'){echo'<td>'.$dbrow['DocType'].'</td>'; $data.='"'.$dbrow['DocType'].'"'."\t";}
if ($list_DocCategory =='yes'){echo'<td>'.$dbrow['DocCategory'].'</td>'; $data.='"'.$dbrow['DocCategory'].'"'."\t";}
if ($list_DocName =='yes'){echo'<td>'.$dbrow['DocName'].'</td>'; $data.='"'.$dbrow['DocName'].'"'."\t";}
if ($list_DocStatus =='yes'){echo'<td>'.$dbrow['DocStatus'].'</td>'; $data.='"'.$dbrow['DocStatus'].'"'."\t";}
	
if($list_DocFilename=='yes')		
	{
	$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['DocProject'].'/';
	
	echo	'<td class="bluelink">'
			.'<a style="font-size:xx-small" href="'
			.$ProjectFilePath
			.$dbrow['DocFilename']
			.'">'
			.$dbrow['DocFilename'].'['.$dbrow['DocFilesize'].'kB]'
			.'</a>';
	$data.='"'.$dbrow['DocFilename'].'['.$dbrow['DocFilesize'].'kB]'.'"'."\t";
	}
	
if ($list_DocRemarks =='yes'){echo'<td>'.$dbrow['DocRemarks'].'</td>'; $data.='"'.$dbrow['DocRemarks'].'"'."\t";}
	
	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)
	}
echo '<tr>';
echo '<td>'.$DatasetCount.' Datasets</td>';
echo '</tr>';	
echo '</table>';

echo '</form>';
	
/*
export selected table to .xls onto local server folder to download as offered from there
*/

$fp = fopen('docslist.xls','w');
fwrite($fp,$header);
fwrite($fp,"\n");
fwrite($fp,$data);
fclose($fp);

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-03 18:00</div>';

?>
</body>
</html>
