<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	input								{ font-size:12px ; font-family: Arial, Verdana, sans-serif;}
	select,option,textarea 				{ font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td                         { font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	*  									{ font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>
<?php

include 'include_setEventconstants.php';
include 'include_events_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

if (!empty($_POST["historydataset"])) 
	{
	$_POST['partEventGUID']=$EventGUID;	
	$_POST['partEventArchiveID']='';
	$_POST['list_EventGUID']='yes';	
	$_POST['list_EventCreateID']='yes';	
	$_POST['list_EventArchiveID']='yes';	
	$_POST['list_EventOwner']='yes';	
	$_POST['list_EventName']='yes';	
	$_POST['list_EventStatus']='yes';	
	include 'events_list.php';
	}

$CurrentTimeStamp = date("Y-m-d H:i:s");

// deletedataset: puts current dataset in archive status by entering current datetime into ArchiveID
// updatedataset: puts current dataset in archive status by entering current datetime into ArchiveID and then createdataset

if (!empty($_POST["updatedataset"]) OR !empty($_POST["deletedataset"]))
	{	
	$dbchange = "UPDATE events SET EventArchiveID = '$CurrentTimeStamp' WHERE EventCreateID = '$EventCreateID'";
	$dbquery = mysqli_query($link,$dbchange) or die ("not updated!");
	}
// updatedataset: creates new dataset with same GUID and with current datetime in CreateID
// createdataset: creates new dataset with new GUID and with current datetime in CreateID, leaving the current untouched  

if (!empty($_POST["updatedataset"]) OR !empty($_POST["createdataset"]))
	
	{
	if (!empty($_POST["createdataset"])) {$EventGUID = $CurrentTimeStamp;}
	$EventCreateID = $CurrentTimeStamp;
	$EventArchiveID='0000-00-00 00:00:00';
	
	$ProjectFilePath='/data/ftp/'.$dataftpfolder.'/'.$EventProject.'/'; 
	$ProjectInitialFilePath='/data/ftp/'.$dataftpfolder.'/'.$EventInitialProject.'/';
	
	if (!empty($_FILES['EventFilename']['tmp_name']))
        {if ( move_uploaded_file($_FILES['EventFilename']['tmp_name'], $ProjectFilePath.basename ($_FILES['EventFilename']['name'])))
			{ echo $EventFilename = basename ($_FILES['EventFilename']['name']); echo ' uploaded!<br>'; $MediaPDFsize = round($_FILES['EventFilename']['size']/1000);}
			else { echo ' error uploading!<br>'; }
		} else {if (!$existor){$EventFilename=''; $EventFilesize='';}} 

	if ((!empty ($EventFilename)) AND empty($_FILES['EventFilename']['tmp_name'])) 
    	{ 	
		$EventFilenameExist = $EventFilename;
		copy ($ProjectInitialFilePath.$EventFilenameExist,$ProjectFilePath.$EventFilename);
		}	

	$dbchange = "INSERT INTO events SET

EventGUID = '$EventGUID',
EventCreateID = '$EventCreateID',
EventArchiveID = '$EventArchiveID',
EventProject = '$EventProject',
EventOwner = '$EventOwner',
EventType = '$EventType',
EventCategory = '$EventCategory',
EventBegin = '$EventBegin',
EventYear = '$EventYear',
EventDuration = '$EventDuration',
EventName = '$EventName',
EventContent = '$EventContent',
EventLocation = '$EventLocation',
EventAddress = '$EventAddress',
EventCountry = '$EventCountry',
EventLanguage = '$EventLanguage',
EventHost = '$EventHost',
EventSponsor = '$EventSponsor',
EventContact = '$EventContact',
EventRegistration = '$EventRegistration',
EventFee = '$EventFee',
EventWeb = '$EventWeb',
EventStatus = '$EventStatus',
EventFilename = '$EventFilename',
EventFilesize = '$EventFilesize',
EventRemarks = '$EventRemarks'
		";
	$dbquery = mysqli_query($link,$dbchange) or die ("not created!");
	}

$_GET['EventCreateID']=$EventCreateID;	
include 'events_modify.php';

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2020-10-30 14:00</div>';

?>
</body>
</html>

