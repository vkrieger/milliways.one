<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input                               {font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

You may download <a href="eventslist.xls">this Table</a> in Spreadsheet-Format (e.g. as CSV for MS-Excel) from Server.
<br><br>

<?php

include 'include_setEventconstants.php';
include 'include_events_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
	{
	echo '<form method="post" enctype="multipart/form-data" action="events_save.php">';
	echo '<input type="submit" name="createdataset" value="create dataset">';
	echo '</form>';
	}


/*
select datasets according to criteria. 
*/

  	$dbquery = "SELECT * FROM events WHERE

LOCATE('$partEventGUID', EventGUID)>0 AND
LOCATE('$partEventCreateID', EventCreateID)>0 AND
LOCATE('$partEventArchiveID', EventArchiveID)>0 AND
(LOCATE('$partEventProject', EventProject)>0  OR LOCATE('public', EventType)>0) AND
LOCATE('$partEventOwner', EventOwner)>0 AND
LOCATE('$partEventType', EventType)>0 AND
LOCATE('$partEventCategory', EventCategory)>0 AND
LOCATE('$partEventBegin', EventBegin)>0 AND
LOCATE('$partEventYear', EventYear)>0 AND
LOCATE('$partEventDuration', EventDuration)>0 AND
LOCATE('$partEventName', EventName)>0 AND
LOCATE('$partEventContent', EventContent)>0 AND
LOCATE('$partEventLocation', EventLocation)>0 AND
LOCATE('$partEventAddress', EventAddress)>0 AND
LOCATE('$partEventCountry', EventCountry)>0 AND
LOCATE('$partEventLanguage', EventLanguage)>0 AND
LOCATE('$partEventHost', EventHost)>0 AND
LOCATE('$partEventSponsor', EventSponsor)>0 AND
LOCATE('$partEventContact', EventContact)>0 AND
LOCATE('$partEventRegistration', EventRegistration)>0 AND
LOCATE('$partEventFee', EventFee)>0 AND
LOCATE('$partEventWeb', EventWeb)>0 AND
LOCATE('$partEventStatus', EventStatus)>0 AND
LOCATE('$partEventFilename', EventFilename)>0 AND
LOCATE('$partEventRemarks', EventRemarks)>0

ORDER BY $first_sorted_by, $then_sorted_by, $last_sorted_by " ;
	
  	$dbresult = mysqli_query($link,$dbquery);  echo mysqli_error($link);
  	
  	$header =""; //empty header for excel file (dataset structure)
  	$data	=""; //empty $data variable for excel file (all rows in one variable)

echo '<table border="1" cellspacing="0">';
 
echo '<tr>';

if ($list_EventGUID =='yes'){echo '<td>GUID</td>'; $header.="GUID". "\t";}
if ($list_EventCreateID =='yes'){echo '<td>CreateID</td>'; $header.="CreateID". "\t";}
if ($list_EventArchiveID =='yes'){echo '<td>ArchiveID</td>'; $header.="ArchiveID". "\t";}
if ($list_EventProject =='yes'){echo '<td>Project</td>'; $header.="Project". "\t";}
if ($list_EventOwner =='yes'){echo '<td>Owner</td>'; $header.="Owner". "\t";}
if ($list_EventType =='yes'){echo '<td>Type</td>'; $header.="Type". "\t";}
if ($list_EventCategory =='yes'){echo '<td>Category</td>'; $header.="Category". "\t";}
if ($list_EventBegin =='yes'){echo '<td>Begin</td>'; $header.="Begin". "\t";}
if ($list_EventYear =='yes'){echo '<td>Year</td>'; $header.="Year". "\t";}
if ($list_EventDuration =='yes'){echo '<td>Duration</td>'; $header.="Duration". "\t";}
if ($list_EventName =='yes'){echo '<td>Name</td>'; $header.="Name". "\t";}
if ($list_EventContent =='yes'){echo '<td>Content</td>'; $header.="Content". "\t";}
if ($list_EventLocation =='yes'){echo '<td>Location</td>'; $header.="Location". "\t";}
if ($list_EventAddress =='yes'){echo '<td>Address</td>'; $header.="Address". "\t";}
if ($list_EventCountry =='yes'){echo '<td>Country</td>'; $header.="Country". "\t";}
if ($list_EventLanguage =='yes'){echo '<td>Language</td>'; $header.="Language". "\t";}
if ($list_EventHost =='yes'){echo '<td>Host</td>'; $header.="Host". "\t";}
if ($list_EventSponsor =='yes'){echo '<td>Sponsor</td>'; $header.="Sponsor". "\t";}
if ($list_EventContact =='yes'){echo '<td>Contact</td>'; $header.="Contact". "\t";}
if ($list_EventRegistration =='yes'){echo '<td>Registration</td>'; $header.="Registration". "\t";}
if ($list_EventFee =='yes'){echo '<td>Fee</td>'; $header.="Fee". "\t";}
if ($list_EventWeb =='yes'){echo '<td>Web</td>'; $header.="Web". "\t";}
if ($list_EventStatus =='yes'){echo '<td>Status</td>'; $header.="Status". "\t";}
if ($list_EventFilename =='yes'){echo '<td>Filename</td>'; $header.="Filename". "\t";}
if ($list_EventRemarks =='yes'){echo '<td>Remarks</td>'; $header.="Remarks". "\t";}

	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)


/*
outputs datasets and fills $data variable
*/

$DatasetCount = 0;

  	while($dbrow = mysqli_fetch_array($dbresult))
   	{
	$DatasetCount += 1;
	
	// output to main window and selected data to $data excel variable

   	echo '<tr>';
if ($list_EventGUID =='yes'){echo'<td>'.$dbrow['EventGUID'].'</td>'; $data.='"'.$dbrow['EventGUID'].'"'."\t";}
if ($list_EventCreateID=='yes')
		{
		echo '<td class="bluelink"><a style="font-size:xx-small" href="events_modify.php?EventCreateID='.$dbrow['EventCreateID'].'">'.$dbrow['EventCreateID'].'</a></td>';
		$data .= '"' . $dbrow['EventCreateID'] . '"' . "\t";
		}
if ($list_EventArchiveID =='yes'){echo'<td>'.$dbrow['EventArchiveID'].'</td>'; $data.='"'.$dbrow['EventArchiveID'].'"'."\t";}
if ($list_EventProject =='yes'){echo'<td>'.$dbrow['EventProject'].'</td>'; $data.='"'.$dbrow['EventProject'].'"'."\t";}
if ($list_EventOwner =='yes'){echo'<td>'.$dbrow['EventOwner'].'</td>'; $data.='"'.$dbrow['EventOwner'].'"'."\t";}
if ($list_EventType =='yes'){echo'<td>'.$dbrow['EventType'].'</td>'; $data.='"'.$dbrow['EventType'].'"'."\t";}
if ($list_EventCategory =='yes'){echo'<td>'.$dbrow['EventCategory'].'</td>'; $data.='"'.$dbrow['EventCategory'].'"'."\t";}
if ($list_EventBegin =='yes'){echo'<td>'.$dbrow['EventBegin'].'</td>'; $data.='"'.$dbrow['EventBegin'].'"'."\t";}
if ($list_EventYear =='yes'){echo'<td>'.$dbrow['EventYear'].'</td>'; $data.='"'.$dbrow['EventYear'].'"'."\t";}
if ($list_EventDuration =='yes'){echo'<td>'.$dbrow['EventDuration'].'</td>'; $data.='"'.$dbrow['EventDuration'].'"'."\t";}
if ($list_EventName =='yes'){echo'<td>'.$dbrow['EventName'].'</td>'; $data.='"'.$dbrow['EventName'].'"'."\t";}
if ($list_EventContent =='yes'){echo'<td>'.$dbrow['EventContent'].'</td>'; $data.='"'.$dbrow['EventContent'].'"'."\t";}
if ($list_EventLocation =='yes'){echo'<td>'.$dbrow['EventLocation'].'</td>'; $data.='"'.$dbrow['EventLocation'].'"'."\t";}
if ($list_EventAddress =='yes'){echo'<td>'.$dbrow['EventAddress'].'</td>'; $data.='"'.$dbrow['EventAddress'].'"'."\t";}
if ($list_EventCountry =='yes'){echo'<td>'.$dbrow['EventCountry'].'</td>'; $data.='"'.$dbrow['EventCountry'].'"'."\t";}
if ($list_EventLanguage =='yes'){echo'<td>'.$dbrow['EventLanguage'].'</td>'; $data.='"'.$dbrow['EventLanguage'].'"'."\t";}
if ($list_EventHost =='yes'){echo'<td>'.$dbrow['EventHost'].'</td>'; $data.='"'.$dbrow['EventHost'].'"'."\t";}
if ($list_EventSponsor =='yes'){echo'<td>'.$dbrow['EventSponsor'].'</td>'; $data.='"'.$dbrow['EventSponsor'].'"'."\t";}
if ($list_EventContact =='yes'){echo'<td>'.$dbrow['EventContact'].'</td>'; $data.='"'.$dbrow['EventContact'].'"'."\t";}
if ($list_EventRegistration =='yes'){echo'<td>'.$dbrow['EventRegistration'].'</td>'; $data.='"'.$dbrow['EventRegistration'].'"'."\t";}
if ($list_EventFee =='yes'){echo'<td>'.$dbrow['EventFee'].'</td>'; $data.='"'.$dbrow['EventFee'].'"'."\t";}
if ($list_EventWeb =='yes'){echo'<td>'.$dbrow['EventWeb'].'</td>'; $data.='"'.$dbrow['EventWeb'].'"'."\t";}
if ($list_EventStatus =='yes'){echo'<td>'.$dbrow['EventStatus'].'</td>'; $data.='"'.$dbrow['EventStatus'].'"'."\t";}


if($list_EventFilename=='yes')		
	{
	$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['EventProject'].'/';
	
	echo	'<td class="bluelink">'
			.'<a style="font-size:xx-small" href="'
			.$ProjectFilePath
			.$dbrow['EventFilename']
			.'">'
			.$dbrow['EventFilename'].'['.$dbrow['EventFilesize'].'kB]'
			.'</a>';
	$data.='"'.$dbrow['EventFilename'].'['.$dbrow['EventFilesize'].'kB]'.'"'."\t";
	}

if ($list_EventRemarks =='yes'){echo'<td>'.$dbrow['EventRemarks'].'</td>'; $data.='"'.$dbrow['EventRemarks'].'"'."\t";}
	
	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)
	}
echo '<tr>';
echo '<td>'.$DatasetCount.' Datasets</td>';
echo '</tr>';	
echo '</table>';
/*
export selected table to .xls onto local server folder to download as offered from there
*/

$fp = fopen('eventslist.xls','w');
fwrite($fp,$header);
fwrite($fp,"\n");
fwrite($fp,$data);
fclose($fp);

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2020-10-30 18:00</div>';

?>
</body>
</html>
