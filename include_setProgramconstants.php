<?php

/*
the following Program constants are defined 
*/

// Arrays of constants

// Category classifies Program dataset in terms of presentation mode
$ProgramCategoryArray = [
'seminar',
'conference',
'workshop',
'webtutorial',
'webinar',
'webconference',
'cluster event',
'lecture',
'presentation',
'meeting',
'sitevisit',
'other',
];

$ProgramTimelineArray = [
    'today',
    'this week',
    'this month',
    'past',
    ];

// last change vkrieger 2021-02-07 20:00

?>