<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>
<?php

include 'include_setAddressconstants.php';
include 'include_addresses_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

if (!empty($_POST["historydataset"])) 
	{
	$_POST['partAddressGUID']=$AddressGUID;	
	$_POST['partAddressArchiveID']='';
	$_POST['list_AddressGUID']='yes';	
	$_POST['list_AddressCreateID']='yes';	
	$_POST['list_AddressArchiveID']='yes';	
	$_POST['list_AddressOwner']='yes';	
	$_POST['list_AddressName']='yes';	
	$_POST['list_AddressStatus']='yes';	
	include 'addresses_list.php';
	}


$CurrentTimeStamp = date("Y-m-d H:i:s");

// deletedataset: puts current dataset in archive status by entering current datetime into ArchiveID
// updatedataset: puts current dataset in archive status by entering current datetime into ArchiveID and then createdataset

if (!empty($_POST["updatedataset"]) OR !empty($_POST["deletedataset"]))
	{
	// control codeecho
	//echo $AddressGUID.'<br>';
	//echo $AddressCreateID.'<br>';
	//echo $AddressArchiveID.'<br>';
	//echo $CurrentTimeStamp.'<br>';
	
	$dbchange = "UPDATE addresses SET AddressArchiveID = '$CurrentTimeStamp' WHERE AddressCreateID = '$AddressCreateID'";
	$dbquery = mysqli_query($link,$dbchange) or die ("not updated!");
	}

// createdataset: creates new dataset with current datetime in GUID and CreateID
// updatedataset: creates new dataset with same GUID and with current datetime in CreateID

if (!empty($_POST["updatedataset"]) OR !empty($_POST["createdataset"]))
	{
	if (!empty($_POST["createdataset"])) {$AddressGUID = $CurrentTimeStamp;}
	$AddressCreateID = $CurrentTimeStamp;
	$AddressArchiveID='0000-00-00 00:00:00';
	
	// control code	
	// echo $AddressGUID.'<br>';
	// echo $AddressCreateID.'<br>';
	// echo $AddressArchiveID.'<br>';
	// echo $CurrentTimeStamp.'<br>';
	
	$dbchange = "INSERT INTO addresses SET

		AddressGUID = '$AddressGUID',
		AddressCreateID = '$AddressCreateID',
		AddressArchiveID = '$AddressArchiveID',
		AddressProject = '$AddressProject',
		AddressOwner = '$AddressOwner',
		AddressCategory = '$AddressCategory',
		AddressType = '$AddressType',
		AddressFirstname = '$AddressFirstname',
		AddressLastname = '$AddressLastname',
		AddressEmailPrim = '$AddressEmailPrim',
		AddressEmailSec = '$AddressEmailSec',
		AddressTelBusiness = '$AddressTelBusiness',
		AddressTelPrivate = '$AddressTelPrivate',
		AddressTelMobile = '$AddressTelMobile',
		AddressFax = '$AddressFax',
		AddressPrivate = '$AddressPrivate',
		AddressPrivateTown = '$AddressPrivateTown',
		AddressPrivateZIP = '$AddressPrivateZIP',
		AddressPrivateCountry = '$AddressPrivateCountry',
		AddressBusiness = '$AddressBusiness',
		AddressBusinessTown = '$AddressBusinessTown',
		AddressBusinessZIP = '$AddressBusinessZIP',
		AddressBusinessCountry = '$AddressBusinessCountry',
		AddressSector = '$AddressSector',
		AddressOrganization = '$AddressOrganization',
		AddressDepartment = '$AddressDepartment',
		AddressPosition = '$AddressPosition',
		AddressRemarks = '$AddressRemarks'
		";
	$dbquery = mysqli_query($link,$dbchange) or die ("not created!");
	}

$_GET['AddressCreateID']=$AddressCreateID;	
include 'addresses_modify.php';

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-03 18:00</div>';

?>
</body>
</html>

