<?php

/*
criteria are transported into this php by POST. empty POST variable causes empty part-variable thus no criteria
empty variables cause warnings.
*/

//
if (!empty($_POST["last_sorted_by"])) 	{ $last_sorted_by  	= $_POST["last_sorted_by"];} 	else {$last_sorted_by  	= "ReferenceGUID";}
if (!empty($_POST["first_sorted_by"])) 	{ $first_sorted_by  = $_POST["first_sorted_by"];} 	else {$first_sorted_by  = "ReferenceCategory";}
if (!empty($_POST["then_sorted_by"])) 	{ $then_sorted_by  	= $_POST["then_sorted_by"];} 	else {$then_sorted_by  	= "ReferenceOwner";}
if (!empty($_POST["existor"])) 			{ $existor  		= $_POST["existor"];} 			else {$existor  		= "";}


// generic variables
if (!empty($_POST['ReferenceEN'])) {$ReferenceEN =$_POST['ReferenceEN'];} else {$ReferenceEN ='';}
if (!empty($_POST['ReferenceDE'])) {$ReferenceDE =$_POST['ReferenceDE'];} else {$ReferenceDE ='';}
if (!empty($_POST['ReferenceGUID'])) {$ReferenceGUID =$_POST['ReferenceGUID'];} else {$ReferenceGUID ='';}
if (!empty($_POST['ReferenceCreateID'])) {$ReferenceCreateID =$_POST['ReferenceCreateID'];} else {$ReferenceCreateID ='';}
if (!empty($_POST['ReferenceArchiveID'])) {$ReferenceArchiveID =$_POST['ReferenceArchiveID'];} else {$ReferenceArchiveID ='';}
if (!empty($_POST['ReferenceProject'])) {$ReferenceProject =$_POST['ReferenceProject'];} else {$ReferenceProject ='';}
if (!empty($_POST['ReferenceInitialProject'])) {$ReferenceInitialProject =$_POST['ReferenceInitialProject'];} else {$ReferenceInitialProject ='';}
if (!empty($_POST['ReferenceOwner'])) {$ReferenceOwner =$_POST['ReferenceOwner'];} else {$ReferenceOwner ='';}
if (!empty($_POST['ReferenceType'])) {$ReferenceType =$_POST['ReferenceType'];} else {$ReferenceType ='';}
if (!empty($_POST['ReferenceCategory'])) {$ReferenceCategory =$_POST['ReferenceCategory'];} else {$ReferenceCategory ='';}
if (!empty($_POST['ReferenceName'])) {$ReferenceName =$_POST['ReferenceName'];} else {$ReferenceName ='';}
if (!empty($_POST['ReferenceNameDE'])) {$ReferenceNameDE =$_POST['ReferenceNameDE'];} else {$ReferenceNameDE ='';}
if (!empty($_POST['ReferenceSummary'])) {$ReferenceSummary =$_POST['ReferenceSummary'];} else {$ReferenceSummary ='';}
if (!empty($_POST['ReferenceSummaryDE'])) {$ReferenceSummaryDE =$_POST['ReferenceSummaryDE'];} else {$ReferenceSummaryDE ='';}
if (!empty($_POST['ReferenceClient'])) {$ReferenceClient =$_POST['ReferenceClient'];} else {$ReferenceClient ='';}
if (!empty($_POST['ReferenceDivision'])) {$ReferenceDivision =$_POST['ReferenceDivision'];} else {$ReferenceDivision ='';}
if (!empty($_POST['ReferenceDisziplin'])) {$ReferenceDisziplin =$_POST['ReferenceDisziplin'];} else {$ReferenceDisziplin ='';}
if (!empty($_POST['ReferenceContractor'])) {$ReferenceContractor =$_POST['ReferenceContractor'];} else {$ReferenceContractor ='';}
if (!empty($_POST['ReferenceCode'])) {$ReferenceCode =$_POST['ReferenceCode'];} else {$ReferenceCode ='';}
if (!empty($_POST['ReferenceContact'])) {$ReferenceContact =$_POST['ReferenceContact'];} else {$ReferenceContact ='';}
if (!empty($_POST['ReferenceVolume'])) {$ReferenceVolume =$_POST['ReferenceVolume'];} else {$ReferenceVolume ='';}
if (!empty($_POST['ReferenceFee'])) {$ReferenceFee =$_POST['ReferenceFee'];} else {$ReferenceFee ='';}
if (!empty($_POST['ReferenceStartdate'])) {$ReferenceStartdate =$_POST['ReferenceStartdate'];} else {$ReferenceStartdate ='';}
if (!empty($_POST['ReferenceEnddate'])) {$ReferenceEnddate =$_POST['ReferenceEnddate'];} else {$ReferenceEnddate ='';}
if (!empty($_POST['ReferenceLocation'])) {$ReferenceLocation =$_POST['ReferenceLocation'];} else {$ReferenceLocation ='';}
if (!empty($_POST['ReferenceCountry'])) {$ReferenceCountry =$_POST['ReferenceCountry'];} else {$ReferenceCountry ='';}
if (!empty($_POST['ReferenceDescription'])) {$ReferenceDescription =$_POST['ReferenceDescription'];} else {$ReferenceDescription ='';}
if (!empty($_POST['ReferenceDescriptionDE'])) {$ReferenceDescriptionDE =$_POST['ReferenceDescriptionDE'];} else {$ReferenceDescriptionDE ='';}
if (!empty($_POST['ReferenceScope'])) {$ReferenceScope =$_POST['ReferenceScope'];} else {$ReferenceScope ='';}
if (!empty($_POST['ReferenceScopeDE'])) {$ReferenceScopeDE =$_POST['ReferenceScopeDE'];} else {$ReferenceScopeDE ='';}
if (!empty($_POST['ReferenceDetails'])) {$ReferenceDetails =$_POST['ReferenceDetails'];} else {$ReferenceDetails ='';}
if (!empty($_POST['ReferenceDetailsDe'])) {$ReferenceDetailsDE =$_POST['ReferenceDetailsDE'];} else {$ReferenceDetailsDE ='';}

if (!empty($_POST['ReferenceStatus'])) {$ReferenceStatus =$_POST['ReferenceStatus'];} else {$ReferenceStatus ='';}
if (!empty($_POST['ReferenceFilename'])) {$ReferenceFilename =$_POST['ReferenceFilename'];} else {$ReferenceFilename ='';}
if (!empty($_POST['ReferenceFilesize'])) {$ReferenceFilesize =$_POST['ReferenceFilesize'];} else {$ReferenceFilesize ='';}
if (!empty($_POST['ReferenceRemarks'])) {$ReferenceRemarks =$_POST['ReferenceRemarks'];} else {$ReferenceRemarks ='';}

// list variables
if (!empty($_POST['list_ReferenceEN'])) {$list_ReferenceEN =$_POST['list_ReferenceEN'];} else {$list_ReferenceEN ='';}
if (!empty($_POST['list_ReferenceDE'])) {$list_ReferenceDE =$_POST['list_ReferenceDE'];} else {$list_ReferenceDE ='';}
if (!empty($_POST['list_ReferenceGUID'])) {$list_ReferenceGUID =$_POST['list_ReferenceGUID'];} else {$list_ReferenceGUID ='';}
if (!empty($_POST['list_ReferenceCreateID'])) {$list_ReferenceCreateID =$_POST['list_ReferenceCreateID'];} else {$list_ReferenceCreateID ='';}
if (!empty($_POST['list_ReferenceArchiveID'])) {$list_ReferenceArchiveID =$_POST['list_ReferenceArchiveID'];} else {$list_ReferenceArchiveID ='';}
if (!empty($_POST['list_ReferenceProject'])) {$list_ReferenceProject =$_POST['list_ReferenceProject'];} else {$list_ReferenceProject ='';}
if (!empty($_POST['list_ReferenceOwner'])) {$list_ReferenceOwner =$_POST['list_ReferenceOwner'];} else {$list_ReferenceOwner ='';}
if (!empty($_POST['list_ReferenceType'])) {$list_ReferenceType =$_POST['list_ReferenceType'];} else {$list_ReferenceType ='';}
if (!empty($_POST['list_ReferenceCategory'])) {$list_ReferenceCategory =$_POST['list_ReferenceCategory'];} else {$list_ReferenceCategory ='';}
if (!empty($_POST['list_ReferenceName'])) {$list_ReferenceName =$_POST['list_ReferenceName'];} else {$list_ReferenceName ='';}
if (!empty($_POST['list_ReferenceSummary'])) {$list_ReferenceSummary =$_POST['list_ReferenceSummary'];} else {$list_ReferenceSummary ='';}
if (!empty($_POST['list_ReferenceClient'])) {$list_ReferenceClient =$_POST['list_ReferenceClient'];} else {$list_ReferenceClient ='';}
if (!empty($_POST['list_ReferenceDivision'])) {$list_ReferenceDivision =$_POST['list_ReferenceDivision'];} else {$list_ReferenceDivision ='';}
if (!empty($_POST['list_ReferenceDisziplin'])) {$list_ReferenceDisziplin =$_POST['list_ReferenceDisziplin'];} else {$list_ReferenceDisziplin ='';}
if (!empty($_POST['list_ReferenceContractor'])) {$list_ReferenceContractor =$_POST['list_ReferenceContractor'];} else {$list_ReferenceContractor ='';}
if (!empty($_POST['list_ReferenceCode'])) {$list_ReferenceCode =$_POST['list_ReferenceCode'];} else {$list_ReferenceCode ='';}
if (!empty($_POST['list_ReferenceContact'])) {$list_ReferenceContact =$_POST['list_ReferenceContact'];} else {$list_ReferenceContact ='';}
if (!empty($_POST['list_ReferenceVolume'])) {$list_ReferenceVolume =$_POST['list_ReferenceVolume'];} else {$list_ReferenceVolume ='';}
if (!empty($_POST['list_ReferenceFee'])) {$list_ReferenceFee =$_POST['list_ReferenceFee'];} else {$list_ReferenceFee ='';}
if (!empty($_POST['list_ReferenceStartdate'])) {$list_ReferenceStartdate =$_POST['list_ReferenceStartdate'];} else {$list_ReferenceStartdate ='';}
if (!empty($_POST['list_ReferenceEnddate'])) {$list_ReferenceEnddate =$_POST['list_ReferenceEnddate'];} else {$list_ReferenceEnddate ='';}
if (!empty($_POST['list_ReferenceLocation'])) {$list_ReferenceLocation =$_POST['list_ReferenceLocation'];} else {$list_ReferenceLocation ='';}
if (!empty($_POST['list_ReferenceCountry'])) {$list_ReferenceCountry =$_POST['list_ReferenceCountry'];} else {$list_ReferenceCountry ='';}
if (!empty($_POST['list_ReferenceDescription'])) {$list_ReferenceDescription =$_POST['list_ReferenceDescription'];} else {$list_ReferenceDescription ='';}
if (!empty($_POST['list_ReferenceScope'])) {$list_ReferenceScope =$_POST['list_ReferenceScope'];} else {$list_ReferenceScope ='';}
if (!empty($_POST['list_ReferenceDetails'])) {$list_ReferenceDetails =$_POST['list_ReferenceDetails'];} else {$list_ReferenceDetails ='';}

if (!empty($_POST['list_ReferenceStatus'])) {$list_ReferenceStatus =$_POST['list_ReferenceStatus'];} else {$list_ReferenceStatus ='';}
if (!empty($_POST['list_ReferenceFilename'])) {$list_ReferenceFilename =$_POST['list_ReferenceFilename'];} else {$list_ReferenceFilename ='';}
if (!empty($_POST['list_ReferenceFilesize'])) {$list_ReferenceFilesize =$_POST['list_ReferenceFilesize'];} else {$list_ReferenceFilesize ='';}
if (!empty($_POST['list_ReferenceRemarks'])) {$list_ReferenceRemarks =$_POST['list_ReferenceRemarks'];} else {$list_ReferenceRemarks ='';}

 // part variables
 
if (!empty($_POST['partReferenceEN'])) {$partReferenceEN =$_POST['partReferenceEN'];} else {$partReferenceEN ='';}
if (!empty($_POST['partReferenceDE'])) {$partReferenceDE =$_POST['partReferenceDE'];} else {$partReferenceDE ='';}
if (!empty($_POST['partReferenceGUID'])) {$partReferenceGUID =$_POST['partReferenceGUID'];} else {$partReferenceGUID ='';}
if (!empty($_POST['partReferenceCreateID'])) {$partReferenceCreateID =$_POST['partReferenceCreateID'];} else {$partReferenceCreateID ='';}
if (!empty($_POST['partReferenceArchiveID'])) {$partReferenceArchiveID =$_POST['partReferenceArchiveID'];} else {$partReferenceArchiveID ='';}
if (!empty($_POST['partReferenceProject'])) {$partReferenceProject =$_POST['partReferenceProject'];} else {$partReferenceProject ='';}
if (!empty($_POST['partReferenceOwner'])) {$partReferenceOwner =$_POST['partReferenceOwner'];} else {$partReferenceOwner ='';}
if (!empty($_POST['partReferenceType'])) {$partReferenceType =$_POST['partReferenceType'];} else {$partReferenceType ='';}
if (!empty($_POST['partReferenceCategory'])) {$partReferenceCategory =$_POST['partReferenceCategory'];} else {$partReferenceCategory ='';}
if (!empty($_POST['partReferenceName'])) {$partReferenceName =$_POST['partReferenceName'];} else {$partReferenceName ='';}
if (!empty($_POST['partReferenceSummary'])) {$partReferenceSummary =$_POST['partReferenceSummary'];} else {$partReferenceSummary ='';}
if (!empty($_POST['partReferenceClient'])) {$partReferenceClient =$_POST['partReferenceClient'];} else {$partReferenceClient ='';}
if (!empty($_POST['partReferenceDivision'])) {$partReferenceDivision =$_POST['partReferenceDivision'];} else {$partReferenceDivision ='';}
if (!empty($_POST['partReferenceDisziplin'])) {$partReferenceDisziplin =$_POST['partReferenceDisziplin'];} else {$partReferenceDisziplin ='';}
if (!empty($_POST['partReferenceContractor'])) {$partReferenceContractor =$_POST['partReferenceContractor'];} else {$partReferenceContractor ='';}
if (!empty($_POST['partReferenceCode'])) {$partReferenceCode =$_POST['partReferenceCode'];} else {$partReferenceCode ='';}
if (!empty($_POST['partReferenceContact'])) {$partReferenceContact =$_POST['partReferenceContact'];} else {$partReferenceContact ='';}
if (!empty($_POST['partReferenceVolume'])) {$partReferenceVolume =$_POST['partReferenceVolume'];} else {$partReferenceVolume ='';}
if (!empty($_POST['partReferenceFee'])) {$partReferenceFee =$_POST['partReferenceFee'];} else {$partReferenceFee ='';}
if (!empty($_POST['partReferenceStartdate'])) {$partReferenceStartdate =$_POST['partReferenceStartdate'];} else {$partReferenceStartdate ='';}
if (!empty($_POST['partReferenceEnddate'])) {$partReferenceEnddate =$_POST['partReferenceEnddate'];} else {$partReferenceEnddate ='';}
if (!empty($_POST['partReferenceLocation'])) {$partReferenceLocation =$_POST['partReferenceLocation'];} else {$partReferenceLocation ='';}
if (!empty($_POST['partReferenceCountry'])) {$partReferenceCountry =$_POST['partReferenceCountry'];} else {$partReferenceCountry ='';}
if (!empty($_POST['partReferenceDescription'])) {$partReferenceDescription =$_POST['partReferenceDescription'];} else {$partReferenceDescription ='';}
if (!empty($_POST['partReferenceScope'])) {$partReferenceScope =$_POST['partReferenceScope'];} else {$partReferenceScope ='';}
if (!empty($_POST['partReferenceDetails'])) {$partReferenceDetails =$_POST['partReferenceDetails'];} else {$partReferenceDetails ='';}

if (!empty($_POST['partReferenceStatus'])) {$partReferenceStatus =$_POST['partReferenceStatus'];} else {$partReferenceStatus ='';}
if (!empty($_POST['partReferenceFilename'])) {$partReferenceFilename =$_POST['partReferenceFilename'];} else {$partReferenceFilename ='';}
if (!empty($_POST['partReferenceFilesize'])) {$partReferenceFilesize =$_POST['partReferenceFilesize'];} else {$partReferenceFilesize ='';}
if (!empty($_POST['partReferenceRemarks'])) {$partReferenceRemarks =$_POST['partReferenceRemarks'];} else {$partReferenceRemarks ='';}

 // last change vk 2017-05-14
?>
