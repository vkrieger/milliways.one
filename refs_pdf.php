<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<!-- 
dont forget enctype="multipart/form-data" to upload files!!!
Datei-Uploads funktionieren nur mit method="post".
Wichtig ist außerdem, dass Sie im einleitenden <form>-Tag die Angabe enctype="multipart/form-data" notieren.
Andernfalls erhalten Sie lediglich den Dateinamen der ausgewählten Datei übermittelt, nicht jedoch die Datei selbst.
ENCTYPE steht für die Art des Übertragunsformats, das bei der Kommunikation zwischen Browser und Server genutzt wird.
application/x-www-form-urlencoded der Standardwert - für die Übertragung großer Datenmengen, die Nicht-ASCII-Zeichen enthalten, weniger geeignet
multipart/form-data" wird z.B. beim File-Upload verwendet
enctype="text/plain" reiner Text / Ausgabe erfolgt an menschliche Wesen
-->

<body>

<?php

include 'include_setReferenceconstants.php';
include 'include_refs_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

// fpdf section to produce application.pdf 
// include fpdf library
include ('./includes/include_fpdf_includeFPDF.php');
// extending class fpdf
include ('./includes/include_fpdf_extendPDF.php');

// EN references
echo '<a href="references_EN.pdf" target="_blank"><img src="logo_pdf_EN.jpg"></a>';
// Instanciation of class PDF
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->SetMargins(0,0);
// prepare table
echo '<table border="1" cellspacing="0">';
echo '<tr>';
echo '<td>CreateID</td>';
echo '<td>Name</td>';
echo '</tr>';
if ($ReferenceEN)
	{
	// replace underscore with blank in ReferenceEN[] to generate correct timestamp array
	$ReferenceID= str_replace('_',' ',$ReferenceEN); 
	$ReferenceString = implode(' , ',$ReferenceID);
	$dbquery = "SELECT * FROM refs WHERE INSTR('$ReferenceString',ReferenceCreateID)>0";	
	$dbresult = mysql_query($dbquery);  echo mysql_error();	
	$DatasetCount = 0;
	while($dbrow = mysql_fetch_array($dbresult)) 
		{
		include ('./includes/include_fpdf_addpage_EN.php');
		echo '<tr>';
		echo '<td>'.$dbrow['ReferenceCreateID'].'</td>';
		echo '<td>'.$dbrow['ReferenceName'].'</td>';
		echo '</tr>';
		}	
	}	
echo '</table>';
// table end
// export selected datasets to .pdf onto local server folder to download from there by href-link
$pdf->Output('F','references_EN.pdf');
unset ($pdf);

// DE references
echo '<a href="references_DE.pdf" target="_blank"><img src="logo_pdf_DE.jpg"></a>';
// Instanciation of class PDF
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->SetMargins(0,0);
// prepare table
echo '<table border="1" cellspacing="0">';
echo '<tr>';
echo '<td>CreateID</td>';
echo '<td>Name</td>';
echo '</tr>';
if ($ReferenceDE)	
	{
	$ReferenceIDE= str_replace('_',' ',$ReferenceDE); 
	$ReferenceStringDE = implode(' , ',$ReferenceIDE);
	$dbquery = "SELECT * FROM refs WHERE INSTR('$ReferenceStringDE',ReferenceCreateID)>0";	
	$dbresult = mysql_query($dbquery);  echo mysql_error();	
	$DatasetCount = 0;
	while($dbrow = mysql_fetch_array($dbresult))
		{
		include ('./includes/include_fpdf_addpage_DE.php');
		echo '<tr>';
		echo '<td>'.$dbrow['ReferenceCreateID'].'</td>';
		echo '<td>'.$dbrow['ReferenceNameDE'].'</td>';
		echo '</tr>';
		}	
	}
echo '</table>';
// table end
// export selected datasets to .pdf onto local server folder to download from there by href-link
$pdf->Output('F','references_DE.pdf');
unset($pdf);


echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 17.04.17 23:00</div>';

?>
</body>
</html>
