<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>
!!! Do <b>NOT</b> use semicolon (;) in input fields !!! <b>NO</b> blanks in filenames - use underscore ( _ ) instead!!! 
<br /><br />

<?php

// new error handling 

error_reporting(E_ALL);
ini_set("display_errors", 1);

		
include 'include_setProgramconstants.php';
include 'include_programs_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

$ProgramCreateID = $_GET["ProgramCreateID"];

$dbquery = " SELECT * FROM programs WHERE LOCATE ('$ProgramCreateID', ProgramCreateID) >0 ";
$dbresult = mysqli_query($link,$dbquery);
$dbrow = mysqli_fetch_array($dbresult);

	$updatedataset="";
	$createdataset="";
	$deletedataset="";
	$historydataset="";
	
if ($updatedataset == "" AND $createdataset == "" AND $deletedataset == "" AND $historydataset =="")
	{
	echo '<form method="post" action="programs_save.php" enctype="multipart/form-data" >';
	
	if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
		{
		echo '<input type="submit" name="createdataset" value="create dataset">';
		echo '<input type="submit" name="updatedataset" value="update dataset">';
    	echo '<input type="submit" name="deletedataset" value="delete dataset">';
      	echo '<input type="reset" value="reset values">';
      	echo '<br /><br />';
	    }

	echo '<table>';
	
	echo '<tr>';
	echo '<td align="right">GUID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="ProgramGUID" size="60" maxlength="100" value="'.$dbrow['ProgramGUID'].'" readonly></td>';
	echo '<td><input type="submit" name="historydataset" value="history of datasets"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">CreateID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="ProgramCreateID" size="60" maxlength="100" value="'.$dbrow['ProgramCreateID'].'" readonly></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">ArchiveID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="ProgramArchiveID" size="60" maxlength="100" value="'.$dbrow['ProgramArchiveID'].'" readonly></td>';
	echo '</tr>';
	
	echo '<input type="hidden" name="ProgramInitialProject" value="'.$dbrow['ProgramProject'].'">';
	echo '<input type="hidden" name="ProgramProject" value="'.$dbrow['ProgramProject'].'">';
	echo '<td align="right">Project</td>';
	echo '<td><select name="ProgramProject" size="1">';
		if (!empty($SystemProject))
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($SystemProject==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		else
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($dbrow['ProgramProject']==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		echo '</select></td>';		
	
	echo '<tr>';
	echo '<td align="right">Owner (readonly)</td>';
	echo '<td>'.'['.$dbrow['ProgramOwner'].'] - create/update transfers to '.'<input style="background-color:#C0C0C0" type="text" name="ProgramOwner" size="15" maxlength="100" value="'.$_SESSION['LoginLogin'].'" readonly></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">Type</td>';
	echo '<td><select name="ProgramType" size="1">';
			foreach ($SystemTypeArray as $Type) {echo '<option'; if ($dbrow['ProgramType']==$Type) {echo ' selected';} echo '>'.$Type.'</option>';} 
			echo '</select></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">Category</td>';
	echo '<td><select name="ProgramCategory" size="1">';
			foreach ($ProgramCategoryArray as $Category) {echo '<option'; if ($dbrow['ProgramCategory']==$Category) {echo ' selected';} echo '>'.$Category.'</option>';} 
			echo '</select></td>';
	echo '</tr>';

echo '<tr><td align="right">Name</td><td><input type="text" name="ProgramName" size="60" maxlength="255" value="'.$dbrow['ProgramName'].'"></td></tr>';
echo '<tr><td align="right">Teaser</td><td><textarea name="ProgramTeaser" cols="120" rows="4" value="'.$dbrow['ProgramTeaser'].'">'.$dbrow['ProgramTeaser'].'</textarea></td></tr>';
echo '<tr><td align="right">Intro</td><td><textarea name="ProgramIntro" cols="120" rows="4" value="'.$dbrow['ProgramIntro'].'">'.$dbrow['ProgramIntro'].'</textarea></td></tr>';
echo '<tr><td align="right">Target</td><td><textarea name="ProgramTarget" cols="120" rows="8" value="'.$dbrow['ProgramTarget'].'">'.$dbrow['ProgramTarget'].'</textarea></td></tr>';
echo '<tr><td align="right">Promo</td><td><textarea name="ProgramPromo" cols="120" rows="4" value="'.$dbrow['ProgramPromo'].'">'.$dbrow['ProgramPromo'].'</textarea></td></tr>';
echo '<tr><td align="right">Text</td><td><textarea name="ProgramText" cols="120" rows="8" value="'.$dbrow['ProgramText'].'">'.$dbrow['ProgramText'].'</textarea></td></tr>';
echo '<tr><td align="right">Content</td><td><textarea name="ProgramContent" cols="120" rows="8" value="'.$dbrow['ProgramContent'].'">'.$dbrow['ProgramContent'].'</textarea></td></tr>';
echo '<tr><td align="right">Language</td><td><input type="text" name="ProgramLanguage" size="60" maxlength="255" value="'.$dbrow['ProgramLanguage'].'"></td></tr>';

echo '<tr><td align="right">Registration</td><td><textarea name="ProgramRegistration" cols="62" rows="4" value="'.$dbrow['ProgramRegistration'].'">'.$dbrow['ProgramRegistration'].'</textarea></td></tr>';
echo '<tr><td align="right">Fee</td><td><input type="text" name="ProgramFee" size="60" maxlength="255" value="'.$dbrow['ProgramFee'].'"></td></tr>';
$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['ProgramProject'].'/';	
echo '<tr>';
echo '<td align="right">';
echo '<input type="checkbox" name="existor" value="File"'; if ($dbrow['ProgramFilename']) {echo ' checked';} echo '> Filename (max.10MB)</td>';
echo '<input type="hidden" name="ProgramFilename" value="'.$dbrow['ProgramFilename'].'">';
echo '<input type="hidden" name="ProgramFilesize" value="'.$dbrow['ProgramFilesize'].'">';
echo '<td align="left" class="bluelink"><a href="'.$ProjectFilePath.$dbrow['ProgramFilename'].'">'.$dbrow['ProgramFilename'].'</a> ['.$dbrow['ProgramFilesize'].'kB]</td>';
echo '<td><input type="file" name="ProgramFilename" size="15"></td>';
echo '</tr>';
echo '<tr><td align="right">Remarks </td><td><input type="text" name="ProgramRemarks" size="60" maxlength="255" value="'.$dbrow['ProgramRemarks'].'"></td></tr>';

	echo '</table>';

	echo '</form>';

} elseif ( $updatedataset OR $createdataset OR $deletedataset OR $historydataset)

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-07 18:00</div>';

?>
</body>
</html>
