<?php

/*
the following Seminar constants are defined 
*/

// Arrays of constants

// Category classifies Seminar dataset in terms of presentation mode
$SeminarCategoryArray = [
'seminar',
'conference',
'workshop',
'webtutorial',
'webinar',
'webconference',
'cluster event',
'lecture',
'presentation',
'meeting',
'sitevisit',
'other',
];

$SeminarSponsorArray = [
'scholarum',
'PMC',
'iskom',
'other',
];

$SeminarStatusArray = [
    'published',
    'inactive',
    'planned',
    'scheduled',
    'booked',
    'cancelled',
    'stateless',
    ];

$SeminarTimelineArray = [
    'today',
    'this week',
    'this month',
    'past',
    ];

// last change vkrieger 2021-02-06 20:00

?>