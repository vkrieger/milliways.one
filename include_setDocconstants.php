<?php

/*
the following Doc constants are defined 
*/

// Arrays of Doc constants

// Category classifies Doc dataset in terms of status and mode type
$DocCategoryArray = [
'undefined',
'other',
'brochure',
'CV',
'drawing',
'graphics',
'invoice',
'letter',
'minutes',
'movie',
'norm',
'presentation',
'regulation',
'request',
'report',
'schedule',
'spreadsheet',
'template',
];

// last change vkrieger 19.05.2017

?>