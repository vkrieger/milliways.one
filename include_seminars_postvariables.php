<?php

/*
criteria are transported into this php by POST. empty POST variable causes empty part-variable thus no criteria
empty variables cause warnings.
*/

//
if (!empty($_POST["last_sorted_by"])) 	{ $last_sorted_by  	= $_POST["last_sorted_by"];} 	else {$last_sorted_by  	= "SeminarGUID";}
if (!empty($_POST["first_sorted_by"])) 	{ $first_sorted_by  = $_POST["first_sorted_by"];} 	else {$first_sorted_by  = "SeminarCategory";}
if (!empty($_POST["then_sorted_by"])) 	{ $then_sorted_by  	= $_POST["then_sorted_by"];} 	else {$then_sorted_by  	= "SeminarOwner";}
if (!empty($_POST["existor"])) 			{ $existor  		= $_POST["existor"];} 			else {$existor  		= "";}


// generic variables
if (!empty($_POST['SeminarGUID'])) {$SeminarGUID =$_POST['SeminarGUID'];} else {$SeminarGUID ='';}
if (!empty($_POST['SeminarCreateID'])) {$SeminarCreateID =$_POST['SeminarCreateID'];} else {$SeminarCreateID ='';}
if (!empty($_POST['SeminarArchiveID'])) {$SeminarArchiveID =$_POST['SeminarArchiveID'];} else {$SeminarArchiveID ='';}
if (!empty($_POST['SeminarProject'])) {$SeminarProject =$_POST['SeminarProject'];} else {$SeminarProject ='';}
if (!empty($_POST['SeminarInitialProject'])) {$SeminarInitialProject =$_POST['SeminarInitialProject'];} else {$SeminarInitialProject ='';}
if (!empty($_POST['SeminarOwner'])) {$SeminarOwner =$_POST['SeminarOwner'];} else {$SeminarOwner ='';}
if (!empty($_POST['SeminarType'])) {$SeminarType =$_POST['SeminarType'];} else {$SeminarType ='';}
if (!empty($_POST['SeminarCategory'])) {$SeminarCategory =$_POST['SeminarCategory'];} else {$SeminarCategory ='';}
if (!empty($_POST['SeminarBegin'])) {$SeminarBegin =$_POST['SeminarBegin'];} else {$SeminarBegin ='';}
if (!empty($_POST['SeminarYear'])) {$SeminarYear =$_POST['SeminarYear'];} else {$SeminarYear ='';}
if (!empty($_POST['SeminarDuration'])) {$SeminarDuration =$_POST['SeminarDuration'];} else {$SeminarDuration ='';}
if (!empty($_POST['SeminarName'])) {$SeminarName =$_POST['SeminarName'];} else {$SeminarName ='';}
if (!empty($_POST['SeminarContent'])) {$SeminarContent =$_POST['SeminarContent'];} else {$SeminarContent ='';}
if (!empty($_POST['SeminarReferent1'])) {$SeminarReferent1 =$_POST['SeminarReferent1'];} else {$SeminarReferent1 ='';}
if (!empty($_POST['SeminarReferent2'])) {$SeminarReferent2 =$_POST['SeminarReferent2'];} else {$SeminarReferent2 ='';}
if (!empty($_POST['SeminarReferent3'])) {$SeminarReferent3 =$_POST['SeminarReferent3'];} else {$SeminarReferent3 ='';}
if (!empty($_POST['SeminarLanguage'])) {$SeminarLanguage =$_POST['SeminarLanguage'];} else {$SeminarLanguage ='';}
if (!empty($_POST['SeminarLocation'])) {$SeminarLocation =$_POST['SeminarLocation'];} else {$SeminarLocation ='';}
//if (!empty($_POST['SeminarAddress'])) {$SeminarAddress =$_POST['SeminarAddress'];} else {$SeminarAddress ='';}
//if (!empty($_POST['SeminarCountry'])) {$SeminarCountry =$_POST['SeminarCountry'];} else {$SeminarCountry ='';}
if (!empty($_POST['SeminarHost'])) {$SeminarHost =$_POST['SeminarHost'];} else {$SeminarHost ='';}
if (!empty($_POST['SeminarSponsor'])) {$SeminarSponsor =$_POST['SeminarSponsor'];} else {$SeminarSponsor ='';}
if (!empty($_POST['SeminarContact'])) {$SeminarContact =$_POST['SeminarContact'];} else {$SeminarContact ='';}
if (!empty($_POST['SeminarRegistration'])) {$SeminarRegistration =$_POST['SeminarRegistration'];} else {$SeminarRegistration ='';}
if (!empty($_POST['SeminarFee'])) {$SeminarFee =$_POST['SeminarFee'];} else {$SeminarFee ='';}
if (!empty($_POST['SeminarWeb'])) {$SeminarWeb =$_POST['SeminarWeb'];} else {$SeminarWeb ='';}
if (!empty($_POST['SeminarStatus'])) {$SeminarStatus =$_POST['SeminarStatus'];} else {$SeminarStatus ='';}
if (!empty($_POST['SeminarFilename'])) {$SeminarFilename =$_POST['SeminarFilename'];} else {$SeminarFilename ='';}
if (!empty($_POST['SeminarFilesize'])) {$SeminarFilesize =$_POST['SeminarFilesize'];} else {$SeminarFilesize ='';}
if (!empty($_POST['SeminarRemarks'])) {$SeminarRemarks =$_POST['SeminarRemarks'];} else {$SeminarRemarks ='';}


// list variables
if (!empty($_POST['list_SeminarGUID'])) {$list_SeminarGUID =$_POST['list_SeminarGUID'];} else {$list_SeminarGUID ='';}
if (!empty($_POST['list_SeminarCreateID'])) {$list_SeminarCreateID =$_POST['list_SeminarCreateID'];} else {$list_SeminarCreateID ='';}
if (!empty($_POST['list_SeminarArchiveID'])) {$list_SeminarArchiveID =$_POST['list_SeminarArchiveID'];} else {$list_SeminarArchiveID ='';}
if (!empty($_POST['list_SeminarProject'])) {$list_SeminarProject =$_POST['list_SeminarProject'];} else {$list_SeminarProject ='';}
if (!empty($_POST['list_SeminarOwner'])) {$list_SeminarOwner =$_POST['list_SeminarOwner'];} else {$list_SeminarOwner ='';}
if (!empty($_POST['list_SeminarType'])) {$list_SeminarType =$_POST['list_SeminarType'];} else {$list_SeminarType ='';}
if (!empty($_POST['list_SeminarCategory'])) {$list_SeminarCategory =$_POST['list_SeminarCategory'];} else {$list_SeminarCategory ='';}
if (!empty($_POST['list_SeminarBegin'])) {$list_SeminarBegin =$_POST['list_SeminarBegin'];} else {$list_SeminarBegin ='';}
if (!empty($_POST['list_SeminarYear'])) {$list_SeminarYear =$_POST['list_SeminarYear'];} else {$list_SeminarYear ='';}
if (!empty($_POST['list_SeminarDuration'])) {$list_SeminarDuration =$_POST['list_SeminarDuration'];} else {$list_SeminarDuration ='';}
if (!empty($_POST['list_SeminarName'])) {$list_SeminarName =$_POST['list_SeminarName'];} else {$list_SeminarName ='';}
if (!empty($_POST['list_SeminarContent'])) {$list_SeminarContent =$_POST['list_SeminarContent'];} else {$list_SeminarContent ='';}
if (!empty($_POST['list_SeminarLocation'])) {$list_SeminarLocation =$_POST['list_SeminarLocation'];} else {$list_SeminarLocation ='';}
if (!empty($_POST['list_SeminarAddress'])) {$list_SeminarAddress =$_POST['list_SeminarAddress'];} else {$list_SeminarAddress ='';}
if (!empty($_POST['list_SeminarCountry'])) {$list_SeminarCountry =$_POST['list_SeminarCountry'];} else {$list_SeminarCountry ='';}
if (!empty($_POST['list_SeminarLanguage'])) {$list_SeminarLanguage =$_POST['list_SeminarLanguage'];} else {$list_SeminarLanguage ='';}
if (!empty($_POST['list_SeminarHost'])) {$list_SeminarHost =$_POST['list_SeminarHost'];} else {$list_SeminarHost ='';}
if (!empty($_POST['list_SeminarSponsor'])) {$list_SeminarSponsor =$_POST['list_SeminarSponsor'];} else {$list_SeminarSponsor ='';}
if (!empty($_POST['list_SeminarContact'])) {$list_SeminarContact =$_POST['list_SeminarContact'];} else {$list_SeminarContact ='';}
if (!empty($_POST['list_SeminarRegistration'])) {$list_SeminarRegistration =$_POST['list_SeminarRegistration'];} else {$list_SeminarRegistration ='';}
if (!empty($_POST['list_SeminarFee'])) {$list_SeminarFee =$_POST['list_SeminarFee'];} else {$list_SeminarFee ='';}
if (!empty($_POST['list_SeminarWeb'])) {$list_SeminarWeb =$_POST['list_SeminarWeb'];} else {$list_SeminarWeb ='';}
if (!empty($_POST['list_SeminarStatus'])) {$list_SeminarStatus =$_POST['list_SeminarStatus'];} else {$list_SeminarStatus ='';}
if (!empty($_POST['list_SeminarFilename'])) {$list_SeminarFilename =$_POST['list_SeminarFilename'];} else {$list_SeminarFilename ='';}
if (!empty($_POST['list_SeminarFilesize'])) {$list_SeminarFilesize =$_POST['list_SeminarFilesize'];} else {$list_SeminarFilesize ='';}
if (!empty($_POST['list_SeminarRemarks'])) {$list_SeminarRemarks =$_POST['list_SeminarRemarks'];} else {$list_SeminarRemarks ='';}

 // part variables
 
if (!empty($_POST['partSeminarGUID'])) {$partSeminarGUID =$_POST['partSeminarGUID'];} else {$partSeminarGUID ='';}
if (!empty($_POST['partSeminarCreateID'])) {$partSeminarCreateID =$_POST['partSeminarCreateID'];} else {$partSeminarCreateID ='';}
if (!empty($_POST['partSeminarArchiveID'])) {$partSeminarArchiveID =$_POST['partSeminarArchiveID'];} else {$partSeminarArchiveID ='';}
if (!empty($_POST['partSeminarProject'])) {$partSeminarProject =$_POST['partSeminarProject'];} else {$partSeminarProject ='';}
if (!empty($_POST['partSeminarOwner'])) {$partSeminarOwner =$_POST['partSeminarOwner'];} else {$partSeminarOwner ='';}
if (!empty($_POST['partSeminarType'])) {$partSeminarType =$_POST['partSeminarType'];} else {$partSeminarType ='';}
if (!empty($_POST['partSeminarCategory'])) {$partSeminarCategory =$_POST['partSeminarCategory'];} else {$partSeminarCategory ='';}
if (!empty($_POST['partSeminarBegin'])) {$partSeminarBegin =$_POST['partSeminarBegin'];} else {$partSeminarBegin ='';}
if (!empty($_POST['partSeminarYear'])) {$partSeminarYear =$_POST['partSeminarYear'];} else {$partSeminarYear ='';}
if (!empty($_POST['partSeminarDuration'])) {$partSeminarDuration =$_POST['partSeminarDuration'];} else {$partSeminarDuration ='';}
if (!empty($_POST['partSeminarName'])) {$partSeminarName =$_POST['partSeminarName'];} else {$partSeminarName ='';}
if (!empty($_POST['partSeminarContent'])) {$partSeminarContent =$_POST['partSeminarContent'];} else {$partSeminarContent ='';}
if (!empty($_POST['partSeminarLocation'])) {$partSeminarLocation =$_POST['partSeminarLocation'];} else {$partSeminarLocation ='';}
if (!empty($_POST['partSeminarAddress'])) {$partSeminarAddress =$_POST['partSeminarAddress'];} else {$partSeminarAddress ='';}
if (!empty($_POST['partSeminarCountry'])) {$partSeminarCountry =$_POST['partSeminarCountry'];} else {$partSeminarCountry ='';}
if (!empty($_POST['partSeminarLanguage'])) {$partSeminarLanguage =$_POST['partSeminarLanguage'];} else {$partSeminarLanguage ='';}
if (!empty($_POST['partSeminarHost'])) {$partSeminarHost =$_POST['partSeminarHost'];} else {$partSeminarHost ='';}
if (!empty($_POST['partSeminarSponsor'])) {$partSeminarSponsor =$_POST['partSeminarSponsor'];} else {$partSeminarSponsor ='';}
if (!empty($_POST['partSeminarContact'])) {$partSeminarContact =$_POST['partSeminarContact'];} else {$partSeminarContact ='';}
if (!empty($_POST['partSeminarRegistration'])) {$partSeminarRegistration =$_POST['partSeminarRegistration'];} else {$partSeminarRegistration ='';}
if (!empty($_POST['partSeminarFee'])) {$partSeminarFee =$_POST['partSeminarFee'];} else {$partSeminarFee ='';}
if (!empty($_POST['partSeminarWeb'])) {$partSeminarWeb =$_POST['partSeminarWeb'];} else {$partSeminarWeb ='';}
if (!empty($_POST['partSeminarStatus'])) {$partSeminarStatus =$_POST['partSeminarStatus'];} else {$partSeminarStatus ='';}
if (!empty($_POST['partSeminarFilename'])) {$partSeminarFilename =$_POST['partSeminarFilename'];} else {$partSeminarFilename ='';}
if (!empty($_POST['partSeminarFilesize'])) {$partSeminarFilesize =$_POST['partSeminarFilesize'];} else {$partSeminarFilesize ='';}
if (!empty($_POST['partSeminarRemarks'])) {$partSeminarRemarks =$_POST['partSeminarRemarks'];} else {$partSeminarRemarks ='';}

 // last change vk 2017-05-13
?>
