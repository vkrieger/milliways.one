<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	input								{ font-size:12px ; font-family: Arial, Verdana, sans-serif;}
	select,option,textarea 				{ font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td                         { font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	*  									{ font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>
!!! Do <b>NOT</b> use semicolon (;) in input fields !!!
<br>
<br>

<?php
	
$LoginCreateID = $_GET["LoginCreateID"];
	
include 'include_setSystemvariables.php';
include 'include_dbconnect.php';

	$dbquery = "SELECT * FROM logins WHERE '$LoginCreateID'=LoginCreateID ";
  	$dbresult = mysqli_query($link,$dbquery);  echo mysqli_error($link);
  	$dbrow = mysqli_fetch_array($dbresult);

	$updatedataset="";
	$createdataset="";
	$deletedataset="";

    session_start();
    //echo $_SESSION['LoginType'];
    
if ($updatedataset == "" AND $createdataset == "" AND $deletedataset == "")
	{
	echo '<form method="post" enctype="multipart/form-data" action="logins_save.php">';
	// if ($_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='superviewer' OR $_SESSION['LoginType']=='admin')
		{
		echo '<input type="submit" name="createdataset" value="create dataset">';
		echo '<input type="submit" name="updatedataset" value="update dataset">';
		echo '<input type="submit" name="deletedataset" value="delete dataset">';
		echo '<input type="reset" value="reset values">';
		echo '<br><br>';
		}

	echo '<table>';
	
	echo '<tr>';
	echo '<td align="right">GUID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="LoginGUID" size="60" maxlength="100" value="'.$dbrow['LoginGUID'].'" readonly></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">CreateID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="LoginCreateID" size="60" maxlength="100" value="'.$dbrow['LoginCreateID'].'" readonly></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">ArchiveID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="LoginArchiveID" size="60" maxlength="100" value="'.$dbrow['LoginArchiveID'].'" readonly></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">Owner</td>';
	echo '<td><input type="text" name="LoginOwner" size="60" maxlength="100" value="'.$dbrow['LoginOwner'].'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Login</td>';
	echo '<td><input type="text" name="LoginLogin" size="60" maxlength="100" value="'.$dbrow['LoginLogin'].'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Password</td>';
	echo '<td><input type="password" name="LoginPassword" size="60" maxlength="100" value="'.$dbrow['LoginPassword'].'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Type</td>';
	echo '<td>';

switch ($dbrow['LoginType'])
	{
	case "admin":
				echo '<select name="LoginType" size="1">';
				echo '<option>admin</option>';
				echo '<option>supereditor</option>';
				echo '<option>editor</option>';
				echo '<option>superviewer</option>';
				echo '<option>viewer</option>';
				echo '</select>';
				break;
	case "supereditor":
				echo '<select name="LoginType" size="1">';
				echo '<option>supereditor</option>';
				echo '<option>editor</option>';
				echo '<option>superviewer</option>';
				echo '<option>viewer</option>';
				echo '</select>';
				break;
	case "editor":
				echo '<select name="LoginType" size="1">';
				echo '<option>editor</option>';
				echo '<option>superviewer</option>';
				echo '<option>viewer</option>';
				echo '</select>';
				break;
	case "superviewer":
				echo '<select name="LoginType" size="1">';
				echo '<option>superviewer</option>';
				echo '<option>viewer</option>';
				echo '</select>';
				break;
	case "viewer":
				echo '<select name="LoginType" size="1">';
				echo '<option>viewer</option>';
				echo '</select>';
				break;
	}

	/*
	 echo '<tr>';
	echo '<td align="right">Project</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="LoginProject" size="60" maxlength="100" value="'.$dbrow['LoginProject'].'" readonly></td>';
	echo '</tr>';
	*/
	if (empty($SystemProject))
		{
		echo '<tr>';
		echo '<td align="right">Project</td>';
		echo '<td><select name="LoginProject" size="1">';
			foreach ($SystemProjectArray as $Project) 
				{echo '<option'; if ($dbrow['LoginProject']==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}
			echo '</select>';
		echo '</td>';		
		echo '</tr>';	
		}
		else 
		{
		echo '<input type="hidden" name="LoginProject" value="'.$SystemProject.'">';
		}echo '<tr>';
	echo '<td align="right">Remarks</td>';
	echo '<td><input type="text" name="LoginRemarks" size="60" maxlength="100" value="'.$dbrow['LoginRemarks'].'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Lastname</td>';
	echo '<td><input type="text" name="LoginLastname" size="60" maxlength="100" value="'.$dbrow['LoginLastname'].'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Firstname</td>';
	echo '<td><input type="text" name="LoginFirstname" size="60" maxlength="100" value="'.$dbrow['LoginFirstname'].'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Email</td>';
	echo '<td><input type="text" name="LoginPrimaryEmail" size="60" maxlength="100" value="'.$dbrow['LoginPrimaryEmail'].'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Tel</td>';
	echo '<td><input type="text" name="LoginTelBusiness" size="60" maxlength="100" value="'.$dbrow['LoginTelBusiness'].'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Mobile</td>';
	echo '<td><input type="text" name="LoginTelMobile" size="60" maxlength="100" value="'.$dbrow['LoginTelMobile'].'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Organization</td>';
	echo '<td><input type="text" name="LoginOrganization" size="60" maxlength="100" value="'.$dbrow['LoginOrganization'].'"></td>';
	echo '</tr>';
	
	echo '</table>';
	
	echo '</form>';
	
	} elseif ($updatedataset OR $createdataset OR $deletedataset);

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 23.03.20 11:00</div>';
?>
</font>
</body>
</html>
