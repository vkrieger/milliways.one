<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

!!! Do <b>NOT</b> use semicolon (;) in input fields !!! <b>NO</b> special characters and blanks in filenames - use underscore ( _ ) instead!!! 
<br><br>

<?php
	
include 'include_setAddressconstants.php';
include 'include_addresses_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

$AddressCreateID = $_GET["AddressCreateID"];

$dbquery = " SELECT * FROM addresses WHERE LOCATE ('$AddressCreateID', AddressCreateID) >0 ";
$dbresult = mysqli_query($link,$dbquery);
$dbrow = mysqli_fetch_array($dbresult);

	$updatedataset="";
	$createdataset="";
	$deletedataset="";
	$historydataset="";
	
if ($updatedataset == "" AND $createdataset == "" AND $deletedataset == ""  AND $historydataset == "" )
	{
	echo '<form method="post" action="addresses_save.php" enctype="multipart/form-data" >';
	if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
		{
		echo '<input type="submit" name="createdataset" value="create dataset">';
		echo '<input type="submit" name="updatedataset" value="update dataset">';
    	echo '<input type="submit" name="deletedataset" value="delete dataset">';
      	echo '<input type="reset" value="reset values">';
      	echo '<br><br>';
	    }

	echo '<table>';
	echo '<tr>';
	echo '<td align="right">GUID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="AddressGUID" size="60" maxlength="100" value="'.$dbrow['AddressGUID'].'" readonly></td>';
	echo '<td><input type="submit" name="historydataset" value="history of datasets"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">CreateID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="AddressCreateID" size="60" maxlength="100" value="'.$dbrow['AddressCreateID'].'" readonly></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">ArchiveID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="AddressArchiveID" size="60" maxlength="100" value="'.$dbrow['AddressArchiveID'].'" readonly></td>';
	echo '</tr>';
	echo '<input type="hidden" name="AddressInitialProject" value="'.$dbrow['AddressProject'].'">';
	echo '<input type="hidden" name="AddressProject" value="'.$dbrow['AddressProject'].'">';
	echo '<tr>';
	echo '<td align="right">Project</td>';
	// if SystemProject is not empty this should preselect the selected project 
	// but if this is called directly from listing no project is selected
	// then the SystemProject should be preselected
	// if SystemProject is empty this should preselect the selected project by the Project value of the selected dataset
	// but if this is called directly from listing public project is selected
	// SystemTypeArray and SystemRegimeArray are stored in SystemConstants
	/*
	from    list           modify
	empty   undef(public)  dbrow
	project system/dbrow   dbrow
	*/	
	echo '<td><select name="AddressProject" size="1">';
		if (!empty($SystemProject))
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($SystemProject==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		else
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($dbrow['AddressProject']==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		echo '</select></td>';		
	echo '</td>';		
	echo '</tr>';	
	
	echo '<tr>';
	echo '<td align="right">Owner (readonly)</td>';
	echo '<td>'.'['.$dbrow['AddressOwner'].'] - create/update transfers to '.'<input style="background-color:#C0C0C0" type="text" name="AddressOwner" size="15" maxlength="100" value="'.$_SESSION['LoginLogin'].'" readonly></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">Type</td>';
	echo '<td><select name="AddressType" size="1">';
			foreach ($SystemTypeArray as $Type) {echo '<option'; if ($dbrow['AddressType']==$Type) {echo ' selected';} echo '>'.$Type.'</option>';} 
			echo '</select></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">Category</td>';
	echo '<td><select name="AddressCategory" size="1">';
			foreach ($AddressCategoryArray as $Category) {echo '<option'; if ($dbrow['AddressCategory']==$Category) {echo ' selected';} echo '>'.$Category.'</option>';} 
			echo '</select></td>';
	echo '</tr>';
	
	echo '<tr><td align="right">Firstname </td><td><input type="text" name="AddressFirstname" size="60" maxlength="255" value="'.$dbrow['AddressFirstname'].'"></td></tr>';

	echo '<tr><td align="right">Lastname </td><td><input type="text" name="AddressLastname" size="60" maxlength="255" value="'.$dbrow['AddressLastname'].'"></td></tr>';

	echo '<tr><td align="right">Email Prim </td><td><input type="text" name="AddressEmailPrim" size="60" maxlength="255" value="'.$dbrow['AddressEmailPrim'].'"></td></tr>';

	echo '<tr><td align="right">Email Sec </td><td><input type="text" name="AddressEmailSec" size="60" maxlength="255" value="'.$dbrow['AddressEmailSec'].'"></td></tr>';

	echo '<tr><td align="right">Tel Business </td><td><input type="text" name="AddressTelBusiness" size="60" maxlength="255" value="'.$dbrow['AddressTelBusiness'].'"></td></tr>';

	echo '<tr><td align="right">Tel Private </td><td><input type="text" name="AddressTelPrivate" size="60" maxlength="255" value="'.$dbrow['AddressTelPrivate'].'"></td></tr>';

	echo '<tr><td align="right">Tel Mobile </td><td><input type="text" name="AddressTelMobile" size="60" maxlength="255" value="'.$dbrow['AddressTelMobile'].'"></td></tr>';

	echo '<tr><td align="right">Fax </td><td><input type="text" name="AddressFax" size="60" maxlength="255" value="'.$dbrow['AddressFax'].'"></td></tr>';

	echo '<tr><td align="right">Priv Address</td><td><input type="text" name="AddressPrivate" size="60" maxlength="255" value="'.$dbrow['AddressPrivate'].'"></td></tr>';

	echo '<tr><td align="right">Priv Town </td><td><input type="text" name="AddressPrivateTown" size="60" maxlength="255" value="'.$dbrow['AddressPrivateTown'].'"></td></tr>';

	echo '<tr><td align="right">Priv ZIP </td><td><input type="text" name="AddressPrivateZIP" size="60" maxlength="255" value="'.$dbrow['AddressPrivateZIP'].'"></td></tr>';

	echo '<tr><td align="right">Priv Country </td><td><input type="text" name="AddressPrivateCountry" size="60" maxlength="255" value="'.$dbrow['AddressPrivateCountry'].'"></td></tr>';

	echo '<tr><td align="right">Biz Address </td><td><input type="text" name="AddressBusiness" size="60" maxlength="255" value="'.$dbrow['AddressBusiness'].'"></td></tr>';

	echo '<tr><td align="right">Biz Town </td><td><input type="text" name="AddressBusinessTown" size="60" maxlength="255" value="'.$dbrow['AddressBusinessTown'].'"></td></tr>';

	echo '<tr><td align="right">Biz ZIP </td><td><input type="text" name="AddressBusinessZIP" size="60" maxlength="255" value="'.$dbrow['AddressBusinessZIP'].'"></td></tr>';

	echo '<tr><td align="right">Biz Country </td><td><input type="text" name="AddressBusinessCountry" size="60" maxlength="255" value="'.$dbrow['AddressBusinessCountry'].'"></td></tr>';

	echo '<tr><td align="right">Sector </td><td><input type="text" name="AddressSector" size="60" maxlength="255" value="'.$dbrow['AddressSector'].'"></td></tr>';

	echo '<tr><td align="right">Organization </td><td><input type="text" name="AddressOrganization" size="60" maxlength="255" value="'.$dbrow['AddressOrganization'].'"></td></tr>';

	echo '<tr><td align="right">Department </td><td><input type="text" name="AddressDepartment" size="60" maxlength="255" value="'.$dbrow['AddressDepartment'].'"></td></tr>';

	echo '<tr><td align="right">Position </td><td><input type="text" name="AddressPosition" size="60" maxlength="255" value="'.$dbrow['AddressPosition'].'"></td></tr>';

	echo '<tr><td align="right">Remarks </td><td><input type="text" name="AddressRemarks" size="60" maxlength="255" value="'.$dbrow['AddressRemarks'].'"></td></tr>';
	
	echo '</table>';

echo '</form>';

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-03 18:00</div>';

} elseif ( $updatedataset OR $createdataset OR $deletedataset OR historydataset)

?>
</body>
</html>
