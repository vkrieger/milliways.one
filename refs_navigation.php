<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; background-color:<?php echo $SystemColor; ?>;}
	input                               {font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

references listing criteria<br>

<?php

include 'include_setReferenceconstants.php';
include 'include_refs_postvariables.php';

if (!isset($_SESSION)) { session_start();}

	$listdatasets = "";
	
if ($listdatasets == "" AND $_SESSION['LoginType'])
	{		
    echo '<form method="post" enctype="multipart/form-data" action="refs_list.php" target="main">';
	
	$partReferenceArchiveID ='0000-00-00 00:00:00'; // to firstly display only non-archived datasets when navigation is called
	
	echo '<table>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ReferenceEN" value="yes" checked>PDF</td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ReferenceGUID" value="yes" >GUID</td>';
	echo '<td><input type="text" name="partReferenceGUID" size="20" maxlength="40" value="'.$partReferenceGUID.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ReferenceCreateID" value="yes" checked>CreateID(Link)</td>';
	echo '<td><input type="text" name="partReferenceCreateID" size="20" maxlength="40" value="'.$partReferenceCreateID.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ReferenceArchiveID" value="yes" >ArchiveID</td>';
	echo '<td><input type="text" name="partReferenceArchiveID" size="20" maxlength="40" value="'.$partReferenceArchiveID.'"></td>';
	echo '</tr>';
	echo '</table>';
	
	echo '<table>';
	// echo 'This is control code for SystemProject '.$SystemProject.'<br>';
	if (empty($SystemProject))
		{
		echo '<tr>';
		echo '<td><input type="checkbox" name="list_ReferenceProject" value="yes" checked>Project</td>';
		echo '<td><select type="text" name="partReferenceProject" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($SystemProjectArray as $Project) {echo '<option>'.$Project.'</option>';}
			echo '</select>';
		echo '</tr>';
		}
		else
		{
		echo '<input type="hidden" name="partReferenceProject" value="'.$SystemProject.'">';
		}
	
	// echo 'This is control code for partReferenceProject '.$partReferenceProject.'<br>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ReferenceOwner" value="yes" >Owner</td>';
	echo '<td><input type="text" name="partReferenceOwner" size="8" maxlength="40" value="'.$partReferenceOwner.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ReferenceType" value="yes" checked>Type</td>';
	echo '<td><select type="text" name="partReferenceType" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($ReferenceTypeArray as $Type) {echo '<option>'.$Type.'</option>';}
			echo '</select>';
    echo '</td>';
	echo '</tr>';    
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_ReferenceCategory" value="yes" checked>Category</td>';
	echo '<td><select type="text" name="partReferenceCategory" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($ReferenceCategoryArray as $Category) {echo '<option>'.$Category.'</option>';}
			echo '</select>';
    echo '</td>';
	echo '</tr>';    
	
echo '<tr><td><input type="checkbox" name="list_ReferenceName" value="yes" checked>Name</td><td><input type="text" name="partReferenceName" size="8" maxlength="40" value="'.$partReferenceName.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ReferenceSummary" value="yes" >Summary</td><td><input type="text" name="partReferenceSummary" size="8" maxlength="40" value="'.$partReferenceSummary.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ReferenceClient" value="yes" checked>Client</td><td><input type="text" name="partReferenceClient" size="8" maxlength="40" value="'.$partReferenceClient.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ReferenceDivision" value="yes" checked>Division</td><td><input type="text" name="partReferenceDivision" size="8" maxlength="40" value="'.$partReferenceDivision.'"></td></tr>';
	 '<tr><td><input type="checkbox" name="list_ReferenceContractor" value="yes" >Contractor</td><td><input type="text" name="partReferenceContractor" size="8" maxlength="40" value="'.$partReferenceContractor.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ReferenceCode" value="yes" >Code</td><td><input type="text" name="partReferenceCode" size="8" maxlength="40" value="'.$partReferenceCode.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ReferenceContact" value="yes" >Contact</td><td><input type="text" name="partReferenceContact" size="8" maxlength="40" value="'.$partReferenceContact.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ReferenceVolume" value="yes" >Volume</td><td><input type="text" name="partReferenceVolume" size="8" maxlength="40" value="'.$partReferenceVolume.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ReferenceFee" value="yes" >Fee</td><td><input type="text" name="partReferenceFee" size="8" maxlength="40" value="'.$partReferenceFee.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ReferenceStartdate" value="yes" >Startdate</td><td><input type="text" name="partReferenceStartdate" size="8" maxlength="40" value="'.$partReferenceStartdate.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ReferenceEnddate" value="yes" >Enddate</td><td><input type="text" name="partReferenceEnddate" size="8" maxlength="40" value="'.$partReferenceEnddate.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ReferenceLocation" value="yes" checked>Location</td><td><input type="text" name="partReferenceLocation" size="8" maxlength="40" value="'.$partReferenceLocation.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ReferenceCountry" value="yes" checked>Country</td><td><input type="text" name="partReferenceCountry" size="8" maxlength="40" value="'.$partReferenceCountry.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ReferenceDescription" value="yes" >Description</td><td><input type="text" name="partReferenceDescription" size="8" maxlength="40" value="'.$partReferenceDescription.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ReferenceScope" value="yes" >Scope</td><td><input type="text" name="partReferenceScope" size="8" maxlength="40" value="'.$partReferenceScope.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ReferenceDetails" value="yes" >Details</td><td><input type="text" name="partReferenceDetails" size="8" maxlength="40" value="'.$partReferenceDetails.'"></td></tr>';

echo '<tr><td><input type="checkbox" name="list_ReferenceStatus" value="yes" checked>Status</td><td><input type="text" name="partReferenceStatus" size="8" maxlength="40" value="'.$partReferenceStatus.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ReferenceFilename" value="yes" checked>Filename</td><td><input type="text" name="partReferenceFilename" size="8" maxlength="40" value="'.$partReferenceFilename.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_ReferenceRemarks" value="yes" >Remarks</td><td><input type="text" name="partReferenceRemarks" size="8" maxlength="40" value="'.$partReferenceRemarks.'"></td></tr>';

	echo '</table>';

	?>
		
		<!-- sorting part -->
        <table>
		<tr><td>first sorted by</td>
			<td><select name="first_sorted_by" size="1">
				<option value="ReferenceGUID" selected>GUID</option>
				<option value="ReferenceCreateID">CreateID</option>
				<option value="ReferenceArchiveID">ArchiveID</option>
				<option value="ReferenceProject">Project</option>
				<option value="ReferenceOwner" >Owner</option>
				<option value="ReferenceType" >Type</option>
				<option value="ReferenceCategory">Category</option>
				<option value="ReferenceName" >Name</option>
				<option value="ReferenceContact" >Contact</option>
				<option value="ReferenceStatus" >Status</option>
				<option value="ReferenceFilename" >Filename</option>
				<option value="ReferenceRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
      	<tr><td>then sorted by</td>
			<td><select name="then_sorted_by" size="1">
				<option value="ReferenceGUID">GUID</option>
				<option value="ReferenceCreateID">CreateID</option>
				<option value="ReferenceArchiveID">ArchiveID</option>
				<option value="ReferenceProject" >Project</option>
				<option value="ReferenceOwner" >Owner</option>
				<option value="ReferenceType" >Type</option>
				<option value="ReferenceCategory" selected>Category</option>
				<option value="ReferenceName" >Name</option>
				<option value="ReferenceContact" >Contact</option>
				<option value="ReferenceStatus" >Status</option>
				<option value="ReferenceFilename" >Filename</option>
				<option value="ReferenceRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	<tr><td>last sorted by</td>
			<td><select name="last_sorted_by" size="1">
				<option value="ReferenceGUID">GUID</option>
				<option value="ReferenceCreateID">CreateID</option>
				<option value="ReferenceArchiveID">ArchiveID</option>
				<option value="ReferenceProject" >Project</option>
				<option value="ReferenceOwner" selected>Owner</option>
				<option value="ReferenceType" >Type</option>
				<option value="ReferenceCategory" >Category</option>
				<option value="ReferenceName" >Name</option>
				<option value="ReferenceContact" >Contact</option>
				<option value="ReferenceStatus" >Status</option>
				<option value="ReferenceFilename" >Filename</option>
				<option value="ReferenceRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	</table>

		<br>

		<table>
		<input type="submit" name="listdatasets" value="list datasets">
      	<input type="reset" value="reset values">
		</table>

    </form>
<?php
} elseif ( $listdatasets );

echo '<div align="right" style="font-size: 8px;">last source change vk 18.03.17 18:00</div>';

?>
</body>
</html>