<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; background-color:<?php echo $SystemColor; ?>;}
	input                               {font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

pendings listing criteria<br>

<?php

include 'include_setPendingconstants.php';
include 'include_pendings_postvariables.php';

if (!isset($_SESSION)) { session_start();}

	$listdatasets = "";
	
if ($listdatasets == "" AND $_SESSION['LoginType'])
	{		
    echo '<form method="post" enctype="multipart/form-data" action="pendings_list.php" target="main">';
	
	$partPendingArchiveID ='0000-00-00 00:00:00'; // to firstly display only non-archived datasets when navigation is called
	
	echo '<table>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_PendingGUID" value="yes" >GUID</td>';
	echo '<td><input type="text" name="partPendingGUID" size="20" maxlength="40" value="'.$partPendingGUID.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_PendingCreateID" value="yes" checked>CreateID(Link)</td>';
	echo '<td><input type="text" name="partPendingCreateID" size="20" maxlength="40" value="'.$partPendingCreateID.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_PendingArchiveID" value="yes" >ArchiveID</td>';
	echo '<td><input type="text" name="partPendingArchiveID" size="20" maxlength="40" value="'.$partPendingArchiveID.'"></td>';
	echo '</tr>';
	echo '</table>';
	
	echo '<table>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_PendingOwner" value="yes" >Owner</td>';
	echo '<td><input type="text" name="partPendingOwner" size="8" maxlength="40" value="'.$partPendingOwner.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_PendingProject" value="yes" checked>Project</td>';
	if (empty($SystemProject))
		{
		echo '<td><select type="text" name="partPendingProject" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($SystemProjectArray as $Project) {echo '<option>'.$Project.'</option>';}
			echo '</select>';}
		else { echo '<input type="hidden" name="partPendingProject" value="'.$SystemProject.'">';}
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_PendingRaiser" value="yes" checked>Raiser</td><td>';
	echo '<input type="text" name="partPendingRaiser" size="8" maxlength="40" value="'.$partPendingRaiser.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_PendingOwner" value="yes" checked>Owner</td>';
	echo '<td><input type="text" name="partPendingOwner" size="8" maxlength="40" value="'.$partPendingOwner.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_PendingType" value="yes" >Type</td>';
	if ($SystemType == "public")
		{echo '<td><input style="background-color:#C0C0C0" type="text" name="partPendingType" size="8" maxlength="40" value="public" readonly></td>';}
		else
		{echo '<td><input type="text" name="partPendingType" size="8" maxlength="40" value="'.$partPendingType.'"></td>';}
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_PendingCategory" value="yes" >Category</td>';
	echo '<td><select type="text" name="partPendingCategory" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($PendingCategoryArray as $Category) {echo '<option>'.$Category.'</option>';}
			echo '</select>';
    echo '</td>';
	echo '</tr>';    
	
	echo '<tr><td><input type="checkbox" name="list_PendingName" value="yes" checked>Name</td><td><input type="text" name="partPendingName" size="8" maxlength="40" value="'.$partPendingName.'"></td></tr>';

	echo '<td><input type="checkbox" name="list_PendingStatus" value="yes" checked>Status</td>';
	echo '<td><select type="text" name="partPendingStatus" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($PendingStatusArray as $Status) {echo '<option>'.$Status.'</option>';}
			echo '</select>';
    echo '</td>';
	echo '</tr>';    

	
	echo '<tr><td><input type="checkbox" name="list_PendingContent" value="yes" >Content</td><td><input type="text" name="partPendingContent" size="8" maxlength="40" value="'.$partPendingContent.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_PendingTask" value="yes" >Task</td><td><input type="text" name="partPendingTask" size="8" maxlength="40" value="'.$partPendingTask.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_PendingResponsible" value="yes" >Responsible</td><td><input type="text" name="partPendingResponsible" size="8" maxlength="40" value="'.$partPendingResponsible.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_PendingProcess" value="yes" >Process</td><td><input type="text" name="partPendingProcess" size="8" maxlength="40" value="'.$partPendingProcess.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_PendingDeliverable" value="yes" >Deliverable</td><td><input type="text" name="partPendingDeliverable" size="8" maxlength="40" value="'.$partPendingDeliverable.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_PendingInterim" value="yes" >Interim</td><td><input type="text" name="partPendingInterim" size="8" maxlength="40" value="'.$partPendingInterim.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_PendingResult" value="yes" >Result</td><td><input type="text" name="partPendingResult" size="8" maxlength="40" value="'.$partPendingResult.'"></td></tr>';
	echo '<tr><td><input type="checkbox" name="list_PendingContract" value="yes" >Contract</td><td><input type="text" name="partPendingContract" size="8" maxlength="40" value="'.$partPendingContract.'"></td></tr>';
	
echo '<tr><td><input type="checkbox" name="list_PendingFilename" value="yes" >Filename</td><td><input type="text" name="partPendingFilename" size="8" maxlength="40" value="'.$partPendingFilename.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_PendingRemarks" value="yes" >Remarks</td><td><input type="text" name="partPendingRemarks" size="8" maxlength="40" value="'.$partPendingRemarks.'"></td></tr>';

	echo '</table>';

	?>
		
		<!-- sorting part -->
        <table>
		<tr><td>first sorted by</td>
			<td><select name="first_sorted_by" size="1">
				<option value="PendingGUID" selected>GUID</option>
				<option value="PendingCreateID">CreateID</option>
				<option value="PendingArchiveID">ArchiveID</option>
				<option value="PendingProject">Project</option>
				<option value="PendingOwner" >Owner</option>
				<option value="PendingType" >Type</option>
				<option value="PendingCategory">Category</option>
				<option value="PendingName" >Name</option>
				<option value="PendingStatus" >Status</option>
				<option value="PendingFilename" >Filename</option>
				<option value="PendingRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
      	<tr><td>then sorted by</td>
			<td><select name="then_sorted_by" size="1">
				<option value="PendingGUID">GUID</option>
				<option value="PendingCreateID">CreateID</option>
				<option value="PendingArchiveID">ArchiveID</option>
				<option value="PendingProject" >Project</option>
				<option value="PendingOwner" >Owner</option>
				<option value="PendingType" >Type</option>
				<option value="PendingCategory" selected>Category</option>
				<option value="PendingName" >Name</option>
				<option value="PendingStatus" >Status</option>
				<option value="PendingFilename" >Filename</option>
				<option value="PendingRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	<tr><td>last sorted by</td>
			<td><select name="last_sorted_by" size="1">
				<option value="PendingGUID">GUID</option>
				<option value="PendingCreateID">CreateID</option>
				<option value="PendingArchiveID">ArchiveID</option>
				<option value="PendingProject" >Project</option>
				<option value="PendingOwner" selected>Owner</option>
				<option value="PendingType" >Type</option>
				<option value="PendingCategory" >Category</option>
				<option value="PendingName" >Name</option>
				<option value="PendingStatus" >Status</option>
				<option value="PendingFilename" >Filename</option>
				<option value="PendingRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	</table>

		<br>

		<table>
		<input type="submit" name="listdatasets" value="list datasets">
      	<input type="reset" value="reset values">
		</table>

    </form>
<?php
} elseif ( $listdatasets );

echo '<div align="right" style="font-size: 8px;">last source change vk 15.05.17 18:00</div>';

?>
</body>
</html>