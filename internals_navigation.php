<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; background-color:<?php echo $SystemColor; ?>;}
	input                               {font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

internals listing criteria<br>

<?php

include 'include_setInternalconstants.php';
include 'include_internals_postvariables.php';

if (!isset($_SESSION)) { session_start();}

	$listdatasets = "";
	
if ($listdatasets == "" AND $_SESSION['LoginType'])
	{		
    echo '<form method="post" enctype="multipart/form-data" action="internals_list.php" target="main">';
	
	$partInternalArchiveID ='0000-00-00 00:00:00'; // to firstly display only non-archived datasets when navigation is called
	
	echo '<table>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_InternalGUID" value="yes" >GUID</td>';
	echo '<td><input type="text" name="partInternalGUID" size="20" maxlength="40" value="'.$partInternalGUID.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_InternalCreateID" value="yes" checked>CreateID(Link)</td>';
	echo '<td><input type="text" name="partInternalCreateID" size="20" maxlength="40" value="'.$partInternalCreateID.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_InternalArchiveID" value="yes" >ArchiveID</td>';
	echo '<td><input type="text" name="partInternalArchiveID" size="20" maxlength="40" value="'.$partInternalArchiveID.'"></td>';
	echo '</tr>';
	echo '</table>';
	
	echo '<table>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_InternalOwner" value="yes" >Owner</td>';
	echo '<td><input type="text" name="partInternalOwner" size="8" maxlength="40" value="'.$partInternalOwner.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_InternalProject" value="yes" checked>Project</td>';
	if (empty($SystemProject))
		{
		echo '<td><select type="text" name="partInternalProject" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($SystemProjectArray as $Project) {echo '<option>'.$Project.'</option>';}
			echo '</select>';}
		else { echo '<input type="hidden" name="partInternalProject" value="'.$SystemProject.'">';}
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_InternalOwner" value="yes" >Owner</td>';
	echo '<td><input type="text" name="partInternalOwner" size="8" maxlength="40" value="'.$partInternalOwner.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_InternalInterners" value="yes" >Interners</td>';
	echo '<td><input type="text" name="partInternalInterners" size="8" maxlength="40" value="'.$partInternalInterners.'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_InternalType" value="yes" >Type</td>';
	if ($SystemType == "public")
		{echo '<td><input style="background-color:#C0C0C0" type="text" name="partInternalType" size="8" maxlength="40" value="public" readonly></td>';}
		else
		{echo '<td><input type="text" name="partInternalType" size="8" maxlength="40" value="'.$partInternalType.'"></td>';}
	echo '</tr>';
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_InternalCategory" value="yes" >Category</td>';
	echo '<td><select type="text" name="partInternalCategory" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($InternalCategoryArray as $Category) {echo '<option>'.$Category.'</option>';}
			echo '</select>';
    echo '</td>';
	echo '</tr>';    
	
echo '<tr><td><input type="checkbox" name="list_InternalName" value="yes" checked>Name</td><td><input type="text" name="partInternalName" size="8" maxlength="40" value="'.$partInternalName.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_InternalStatus" value="yes" >Status</td><td><input type="text" name="partInternalStatus" size="8" maxlength="40" value="'.$partInternalStatus.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_InternalFilenames" value="yes" checked>Filenames</td><td><input type="text" name="partInternalFilenames" size="8" maxlength="40" value="'.$partInternalFilenames.'"></td></tr>';
echo '<tr><td><input type="checkbox" name="list_InternalRemarks" value="yes" >Remarks</td><td><input type="text" name="partInternalRemarks" size="8" maxlength="40" value="'.$partInternalRemarks.'"></td></tr>';

	echo '</table>';

	?>
		
		<!-- sorting part -->
        <table>
		<tr><td>first sorted by</td>
			<td><select name="first_sorted_by" size="1">
				<option value="InternalGUID" selected>GUID</option>
				<option value="InternalCreateID">CreateID</option>
				<option value="InternalArchiveID">ArchiveID</option>
				<option value="InternalProject">Project</option>
				<option value="InternalOwner" >Owner</option>
				<option value="InternalType" >Type</option>
				<option value="InternalCategory">Category</option>
				<option value="InternalName" >Name</option>
				<option value="InternalStatus" >Status</option>
				<option value="InternalFilenames" >Filename</option>
				<option value="InternalRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
      	<tr><td>then sorted by</td>
			<td><select name="then_sorted_by" size="1">
				<option value="InternalGUID">GUID</option>
				<option value="InternalCreateID">CreateID</option>
				<option value="InternalArchiveID">ArchiveID</option>
				<option value="InternalProject" >Project</option>
				<option value="InternalOwner" >Owner</option>
				<option value="InternalType" >Type</option>
				<option value="InternalCategory" selected>Category</option>
				<option value="InternalName" >Name</option>
				<option value="InternalStatus" >Status</option>
				<option value="InternalFilenames" >Filename</option>
				<option value="InternalRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	<tr><td>last sorted by</td>
			<td><select name="last_sorted_by" size="1">
				<option value="InternalGUID">GUID</option>
				<option value="InternalCreateID">CreateID</option>
				<option value="InternalArchiveID">ArchiveID</option>
				<option value="InternalProject" >Project</option>
				<option value="InternalOwner" selected>Owner</option>
				<option value="InternalType" >Type</option>
				<option value="InternalCategory" >Category</option>
				<option value="InternalName" >Name</option>
				<option value="InternalStatus" >Status</option>
				<option value="InternalFilenames" >Filename</option>
				<option value="InternalRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	</table>

		<br>

		<table>
		<input type="submit" name="listdatasets" value="list datasets">
      	<input type="reset" value="reset values">
		</table>

    </form>
<?php
} elseif ( $listdatasets );

echo '<div align="right" style="font-size: 8px;">last source change vk 21.05.17 18:00</div>';

?>
</body>
</html>