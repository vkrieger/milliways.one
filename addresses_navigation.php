<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; background-color:<?php echo $SystemColor; ?>;}
	input                               {font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; background-color:#FFFFFF; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>
addresses listing criteria<br />
<?php

include 'include_setAddressconstants.php';
include 'include_addresses_postvariables.php';

if (!isset($_SESSION)) { session_start();}

	$listdatasets = "";
	
if ($listdatasets == "" AND $_SESSION['LoginType'])
	{		
    echo '<form method="post" enctype="multipart/form-data" action="addresses_list.php" target="main">';
	
	$partAddressArchiveID ='0000-00-00 00:00:00'; // to firstly display only non-archived datasets when navigation is called
	
	echo '<table>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_AddressGUID" value="yes" >GUID</td>';
	echo '<td><input type="text" name="partAddressGUID" size="20" maxlength="40" value="'.$partAddressGUID.'"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_AddressCreateID" value="yes" checked>CreateID(Link)</td>';
	echo '<td><input type="text" name="partAddressCreateID" size="20" maxlength="40" value="'.$partAddressCreateID.'"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_AddressArchiveID" value="yes" >ArchiveID</td>';
	echo '<td><input type="text" name="partAddressArchiveID" size="20" maxlength="40" value="'.$partAddressArchiveID.'"></td>';
	echo '</tr>';
	
	echo '</table>';
	
	echo '<table>';
	
	// echo 'This is control code for SystemProject '.$SystemProject.'<br>';
	// at this point all project CDEs with empty SystemProject (e.g. admin etc.) will show Project drop down
	// CDEs with non-empty System Project stay only in their regime list additionally all public type datasets
	// but they can choose the list_AppProject option for listing
	// the distinction towards SystemProject is necessary to avoid all projects in listing
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_AddressProject" value="yes" checked>Project</td>';
	if (empty($SystemProject))
		{
		echo '<td><select type="text" name="partAddressProject" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($SystemProjectArray as $Project) {echo '<option>'.$Project.'</option>';}
			echo '</select>';}
		else { echo '<input type="hidden" name="partAddressProject" value="'.$SystemProject.'">';}
	echo '</tr>';

	// echo 'This is control code for partAddressProject '.$partAddressProject.'<br />';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_AddressOwner" value="yes" >Owner</td>';
	echo '<td><input type="text" name="partAddressOwner" size="8" maxlength="40" value="'.$partAddressOwner.'"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_AddressType" value="yes" >Type</td>';
	if ($SystemType == "public")
		{echo '<td><input style="background-color:#C0C0C0" type="text" name="partAddressType" size="8" maxlength="40" value="public" readonly></td>';}
		else
		{echo '<td><input type="text" name="partAddressType" size="8" maxlength="40" value="'.$partAddressType.'"></td>';}
	echo '</tr>';
	
	echo '<tr>';
	echo '<td><input type="checkbox" name="list_AddressCategory" value="yes" >Category</td>';
	echo '<td><select type="text" name="partAddressCategory" size="1">';
			echo '<option value="" selected>all</option>';
			foreach ($AddressCategoryArray as $Category) {echo '<option>'.$Category.'</option>';}
			echo '</select>';
    echo '</td>';
	echo '</tr>';    
	
	echo '<tr><td><input type="checkbox" name="list_AddressFirstname" value="yes" >Firstname</td><td><input type="text" name="partAddressFirstname" size="8" maxlength="40" value="'.$partAddressFirstname.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_AddressLastname" value="yes" checked>Lastname</td><td><input type="text" name="partAddressLastname" size="8" maxlength="40" value="'.$partAddressLastname.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_AddressEmail" value="yes" checked>Email</td><td><input type="text" name="partAddressEmail" size="8" maxlength="40" value="'.$partAddressEmail.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_AddressTelecom" value="yes" checked>Telecom</td><td><input type="text" name="partAddressTelecom" size="8" maxlength="40" value="'.$partAddressTelecom.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_AddressPrivate" value="yes" >Private</td><td><input type="text" name="partAddressPrivate" size="8" maxlength="40" value="'.$partAddressPrivate.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_AddressBusiness" value="yes" >Business</td><td><input type="text" name="partAddressBusiness" size="8" maxlength="40" value="'.$partAddressBusiness.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_AddressSector" value="yes" >Sector</td><td><input type="text" name="partAddressSector" size="8" maxlength="40" value="'.$partAddressSector.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_AddressOrganization" value="yes" >Organization</td><td><input type="text" name="partAddressOrganization" size="8" maxlength="40" value="'.$partAddressOrganization.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_AddressDepartment" value="yes" >Department</td><td><input type="text" name="partAddressDepartment" size="8" maxlength="40" value="'.$partAddressDepartment.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_AddressPosition" value="yes" >Position</td><td><input type="text" name="partAddressPosition" size="8" maxlength="40" value="'.$partAddressPosition.'"></td></tr>';

	echo '<tr><td><input type="checkbox" name="list_AddressRemarks" value="yes" >Remarks</td><td><input type="text" name="partAddressRemarks" size="8" maxlength="40" value="'.$partAddressRemarks.'"></td></tr>';

	echo '</table>';

	?>
		
		<!-- sorting part -->
        <table>
		<tr><td>first sorted by</td>
			<td><select name="first_sorted_by" size="1">
				<option value="AddressGUID" selected>GUID</option>
				<option value="AddressCreateID">CreateID</option>
				<option value="AddressArchiveID">ArchiveID</option>
				<option value="AddressProject">Project</option>
				<option value="AddressOwner" >Owner</option>
				<option value="AddressType" >Type</option>
				<option value="AddressCategory">Category</option>
				<option value="AddressFirstname" >First Name</option>
				<option value="AddressLastname" >Last Name</option>
				<option value="AddressPrivateTown" >Priv Town</option>
				<option value="AddressPrivateZIP" >Priv ZIP</option>
				<option value="AddressPrivateCountry" >Priv Country</option>
				<option value="AddressBusinessTown" >Biz Town</option>
				<option value="AddressBusinessZIP" >Biz ZIP</option>
				<option value="AddressBusinessCountry" >Biz Country</option>
				<option value="AddressSector" >Sector</option>
				<option value="AddressOrganization" >Orga</option>
				<option value="AddressDepartment" >Department</option>
				<option value="AddressPosition" >Position</option>
				<option value="AddressRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
      	<tr><td>then sorted by</td>
			<td><select name="then_sorted_by" size="1">
				<option value="AddressGUID" >GUID</option>
				<option value="AddressCreateID">CreateID</option>
				<option value="AddressArchiveID">ArchiveID</option>
				<option value="AddressProject" >Project</option>
				<option value="AddressOwner" >Owner</option>
				<option value="AddressType" >Type</option>
				<option value="AddressCategory" selected>Category</option>
				<option value="AddressFirstname" >First Name</option>
				<option value="AddressLastname" >Last Name</option>
				<option value="AddressPrivateTown" >Priv Town</option>
				<option value="AddressPrivateZIP" >Priv ZIP</option>
				<option value="AddressPrivateCountry" >Priv Country</option>
				<option value="AddressBusinessTown" >Biz Town</option>
				<option value="AddressBusinessZIP" >Biz ZIP</option>
				<option value="AddressBusinessCountry" >Biz Country</option>
				<option value="AddressSector" >Sector</option>
				<option value="AddressOrganization" >Orga</option>
				<option value="AddressDepartment" >Department</option>
				<option value="AddressPosition" >Position</option>
				<option value="AddressRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	<tr><td>last sorted by</td>
			<td><select name="last_sorted_by" size="1">
				<option value="AddressGUID" >GUID</option>
				<option value="AddressCreateID">CreateID</option>
				<option value="AddressArchiveID">ArchiveID</option>
				<option value="AddressProject" >Project</option>
				<option value="AddressOwner" selected>Owner</option>
				<option value="AddressType" >Type</option>
				<option value="AddressCategory" >Category</option>
				<option value="AddressFirstname" >First Name</option>
				<option value="AddressLastname" >Last Name</option>
				<option value="AddressPrivateTown" >Priv Town</option>
				<option value="AddressPrivateZIP" >Priv ZIP</option>
				<option value="AddressPrivateCountry" >Priv Country</option>
				<option value="AddressBusinessTown" >Biz Town</option>
				<option value="AddressBusinessZIP" >Biz ZIP</option>
				<option value="AddressBusinessCountry" >Biz Country</option>
				<option value="AddressSector" >Sector</option>
				<option value="AddressOrganization" >Orga</option>
				<option value="AddressDepartment" >Department</option>
				<option value="AddressPosition" >Position</option>
				<option value="AddressRemarks" >Remarks</option>
				</select>
      		</td>
      	</tr>
    	</table>

		<br>

		<table>
		<input type="submit" name="listdatasets" value="list datasets">
      	<input type="reset" value="reset values">
		</table>

    </form>
<?php
} elseif ( $listdatasets );

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-03 18:00</div>';

?>
</body>
</html>
