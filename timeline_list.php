<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

You may download <a href="docslist.xls">this Table</a> in Spreadsheet-Format (e.g. as CSV for MS-Excel) from Server.
<br /><br />
    
<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

include 'include_setTimelineconstants.php';
include 'include_timeline_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
	{
	echo '<form method="post" enctype="multipart/form-data" action="timeline_save.php">';
	echo '<input type="submit" name="createdataset" value="create dataset">';
	echo '</form>';
	}

	// all timeline entries are stored in strings as string are arrays. first stringposition is $string[0]. thus first day is at this position.
	// making startday and endday (1-365) from unix-timestamp of start- and enddate by using position 'yday' of getdatearray
	// day 1 is on stringpos 0 of stringarrays
	if (!empty($_POST["startdate"])) 			{ $startdate   		= strtotime($_POST["startdate"]);} 	else {$startdate   		= "";}
	if (!empty($_POST["enddate"])) 				{ $enddate   		= strtotime($_POST["enddate"]);} 	else {$enddate   		= "";}
	
	if (!empty($_POST["TimelineNameArray"])) 	{ $TimelineNameArray	= $_POST["TimelineNameArray"];} else {$TimelineNameArray 	= "";}
	// control code print_r ($TimelineNameArray);echo '<br>';
		
	echo '<br /><br />';
	
	$startarray = getdate($startdate); 	
	$startday = $startarray['yday']+1; // array getdate contains yearday (0-365) at postion 'yday'
	$startyear = $startarray['year'];
	$endarray = getdate($enddate); 	
	$endday = $endarray['yday']+1; // array getdate contains yearday (0-365) at postion 'yday'
	$endyear = $endarray['year'];
	

  	$dbquery = "SELECT * FROM timeline WHERE
LOCATE('$partTimelineGUID', TimelineGUID)>0 AND
LOCATE('$partTimelineCreateID', TimelineCreateID)>0 AND
LOCATE('$partTimelineArchiveID', TimelineArchiveID)>0 AND
(LOCATE('$partTimelineProject', TimelineProject)>0  OR LOCATE('public', TimelineType)>0) AND 
LOCATE('$partTimelineOwner', TimelineOwner)>0 AND
LOCATE('$partTimelineType', TimelineType)>0 AND
LOCATE('$partTimelineCategory', TimelineCategory)>0 AND
LOCATE('$partTimelineOwner', TimelineOwner)>0 AND
LOCATE('$partTimelineRemarks', TimelineRemarks)>0 
	ORDER BY TimelineName " ;
	
  	$dbresult = mysqli_query($link,$dbquery);  echo mysqli_error($link);
  	
  	$header =""; //empty header for excel file (dataset structure)
  	$data	=""; //empty $data variable for excel file (all rows in one variable)

	echo '<table border="1" cellspacing="0" style="width:200px">';

	// table header
	echo '<tr>'; 
	// 
	if ($list_TimelineGUID=='yes') 		{echo '<td valign="top">GUID</td>'; $header .=""."\t";}
	if ($list_TimelineCreateID=='yes') 	{echo '<td valign="top">CreateID</td>'; $header .=""."\t";}
	if ($list_TimelineArchiveID=='yes') {echo '<td valign="top">ArchiveID</td>'; $header .=""."\t";}
	if ($list_TimelineProject=='yes') 	{echo '<td valign="top">Project</td>'; $header .=""."\t";}
	if ($list_TimelineOwner=='yes') 	{echo '<td valign="top">Owner</td>'; $header .=""."\t";}
	if ($list_TimelineName=='yes') 		{echo '<td valign="top">Name</td>'; $header .=""."\t";}
	if ($list_TimelineType=='yes') 		{echo '<td valign="top">Type</td>'; $header .=""."\t";}
	if ($list_TimelineCategory=='yes') 	{echo '<td valign="top">Category</td>'; $header .=""."\t";}
	if ($list_TimelineRemarks=='yes') 	{echo '<td valign="top">Remarks</td>'; $header .=""."\t";}
	//Month, Monthdate and weekday header
	$d = $startday;
	$y = $startyear;
	while (($d <= $endday) AND ($y <= $endyear)) 
		{	
		echo '<td align="right">';
		echo date("Y", mktime(0, 0, 0, 1, $d, $y)).'<br>';
		echo date("M", mktime(0, 0, 0, 1, $d, $y)).'<br>';
		echo date("j", mktime(0, 0, 0, 1, $d, $y)).'<br>';
		echo date("D", mktime(0, 0, 0, 1, $d, $y));
		echo '</td>';
		$header .= date("Y", mktime(0, 0, 0, 1, $d, $y)); 
		$header .= date("M", mktime(0, 0, 0, 1, $d, $y)); 
		$header .= date("j", mktime(0, 0, 0, 1, $d, $y)); 
		$header .= date("D", mktime(0, 0, 0, 1, $d, $y));
		$header .=""."\t";
		if ($d<=365+date("L",mktime(0,0,0,1,1,$y))) 
			{$d++;} else {$d=0; $y++;}	
		}
	echo '</tr>';
	$header .= "\n"; //end of datarow in excel file 
	

	// table body
	while($dbrow = mysqli_fetch_array($dbresult))
   		{
		// control code echo 'array_search:'.array_search($dbrow['TimelineName'], $TimelineNameArray);
		// control code echo '<br>';
		// control code echo '$key:'.$key=(array_search($dbrow['TimelineName'], $TimelineNameArray));
		// control code echo '<br>';
		if (!empty(array_search($dbrow['TimelineName'], $TimelineNameArray)) OR (array_search($dbrow['TimelineName'], $TimelineNameArray))===0)
			{
			echo '<tr>';
			if ($list_TimelineGUID=='yes') {echo '<td>'.$dbrow['TimelineGUID'].'</td>'; $data .= '"'.$dbrow['TimelineGUID'].'"'. "\t";}
			if ($list_TimelineCreateID=='yes')
				{
				echo '<td class="bluelink"><a style="font-size:xx-small" href="timeline_modify.php?TimelineCreateID='.$dbrow['TimelineCreateID'].'">'.$dbrow['TimelineCreateID'].'</a></td>';
				$data .= '"'.$dbrow['TimelineCreateID'].'"'. "\t";
				}
			if ($list_TimelineArchiveID=='yes') {echo '<td>'.$dbrow['TimelineArchiveID'].'</td>'; $data .= '"'.$dbrow['TimelineArchiveID'].'"'. "\t";}
			if ($list_TimelineProject=='yes') {echo '<td>'.$dbrow['TimelineProject'].'</td>'; $data .= '"'.$dbrow['TimelineProject'].'"'. "\t";}
			if ($list_TimelineOwner=='yes') {echo '<td>'.$dbrow['TimelineOwner'].'</td>'; $data .= '"'.$dbrow['TimelineOwner'].'"'. "\t";}
			if ($list_TimelineName=='yes') {echo '<td>'.$dbrow['TimelineName'].'</td>'; $data .= '"'.$dbrow['TimelineName'].'"'. "\t";}
			if ($list_TimelineType=='yes') {echo '<td>'.$dbrow['TimelineType'].'</td>'; $data .= '"'.$dbrow['TimelineType'].'"'. "\t";}
			if ($list_TimelineCategory=='yes') {echo '<td>'.$dbrow['TimelineCategory'].'</td>'; $data .= '"'.$dbrow['TimelineCategory'].'"'. "\t";}
			if ($list_TimelineRemarks=='yes') {echo '<td>'.$dbrow['TimelineRemarks'].'</td>'; $data .= '"'.$dbrow['TimelineRemarks'].'"'. "\t";}
		
			$d = $startday;
			$y = $startyear;
			while (($d <= $endday) AND ($y <= $endyear)) 
				{	
				echo '<td ';
				switch (substr($dbrow['Timeline'.$y],$d-1,1)) //day 1 is on stringposition 0
					{
					case "X": echo 'style="background-color:#FE0000"'; break;
					case "S": echo 'style="background-color:#00FF00"'; break;
					}
				echo '>'.substr($dbrow['Timeline'.$y],$d-1,1).'</td>';
				$data .= '"'.substr($dbrow['Timeline'.$y],$d-1,1).'"'. "\t";
				// 	increase $d++ if not exceeding total days of current year
				//  total days of current year is given by $y+1 for leap year
				if ($d<=365+date("L",mktime(0,0,0,1,1,$y))) // to detect leap year
					{$d++;} else {$d=0; $y++;}	
				}
			echo '</tr>';
			$data .= "\n"; //end of dataset in excel file (marks row end in data)
			}
		}

	$data .= "\n"; //end of dataset in excel file (marks row end in data)

	echo '</table>';
	echo '';

/*
export selected table to .xls onto local server folder to download as offered from there
*/

$fp = fopen('timelinelist.xls','w');
fwrite($fp,$header);
fwrite($fp,"\n");
fwrite($fp,$data);
fclose($fp);

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-08 18:00</div>';
?>
</body>
</html>
