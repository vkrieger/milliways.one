<?php

/*
Systemvariables and Systemconstants apply to all application routines in the System
Applicationconstants apply only to specific application routine
the following Systemvariables are defined
*/
		/*
		to differ between and set system dataftp folder
		must be created on server at /data/ftp/
		all files will be stored in /data/ftp/dataftpfolder

		insert the following in httpd.conf after last <Directory> section
		to make sure stored files can be accessed outside the document route
		
		# change by ... on 2020-11-22
		Alias /dataftp /data/ftp
		Alias /dataftp/ /data/ftp/
		<Directory "/data/ftp/">
		Options Indexes FollowSymLinks ExecCGI Includes
		AllowOverride All
		Require all granted
		</Directory>

		make sure apache:apache is recursive owner of both folder tree
		extend write access to group of public_html folder(s) 
		to allow for ftp write transfer of users that are member of group apache

		make sure selinux context is properly set
		try permissive mode if upload is not working
		then try chcon -t httpd_sys_rw_content_t /data/ftp -R
		it may be necessary to engage the VNC-GUI and use the SE Troubleshooter for further adaptation
		# restorecon -v 'data/ftp'
		# ausearch -c 'httpd' --raw | audit2allow -M my-httpd
		# semodule -i my-httpd-pp

		*/
		$dataftpfolder = "brecht-com";
		
		/*
		weblogin determines SystemProject
		valid weblogins are listed in .htaccess file
		password for valid weblogins are in dedicated passwordfiles 
		
		$SystemProject determines project ownership of dataset		
		$SystemProject admin and public accesses all projects thus $SystemProject is empty for all projects
		$SystemProject public has restricted $SystemType public

		$SystemType determines accessibilty
		$SystemType can be "public" or empty
		$SystemType "public" results

		$SystemRegime is placeholder for future extensions
		$SystemRegime is intended to be used in CDE project context

		$SystemColor is background color os header and navigation bar/frame
		$SystemColor is often determined by the clients typical logo color
		$SystemColor value may be picked by color picking on clients webpage

		$SystemLogo is the file name of the logo be displayed in the logo frame by logo.php
		$SystemLogo file must be placed in document root public_html
		$SystemLogo may be JPEG or PNG file of maximum 150x150 px

		$SystemApp is a list of app names separated by blanks
		$SystemApp is checked in header.php if app is to be displayed and make it accessible

		$Systemowner is currently not in use 

		*/
		switch ($_SERVER['PHP_AUTH_USER'])
			{
				case "admin": 					// shows also public projects //123guede
					$SystemProject	=""; 			// all projects displayed
					$SystemType		=""; 		// all types displayed
					$SystemRegime	=""; 			// all regimes displayed
					$SystemColor    ="#C0FF00"; 		// 
					$SystemLogo	="the_unix_qr-code.png";
					$SystemApps	="addresses contacts docs events internals napkins programs seminars timeline";
					break;
					
				case "BrechtNet": 				// shows also public projects //VergissEs
					$SystemProject	="BrechtNet"; 		// only intranet projects displayed
					$SystemType		=""; 		// all types displayed
					$SystemRegime	=""; 			// all regimes displayed
					$SystemColor    ="#38FFA8"; 		// 
					$SystemLogo	="the_unix_qr-code.png";
					$SystemApps	="contacts docs internals napkins programs seminars timeline";
					break;
					
			}

// last change vkrieger 2021-02-05 21:00

?>
