<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

!!! Do <b>NOT</b> use semicolon (;) in input fields !!! <b>NO</b> blanks in filenames - use underscore ( _ ) instead!!! 
<br><br>

<?php
		
include 'include_setProjectconstants.php';
include 'include_projects_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

$ProjectCreateID = $_GET["ProjectCreateID"];

$dbquery = " SELECT * FROM projects WHERE LOCATE ('$ProjectCreateID', ProjectCreateID) >0 ";
$dbresult = mysqli_query($link,$dbquery);
$dbrow = mysqli_fetch_array($dbresult);

	$updatedataset="";
	$createdataset="";
	$deletedataset="";
	
if ($updatedataset == "" AND $createdataset == "" AND $deletedataset == "" )
	{
	echo '<form method="post" action="projects_save.php" enctype="multipart/form-data" >';
	if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
		{
		echo '<input type="submit" name="createdataset" value="create dataset">';
		echo '<input type="submit" name="updatedataset" value="update dataset">';
    	echo '<input type="submit" name="deletedataset" value="delete dataset">';
      	echo '<input type="reset" value="reset values">';
      	echo '<br><br>';
	    }

	echo '<table>';
	echo '<tr>';
	echo '<td align="right">GUID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="ProjectGUID" size="60" maxlength="100" value="'.$dbrow['ProjectGUID'].'" readonly></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">CreateID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="ProjectCreateID" size="60" maxlength="100" value="'.$dbrow['ProjectCreateID'].'" readonly></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">ArchiveID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="ProjectArchiveID" size="60" maxlength="100" value="'.$dbrow['ProjectArchiveID'].'" readonly></td>';
	echo '</tr>';
	echo '<input type="hidden" name="ProjectInitialProject" value="'.$dbrow['ProjectProject'].'">';
	echo '<input type="hidden" name="ProjectProject" value="'.$dbrow['ProjectProject'].'">';
	echo '<tr>';
	echo '<td align="right">Project</td>';
	echo '<td><select name="ProjectProject" size="1">';
		if (!empty($SystemProject))
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($SystemProject==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		else
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($dbrow['ProjectProject']==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		echo '</select></td>';		
		echo '<tr>';
	
	echo '<td align="right">Owner (readonly)</td>';
	echo '<td>'.'['.$dbrow['ProjectOwner'].'] - create/update transfers to '.'<input style="background-color:#C0C0C0" type="text" name="ProjectOwner" size="15" maxlength="100" value="'.$_SESSION['LoginLogin'].'" readonly></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Type</td>';
	echo '<td><select name="ProjectType" size="1">';
			foreach ($SystemTypeArray as $Type) {echo '<option'; if ($dbrow['ProjectType']==$Type) {echo ' selected';} echo '>'.$Type.'</option>';} 
			echo '</select></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Category</td>';
	echo '<td><select name="ProjectCategory" size="1">';
			foreach ($ProjectCategoryArray as $Category) {echo '<option'; if ($dbrow['ProjectCategory']==$Category) {echo ' selected';} echo '>'.$Category.'</option>';} 
			echo '</select></td>';
	echo '</tr>';

echo '<tr><td align="right">Name </td><td><input type="text" name="ProjectName" size="60" maxlength="255" value="'.$dbrow['ProjectName'].'"></td></tr>';

	echo '<tr>';
	echo '<td align="right">Status</td>';
	echo '<td><select name="ProjectStatus" size="1">';
			foreach ($ProjectStatusArray as $Status) {echo '<option'; if ($dbrow['ProjectStatus']==$Status) {echo ' selected';} echo '>'.$Status.'</option>';} 
			echo '</select></td>';
	echo '</tr>';

echo '<tr><td align="right">AquisitionNo</td><td><input type="text" name="ProjectAquisitionNo" size="60" maxlength="255" value="'.$dbrow['ProjectAquisitionNo'].'"></td></tr>';
echo '<tr><td align="right">Number </td><td><input type="text" name="ProjectNumber" size="60" maxlength="255" value="'.$dbrow['ProjectNumber'].'"></td></tr>';
echo '<tr><td align="right">Code </td><td><input type="text" name="ProjectCode" size="60" maxlength="255" value="'.$dbrow['ProjectCode'].'"></td></tr>';
echo '<tr><td align="right">Description </td><td><input type="text" name="ProjectDescription" size="60" maxlength="255" value="'.$dbrow['ProjectDescription'].'"></td></tr>';
echo '<tr><td align="right">Country </td><td><input type="text" name="ProjectCountry" size="60" maxlength="255" value="'.$dbrow['ProjectCountry'].'"></td></tr>';
echo '<tr><td align="right">Location </td><td><input type="text" name="ProjectLocation" size="60" maxlength="255" value="'.$dbrow['ProjectLocation'].'"></td></tr>';
echo '<tr><td align="right">Client </td><td><input type="text" name="ProjectClient" size="60" maxlength="255" value="'.$dbrow['ProjectClient'].'"></td></tr>';
echo '<tr><td align="right">Origin </td><td><input type="text" name="ProjectOrigin" size="60" maxlength="255" value="'.$dbrow['ProjectOrigin'].'"></td></tr>';
echo '<tr><td align="right">Budget </td><td><input type="text" name="ProjectBudget" size="60" maxlength="255" value="'.$dbrow['ProjectBudget'].'"></td></tr>';
echo '<tr><td align="right">Figures </td><td><input type="text" name="ProjectFigures" size="60" maxlength="255" value="'.$dbrow['ProjectFigures'].'"></td></tr>';
echo '<tr><td align="right">Period </td><td><input type="text" name="ProjectPeriod" size="60" maxlength="255" value="'.$dbrow['ProjectPeriod'].'"></td></tr>';
echo '<tr><td align="right">Fee </td><td><input type="text" name="ProjectFee" size="60" maxlength="255" value="'.$dbrow['ProjectFee'].'"></td></tr>';
echo '<tr><td align="right">Charges </td><td><input type="text" name="ProjectCharges" size="60" maxlength="255" value="'.$dbrow['ProjectCharges'].'"></td></tr>';
echo '<tr><td align="right">IntServices </td><td><input type="text" name="ProjectIntServices" size="60" maxlength="255" value="'.$dbrow['ProjectIntServices'].'"></td></tr>';
echo '<tr><td align="right">ExtServices </td><td><input type="text" name="ProjectExtServices" size="60" maxlength="255" value="'.$dbrow['ProjectExtServices'].'"></td></tr>';
echo '<tr><td align="right">Director </td><td><input type="text" name="ProjectDirector" size="60" maxlength="255" value="'.$dbrow['ProjectDirector'].'"></td></tr>';
echo '<tr><td align="right">Manager </td><td><input type="text" name="ProjectManager" size="60" maxlength="255" value="'.$dbrow['ProjectManager'].'"></td></tr>';
echo '<tr><td align="right">Participants </td><td><input type="text" name="ProjectParticipants" size="60" maxlength="255" value="'.$dbrow['ProjectParticipants'].'"></td></tr>';
echo '<tr><td align="right">Partners </td><td><input type="text" name="ProjectPartners" size="60" maxlength="255" value="'.$dbrow['ProjectPartners'].'"></td></tr>';
echo '<tr><td align="right">Competitors </td><td><input type="text" name="ProjectCompetitors" size="60" maxlength="255" value="'.$dbrow['ProjectCompetitors'].'"></td></tr>';
echo '<tr><td align="right">Sector </td><td><input type="text" name="ProjectSector" size="60" maxlength="255" value="'.$dbrow['ProjectSector'].'"></td></tr>';
echo '<tr><td align="right">Department </td><td><input type="text" name="ProjectDepartment" size="60" maxlength="255" value="'.$dbrow['ProjectDepartment'].'"></td></tr>';
echo '<tr><td align="right">Chances </td><td><input type="text" name="ProjectChances" size="60" maxlength="255" value="'.$dbrow['ProjectChances'].'"></td></tr>';
echo '<tr><td align="right">NextDeadline </td><td><input type="text" name="ProjectNextDeadline" size="60" maxlength="255" value="'.$dbrow['ProjectNextDeadline'].'"></td></tr>';
echo '<tr><td align="right">StatusMemo </td><td><input type="text" name="ProjectStatusMemo" size="60" maxlength="255" value="'.$dbrow['ProjectStatusMemo'].'"></td></tr>';
echo '<tr><td align="right">CallDate </td><td><input type="text" name="ProjectCallDate" size="60" maxlength="255" value="'.$dbrow['ProjectCallDate'].'"></td></tr>';
echo '<tr><td align="right">CallMemo </td><td><input type="text" name="ProjectCallMemo" size="60" maxlength="255" value="'.$dbrow['ProjectCallMemo'].'"></td></tr>';
echo '<tr><td align="right">OfferDate </td><td><input type="text" name="ProjectOfferDate" size="60" maxlength="255" value="'.$dbrow['ProjectOfferDate'].'"></td></tr>';
echo '<tr><td align="right">OfferMemo </td><td><input type="text" name="ProjectOfferMemo" size="60" maxlength="255" value="'.$dbrow['ProjectOfferMemo'].'"></td></tr>';
echo '<tr><td align="right">ContractDate </td><td><input type="text" name="ProjectContractDate" size="60" maxlength="255" value="'.$dbrow['ProjectContractDate'].'"></td></tr>';
echo '<tr><td align="right">ContractMemo </td><td><input type="text" name="ProjectContractMemo" size="60" maxlength="255" value="'.$dbrow['ProjectContractMemo'].'"></td></tr>';
echo '<tr><td align="right">LostDate </td><td><input type="text" name="ProjectLostDate" size="60" maxlength="255" value="'.$dbrow['ProjectLostDate'].'"></td></tr>';
echo '<tr><td align="right">LostMemo </td><td><input type="text" name="ProjectLostMemo" size="60" maxlength="255" value="'.$dbrow['ProjectLostMemo'].'"></td></tr>';

$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['ProjectProject'].'/';	

echo '<tr>';
echo '<td align="right">';
echo '<input type="checkbox" name="existor" value="File"'; if ($dbrow['ProjectFilename']) {echo ' checked';} echo '> Filename (max.10MB)</td>';
echo '<input type="hidden" name="ProjectFilename" value="'.$dbrow['ProjectFilename'].'">';
echo '<input type="hidden" name="ProjectFilesize" value="'.$dbrow['ProjectFilesize'].'">';
echo '<td align="left" class="bluelink"><a href="'.$ProjectFilePath.$dbrow['ProjectFilename'].'">'.$dbrow['ProjectFilename'].'</a> ['.$dbrow['ProjectFilesize'].'kB]</td>';
echo '<td><input type="file" name="ProjectFilename" size="15"></td>';
echo '</tr>';

echo '<tr><td align="right">Remarks </td><td><input type="text" name="ProjectRemarks" size="60" maxlength="255" value="'.$dbrow['ProjectRemarks'].'"></td></tr>';
	
	echo '</table>';

echo '</form>';

} elseif ( $updatedataset OR $createdataset OR $deletedataset)

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2019-07-11 18:00</div>';

?>
</font>
</body>
</html>
