<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

You may download <a href="docslist.xls">this Table</a> in Spreadsheet-Format (e.g. as CSV for MS-Excel) from Server.
<br><br>

<?php

include 'include_setProjectconstants.php';
include 'include_projects_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
	{
	echo '<form method="post" enctype="multipart/form-data" action="projects_save.php">';
	echo '<input type="submit" name="createdataset" value="create dataset">';
	echo '</form>';
	}


/*
select datasets according to criteria. 
*/

  	$dbquery = "SELECT * FROM projects WHERE

LOCATE('$partProjectGUID', ProjectGUID)>0 AND
LOCATE('$partProjectCreateID', ProjectCreateID)>0 AND
LOCATE('$partProjectArchiveID', ProjectArchiveID)>0 AND
LOCATE('$partProjectProject', ProjectProject)>0 AND
LOCATE('$partProjectOwner', ProjectOwner)>0 AND
LOCATE('$partProjectType', ProjectType)>0 AND
LOCATE('$partProjectCategory', ProjectCategory)>0 AND
LOCATE('$partProjectName', ProjectName)>0 AND
LOCATE('$partProjectStatus', ProjectStatus)>0 AND
LOCATE('$partProjectAquisitionNo', ProjectAquisitionNo)>0 AND
LOCATE('$partProjectNumber', ProjectNumber)>0 AND
LOCATE('$partProjectCode', ProjectCode)>0 AND
LOCATE('$partProjectDescription', ProjectDescription)>0 AND
LOCATE('$partProjectCountry', ProjectCountry)>0 AND
LOCATE('$partProjectLocation', ProjectLocation)>0 AND
LOCATE('$partProjectClient', ProjectClient)>0 AND
LOCATE('$partProjectOrigin', ProjectOrigin)>0 AND
LOCATE('$partProjectBudget', ProjectBudget)>0 AND
LOCATE('$partProjectFigures', ProjectFigures)>0 AND
LOCATE('$partProjectPeriod', ProjectPeriod)>0 AND
LOCATE('$partProjectFee', ProjectFee)>0 AND
LOCATE('$partProjectCharges', ProjectCharges)>0 AND
LOCATE('$partProjectIntServices', ProjectIntServices)>0 AND
LOCATE('$partProjectExtServices', ProjectExtServices)>0 AND
LOCATE('$partProjectDirector', ProjectDirector)>0 AND
LOCATE('$partProjectManager', ProjectManager)>0 AND
LOCATE('$partProjectParticipants', ProjectParticipants)>0 AND
LOCATE('$partProjectPartners', ProjectPartners)>0 AND
LOCATE('$partProjectCompetitors', ProjectCompetitors)>0 AND
LOCATE('$partProjectSector', ProjectSector)>0 AND
LOCATE('$partProjectDepartment', ProjectDepartment)>0 AND
LOCATE('$partProjectChances', ProjectChances)>0 AND
LOCATE('$partProjectStatus', ProjectStatus)>0 AND
LOCATE('$partProjectNextDeadline', ProjectNextDeadline)>0 AND
LOCATE('$partProjectStatusMemo', ProjectStatusMemo)>0 AND
LOCATE('$partProjectCallDate', ProjectCallDate)>0 AND
LOCATE('$partProjectCallMemo', ProjectCallMemo)>0 AND
LOCATE('$partProjectOfferDate', ProjectOfferDate)>0 AND
LOCATE('$partProjectOfferMemo', ProjectOfferMemo)>0 AND
LOCATE('$partProjectContractDate', ProjectContractDate)>0 AND
LOCATE('$partProjectContractMemo', ProjectContractMemo)>0 AND
LOCATE('$partProjectLostDate', ProjectLostDate)>0 AND
LOCATE('$partProjectLostMemo', ProjectLostMemo)>0 AND
LOCATE('$partProjectRemarks', ProjectRemarks)>0 AND
LOCATE('$partProjectFilename', ProjectFilename)>0 AND
LOCATE('$partProjectFilesize', ProjectFilesize)>0

ORDER BY $first_sorted_by, $then_sorted_by, $last_sorted_by " ;
	
  	$dbresult = mysqli_query($link,$dbquery);  echo mysqli_error($link);
  	
  	$header =""; //empty header for excel file (dataset structure)
  	$data	=""; //empty $data variable for excel file (all rows in one variable)

echo '<table border="1" cellspacing="0">';
 
echo '<tr>';

if ($list_ProjectGUID =='yes'){echo '<td>GUID</td>'; $header.="ProjectGUID". "\t";}
if ($list_ProjectCreateID =='yes'){echo '<td>CreateID</td>'; $header.="ProjectCreateID". "\t";}
if ($list_ProjectArchiveID =='yes'){echo '<td>ArchiveID</td>'; $header.="ProjectArchiveID". "\t";}
if ($list_ProjectProject =='yes'){echo '<td>Project</td>'; $header.="ProjectProject". "\t";}
if ($list_ProjectOwner =='yes'){echo '<td>Owner</td>'; $header.="ProjectOwner". "\t";}
if ($list_ProjectType =='yes'){echo '<td>Type</td>'; $header.="ProjectType". "\t";}
if ($list_ProjectCategory =='yes'){echo '<td>Category</td>'; $header.="ProjectCategory". "\t";}
if ($list_ProjectName =='yes'){echo '<td>Name</td>'; $header.="ProjectName". "\t";}
if ($list_ProjectStatus =='yes'){echo '<td>Status</td>'; $header.="ProjectStatus". "\t";}
if ($list_ProjectAquisitionNo =='yes'){echo '<td>AquisitionNo</td>'; $header.="ProjectAquisitionNo". "\t";}
if ($list_ProjectNumber =='yes'){echo '<td>Number</td>'; $header.="ProjectNumber". "\t";}
if ($list_ProjectCode =='yes'){echo '<td>Code</td>'; $header.="ProjectCode". "\t";}
if ($list_ProjectDescription =='yes'){echo '<td>Description</td>'; $header.="ProjectDescription". "\t";}
if ($list_ProjectCountry =='yes'){echo '<td>Country</td>'; $header.="ProjectCountry". "\t";}
if ($list_ProjectLocation =='yes'){echo '<td>Location</td>'; $header.="ProjectLocation". "\t";}
if ($list_ProjectClient =='yes'){echo '<td>Client</td>'; $header.="ProjectClient". "\t";}
if ($list_ProjectOrigin =='yes'){echo '<td>Origin</td>'; $header.="ProjectOrigin". "\t";}
if ($list_ProjectBudget =='yes'){echo '<td>Budget</td>'; $header.="ProjectBudget". "\t";}
if ($list_ProjectFigures =='yes'){echo '<td>Figures</td>'; $header.="ProjectFigures". "\t";}
if ($list_ProjectPeriod =='yes'){echo '<td>Period</td>'; $header.="ProjectPeriod". "\t";}
if ($list_ProjectFee =='yes'){echo '<td>Fee</td>'; $header.="ProjectFee". "\t";}
if ($list_ProjectCharges =='yes'){echo '<td>Charges</td>'; $header.="ProjectCharges". "\t";}
if ($list_ProjectIntServices =='yes'){echo '<td>IntServices</td>'; $header.="ProjectIntServices". "\t";}
if ($list_ProjectExtServices =='yes'){echo '<td>ExtServices</td>'; $header.="ProjectExtServices". "\t";}
if ($list_ProjectDirector =='yes'){echo '<td>Director</td>'; $header.="ProjectDirector". "\t";}
if ($list_ProjectManager =='yes'){echo '<td>Manager</td>'; $header.="ProjectManager". "\t";}
if ($list_ProjectParticipants =='yes'){echo '<td>Participants</td>'; $header.="ProjectParticipants". "\t";}
if ($list_ProjectPartners =='yes'){echo '<td>Partners</td>'; $header.="ProjectPartners". "\t";}
if ($list_ProjectCompetitors =='yes'){echo '<td>Competitors</td>'; $header.="ProjectCompetitors". "\t";}
if ($list_ProjectSector =='yes'){echo '<td>Sector</td>'; $header.="ProjectSector". "\t";}
if ($list_ProjectDepartment =='yes'){echo '<td>Department</td>'; $header.="ProjectDepartment". "\t";}
if ($list_ProjectChances =='yes'){echo '<td>Chances</td>'; $header.="ProjectChances". "\t";}
if ($list_ProjectNextDeadline =='yes'){echo '<td>NextDeadline</td>'; $header.="ProjectNextDeadline". "\t";}
if ($list_ProjectStatusMemo =='yes'){echo '<td>StatusMemo</td>'; $header.="ProjectStatusMemo". "\t";}
if ($list_ProjectCallDate =='yes'){echo '<td>CallDate</td>'; $header.="ProjectCallDate". "\t";}
if ($list_ProjectCallMemo =='yes'){echo '<td>CallMemo</td>'; $header.="ProjectCallMemo". "\t";}
if ($list_ProjectOfferDate =='yes'){echo '<td>OfferDate</td>'; $header.="ProjectOfferDate". "\t";}
if ($list_ProjectOfferMemo =='yes'){echo '<td>OfferMemo</td>'; $header.="ProjectOfferMemo". "\t";}
if ($list_ProjectContractDate =='yes'){echo '<td>ContractDate</td>'; $header.="ProjectContractDate". "\t";}
if ($list_ProjectContractMemo =='yes'){echo '<td>ContractMemo</td>'; $header.="ProjectContractMemo". "\t";}
if ($list_ProjectLostDate =='yes'){echo '<td>LostDate</td>'; $header.="ProjectLostDate". "\t";}
if ($list_ProjectLostMemo =='yes'){echo '<td>LostMemo</td>'; $header.="ProjectLostMemo". "\t";}
if ($list_ProjectRemarks =='yes'){echo '<td>Remarks</td>'; $header.="ProjectRemarks". "\t";}
if ($list_ProjectFilename =='yes'){echo '<td>Filename</td>'; $header.="ProjectFilename". "\t";}

	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)


/*
outputs datasets and fills $data variable
*/

$DatasetCount = 0;

  	while($dbrow = mysqli_fetch_array($dbresult))
   	{
	$DatasetCount += 1;
	
	// output to main window and selected data to $data excel variable
   	echo '<tr>';
if ($list_ProjectGUID =='yes'){echo'<td>'.$dbrow['ProjectGUID'].'</td>'; $data.='"'.$dbrow['ProjectGUID'].'"'."\t";}
if ($list_ProjectCreateID=='yes')
	{
	echo '<td class="bluelink"><a style="font-size:xx-small" href="projects_modify.php?ProjectCreateID='.$dbrow['ProjectCreateID'].'">'.$dbrow['ProjectCreateID'].'</a></td>';
	$data .= '"' . $dbrow['ProjectCreateID'] . '"' . "\t";
	}
if ($list_ProjectArchiveID =='yes'){echo'<td>'.$dbrow['ProjectArchiveID'].'</td>'; $data.='"'.$dbrow['ProjectArchiveID'].'"'."\t";}
if ($list_ProjectProject =='yes'){echo'<td>'.$dbrow['ProjectProject'].'</td>'; $data.='"'.$dbrow['ProjectProject'].'"'."\t";}
if ($list_ProjectOwner =='yes'){echo'<td>'.$dbrow['ProjectOwner'].'</td>'; $data.='"'.$dbrow['ProjectOwner'].'"'."\t";}
if ($list_ProjectType =='yes'){echo'<td>'.$dbrow['ProjectType'].'</td>'; $data.='"'.$dbrow['ProjectType'].'"'."\t";}
if ($list_ProjectCategory =='yes'){echo'<td>'.$dbrow['ProjectCategory'].'</td>'; $data.='"'.$dbrow['ProjectCategory'].'"'."\t";}
if ($list_ProjectName =='yes'){echo'<td>'.$dbrow['ProjectName'].'</td>'; $data.='"'.$dbrow['ProjectName'].'"'."\t";}
if ($list_ProjectStatus =='yes'){echo'<td>'.$dbrow['ProjectStatus'].'</td>'; $data.='"'.$dbrow['ProjectStatus'].'"'."\t";}
if ($list_ProjectAquisitionNo =='yes'){echo'<td>'.$dbrow['ProjectAquisitionNo'].'</td>'; $data.='"'.$dbrow['ProjectAquisitionNo'].'"'."\t";}
if ($list_ProjectNumber =='yes'){echo'<td>'.$dbrow['ProjectNumber'].'</td>'; $data.='"'.$dbrow['ProjectNumber'].'"'."\t";}
if ($list_ProjectCode =='yes'){echo'<td>'.$dbrow['ProjectCode'].'</td>'; $data.='"'.$dbrow['ProjectCode'].'"'."\t";}
if ($list_ProjectDescription =='yes'){echo'<td>'.$dbrow['ProjectDescription'].'</td>'; $data.='"'.$dbrow['ProjectDescription'].'"'."\t";}
if ($list_ProjectCountry =='yes'){echo'<td>'.$dbrow['ProjectCountry'].'</td>'; $data.='"'.$dbrow['ProjectCountry'].'"'."\t";}
if ($list_ProjectLocation =='yes'){echo'<td>'.$dbrow['ProjectLocation'].'</td>'; $data.='"'.$dbrow['ProjectLocation'].'"'."\t";}
if ($list_ProjectClient =='yes'){echo'<td>'.$dbrow['ProjectClient'].'</td>'; $data.='"'.$dbrow['ProjectClient'].'"'."\t";}
if ($list_ProjectOrigin =='yes'){echo'<td>'.$dbrow['ProjectOrigin'].'</td>'; $data.='"'.$dbrow['ProjectOrigin'].'"'."\t";}
if ($list_ProjectBudget =='yes'){echo'<td>'.$dbrow['ProjectBudget'].'</td>'; $data.='"'.$dbrow['ProjectBudget'].'"'."\t";}
if ($list_ProjectFigures =='yes'){echo'<td>'.$dbrow['ProjectFigures'].'</td>'; $data.='"'.$dbrow['ProjectFigures'].'"'."\t";}
if ($list_ProjectPeriod =='yes'){echo'<td>'.$dbrow['ProjectPeriod'].'</td>'; $data.='"'.$dbrow['ProjectPeriod'].'"'."\t";}
if ($list_ProjectFee =='yes'){echo'<td>'.$dbrow['ProjectFee'].'</td>'; $data.='"'.$dbrow['ProjectFee'].'"'."\t";}
if ($list_ProjectCharges =='yes'){echo'<td>'.$dbrow['ProjectCharges'].'</td>'; $data.='"'.$dbrow['ProjectCharges'].'"'."\t";}
if ($list_ProjectIntServices =='yes'){echo'<td>'.$dbrow['ProjectIntServices'].'</td>'; $data.='"'.$dbrow['ProjectIntServices'].'"'."\t";}
if ($list_ProjectExtServices =='yes'){echo'<td>'.$dbrow['ProjectExtServices'].'</td>'; $data.='"'.$dbrow['ProjectExtServices'].'"'."\t";}
if ($list_ProjectDirector =='yes'){echo'<td>'.$dbrow['ProjectDirector'].'</td>'; $data.='"'.$dbrow['ProjectDirector'].'"'."\t";}
if ($list_ProjectManager =='yes'){echo'<td>'.$dbrow['ProjectManager'].'</td>'; $data.='"'.$dbrow['ProjectManager'].'"'."\t";}
if ($list_ProjectParticipants =='yes'){echo'<td>'.$dbrow['ProjectParticipants'].'</td>'; $data.='"'.$dbrow['ProjectParticipants'].'"'."\t";}
if ($list_ProjectPartners =='yes'){echo'<td>'.$dbrow['ProjectPartners'].'</td>'; $data.='"'.$dbrow['ProjectPartners'].'"'."\t";}
if ($list_ProjectCompetitors =='yes'){echo'<td>'.$dbrow['ProjectCompetitors'].'</td>'; $data.='"'.$dbrow['ProjectCompetitors'].'"'."\t";}
if ($list_ProjectSector =='yes'){echo'<td>'.$dbrow['ProjectSector'].'</td>'; $data.='"'.$dbrow['ProjectSector'].'"'."\t";}
if ($list_ProjectDepartment =='yes'){echo'<td>'.$dbrow['ProjectDepartment'].'</td>'; $data.='"'.$dbrow['ProjectDepartment'].'"'."\t";}
if ($list_ProjectChances =='yes'){echo'<td>'.$dbrow['ProjectChances'].'</td>'; $data.='"'.$dbrow['ProjectChances'].'"'."\t";}
if ($list_ProjectNextDeadline =='yes'){echo'<td>'.$dbrow['ProjectNextDeadline'].'</td>'; $data.='"'.$dbrow['ProjectNextDeadline'].'"'."\t";}
if ($list_ProjectStatusMemo =='yes'){echo'<td>'.$dbrow['ProjectStatusMemo'].'</td>'; $data.='"'.$dbrow['ProjectStatusMemo'].'"'."\t";}
if ($list_ProjectCallDate =='yes'){echo'<td>'.$dbrow['ProjectCallDate'].'</td>'; $data.='"'.$dbrow['ProjectCallDate'].'"'."\t";}
if ($list_ProjectCallMemo =='yes'){echo'<td>'.$dbrow['ProjectCallMemo'].'</td>'; $data.='"'.$dbrow['ProjectCallMemo'].'"'."\t";}
if ($list_ProjectOfferDate =='yes'){echo'<td>'.$dbrow['ProjectOfferDate'].'</td>'; $data.='"'.$dbrow['ProjectOfferDate'].'"'."\t";}
if ($list_ProjectOfferMemo =='yes'){echo'<td>'.$dbrow['ProjectOfferMemo'].'</td>'; $data.='"'.$dbrow['ProjectOfferMemo'].'"'."\t";}
if ($list_ProjectContractDate =='yes'){echo'<td>'.$dbrow['ProjectContractDate'].'</td>'; $data.='"'.$dbrow['ProjectContractDate'].'"'."\t";}
if ($list_ProjectContractMemo =='yes'){echo'<td>'.$dbrow['ProjectContractMemo'].'</td>'; $data.='"'.$dbrow['ProjectContractMemo'].'"'."\t";}
if ($list_ProjectLostDate =='yes'){echo'<td>'.$dbrow['ProjectLostDate'].'</td>'; $data.='"'.$dbrow['ProjectLostDate'].'"'."\t";}
if ($list_ProjectLostMemo =='yes'){echo'<td>'.$dbrow['ProjectLostMemo'].'</td>'; $data.='"'.$dbrow['ProjectLostMemo'].'"'."\t";}
if ($list_ProjectFilename =='yes')
	{
	$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['ProjectProject'].'/';
	
	echo	'<td class="bluelink">'
			.'<a style="font-size:xx-small" href="'
			.$ProjectFilePath
			.$dbrow['ProjectFilename']
			.'">'
			.$dbrow['ProjectFilename'].' ['.$dbrow['ProjectFilesize'].'kB]'
			.'</a>';
	$data.='"'.$dbrow['ProjectFilename'].' ['.$dbrow['ProjectFilesize'].'kB]'.'"'."\t";
	}
if ($list_ProjectRemarks =='yes'){echo'<td>'.$dbrow['ProjectRemarks'].'</td>'; $data.='"'.$dbrow['ProjectRemarks'].'"'."\t";}
	
	echo '</tr>';
	$data .= "\n"; //end of dataset in excel file (marks row end in data)
	}
echo '<tr>';
echo '<td>'.$DatasetCount.' Datasets</td>';
echo '</tr>';	
echo '</table>';
/*
export selected table to .xls onto local server folder to download as offered from there
*/

$fp = fopen('projectslist.xls','w');
fwrite($fp,$header);
fwrite($fp,"\n");
fwrite($fp,$data);
fclose($fp);

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2019-07-11 18:00</div>';

?>
</body>
</html>
