<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

!!! Do <b>NOT</b> use semicolon (;) in input fields - <b>Upper</b> Row Entry overides <b>Lower</b>!
<br>
<br>

<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

include 'include_setTimelineconstants.php';
include 'include_timeline_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

$TimelineCreateID = $_GET["TimelineCreateID"];

$dbquery = " SELECT * FROM timeline WHERE LOCATE ('$TimelineCreateID', TimelineCreateID) >0 ";
$dbresult = mysqli_query($link,$dbquery);
$dbrow = mysqli_fetch_array($dbresult);

	$updatedataset="";
	$createdataset="";
	$deletedataset="";
	
if ($updatedataset == "" AND $createdataset == "" AND $deletedataset == "" )
	{
	echo '<form method="post" action="timeline_save.php" enctype="multipart/form-data" >';
	if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
		{
		echo '<input type="submit" name="createdataset" value="create dataset">';
		echo '<input type="submit" name="updatedataset" value="update dataset">';
    	echo '<input type="submit" name="deletedataset" value="delete dataset">';
      	echo '<input type="reset" value="reset values">';
      	echo '<br><br>';
	    }

	echo '<table>';
	
	echo '<tr>';
	echo '<td align="right">GUID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="TimelineGUID" size="60" maxlength="100" value="'.$dbrow['TimelineGUID'].'" readonly></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">CreateID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="TimelineCreateID" size="60" maxlength="100" value="'.$dbrow['TimelineCreateID'].'" readonly></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">ArchiveID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="TimelineArchiveID" size="60" maxlength="100" value="'.$dbrow['TimelineArchiveID'].'" readonly></td>';
	echo '</tr>';
	
	echo '<input type="hidden" name="TimelineInitialProject" value="'.$dbrow['TimelineProject'].'">';
	echo '<input type="hidden" name="TimelineProject" value="'.$dbrow['TimelineProject'].'">';
	
	echo '<tr>';
	echo '<td align="right">Project</td>';
	echo '<td><select name="TimelineProject" size="1">';
		if (!empty($SystemProject))
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($SystemProject==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		else
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($dbrow['TimelineProject']==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		echo '</select></td>';		
	echo '</td>';		
	echo '</tr>';	
	
	echo '<tr>';
	echo '<td align="right">Owner (readonly)</td>';
	echo '<td>'.'['.$dbrow['TimelineOwner'].'] - create/update transfers to '.'<input style="background-color:#C0C0C0" type="text" name="TimelineOwner" size="15" maxlength="100" value="'.$_SESSION['LoginLogin'].'" readonly></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">Name </td>';
	echo '<td><input type="text" name="TimelineName" size="60" maxlength="255" value="'.$dbrow['TimelineName'].'"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">Type</td>';
	echo '<td><select name="TimelineType" size="1">';
			foreach ($SystemTypeArray as $Type) {echo '<option'; if ($dbrow['TimelineType']==$Type) {echo ' selected';} echo '>'.$Type.'</option>';} 
			echo '</select></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">Category</td>';
	echo '<td><select name="TimelineCategory" size="1">';
			foreach ($TimelineCategoryArray as $Category) {echo '<option'; if ($dbrow['TimelineCategory']==$Category) {echo ' selected';} echo '>'.$Category.'</option>';} 
			echo '</select></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">Remarks </td>';
	echo '<td><input type="text" name="TimelineRemarks" size="60" maxlength="255" value="'.$dbrow['TimelineRemarks'].'"></td>';
	echo '</tr>';
	echo '</table>';
	
	echo '<br>';
	
for ($y = 2021; $y <=2023; $y++)
	{
	echo '<table border="1" cellspacing="0">';
	// Year, Month, Monthdate and Weekday header
	echo '<tr>';
	echo '<td>'.$y.'</td>'; 
	for ($d = 1; $d <= 365+date("L",mktime(0,0,0,1,1,$y)); $d++) 
		{
		echo '<td align="right">';
		echo date("M", mktime(0, 0, 0, 1, $d, $y)).'<br>';
		echo date("j", mktime(0, 0, 0, 1, $d, $y)).'<br>';
		echo date("D", mktime(0, 0, 0, 1, $d, $y));
		echo '</td>';
		}
	echo '</tr>';
	// input checkbox array only stores checked checkboxes. value is used to identify their position
	// the resulting array looks like this: Array ( [2014] => Array ( [0] => 11 [1] => 12 ) [2015] => Array ( [0] => 11 [1] => 12 [2] => 13 ) ) 
	
	echo '<tr>';
	echo '<td align="right" style="background-color:#FE0000">[X]</td>';
	for ($i = 0; $i <= 364+date("L",mktime(0,0,0,1,1,$y)); $i++) 
		{
		echo '<td style="background-color:#FE0000"><input type="checkbox" name="blocked['.$y.'][]" value='.$i.' ';
		if (substr($dbrow['Timeline'.$y],$i,1)=="X") {echo ' checked';}
		echo '></td>';
		} 	
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right" style="background-color:#00FF00">[S]</td>';
	for ($i = 0; $i <= 364+date("L",mktime(0,0,0,1,1,$y)); $i++)
		{
		echo '<td style="background-color:#00FF00"><input type="checkbox" name="seminar['.$y.'][]" value='.$i.' ';
		if (substr($dbrow['Timeline'.$y],$i,1)=="S") {echo ' checked';}
		echo '></td>';
		}
	echo '</tr>';
	echo '</table>';
	echo '<br>';
	}
	
echo '</form>';

} elseif ( $updatedataset OR $createdataset OR $deletedataset)

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2020-02-08 18:00</div>';

?>
</font>
</body>
</html>