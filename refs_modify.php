<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>
!!! Do <b>NOT</b> use semicolon (;) in input fields !!! <b>NO</b> special characters and blanks in filenames - use underscore ( _ ) instead!!!<br>
<a href="reference_EN.pdf" target="_blank"><img src="logo_pdf_EN.jpg"></a>
<a href="reference_DE.pdf" target="_blank"><img src="logo_pdf_DE.jpg"></a>
<a href="reference_UAE.pdf" target="_blank"><img src="logo_pdf_UAE.jpg"></a>
<a href="reference_FACT.pdf" target="_blank"><img src="logo_pdf_FACT.jpg"></a>
<a href="reference_tmp1.pdf" target="_blank"><img src="logo_pdf_tmp1.jpg"></a>
<a href="reference_tmp2.pdf" target="_blank"><img src="logo_pdf_tmp2.jpg"></a>
<a href="reference_tmp3.pdf" target="_blank"><img src="logo_pdf_tmp3.jpg"></a>
<br><br>

<?php
		
include 'include_setReferenceconstants.php';
include 'include_refs_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

$ReferenceCreateID = $_GET["ReferenceCreateID"];

$dbquery = " SELECT * FROM refs WHERE LOCATE ('$ReferenceCreateID', ReferenceCreateID) >0 ";
$dbresult = mysqli_query($link,$dbquery);
$dbrow = mysqli_fetch_array($dbresult);

	$updatedataset="";
	$createdataset="";
	$deletedataset="";
	$historydataset="";
	
	echo '<form method="post" action="refs_save.php" enctype="multipart/form-data" >';
	if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
		{
		echo '<input type="submit" name="createdataset" value="create dataset">';
		echo '<input type="submit" name="updatedataset" value="update dataset">';
    	echo '<input type="submit" name="deletedataset" value="delete dataset">';
      	echo '<input type="reset" value="reset values">';
      	echo '<br><br>';
	    }

	echo '<table>';
	echo '<tr>';
	echo '<td align="right">GUID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="ReferenceGUID" size="60" maxlength="100" value="'.$dbrow['ReferenceGUID'].'" readonly></td>';
	echo '<td><input type="submit" name="historydataset" value="history of datasets"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">CreateID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="ReferenceCreateID" size="60" maxlength="100" value="'.$dbrow['ReferenceCreateID'].'" readonly></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">ArchiveID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="ReferenceArchiveID" size="60" maxlength="100" value="'.$dbrow['ReferenceArchiveID'].'" readonly></td>';
	echo '</tr>';
	echo '<input type="hidden" name="ReferenceInitialProject" value="'.$dbrow['ReferenceProject'].'">';
	echo '<input type="hidden" name="ReferenceProject" value="'.$dbrow['ReferenceProject'].'">';
	echo '<td><select name="ReferenceProject" size="1">';
		if (!empty($SystemProject))
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($SystemProject==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		else
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($dbrow['ReferenceProject']==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		echo '</select></td>';		
	echo '<tr>';
	echo '<td align="right">Owner (readonly)</td>';
	echo '<td>'.'['.$dbrow['ReferenceOwner'].'] - create/update transfers to '.'<input style="background-color:#C0C0C0" type="text" name="ReferenceOwner" size="15" maxlength="100" value="'.$_SESSION['LoginLogin'].'" readonly></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Type</td>';
	echo '<td><select name="ReferenceType" size="1">';
			foreach ($ReferenceTypeArray as $Type) {echo '<option'; if ($dbrow['ReferenceType']==$Type) {echo ' selected';} echo '>'.$Type.'</option>';} 
			echo '</select></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Category</td>';
	echo '<td><select name="ReferenceCategory" size="1">';
			foreach ($ReferenceCategoryArray as $Category) {echo '<option'; if ($dbrow['ReferenceCategory']==$Category) {echo ' selected';} echo '>'.$Category.'</option>';} 
			echo '</select></td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td align="right">Name EN DE</td>';
	echo '<td><input type="text" name="ReferenceName" size="60" maxlength="255" value="'.$dbrow['ReferenceName'].'"></td>';
	echo '<td><input type="text" name="ReferenceNameDE" size="60" maxlength="255" value="'.$dbrow['ReferenceNameDE'].'"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Summary EN DE</td>';
	echo '<td><input type="text" name="ReferenceSummary" size="60" maxlength="255" value="'.$dbrow['ReferenceSummary'].'"></td>';
	echo '<td><input type="text" name="ReferenceSummaryDE" size="60" maxlength="255" value="'.$dbrow['ReferenceSummaryDE'].'"></td>';
	echo '</tr>';
	echo '<tr><td align="right">Client</td><td><input type="text" name="ReferenceClient" size="60" maxlength="255" value="'.$dbrow['ReferenceClient'].'"></td></tr>';
	echo '<tr>';
	echo '<td align="right">Division EN DE</td>';
	echo '<td><select name="ReferenceDivision" size="1">';
			foreach ($ReferenceDivisionArray as $Division) {echo '<option'; if ($dbrow['ReferenceDivision']==$Division) {echo ' selected';} echo '>'.$Division.'</option>';} 
			echo '</select></td>';
	echo '<td><select name="ReferenceDisziplin" size="1">';
			foreach ($ReferenceDisziplinArray as $Disziplin) {echo '<option'; if ($dbrow['ReferenceDisziplin']==$Disziplin) {echo ' selected';} echo '>'.$Disziplin.'</option>';} 
			echo '</select></td>';
	echo '</tr>';
	echo '<tr><td align="right">Contractor</td><td><input type="text" name="ReferenceContractor" size="60" maxlength="255" value="'.$dbrow['ReferenceContractor'].'"></td></tr>';
	echo '<tr><td align="right">Code</td><td><input type="text" name="ReferenceCode" size="60" maxlength="255" value="'.$dbrow['ReferenceCode'].'"></td></tr>';
	echo '<tr><td align="right">Contact</td><td><input type="text" name="ReferenceContact" size="60" maxlength="255" value="'.$dbrow['ReferenceContact'].'"></td></tr>';
	echo '<tr><td align="right">Volume</td><td><input type="text" name="ReferenceVolume" size="60" maxlength="255" value="'.$dbrow['ReferenceVolume'].'"></td></tr>';
	echo '<tr><td align="right">Fee</td><td><input type="text" name="ReferenceFee" size="60" maxlength="255" value="'.$dbrow['ReferenceFee'].'"></td></tr>';
	echo '<tr><td align="right">Startdate</td><td><input type="text" name="ReferenceStartdate" size="60" maxlength="255" value="'.$dbrow['ReferenceStartdate'].'"></td></tr>';
	echo '<tr><td align="right">Enddate</td><td><input type="text" name="ReferenceEnddate" size="60" maxlength="255" value="'.$dbrow['ReferenceEnddate'].'"></td></tr>';
	echo '<tr><td align="right">Location</td><td><input type="text" name="ReferenceLocation" size="60" maxlength="255" value="'.$dbrow['ReferenceLocation'].'"></td></tr>';
	echo '<tr><td align="right">Country</td><td><input type="text" name="ReferenceCountry" size="60" maxlength="255" value="'.$dbrow['ReferenceCountry'].'"></td></tr>';
	echo '<tr>';
	echo '<td align="right">Description EN DE</td>';
	echo '<td><textarea name="ReferenceDescription" cols="60" rows="4" value="'.$dbrow['ReferenceDescription'].'">'.$dbrow['ReferenceDescription'].'</textarea></td>';
	echo '<td><textarea name="ReferenceDescriptionDE" cols="60" rows="4" value="'.$dbrow['ReferenceDescriptionDE'].'">'.$dbrow['ReferenceDescriptionDE'].'</textarea></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Scope EN DE</td>';
	echo '<td><textarea name="ReferenceScope" cols="60" rows="4" value="'.$dbrow['ReferenceScope'].'">'.$dbrow['ReferenceScope'].'</textarea></td>';
	echo '<td><textarea name="ReferenceScopeDE" cols="60" rows="4" value="'.$dbrow['ReferenceScopeDE'].'">'.$dbrow['ReferenceScopeDE'].'</textarea></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Details EN DE</td>';
	echo '<td><textarea name="ReferenceDetails" cols="60" rows="4" value="'.$dbrow['ReferenceDetails'].'">'.$dbrow['ReferenceDetails'].'</textarea></td>';
	echo '<td><textarea name="ReferenceDetailsDE" cols="60" rows="4" value="'.$dbrow['ReferenceDetailsDE'].'">'.$dbrow['ReferenceDetailsDE'].'</textarea></td>';
	echo '</tr>';

	echo '<tr><td align="right">Status </td><td><input type="text" name="ReferenceStatus" size="60" maxlength="255" value="'.$dbrow['ReferenceStatus'].'"></td></tr>';

$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['ReferenceProject'].'/';	

echo '<tr>';
echo '<td align="right">';
echo '<input type="checkbox" name="existor" value="File"'; if ($dbrow['ReferenceFilename']) {echo ' checked';} echo '> Filename (max.10MB)</td>';
echo '<input type="hidden" name="ReferenceFilename" value="'.$dbrow['ReferenceFilename'].'">';
echo '<input type="hidden" name="ReferenceFilesize" value="'.$dbrow['ReferenceFilesize'].'">';
echo '<td align="left" class="bluelink"><a href="'.$ProjectFilePath.$dbrow['ReferenceFilename'].'">'.$dbrow['ReferenceFilename'].'</a> ['.$dbrow['ReferenceFilesize'].'kB]</td>';
echo '<td><input type="file" name="ReferenceFilename" size="15"></td>';
echo '</tr>';

	echo '<tr><td align="right">Remarks </td><td><input type="text" name="ReferenceRemarks" size="60" maxlength="255" value="'.$dbrow['ReferenceRemarks'].'"></td></tr>';
	
	echo '</table>';

	echo '</form>';


/*
fpdf section to produce application.pdf 
*/

// include FPDF library
include ('./includes/include_fpdf_includeFPDF.php');
// extend class FPDF to class PDF and thereby include Header/Footer function
include ('./includes/include_fpdf_extendPDF.php');

// Instanciation of classes $pdf by new. Output() and then PHP unset() instanciated class $pdf per XX reference page

// EN reference page
$pdf=new PDF();
$pdf->AliasNbPages();
// Defines the left, top and right margins. By default, they equal 1 cm. Right margin. Default value is the left one. 
$pdf->SetMargins(0,0);
include ('./includes/include_fpdf_addpage_EN.php');
$pdf->Output('F','reference_EN.pdf'); // make sure of correct attribute sequence and write allowance to folder
unset ($pdf);

//DE reference page
$pdf=new PDF();
$pdf->AliasNbPages();
// Defines the left, top and right margins. By default, they equal 1 cm. Right margin. Default value is the left one. 
$pdf->SetMargins(0,0);
include ('./includes/include_fpdf_addpage_DE.php');
$pdf->Output('F','reference_DE.pdf'); // make sure of correct attribute sequence and write allowance to folder
unset ($pdf);	
	
//TEMP reference pages
$pdf=new PDF();
$pdf->AliasNbPages();
// Defines the left, top and right margins. By default, they equal 1 cm. Right margin. Default value is the left one. 
$pdf->SetMargins(0,0);
include ('./includes/helen/include_fpdf_addpage_tmp1.php');
$pdf->Output('F','reference_tmp1.pdf'); // make sure of correct attribute sequence and write allowance to folder
unset ($pdf);	

$pdf=new PDF();
$pdf->AliasNbPages();
// Defines the left, top and right margins. By default, they equal 1 cm. Right margin. Default value is the left one. 
$pdf->SetMargins(0,0);
include ('./includes/helen/include_fpdf_addpage_tmp2.php');
$pdf->Output('F','reference_tmp2.pdf'); // make sure of correct attribute sequence and write allowance to folder
unset ($pdf);	

$pdf=new PDF();
$pdf->AliasNbPages();
// Defines the left, top and right margins. By default, they equal 1 cm. Right margin. Default value is the left one. 
$pdf->SetMargins(0,0);
include ('./includes/helen/include_fpdf_addpage_tmp3.php');
$pdf->Output('F','reference_tmp3.pdf'); // make sure of correct attribute sequence and write allowance to folder
unset ($pdf);	
	
echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2019-07-11 21:10</div>';

?>
</font>
</body>
</html>
