<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
  	<title><?php echo $SystemProject; ?> database system</title>
	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>
!!! Do <b>NOT</b> use semicolon (;) in input fields !!! <b>NO</b> blanks in filenames - use underscore ( _ ) instead!!! 
<br /><br />

<?php

// new error handling 
error_reporting(E_ALL);
ini_set("display_errors", 1);

		
include 'include_setSeminarconstants.php';
include 'include_seminars_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

$SeminarCreateID = $_GET["SeminarCreateID"];

$dbquery = " SELECT * FROM seminars WHERE LOCATE ('$SeminarCreateID', SeminarCreateID) >0 ";
$dbresult = mysqli_query($link,$dbquery);
$dbrow = mysqli_fetch_array($dbresult);

	$updatedataset="";
	$createdataset="";
	$deletedataset="";
	$historydataset="";
	
if ($updatedataset == "" AND $createdataset == "" AND $deletedataset == "" AND $historydataset =="")
	{
	echo '<form method="post" action="seminars_save.php" enctype="multipart/form-data" >';
	
	if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
		{
		echo '<input type="submit" name="createdataset" value="create dataset">';
		echo '<input type="submit" name="updatedataset" value="update dataset">';
    	echo '<input type="submit" name="deletedataset" value="delete dataset">';
      	echo '<input type="reset" value="reset values">';
      	echo '<br /><br />';
	    }

	echo '<table>';
	
	echo '<tr>';
	echo '<td align="right">GUID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="SeminarGUID" size="60" maxlength="100" value="'.$dbrow['SeminarGUID'].'" readonly></td>';
	echo '<td><input type="submit" name="historydataset" value="history of datasets"></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">CreateID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="SeminarCreateID" size="60" maxlength="100" value="'.$dbrow['SeminarCreateID'].'" readonly></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">ArchiveID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="SeminarArchiveID" size="60" maxlength="100" value="'.$dbrow['SeminarArchiveID'].'" readonly></td>';
	echo '</tr>';
	
	echo '<input type="hidden" name="SeminarInitialProject" value="'.$dbrow['SeminarProject'].'">';
	echo '<input type="hidden" name="SeminarProject" value="'.$dbrow['SeminarProject'].'">';
	echo '<td align="right">Project</td>';
	echo '<td><select name="SeminarProject" size="1">';
		if (!empty($SystemProject))
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($SystemProject==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		else
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($dbrow['SeminarProject']==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		echo '</select></td>';		
	
	echo '<tr>';
	echo '<td align="right">Owner (readonly)</td>';
	echo '<td>'.'['.$dbrow['SeminarOwner'].'] - create/update transfers to '.'<input style="background-color:#C0C0C0" type="text" name="SeminarOwner" size="15" maxlength="100" value="'.$_SESSION['LoginLogin'].'" readonly></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">Type </td>';
	echo '<td><select name="SeminarType" size="1">';
			foreach ($SystemTypeArray as $Type) {echo '<option'; if ($dbrow['SeminarType']==$Type) {echo ' selected';} echo '>'.$Type.'</option>';} 
			echo '</select></td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">Category </td>';
	echo '<td><select name="SeminarCategory" size="1">';
			foreach ($SeminarCategoryArray as $Category) {echo '<option'; if ($dbrow['SeminarCategory']==$Category) {echo ' selected';} echo '>'.$Category.'</option>';} 
			echo '</select></td>';
	echo '</tr>';

echo '<tr><td align="right">Begin </td><td><input type="text" name="SeminarBegin" size="60" maxlength="255" value="'.$dbrow['SeminarBegin'].'"></td></tr>';
echo '<tr><td align="right">Year </td><td><input type="text" name="SeminarYear" size="60" maxlength="255" value="'.$dbrow['SeminarYear'].'"></td></tr>';
echo '<tr><td align="right">Duration </td><td><input type="text" name="SeminarDuration" size="60" maxlength="255" value="'.$dbrow['SeminarDuration'].'"></td></tr>';
echo '<tr>';
	echo '<td align="right">Status </td>';
	echo '<td><select name="SeminarStatus" size="1">';
			foreach ($SeminarStatusArray as $Status) {echo '<option'; if ($dbrow['SeminarStatus']==$Status) {echo ' selected';} echo '>'.$Status.'</option>';} 
			echo '</select></td>';
	echo '</tr>';

echo '<tr><td align="right">Name </td><td><input type="text" name="SeminarName" size="60" maxlength="255" value="'.$dbrow['SeminarName'].'"></td></tr>';

// content from program 
// begin content
			// get all program datasets
			// and ArchiveID is 0000... to only take active datasets
			$lbquery = "SELECT * FROM programs WHERE ProgramArchiveID = '0000-00-00 00:00:00' ORDER BY ProgramName"; 
			$lbresult = mysqli_query($link,$lbquery);  echo mysqli_error($link);
			
			// control code 
			// while ($lbrow = mysqli_fetch_array($lbresult)) {echo $lbrow['ProgramName'];}
			// echo $dbrow['ProgramName'];

			// offer retrieved program datasets
echo '<tr><td align="right">Content </td>';
echo '<td><select name="SeminarContent" size="1">';
			echo '<option>'.''.'</option>'; // offer empty selection
			while ($lbrow = mysqli_fetch_array($lbresult))// offer all selected program datasets
				{
				echo '<option'; 
				if ($dbrow['SeminarContent']==$lbrow['ProgramName']) {echo ' selected';} // show preselection by comparison
				echo '>'.$lbrow['ProgramName'].'</option>';
				}
echo '</select></td>';
echo '</tr>';

echo '<tr><td align="right">Language </td><td><input type="text" name="SeminarLanguage" size="60" maxlength="255" value="'.$dbrow['SeminarLanguage'].'"></td></tr>';

// end content

echo '</table>';

echo '<table>';

// referent from contacts
// begin referent
			// get all contacts datasets with function referent
			// and ArchiveID is 0000... to only take active datasets
			$lbquery = "SELECT * FROM contacts WHERE ((ContactFunction = 'referent') AND (ContactArchiveID = '0000-00-00 00:00:00')) ORDER BY ContactLastname"; 
			$lbresult = mysqli_query($link,$lbquery);  echo mysqli_error($link);
			
			// control code 
			// while ($lbrow = mysqli_fetch_array($lbresult)) {echo $lbrow['ContactLastname'];}
			// echo $dbrow['SeminarLastname'];

			// offer retrieved referent datasets
echo '<tr><td align="right">Referent1 </td>';
echo '<td><select name="SeminarReferent1" size="1">';
			echo '<option>'.''.'</option>'; // offer empty selection
			while ($lbrow = mysqli_fetch_array($lbresult))// offer all selected contact datasets with referent function
				{
				echo '<option'; 
				if ($dbrow['SeminarReferent1']==$lbrow['ContactFirstname'].' '.$lbrow['ContactLastname']) 
					{
					echo ' selected'; // show preselection by comparison
					$greyRef1ZIP = $lbrow['ContactBusinessZIP']; // save for additional greyd-out read-only info
					$greyRef1Town = $lbrow['ContactBusinessTown']; // save for additional greyd-out read-only info
					$greyRef1Email = $lbrow['ContactEmailPrim']; // save for additional greyd-out read-only info
					$greyRef1Web = $lbrow['ContactWeb']; // save for additional greyd-out read-only info
					} 
				echo '>'.$lbrow['ContactFirstname'].' '.$lbrow['ContactLastname'].'</option>';
				}
echo '</select></td>';
echo '<td style="background-color:#C0C0C0">'.$greyRef1ZIP.'</td>';
echo '<td style="background-color:#C0C0C0">'.$greyRef1Town.'</td>';
echo '<td style="background-color:#C0C0C0"><a href="mailto:'.$greyRef1Email.'" >'.$greyRef1Email.'</a></td>';
echo '<td style="background-color:#C0C0C0"><a href="https://'.$greyRef1Web.'" >'.$greyRef1Web.'</a></td>';
echo '</tr>';

$lbresult = mysqli_query($link,$lbquery);  echo mysqli_error($link);

echo '<tr><td align="right">Referent2 </td>';
echo '<td><select name="SeminarReferent2" size="1">';
			echo '<option>'.''.'</option>'; // offer empty selection
			while ($lbrow = mysqli_fetch_array($lbresult))// offer all selected contact datasets with referent function
				{
				echo '<option'; 
				if ($dbrow['SeminarReferent2']==$lbrow['ContactFirstname'].' '.$lbrow['ContactLastname']) 
					{
					echo ' selected'; // show preselection by comparison
					$greyRef2ZIP = $lbrow['ContactBusinessZIP']; // save for additional greyd-out read-only info
					$greyRef2Town = $lbrow['ContactBusinessTown']; // save for additional greyd-out read-only info
					$greyRef2Email = $lbrow['ContactEmailPrim']; // save for additional greyd-out read-only info
					$greyRef2Web = $lbrow['ContactWeb']; // save for additional greyd-out read-only info
					} 
				echo '>'.$lbrow['ContactFirstname'].' '.$lbrow['ContactLastname'].'</option>';
				}
echo '</select></td>';
echo '<td style="background-color:#C0C0C0">'.$greyRef2ZIP.'</td>';
echo '<td style="background-color:#C0C0C0">'.$greyRef2Town.'</td>';
echo '<td style="background-color:#C0C0C0"><a href="mailto:'.$greyRef2Email.'" >'.$greyRef2Email.'</a></td>';
echo '<td style="background-color:#C0C0C0"><a href="https://'.$greyRef2Web.'" >'.$greyRef2Web.'</a></td>';
echo '</tr>';

$lbresult = mysqli_query($link,$lbquery);  echo mysqli_error($link);

echo '<tr><td align="right">Referent3 </td>';
echo '<td><select name="SeminarReferent3" size="1">';
			echo '<option>'.''.'</option>'; // offer empty selection
			while ($lbrow = mysqli_fetch_array($lbresult))// offer all selected contact datasets with referent function
				{
				echo '<option'; 
				if ($dbrow['SeminarReferent3']==$lbrow['ContactFirstname'].' '.$lbrow['ContactLastname']) 
					{
					echo ' selected'; // show preselection by comparison
					$greyRef3ZIP = $lbrow['ContactBusinessZIP']; // save for additional greyd-out read-only info
					$greyRef3Town = $lbrow['ContactBusinessTown']; // save for additional greyd-out read-only info
					$greyRef3Email = $lbrow['ContactEmailPrim']; // save for additional greyd-out read-only info
					$greyRef3Web = $lbrow['ContactWeb']; // save for additional greyd-out read-only info
					} 
				echo '>'.$lbrow['ContactFirstname'].' '.$lbrow['ContactLastname'].'</option>';
				}
echo '</select></td>';
echo '<td style="background-color:#C0C0C0">'.$greyRef3ZIP.'</td>';
echo '<td style="background-color:#C0C0C0">'.$greyRef3Town.'</td>';
echo '<td style="background-color:#C0C0C0"><a href="mailto:'.$greyRef3Email.'" >'.$greyRef3Email.'</a></td>';
echo '<td style="background-color:#C0C0C0"><a href="https://'.$greyRef3Web.'" >'.$greyRef3Web.'</a></td>';
echo '</tr>';
// end referent

echo '</table>';

echo '<table>';

// The following attributes are made up by the string FirstName+Blank+LastName
// The <select> code does not work if either FirstName or LastName is empty - tbd

// location from contacts
// begin location
			// get all contacts datasets with function location
			// and ArchiveID is 0000... to only take active datasets
			$lbquery = "SELECT * FROM contacts WHERE ((ContactFunction = 'location') AND (ContactArchiveID = '0000-00-00 00:00:00')) ORDER BY ContactLastname"; 
			$lbresult = mysqli_query($link,$lbquery);  echo mysqli_error($link);
			
			// control code 
			// while ($lbrow = mysqli_fetch_array($lbresult)) {echo $lbrow['ContactLastname'];}
			// echo $dbrow['SeminarLocation'];

			// offer retrieved location datasets
echo '<tr><td align="right">Location </td>';
echo '<td><select name="SeminarLocation" size="1">';
			echo '<option>'.''.'</option>'; // offer empty selection
			while ($lbrow = mysqli_fetch_array($lbresult))// offer all selected contact datasets with location function
				{
				echo '<option'; 
				if ($dbrow['SeminarLocation']==$lbrow['ContactFirstname'].' '.$lbrow['ContactLastname']) 
					{
					echo ' selected'; // show preselection by comparison
					$greyLocationZIP = $lbrow['ContactBusinessZIP']; // save for additional greyd-out read-only info
					$greyLocationTown = $lbrow['ContactBusinessTown']; // save for additional greyd-out read-only info
					$greyLocationEmail = $lbrow['ContactEmailPrim']; // save for additional greyd-out read-only info
					$greyLocationWeb = $lbrow['ContactWeb']; // save for additional greyd-out read-only info
					} 
				echo '>'.$lbrow['ContactFirstname'].' '.$lbrow['ContactLastname'].'</option>';
				}
echo '</select></td>';
echo '<td style="background-color:#C0C0C0">'.$greyLocationZIP.'</td>';
echo '<td style="background-color:#C0C0C0">'.$greyLocationTown.'</td>';
echo '<td style="background-color:#C0C0C0"><a href="mailto:'.$greyLocationEmail.'" >'.$greyLocationEmail.'</a></td>';
echo '<td style="background-color:#C0C0C0"><a href="https://'.$greyLocationWeb.'" >'.$greyLocationWeb.'</a></td>';
echo '</tr>';
// end location

// host from contacts
// begin host
			// get all contacts datasets with function host
			// and ArchiveID is 0000... to only take active datasets
			$lbquery = "SELECT * FROM contacts WHERE ((ContactFunction = 'host') AND (ContactArchiveID = '0000-00-00 00:00:00')) ORDER BY ContactLastname"; 
			$lbresult = mysqli_query($link,$lbquery);  echo mysqli_error($link);
			
			// control code 
			// while ($lbrow = mysqli_fetch_array($lbresult)) {echo $lbrow['ContactLastname'];}
			// echo $dbrow['SeminarHost'];

			// offer retrieved host datasets
echo '<tr><td align="right">Host </td>';
echo '<td><select name="SeminarHost" size="1">';
			echo '<option>'.''.'</option>'; // offer empty selection
			while ($lbrow = mysqli_fetch_array($lbresult))// offer all selected contact datasets with host function
				{
				echo '<option'; 
				if ($dbrow['SeminarHost']==$lbrow['ContactFirstname'].' '.$lbrow['ContactLastname']) 
					{
					echo ' selected'; // show preselection by comparison
					$greyHostZIP = $lbrow['ContactBusinessZIP']; // save for additional greyd-out read-only info
					$greyHostTown = $lbrow['ContactBusinessTown']; // save for additional greyd-out read-only info
					$greyHostEmail = $lbrow['ContactEmailPrim']; // save for additional greyd-out read-only info
					$greyHostWeb = $lbrow['ContactWeb']; // save for additional greyd-out read-only info
					}
				echo '>'.$lbrow['ContactFirstname'].' '.$lbrow['ContactLastname'].'</option>';
				}
echo '</select></td>';
echo '<td style="background-color:#C0C0C0">'.$greyHostZIP.'</td>';
echo '<td style="background-color:#C0C0C0">'.$greyHostTown.'</td>';
echo '<td style="background-color:#C0C0C0"><a href="mailto:'.$greyHostEmail.'" >'.$greyHostEmail.'</a></td>';
echo '<td style="background-color:#C0C0C0"><a href="https://'.$greyHostWeb.'" >'.$greyHostWeb.'</a></td>';
echo '</tr>';
// end host

// sponsor from contacts
// begin sponsor
			// get all contacts datasets with function sponsor
			// and ArchiveID is 0000... to only take active datasets
			$lbquery = "SELECT * FROM contacts WHERE ((ContactFunction = 'sponsor') AND (ContactArchiveID = '0000-00-00 00:00:00')) ORDER BY ContactLastname"; 
			$lbresult = mysqli_query($link,$lbquery);  echo mysqli_error($link);
			
			// control code 
			// while ($lbrow = mysqli_fetch_array($lbresult)) {echo $lbrow['ContactLastname'];}
			// echo $dbrow['SeminarSponsor'];

			// offer retrieved sponsor datasets
echo '<tr><td align="right">Sponsor </td>';
echo '<td><select name="SeminarSponsor" size="1">';
			echo '<option>'.''.'</option>'; // offer empty selection
			while ($lbrow = mysqli_fetch_array($lbresult))// offer all selected contact datasets with sponsor function
				{
				echo '<option'; 
				if ($dbrow['SeminarSponsor']==$lbrow['ContactFirstname'].' '.$lbrow['ContactLastname']) 
					{
					echo ' selected'; // show preselection by comparison
					$greySponsorZIP = $lbrow['ContactBusinessZIP']; // save for additional greyd-out read-only info
					$greySponsorTown = $lbrow['ContactBusinessTown']; // save for additional greyd-out read-only info
					$greySponsorEmail = $lbrow['ContactEmailPrim']; // save for additional greyd-out read-only info
					$greySponsorWeb = $lbrow['ContactWeb']; // save for additional greyd-out read-only info
					}
				echo '>'.$lbrow['ContactFirstname'].' '.$lbrow['ContactLastname'].'</option>';
				}
echo '</select></td>';
echo '<td style="background-color:#C0C0C0">'.$greySponsorZIP.'</td>';
echo '<td style="background-color:#C0C0C0">'.$greySponsorTown.'</td>';
echo '<td style="background-color:#C0C0C0"><a href="mailto:'.$greySponsorEmail.'" >'.$greySponsorEmail.'</a></td>';
echo '<td style="background-color:#C0C0C0"><a href="https://'.$greySponsorWeb.'" >'.$greySponsorWeb.'</a></td>';
echo '</tr>';
// end sponsor

// contact from contacts
// begin contact
			// get all contacts datasets with function backoffice
			// and ArchiveID is 0000... to only take active datasets
			$lbquery = "SELECT * FROM contacts WHERE ((ContactFunction = 'backoffice') AND (ContactArchiveID = '0000-00-00 00:00:00')) ORDER BY ContactLastname"; 
			$lbresult = mysqli_query($link,$lbquery);  echo mysqli_error($link);
			
			// control code 
			// while ($lbrow = mysqli_fetch_array($lbresult)) {echo $lbrow['ContactLastname'];}
			// echo $dbrow['SeminarContact'];

			// offer retrieved backoffice datasets
echo '<tr><td align="right">Contact </td>';
echo '<td><select name="SeminarContact" size="1">';
			echo '<option>'.''.'</option>'; // offer empty selection
			while ($lbrow = mysqli_fetch_array($lbresult))// offer all selected contact datasets with backoffice function
				{
				echo '<option'; 
				if ($dbrow['SeminarContact']==$lbrow['ContactFirstname'].' '.$lbrow['ContactLastname'])
					{
					echo ' selected'; // show preselection by comparison
					$greyContactZIP = $lbrow['ContactBusinessZIP']; // save for additional greyd-out read-only info
					$greyContactTown = $lbrow['ContactBusinessTown']; // save for additional greyd-out read-only info
					$greyContactEmail = $lbrow['ContactEmailPrim']; // save for additional greyd-out read-only info
					$greyContactWeb = $lbrow['ContactWeb']; // save for additional greyd-out read-only info
					}
				echo '>'.$lbrow['ContactFirstname'].' '.$lbrow['ContactLastname'].'</option>';
				}
echo '</select></td>';
echo '<td style="background-color:#C0C0C0">'.$greyContactZIP.'</td>';
echo '<td style="background-color:#C0C0C0">'.$greyContactTown.'</td>';
echo '<td style="background-color:#C0C0C0"><a href="mailto:'.$greyContactEmail.'" >'.$greyContactEmail.'</a></td>';
echo '<td style="background-color:#C0C0C0"><a href="https://'.$greyContactWeb.'" >'.$greyContactWeb.'</a></td>';
echo '</tr>';
// end contact

echo '</table>';

echo '<table>';

echo '<tr><td align="right">Web </td><td><input type="text" name="SeminarWeb" size="60" maxlength="255" value="'.$dbrow['SeminarWeb'].'"></td></tr>';
echo '<tr><td align="right">Registration </td><td><textarea name="SeminarRegistration" cols="62" rows="4" value="'.$dbrow['SeminarRegistration'].'">'.$dbrow['SeminarRegistration'].'</textarea></td></tr>';
echo '<tr><td align="right">Fee </td><td><input type="text" name="SeminarFee" size="60" maxlength="255" value="'.$dbrow['SeminarFee'].'"></td></tr>';
$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['SeminarProject'].'/';	
echo '<tr>';
echo '<td align="right">';
echo '<input type="checkbox" name="existor" value="File"'; if ($dbrow['SeminarFilename']) {echo ' checked';} echo '> Filename (max.10MB)</td>';
echo '<input type="hidden" name="SeminarFilename" value="'.$dbrow['SeminarFilename'].'">';
echo '<input type="hidden" name="SeminarFilesize" value="'.$dbrow['SeminarFilesize'].'">';
echo '<td align="left" class="bluelink"><a href="'.$ProjectFilePath.$dbrow['SeminarFilename'].'">'.$dbrow['SeminarFilename'].'</a> ['.$dbrow['SeminarFilesize'].'kB]</td>';
echo '<td><input type="file" name="SeminarFilename" size="15"></td>';
echo '</tr>';
echo '<tr><td align="right">Remarks </td><td><input type="text" name="SeminarRemarks" size="60" maxlength="255" value="'.$dbrow['SeminarRemarks'].'"></td></tr>';

	echo '</table>';

	echo '</form>';

} elseif ( $updatedataset OR $createdataset OR $deletedataset OR $historydataset)

echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-07 18:00</div>';

?>
</body>
</html>
