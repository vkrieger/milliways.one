<?php

/*
the following Event constants are defined 
*/

// Arrays of constants

// Category classifies Event dataset in terms of presentation mode
$EventCategoryArray = [
'webtutorial',
'webinar',
'seminar',
'conference',
'webconference',
'cluster event',
'workshop',
'lecture',
'presentation',
'meeting',
'sitevisit',
'submission',
'other',
];

$EventSponsorArray = [
'VBI',
'HvDB',
'bS',
'BvBw',
'BAK',
'BVBS',
'ZIA',
'BVPI',
'BTGA',
'VDMA',
'WvKMt',
'BVMB',
'BDVI',
'BMVI',
'other',
];
// last change vkrieger 23.11.2015

?>