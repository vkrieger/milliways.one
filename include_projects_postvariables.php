<?php

/*
criteria are transported into this php by POST. empty POST variable causes empty part-variable thus no criteria
empty variables cause warnings.
*/

//
if (!empty($_POST["last_sorted_by"])) 	{ $last_sorted_by  	= $_POST["last_sorted_by"];} 	else {$last_sorted_by  	= "ProjectGUID";}
if (!empty($_POST["first_sorted_by"])) 	{ $first_sorted_by  = $_POST["first_sorted_by"];} 	else {$first_sorted_by  = "ProjectCategory";}
if (!empty($_POST["then_sorted_by"])) 	{ $then_sorted_by  	= $_POST["then_sorted_by"];} 	else {$then_sorted_by  	= "ProjectOwner";}
if (!empty($_POST["existor"])) 			{ $existor  		= $_POST["existor"];} 			else {$existor  		= "";}


// generic variables
if (!empty($_POST['ProjectGUID'])) {$ProjectGUID =$_POST['ProjectGUID'];} else {$ProjectGUID ='';}
if (!empty($_POST['ProjectCreateID'])) {$ProjectCreateID =$_POST['ProjectCreateID'];} else {$ProjectCreateID ='';}
if (!empty($_POST['ProjectArchiveID'])) {$ProjectArchiveID =$_POST['ProjectArchiveID'];} else {$ProjectArchiveID ='';}
if (!empty($_POST['ProjectProject'])) {$ProjectProject =$_POST['ProjectProject'];} else {$ProjectProject ='';}
if (!empty($_POST['ProjectInitialProject'])) {$ProjectInitialProject =$_POST['ProjectInitialProject'];} else {$ProjectInitialProject ='';}
if (!empty($_POST['ProjectOwner'])) {$ProjectOwner =$_POST['ProjectOwner'];} else {$ProjectOwner ='';}
if (!empty($_POST['ProjectType'])) {$ProjectType =$_POST['ProjectType'];} else {$ProjectType ='';}
if (!empty($_POST['ProjectCategory'])) {$ProjectCategory =$_POST['ProjectCategory'];} else {$ProjectCategory ='';}
if (!empty($_POST['ProjectName'])) {$ProjectName =$_POST['ProjectName'];} else {$ProjectName ='';}
if (!empty($_POST['ProjectStatus'])) {$ProjectStatus =$_POST['ProjectStatus'];} else {$ProjectStatus ='';}
if (!empty($_POST['ProjectAquisitionNo'])) {$ProjectAquisitionNo =$_POST['ProjectAquisitionNo'];} else {$ProjectAquisitionNo ='';}
if (!empty($_POST['ProjectNumber'])) {$ProjectNumber =$_POST['ProjectNumber'];} else {$ProjectNumber ='';}
if (!empty($_POST['ProjectCode'])) {$ProjectCode =$_POST['ProjectCode'];} else {$ProjectCode ='';}
if (!empty($_POST['ProjectDescription'])) {$ProjectDescription =$_POST['ProjectDescription'];} else {$ProjectDescription ='';}
if (!empty($_POST['ProjectCountry'])) {$ProjectCountry =$_POST['ProjectCountry'];} else {$ProjectCountry ='';}
if (!empty($_POST['ProjectLocation'])) {$ProjectLocation =$_POST['ProjectLocation'];} else {$ProjectLocation ='';}
if (!empty($_POST['ProjectClient'])) {$ProjectClient =$_POST['ProjectClient'];} else {$ProjectClient ='';}
if (!empty($_POST['ProjectOrigin'])) {$ProjectOrigin =$_POST['ProjectOrigin'];} else {$ProjectOrigin ='';}
if (!empty($_POST['ProjectBudget'])) {$ProjectBudget =$_POST['ProjectBudget'];} else {$ProjectBudget ='';}
if (!empty($_POST['ProjectFigures'])) {$ProjectFigures =$_POST['ProjectFigures'];} else {$ProjectFigures ='';}
if (!empty($_POST['ProjectPeriod'])) {$ProjectPeriod =$_POST['ProjectPeriod'];} else {$ProjectPeriod ='';}
if (!empty($_POST['ProjectFee'])) {$ProjectFee =$_POST['ProjectFee'];} else {$ProjectFee ='';}
if (!empty($_POST['ProjectCharges'])) {$ProjectCharges =$_POST['ProjectCharges'];} else {$ProjectCharges ='';}
if (!empty($_POST['ProjectIntServices'])) {$ProjectIntServices =$_POST['ProjectIntServices'];} else {$ProjectIntServices ='';}
if (!empty($_POST['ProjectExtServices'])) {$ProjectExtServices =$_POST['ProjectExtServices'];} else {$ProjectExtServices ='';}
if (!empty($_POST['ProjectDirector'])) {$ProjectDirector =$_POST['ProjectDirector'];} else {$ProjectDirector ='';}
if (!empty($_POST['ProjectManager'])) {$ProjectManager =$_POST['ProjectManager'];} else {$ProjectManager ='';}
if (!empty($_POST['ProjectParticipants'])) {$ProjectParticipants =$_POST['ProjectParticipants'];} else {$ProjectParticipants ='';}
if (!empty($_POST['ProjectPartners'])) {$ProjectPartners =$_POST['ProjectPartners'];} else {$ProjectPartners ='';}
if (!empty($_POST['ProjectCompetitors'])) {$ProjectCompetitors =$_POST['ProjectCompetitors'];} else {$ProjectCompetitors ='';}
if (!empty($_POST['ProjectSector'])) {$ProjectSector =$_POST['ProjectSector'];} else {$ProjectSector ='';}
if (!empty($_POST['ProjectDepartment'])) {$ProjectDepartment =$_POST['ProjectDepartment'];} else {$ProjectDepartment ='';}
if (!empty($_POST['ProjectChances'])) {$ProjectChances =$_POST['ProjectChances'];} else {$ProjectChances ='';}
if (!empty($_POST['ProjectStatus'])) {$ProjectStatus =$_POST['ProjectStatus'];} else {$ProjectStatus ='';}
if (!empty($_POST['ProjectNextDeadline'])) {$ProjectNextDeadline =$_POST['ProjectNextDeadline'];} else {$ProjectNextDeadline ='';}
if (!empty($_POST['ProjectStatusMemo'])) {$ProjectStatusMemo =$_POST['ProjectStatusMemo'];} else {$ProjectStatusMemo ='';}
if (!empty($_POST['ProjectCallDate'])) {$ProjectCallDate =$_POST['ProjectCallDate'];} else {$ProjectCallDate ='';}
if (!empty($_POST['ProjectCallMemo'])) {$ProjectCallMemo =$_POST['ProjectCallMemo'];} else {$ProjectCallMemo ='';}
if (!empty($_POST['ProjectOfferDate'])) {$ProjectOfferDate =$_POST['ProjectOfferDate'];} else {$ProjectOfferDate ='';}
if (!empty($_POST['ProjectOfferMemo'])) {$ProjectOfferMemo =$_POST['ProjectOfferMemo'];} else {$ProjectOfferMemo ='';}
if (!empty($_POST['ProjectContractDate'])) {$ProjectContractDate =$_POST['ProjectContractDate'];} else {$ProjectContractDate ='';}
if (!empty($_POST['ProjectContractMemo'])) {$ProjectContractMemo =$_POST['ProjectContractMemo'];} else {$ProjectContractMemo ='';}
if (!empty($_POST['ProjectLostDate'])) {$ProjectLostDate =$_POST['ProjectLostDate'];} else {$ProjectLostDate ='';}
if (!empty($_POST['ProjectLostMemo'])) {$ProjectLostMemo =$_POST['ProjectLostMemo'];} else {$ProjectLostMemo ='';}
if (!empty($_POST['ProjectRemarks'])) {$ProjectRemarks =$_POST['ProjectRemarks'];} else {$ProjectRemarks ='';}
if (!empty($_POST['ProjectFilename'])) {$ProjectFilename =$_POST['ProjectFilename'];} else {$ProjectFilename ='';}
if (!empty($_POST['ProjectFilesize'])) {$ProjectFilesize =$_POST['ProjectFilesize'];} else {$ProjectFilesize ='';}

// list variables
if (!empty($_POST['list_ProjectGUID'])) {$list_ProjectGUID =$_POST['list_ProjectGUID'];} else {$list_ProjectGUID ='';}
if (!empty($_POST['list_ProjectCreateID'])) {$list_ProjectCreateID =$_POST['list_ProjectCreateID'];} else {$list_ProjectCreateID ='';}
if (!empty($_POST['list_ProjectArchiveID'])) {$list_ProjectArchiveID =$_POST['list_ProjectArchiveID'];} else {$list_ProjectArchiveID ='';}
if (!empty($_POST['list_ProjectProject'])) {$list_ProjectProject =$_POST['list_ProjectProject'];} else {$list_ProjectProject ='';}
if (!empty($_POST['list_ProjectOwner'])) {$list_ProjectOwner =$_POST['list_ProjectOwner'];} else {$list_ProjectOwner ='';}
if (!empty($_POST['list_ProjectType'])) {$list_ProjectType =$_POST['list_ProjectType'];} else {$list_ProjectType ='';}
if (!empty($_POST['list_ProjectCategory'])) {$list_ProjectCategory =$_POST['list_ProjectCategory'];} else {$list_ProjectCategory ='';}
if (!empty($_POST['list_ProjectName'])) {$list_ProjectName =$_POST['list_ProjectName'];} else {$list_ProjectName ='';}
if (!empty($_POST['list_ProjectStatus'])) {$list_ProjectStatus =$_POST['list_ProjectStatus'];} else {$list_ProjectStatus ='';}
if (!empty($_POST['list_ProjectAquisitionNo'])) {$list_ProjectAquisitionNo =$_POST['list_ProjectAquisitionNo'];} else {$list_ProjectAquisitionNo ='';}
if (!empty($_POST['list_ProjectNumber'])) {$list_ProjectNumber =$_POST['list_ProjectNumber'];} else {$list_ProjectNumber ='';}
if (!empty($_POST['list_ProjectCode'])) {$list_ProjectCode =$_POST['list_ProjectCode'];} else {$list_ProjectCode ='';}
if (!empty($_POST['list_ProjectDescription'])) {$list_ProjectDescription =$_POST['list_ProjectDescription'];} else {$list_ProjectDescription ='';}
if (!empty($_POST['list_ProjectCountry'])) {$list_ProjectCountry =$_POST['list_ProjectCountry'];} else {$list_ProjectCountry ='';}
if (!empty($_POST['list_ProjectLocation'])) {$list_ProjectLocation =$_POST['list_ProjectLocation'];} else {$list_ProjectLocation ='';}
if (!empty($_POST['list_ProjectClient'])) {$list_ProjectClient =$_POST['list_ProjectClient'];} else {$list_ProjectClient ='';}
if (!empty($_POST['list_ProjectOrigin'])) {$list_ProjectOrigin =$_POST['list_ProjectOrigin'];} else {$list_ProjectOrigin ='';}
if (!empty($_POST['list_ProjectBudget'])) {$list_ProjectBudget =$_POST['list_ProjectBudget'];} else {$list_ProjectBudget ='';}
if (!empty($_POST['list_ProjectFigures'])) {$list_ProjectFigures =$_POST['list_ProjectFigures'];} else {$list_ProjectFigures ='';}
if (!empty($_POST['list_ProjectPeriod'])) {$list_ProjectPeriod =$_POST['list_ProjectPeriod'];} else {$list_ProjectPeriod ='';}
if (!empty($_POST['list_ProjectFee'])) {$list_ProjectFee =$_POST['list_ProjectFee'];} else {$list_ProjectFee ='';}
if (!empty($_POST['list_ProjectCharges'])) {$list_ProjectCharges =$_POST['list_ProjectCharges'];} else {$list_ProjectCharges ='';}
if (!empty($_POST['list_ProjectIntServices'])) {$list_ProjectIntServices =$_POST['list_ProjectIntServices'];} else {$list_ProjectIntServices ='';}
if (!empty($_POST['list_ProjectExtServices'])) {$list_ProjectExtServices =$_POST['list_ProjectExtServices'];} else {$list_ProjectExtServices ='';}
if (!empty($_POST['list_ProjectDirector'])) {$list_ProjectDirector =$_POST['list_ProjectDirector'];} else {$list_ProjectDirector ='';}
if (!empty($_POST['list_ProjectManager'])) {$list_ProjectManager =$_POST['list_ProjectManager'];} else {$list_ProjectManager ='';}
if (!empty($_POST['list_ProjectParticipants'])) {$list_ProjectParticipants =$_POST['list_ProjectParticipants'];} else {$list_ProjectParticipants ='';}
if (!empty($_POST['list_ProjectPartners'])) {$list_ProjectPartners =$_POST['list_ProjectPartners'];} else {$list_ProjectPartners ='';}
if (!empty($_POST['list_ProjectCompetitors'])) {$list_ProjectCompetitors =$_POST['list_ProjectCompetitors'];} else {$list_ProjectCompetitors ='';}
if (!empty($_POST['list_ProjectSector'])) {$list_ProjectSector =$_POST['list_ProjectSector'];} else {$list_ProjectSector ='';}
if (!empty($_POST['list_ProjectDepartment'])) {$list_ProjectDepartment =$_POST['list_ProjectDepartment'];} else {$list_ProjectDepartment ='';}
if (!empty($_POST['list_ProjectChances'])) {$list_ProjectChances =$_POST['list_ProjectChances'];} else {$list_ProjectChances ='';}
if (!empty($_POST['list_ProjectStatus'])) {$list_ProjectStatus =$_POST['list_ProjectStatus'];} else {$list_ProjectStatus ='';}
if (!empty($_POST['list_ProjectNextDeadline'])) {$list_ProjectNextDeadline =$_POST['list_ProjectNextDeadline'];} else {$list_ProjectNextDeadline ='';}
if (!empty($_POST['list_ProjectStatusMemo'])) {$list_ProjectStatusMemo =$_POST['list_ProjectStatusMemo'];} else {$list_ProjectStatusMemo ='';}
if (!empty($_POST['list_ProjectCallDate'])) {$list_ProjectCallDate =$_POST['list_ProjectCallDate'];} else {$list_ProjectCallDate ='';}
if (!empty($_POST['list_ProjectCallMemo'])) {$list_ProjectCallMemo =$_POST['list_ProjectCallMemo'];} else {$list_ProjectCallMemo ='';}
if (!empty($_POST['list_ProjectOfferDate'])) {$list_ProjectOfferDate =$_POST['list_ProjectOfferDate'];} else {$list_ProjectOfferDate ='';}
if (!empty($_POST['list_ProjectOfferMemo'])) {$list_ProjectOfferMemo =$_POST['list_ProjectOfferMemo'];} else {$list_ProjectOfferMemo ='';}
if (!empty($_POST['list_ProjectContractDate'])) {$list_ProjectContractDate =$_POST['list_ProjectContractDate'];} else {$list_ProjectContractDate ='';}
if (!empty($_POST['list_ProjectContractMemo'])) {$list_ProjectContractMemo =$_POST['list_ProjectContractMemo'];} else {$list_ProjectContractMemo ='';}
if (!empty($_POST['list_ProjectLostDate'])) {$list_ProjectLostDate =$_POST['list_ProjectLostDate'];} else {$list_ProjectLostDate ='';}
if (!empty($_POST['list_ProjectLostMemo'])) {$list_ProjectLostMemo =$_POST['list_ProjectLostMemo'];} else {$list_ProjectLostMemo ='';}
if (!empty($_POST['list_ProjectRemarks'])) {$list_ProjectRemarks =$_POST['list_ProjectRemarks'];} else {$list_ProjectRemarks ='';}
if (!empty($_POST['list_ProjectFilename'])) {$list_ProjectFilename =$_POST['list_ProjectFilename'];} else {$list_ProjectFilename ='';}
if (!empty($_POST['list_ProjectFilesize'])) {$list_ProjectFilesize =$_POST['list_ProjectFilesize'];} else {$list_ProjectFilesize ='';}

 // part variables
if (!empty($_POST['partProjectGUID'])) {$partProjectGUID =$_POST['partProjectGUID'];} else {$partProjectGUID ='';}
if (!empty($_POST['partProjectCreateID'])) {$partProjectCreateID =$_POST['partProjectCreateID'];} else {$partProjectCreateID ='';}
if (!empty($_POST['partProjectArchiveID'])) {$partProjectArchiveID =$_POST['partProjectArchiveID'];} else {$partProjectArchiveID ='';}
if (!empty($_POST['partProjectProject'])) {$partProjectProject =$_POST['partProjectProject'];} else {$partProjectProject ='';}
if (!empty($_POST['partProjectOwner'])) {$partProjectOwner =$_POST['partProjectOwner'];} else {$partProjectOwner ='';}
if (!empty($_POST['partProjectType'])) {$partProjectType =$_POST['partProjectType'];} else {$partProjectType ='';}
if (!empty($_POST['partProjectCategory'])) {$partProjectCategory =$_POST['partProjectCategory'];} else {$partProjectCategory ='';}
if (!empty($_POST['partProjectName'])) {$partProjectName =$_POST['partProjectName'];} else {$partProjectName ='';}
if (!empty($_POST['partProjectStatus'])) {$partProjectStatus =$_POST['partProjectStatus'];} else {$partProjectStatus ='';}
if (!empty($_POST['partProjectAquisitionNo'])) {$partProjectAquisitionNo =$_POST['partProjectAquisitionNo'];} else {$partProjectAquisitionNo ='';}
if (!empty($_POST['partProjectNumber'])) {$partProjectNumber =$_POST['partProjectNumber'];} else {$partProjectNumber ='';}
if (!empty($_POST['partProjectCode'])) {$partProjectCode =$_POST['partProjectCode'];} else {$partProjectCode ='';}
if (!empty($_POST['partProjectDescription'])) {$partProjectDescription =$_POST['partProjectDescription'];} else {$partProjectDescription ='';}
if (!empty($_POST['partProjectCountry'])) {$partProjectCountry =$_POST['partProjectCountry'];} else {$partProjectCountry ='';}
if (!empty($_POST['partProjectLocation'])) {$partProjectLocation =$_POST['partProjectLocation'];} else {$partProjectLocation ='';}
if (!empty($_POST['partProjectClient'])) {$partProjectClient =$_POST['partProjectClient'];} else {$partProjectClient ='';}
if (!empty($_POST['partProjectOrigin'])) {$partProjectOrigin =$_POST['partProjectOrigin'];} else {$partProjectOrigin ='';}
if (!empty($_POST['partProjectBudget'])) {$partProjectBudget =$_POST['partProjectBudget'];} else {$partProjectBudget ='';}
if (!empty($_POST['partProjectFigures'])) {$partProjectFigures =$_POST['partProjectFigures'];} else {$partProjectFigures ='';}
if (!empty($_POST['partProjectPeriod'])) {$partProjectPeriod =$_POST['partProjectPeriod'];} else {$partProjectPeriod ='';}
if (!empty($_POST['partProjectFee'])) {$partProjectFee =$_POST['partProjectFee'];} else {$partProjectFee ='';}
if (!empty($_POST['partProjectCharges'])) {$partProjectCharges =$_POST['partProjectCharges'];} else {$partProjectCharges ='';}
if (!empty($_POST['partProjectIntServices'])) {$partProjectIntServices =$_POST['partProjectIntServices'];} else {$partProjectIntServices ='';}
if (!empty($_POST['partProjectExtServices'])) {$partProjectExtServices =$_POST['partProjectExtServices'];} else {$partProjectExtServices ='';}
if (!empty($_POST['partProjectDirector'])) {$partProjectDirector =$_POST['partProjectDirector'];} else {$partProjectDirector ='';}
if (!empty($_POST['partProjectManager'])) {$partProjectManager =$_POST['partProjectManager'];} else {$partProjectManager ='';}
if (!empty($_POST['partProjectParticipants'])) {$partProjectParticipants =$_POST['partProjectParticipants'];} else {$partProjectParticipants ='';}
if (!empty($_POST['partProjectPartners'])) {$partProjectPartners =$_POST['partProjectPartners'];} else {$partProjectPartners ='';}
if (!empty($_POST['partProjectCompetitors'])) {$partProjectCompetitors =$_POST['partProjectCompetitors'];} else {$partProjectCompetitors ='';}
if (!empty($_POST['partProjectSector'])) {$partProjectSector =$_POST['partProjectSector'];} else {$partProjectSector ='';}
if (!empty($_POST['partProjectDepartment'])) {$partProjectDepartment =$_POST['partProjectDepartment'];} else {$partProjectDepartment ='';}
if (!empty($_POST['partProjectChances'])) {$partProjectChances =$_POST['partProjectChances'];} else {$partProjectChances ='';}
if (!empty($_POST['partProjectStatus'])) {$partProjectStatus =$_POST['partProjectStatus'];} else {$partProjectStatus ='';}
if (!empty($_POST['partProjectNextDeadline'])) {$partProjectNextDeadline =$_POST['partProjectNextDeadline'];} else {$partProjectNextDeadline ='';}
if (!empty($_POST['partProjectStatusMemo'])) {$partProjectStatusMemo =$_POST['partProjectStatusMemo'];} else {$partProjectStatusMemo ='';}
if (!empty($_POST['partProjectCallDate'])) {$partProjectCallDate =$_POST['partProjectCallDate'];} else {$partProjectCallDate ='';}
if (!empty($_POST['partProjectCallMemo'])) {$partProjectCallMemo =$_POST['partProjectCallMemo'];} else {$partProjectCallMemo ='';}
if (!empty($_POST['partProjectOfferDate'])) {$partProjectOfferDate =$_POST['partProjectOfferDate'];} else {$partProjectOfferDate ='';}
if (!empty($_POST['partProjectOfferMemo'])) {$partProjectOfferMemo =$_POST['partProjectOfferMemo'];} else {$partProjectOfferMemo ='';}
if (!empty($_POST['partProjectContractDate'])) {$partProjectContractDate =$_POST['partProjectContractDate'];} else {$partProjectContractDate ='';}
if (!empty($_POST['partProjectContractMemo'])) {$partProjectContractMemo =$_POST['partProjectContractMemo'];} else {$partProjectContractMemo ='';}
if (!empty($_POST['partProjectLostDate'])) {$partProjectLostDate =$_POST['partProjectLostDate'];} else {$partProjectLostDate ='';}
if (!empty($_POST['partProjectLostMemo'])) {$partProjectLostMemo =$_POST['partProjectLostMemo'];} else {$partProjectLostMemo ='';}
if (!empty($_POST['partProjectRemarks'])) {$partProjectRemarks =$_POST['partProjectRemarks'];} else {$partProjectRemarks ='';}
if (!empty($_POST['partProjectFilename'])) {$partProjectFilename =$_POST['partProjectFilename'];} else {$partProjectFilename ='';}
if (!empty($_POST['partProjectFilesize'])) {$partProjectFilesize =$_POST['partProjectFilesize'];} else {$partProjectFilesize ='';}

 // last change vk 2017-05-13
?>
