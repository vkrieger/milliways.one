<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <?php include 'include_setSystemvariables.php'; include 'include_setSystemconstants.php'; ?>
  	<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
 	<title><?php echo $SystemProject; ?> database system</title>
  	<style>
	* 									{font-size:16px ; font-family: Arial, Verdana, sans-serif; }
	input,a                             {font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	select,option,textarea				{font-size:12px ; font-family: Arial, Verdana, sans-serif; }
	table,tr,td 						{font-size:10px ; font-family: Arial, Verdana, sans-serif; }
	</style>
	<link rel="stylesheet" type="text/css" href="formats.css">
</head>

<body>

!!! Do <b>NOT</b> use semicolon (;) in input fields !!! <b>NO</b> special characters and blanks in filenames - use underscore ( _ ) instead!!! 
<br><br>

<?php
		
include 'include_setInternalconstants.php';
include 'include_internals_postvariables.php';
include 'include_dbconnect.php';

if (!isset($_SESSION)) { session_start();}

$InternalCreateID = $_GET["InternalCreateID"];

$dbquery = " SELECT * FROM internals WHERE LOCATE ('$InternalCreateID', InternalCreateID) >0 ";
$dbresult = mysqli_query($link,$dbquery);
$dbrow = mysqli_fetch_array($dbresult,MYSQLI_BOTH);

	$updatedataset="";
	$createdataset="";
	$deletedataset="";
	$historydataset="";
	
if ($updatedataset == "" AND $createdataset == "" AND $deletedataset == "" AND $historydataset =="")
	{
	echo '<form method="post" action="internals_save.php" enctype="multipart/form-data" >';
	if ($_SESSION['LoginType']=='admin' OR $_SESSION['LoginType']=='supereditor' OR $_SESSION['LoginType']=='editor')
		{
		echo '<input type="submit" name="createdataset" value="create dataset">';
		echo '<input type="submit" name="updatedataset" value="update dataset">';
    	echo '<input type="submit" name="deletedataset" value="delete dataset">';
      	echo '<input type="reset" value="reset values">';
      	echo '<br><br>';
	    }

	echo '<table>';
	echo '<tr>';
	echo '<td align="right">GUID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="InternalGUID" size="60" maxlength="100" value="'.$dbrow['InternalGUID'].'" readonly></td>';
	echo '<td><input type="submit" name="historydataset" value="history of datasets"></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">CreateID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="InternalCreateID" size="60" maxlength="100" value="'.$dbrow['InternalCreateID'].'" readonly></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">ArchiveID (readonly)</td>';
	echo '<td><input style="background-color:#C0C0C0" type="text" name="InternalArchiveID" size="60" maxlength="100" value="'.$dbrow['InternalArchiveID'].'" readonly></td>';
	echo '</tr>';
	echo '<input type="hidden" name="InternalInitialProject" value="'.$dbrow['InternalProject'].'">';
	echo '<input type="hidden" name="InternalProject" value="'.$dbrow['InternalProject'].'">';
	echo '<tr>';
	echo '<td align="right">Project</td>';
	echo '<td><select name="InternalProject" size="1">';
		if (!empty($SystemProject))
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($SystemProject==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		else
			{foreach ($SystemProjectArray as $Project) {echo '<option'; if ($dbrow['InternalProject']==$Project) {echo ' selected';} echo '>'.$Project.'</option>';}}
		echo '</select></td>';		
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Owner (readonly)</td>';
	echo '<td>'.'['.$dbrow['InternalOwner'].'] - create/update transfers to '.'<input style="background-color:#C0C0C0" type="text" name="InternalOwner" size="15" maxlength="100" value="'.$_SESSION['LoginLogin'].'" readonly></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right" valign="top">Interners </td>';
	echo '<td valign="top">';
	echo '<textarea name="InternalInterners" cols="15" rows="4" value="'.$dbrow['InternalInterners'].'">'.$dbrow['InternalInterners'].'</textarea>';
	
	// get possible Interner from table logins and attribute LoginLogin
	// to get row query must be fetched and each row must by added to array
	$Project=$dbrow['InternalProject'];
	$loginresult = mysqli_query($link," SELECT LoginLogin FROM logins WHERE LOCATE ('$Project',LoginProject)>0  ORDER BY LoginLogin");
	$Interner = 0;
	while($loginrow = mysqli_fetch_array($loginresult,MYSQLI_BOTH))  
		{
		$Interner += 1;
		$InternerArray[$Interner] = $loginrow['LoginLogin'];
		}
	// display possible Interner to be added to InternalInterners. 
	// "none" should add no Interner. 
	// adding is done with update or create in internals_save.php
	echo ' add Interner ';
	echo '<select name="Interner" size="1">';
		echo '<option>none</option>';
		foreach ($InternerArray as $Interner) {echo '<option>'.$Interner.'</option>';} 
		echo '</select>';
	echo ' ';
	echo '<input type="submit" name="updatedataset" value="update">';
	echo '</td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td align="right">Type</td>';
	echo '<td><select name="InternalType" size="1">';
			foreach ($SystemTypeArray as $Type) {echo '<option'; if ($dbrow['InternalType']==$Type) {echo ' selected';} echo '>'.$Type.'</option>';} 
			echo '</select></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td align="right">Category</td>';
	echo '<td><select name="InternalCategory" size="1">';
			foreach ($InternalCategoryArray as $Category) {echo '<option'; if ($dbrow['InternalCategory']==$Category) {echo ' selected';} echo '>'.$Category.'</option>';} 
			echo '</select></td>';
	echo '</tr>';

echo '<tr><td align="right">Name </td><td><input type="text" name="InternalName" size="60" maxlength="255" value="'.$dbrow['InternalName'].'"></td></tr>';
echo '<tr><td align="right">Status </td><td><input type="text" name="InternalStatus" size="60" maxlength="255" value="'.$dbrow['InternalStatus'].'"></td></tr>';

// must be dataftp instead of data/ftp and make use of Alias in http.conf !!!
$ProjectFilePath='/dataftp/'.$dataftpfolder.'/'.$dbrow['InternalProject'].'/';

// control code
// echo "dataftpfolder:".$dataftpfolder."<br />";
// echo "ProjectFilePath:".$ProjectFilePath."<br />";

// take apart InternalFilenames into single Links
// only if InternalFilenames exists
if (!empty($dbrow['InternalFilenames']))
	{
	// makes FilenameArray from String from $dbrow['InternalFilenames']
	$FilenameArray = explode("\n",$dbrow['InternalFilenames']);
	$FilesizeArray = explode("\n",$dbrow['InternalFilesizes']);
	
	// control code
	// print_r($FilenameArray); 
	// echo '<br>';
	
	// clean FilenameArray from empty fields
	foreach ($FilenameArray as $key => $Filename)	
		{
		// control code
		// echo 'key:'.$key.'<br>';
		// echo 'Filename:'.$Filename.'<br>';
			
		// checks if filename exists by finding the delimiter '.'
		if (!strpos($Filename,'.'))
			{
			// control code
			// echo 'unsetted key:'.$key.'<br>';
			// echo 'unsetted Filename:'.$Filename.'<br>';
			
			unset ($FilenameArray[$key]);
			$FilenameArray = array_values ($FilenameArray);
			unset ($FilesizeArray[$key]);
			$FilesizeArray = array_values ($FilesizeArray);
			}
		}

	// control code
	// print_r($FilenameArray); 
	// echo '<br>';
	
/*
- ExistorArray[key] has value "Filename" when Filename when checkbox is not unchecked
- $_FILES['InternalFilename']['tmp_name'] is given when new Filename is uploaded 
- when ExistorArray[key] is unchecked InternalFilenames should be emptied from Filename in app_save.php
*/
	
	// lists Filenames from FilenameArray as Links
	foreach ($FilenameArray as $key => $Filename)
		{
		// control code
		// echo 'Filename:'.$Filename.'<br />';
		echo '<tr>';
		echo '<td align="right" >';
		echo '<input type="checkbox" name="ExistorArray[]" value="'.$Filename.' ['.$FilesizeArray[$key].'kB]" checked>';
		echo '</td>';
		echo '<td class="bluelink">';
		echo '<a href="'.$ProjectFilePath.$Filename.'">'.$Filename.'</a> ['.$FilesizeArray[$key].'kB] ';
		// the following code would allows external URL to be included - these URL must be changed directly and manually in database
		// it was commented ou as of problems 2021-02-02
		/*
		if (str_contains($Filename,'http'))
			// external URL - modify correctly in database directly
			{echo '<a href="'$Filename'" target="_blank">'.$Filename.'</a>';}
			else
			// normal Link  - no external URL
			{echo '<a href="'.$ProjectFilePath.$Filename.'">'.$Filename.'</a> ['.$FilesizeArray[$key].'kB] ';}	
		*/
		echo '</td>';
		echo '</tr>';
		}
		
	// implode FilenameArray to InternalFilenames 
	$InternalFilenames = implode ("\n",$FilenameArray);
	$InternalFilesizes = implode ("\n",$FilesizeArray);
		
	// makes sure that InternalFilenames is available in app_save.php
	echo '<input type="hidden" name="InternalFilenames" value="'.$InternalFilenames.'">';
	echo '<input type="hidden" name="InternalFilesizes" value="'.$InternalFilesizes.'">';

	}

echo '<tr>';
echo '<td>';
echo '</td>';
echo '<td>';
echo ' add File (NO blanks in Filename!!!) ';
echo '<input type="file" name="Filename" size="15">';
echo ' ';
echo '<input type="submit" name="updatedataset" value="update">';
echo '</td>';
echo '</tr>';


echo '<tr><td align="right">Remarks </td><td><input type="text" name="InternalRemarks" size="60" maxlength="255" value="'.$dbrow['InternalRemarks'].'"></td></tr>';
	
	echo '</table>';

echo '</form>';
echo '<div align="right" style="font-size: 8px;">printed at '.$datum = date("d.m.Y - H:i").' - last source change vk 2021-02-02 18:00</div>';

} elseif ( $updatedataset OR $createdataset OR $deletedataset OR $historydataset)

?>
</font>
</body>
</html>
